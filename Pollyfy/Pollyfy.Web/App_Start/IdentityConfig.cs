﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System.Xml;
using System.Diagnostics;
using Pollyfy.Web.Providers;
using Pollyfy.Data.Interfaces;
using Pollyfy.Web.Core;


namespace Pollyfy.Web
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.    
    public class ApplicationUserManager : UserManager<SimpleUser>
    {
        public ApplicationUserManager(IUserStore<SimpleUser> store)
            : base(store)
        {
        }

        public override Task<SimpleUser> FindAsync(string userName, string password)
        {
            return base.FindAsync(userName, password);
        }

        public override Task<SimpleUser> FindByEmailAsync(string email)
        {
            return base.FindByEmailAsync(email);
        }

        public override Task<SimpleUser> FindByNameAsync(string userName)
        {
            return base.FindByNameAsync(userName);
        }

        protected override Task<bool> VerifyPasswordAsync(IUserPasswordStore<SimpleUser, string> store, SimpleUser user, string password)
        {
            return base.VerifyPasswordAsync(store, user, password);
        }

        public override Task<bool> IsLockedOutAsync(string userId)
        {
            return base.IsLockedOutAsync(userId);
        }

        public override Task<bool> GetTwoFactorEnabledAsync(string userId)
        {
            return Task.FromResult<bool>(false);
        }

        public override Task<string> GetSecurityStampAsync(string userId)
        {
            throw new NotImplementedException();
        }

        public override Task<IList<string>> GetRolesAsync(string userId)
        {
            throw new NotImplementedException();
        }

        public override Task<IList<Claim>> GetClaimsAsync(string userId)
        {
            throw new NotImplementedException();
        }

        public override Task<ClaimsIdentity> CreateIdentityAsync(SimpleUser user, string authenticationType)
        {

            ClaimsIdentity claims = new ClaimsIdentity(new[] {
                    new Claim("UserId", user.UserId.ToString()),
                    new Claim("CompanyId", user.CompanyId.ToString()),
                    new Claim("UserName", user.UserName),
                    new Claim("FirstName", user.FirstName),
                    new Claim("LastName", user.LastName),
                    new Claim("Email", user.Email),
                    new Claim("Types", ((int)user.Types).ToString())
                }, authenticationType);
            return Task.FromResult(claims);
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            IUow Uow = (IUow)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUow));
            var manager = new ApplicationUserManager(new LoginProvider(Uow));
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<SimpleUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<SimpleUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {

        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(SimpleUser user)
        {

            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public override Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
        {
            return base.PasswordSignInAsync(userName, password, isPersistent, shouldLockout);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }
}
