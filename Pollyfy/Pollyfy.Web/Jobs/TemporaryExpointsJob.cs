﻿#region

using System;
using Pollyfy.Web.Jobs.Base;
using System.Linq;
using Pollyfy.Models;
using Quartz;

#endregion

namespace Pollyfy.Web.Jobs
{
    public class TemporaryExpointsJob : JobBase
    {
        public override void ExecuteJob(IJobExecutionContext context)
        {
            var runningJobs = context.Scheduler.GetCurrentlyExecutingJobs().Count(p => p.JobInstance is TemporaryExpointsJob);

            if (runningJobs > 1)
                return;

            Logger.Info("TemporaryExpoints Job is Started");

            var temporaryExpoints = Core.Common.Uow.TemporaryExpoints.GetAll(ActiveStatus.Active);

            var today = DateTime.Now;

            temporaryExpoints = temporaryExpoints.Where(p => p.CreatedOn.HasValue && today.Subtract(p.CreatedOn.Value).TotalHours >= 24);

            foreach (var temporaryExpoint in temporaryExpoints)
            {
                var user = Core.Common.Uow.Users.GetById(temporaryExpoint.UserId);

                user.ExPoint += temporaryExpoint.Expoint;

                var result = Core.Common.Uow.Users.DoUpdate(user);

                if (result > 0)
                    Core.Common.Uow.TemporaryExpoints.DoDelete(temporaryExpoint);
            }

            Logger.Info("TemporaryExpoints Job Finished");
        }
    }
}