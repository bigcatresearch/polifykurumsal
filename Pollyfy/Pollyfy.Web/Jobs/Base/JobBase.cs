﻿#region

using NLog;
using Quartz;
using System;

#endregion

namespace Pollyfy.Web.Jobs.Base
{
    public abstract class JobBase : IJob
    {
        public static Logger Logger = LogManager.GetLogger("ResetWeeklyLeadersJob");

        public void Execute(IJobExecutionContext context)
        {
            var logSource = context.JobDetail.Key.Name;

            try
            {
                ExecuteJob(context);
            }
            catch (Exception e)
            {
                Logger.Trace(e, logSource);
            }
        }

        public abstract void ExecuteJob(IJobExecutionContext context);
    }
}