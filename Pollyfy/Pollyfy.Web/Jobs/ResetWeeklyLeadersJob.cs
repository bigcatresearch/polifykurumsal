﻿#region

using Pollyfy.Web.Jobs.Base;
using System.Linq;
using Quartz;

#endregion

namespace Pollyfy.Web.Jobs
{
    public class ResetWeeklyLeadersJob : JobBase
    {
        public override void ExecuteJob(IJobExecutionContext context)
        {
            var runningJobs = context.Scheduler.GetCurrentlyExecutingJobs().Count(p => p.JobInstance is ResetWeeklyLeadersJob);

            if (runningJobs > 1)
                return;

            Logger.Info("Leaders Job is Started");

            var result = Core.Common.Uow.WeeklyLeaders.DeleteAll();

            if (result)
            {
                Logger.Info("WeeklyLeaders table cleared");
            }
        }
    }
}