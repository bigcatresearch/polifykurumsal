﻿#region

using Quartz;
using Quartz.Impl;
using System;

#endregion

namespace Pollyfy.Web.Jobs
{
    public class JobManager
    {
        private static void ExecuteWeeklyLeaderJob()
        {
            var sf = new StdSchedulerFactory();

            var sched = sf.GetScheduler();
            sched.Start();

            var job = JobBuilder.Create<ResetWeeklyLeadersJob>()
                .WithIdentity("ResetWeeklyLeadersJob", "pollyfy")
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity("ResetWeeklyLeadersJob", "pollyfy")
                .StartAt(DateTime.Now.AddMinutes(1))
                .WithSimpleSchedule(x => x.WithIntervalInHours(168)
                .RepeatForever())
                .Build();

            sched.ScheduleJob(job, trigger);
        }

        private static void ExecuteTemporaryExpointJob()
        {
            var sf = new StdSchedulerFactory();

            var sched = sf.GetScheduler();
            sched.Start();

            var job = JobBuilder.Create<TemporaryExpointsJob>()
                .WithIdentity("TemporaryExpointsJob", "pollyfy")
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity("TemporaryExpointsJob", "pollyfy")
                .StartAt(DateTime.Now.AddMinutes(1))
                .WithSimpleSchedule(x => x.WithIntervalInHours(12)
                    .RepeatForever())
                .Build();

            sched.ScheduleJob(job, trigger);
        }

        public static void Initialize()
        {
            ExecuteWeeklyLeaderJob();

            ExecuteTemporaryExpointJob();
        }
    }
}