﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using Pollyfy.Models.ViewModel;
using Pollyfy.Web.Controllers;
using Pollyfy.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Pollyfy.Web.Controllers
{
    [Authorize]
    public class SystemController : BaseController
    {
        public SystemController(IUow uow) : base(uow) { }
        // GET: System
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Categories()
        {
            return View();
        }

        [ValidateInput(false)]
        public ActionResult GetCategoryList()
        {
            return PartialView("_CategoryList", Core.Common.GetCategoryView());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult AddNewCategory(CategoryModel model)
        {
            if (model != null && model.Caption != null) model.Caption = model.Caption.Replace("\"", "");
            if (ModelState.IsValid && !string.IsNullOrEmpty(model.Caption))
            {
                try
                {
                    if (string.IsNullOrEmpty(model.ParentId))
                    {
                        HelperCategory category = new HelperCategory()
                        {
                            Text = model.Caption,
                            Types = (CategoryType)model.CategoryType,
                            IsVisible = model.IsVisible,
                            IsMultiple = model.IsMultiple,
                            IsDeleteAllowed = true,
                            AlwaysShow = model.AlwaysShow,
                            Code = ""
                        };
                        Uow.HelperCategories.DoInsert(category);
                    }
                    else
                    {
                        HelperValue value = new HelperValue()
                        {
                            Text = model.Caption,
                            CategoryId = int.Parse(model.ParentId.Split(':')[1]),
                            DisplayOrder = 0,
                            IsLevelAllowed = false,
                            Value = ""
                        };
                        Uow.HelperValues.DoInsert(value);
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditNodeError"] = e.Message;
                }
            }
            else
                ViewData["EditNodeError"] = "Hatalı veya eksik değer giriyorsunuz.";

            return PartialView("_CategoryList", Core.Common.GetCategoryView());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult DeleteItemCategory(string Id)
        {
            try
            {
                if (!string.IsNullOrEmpty(Id))
                {
                    int id = 0;
                    int.TryParse(Id.Split(':')[1], out id);
                    if (Id.StartsWith("c:"))
                    {
                        Uow.HelperCategories.UpdateStatus(id, ActiveStatus.Deleted);
                    }
                    else
                    {
                        Uow.HelperValues.UpdateStatus(id, ActiveStatus.Deleted);
                    }
                }
            }
            catch (Exception e)
            {
                ViewData["EditNodeError"] = e.Message;
            }
            return PartialView("_CategoryList", Core.Common.GetCategoryView());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateItemCategory(CategoryModel model)
        {
            if (model != null && model.Caption != null) model.Caption = model.Caption.Replace("\"", "");
            if (ModelState.IsValid && !string.IsNullOrEmpty(model.Caption))
            {
                try
                {
                    int id = 0;
                    int.TryParse(model.Id.Split(':')[1], out id);
                    if (string.IsNullOrEmpty(model.ParentId))
                    {

                        HelperCategory category = Uow.HelperCategories.GetByKey(id);
                        if (category != null)
                        {
                            category.Text = model.Caption;
                            category.Types = (CategoryType)model.CategoryType;
                            category.IsVisible = model.IsVisible;
                            category.IsMultiple = model.IsMultiple;
                            category.AlwaysShow = model.AlwaysShow;
                            Uow.HelperCategories.DoUpdate(category);
                        }

                    }
                    else
                    {
                        HelperValue value = Uow.HelperValues.GetByKey(id);
                        if (value != null)
                        {
                            value.Text = model.Caption;
                            Uow.HelperValues.DoUpdate(value);
                        }
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditNodeError"] = e.Message;
                }
            }
            else
                ViewData["EditNodeError"] = "Hatalı veya eksik değer giriyorsunuz.";
            return PartialView("_CategoryList", Core.Common.GetCategoryView());
        }

        [HttpGet]
        [AllowAnonymous]
        [AsyncTimeout(1000)]
        public async Task<string> Ping(CancellationToken cancellationToken)
        {
            if (CurrentUser.UserId > 0)
            {
                await Uow.Users.UpdateAccessTimeAsync(CurrentUser.UserId);
            }
            return "ok";
        }

    }
}