﻿using Pollyfy.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;
using Pollyfy.Models;
using Microsoft.AspNet.Identity;
using Pollyfy.Web.Core;
using Pollyfy.Core.Extensions;
using Pollyfy.Models.ViewModel;
using Pollyfy.Web.Providers;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Excel = Microsoft.Office.Interop.Excel;

namespace Pollyfy.Web.Controllers
{
    [Authorize]
    public class UserController : BaseController
    {
        public UserController(IUow uow) : base(uow) { }


        public ActionResult Index()
        {
            RedirectForUnauthorized();
            return View();
        }

        public ActionResult New()
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = ModelState.IsValid;
            User model = new User();
            model.ImportFileContainer = new FileContainer()
            {
                FileType = FileType.Excell,
                OwnerType = FileOwnerType.Temp
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult New(User model)
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = ModelState.IsValid;
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                model.Password = new PasswordHasher().HashPassword(model.Password);
                model.UserName = model.Email;
                var exUser = Core.Common.Uow.Users.GetUserByUserName(model.Email);
                if (exUser == null)
                {
                    int userId = Uow.Users.DoInsert(model);
                    if (userId > 0)
                    {
                        Core.Common.SendEmail(model, EmailType.NewUser);
                        ViewBag.InsertIsSuccess = true;
                        SetNotify("Ekleme işlemi tamamlandı.", "Kullanıcı");
                        return RedirectToAction("Edit", "User", new { id = userId.GetIndirectRef(typeof(User)) });
                    }
                    else
                        ViewBag.InsertIsSuccess = false;
                }
                else
                {
                    ModelState.AddModelError("Email", "E-Posta adresi zaten kullanımda");
                }
            }
            model.ImportFileContainer = new FileContainer()
            {
                FileType = FileType.Excell,
                OwnerType = FileOwnerType.Temp
            };
            return View(model);
        }


        [ValidateInput(false)]
        public ActionResult GetUserList()
        {
            RedirectForUnauthorized();
            var model = Uow.Users.GetAll(CurrentUser.CompanyId);
            return PartialView("_UserList", model);
        }
        [HttpGet]
        public ActionResult Edit(string id)
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = true;
            int userId = id.GetDirectRefForInt(typeof(User));
            User model = null;
            if (userId > 0)
            {
                model = Uow.Users.GetByKey(userId);
                if (model == null)
                {
                    SetNotify("Düzenlenemez!", "Kullanıcı", NotifyMessageType.Warning);
                    return RedirectToAction("Index", "User");
                }
            }
            else
            {
                SetNotify("Hatalı parametre!", "Kullanıcı", NotifyMessageType.Warning);
                if (model == null) return RedirectToAction("Index", "User");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(User model)
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = ModelState.IsValid;
            int userId = Request.Form["UserIdRef"] != null ? Request.Form["UserIdRef"].ToString().GetDirectRefForInt(typeof(User)) : 0;
            if (ModelState.IsValid && userId > 0)
            {
                model.UserId = userId;
                model.LastUpdatedDate = DateTime.Now;
                model.UserName = model.Email;
                var exUser = Core.Common.Uow.Users.GetUserByUserName(model.Email);
                if (exUser == null || exUser.UserId == model.UserId)
                {
                    Uow.Users.DoUpdate(model);
                    ViewBag.UpdateIsSuccess = true;
                    SetNotify("Güncelleme işlemi tamamlandı.", "Kullanıcı");
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    ModelState.AddModelError("Email", "E-Posta adresi zaten kullanımda");
                }
            }
            else
            {
                ViewBag.UpdateIsSuccess = false;
                SetNotify("Güncelleme işlemi başarısız.", "Kullanıcı", NotifyMessageType.Warning);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult ImportUser(FileContainer model, string Tablolar)
        {
            string tabloAdi = Tablolar;
            ViewBag.IsValidImport = true;
            FileUploadSettings uploader = new FileUploadSettings();
            string fileUrl = uploader.GetPhysicalFileLink(model);
            if (!string.IsNullOrEmpty(fileUrl))
            {
                ExcellProvider excell = new ExcellProvider();
                var users = excell.ImportData(fileUrl, tabloAdi);
                if (users == true)
                {
                    SetNotify("Aktarma işlemi tamamlandı.", "Kullanıcı");
                }
            }
            else
            {
                ViewBag.IsValidImport = false;
                if (string.IsNullOrEmpty(fileUrl))
                {
                    SetNotify("Bulunamadı!", "Dosya", NotifyMessageType.Warning);
                    ModelState.AddModelError("FileName", "Lütfen dosyayı yükleyin");
                }
                if (model.CompanyId <= 0)
                    ModelState.AddModelError("FileCompanyId", "Lütfen şirket seçiniz");
            }
            User userModel = new User();
            userModel.ImportFileContainer = new FileContainer()
            {
                FileType = FileType.Excell,
                OwnerType = FileOwnerType.Temp
            };
            return View("New", userModel);
        }

        [HttpGet]
        public ActionResult Profile()
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = true;
            User model = null;
            if (CurrentUser.UserId > 0)
            {
                model = Uow.Users.GetByKey(CurrentUser.UserId);
                if (model == null)
                {
                    SetNotify("Düzenlenemez!", "Bilgilerim", NotifyMessageType.Warning);
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                SetNotify("Hatalı parametre!", "Bilgilerim", NotifyMessageType.Warning);
                if (model == null) return RedirectToAction("Index", "Home");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Profile(User model)
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = ModelState.IsValid;
            int userId = Request.Form["UserIdRef"] != null ? Request.Form["UserIdRef"].ToString().GetDirectRefForInt(typeof(User)) : 0;
            if (ModelState.IsValid && userId > 0)
            {
                model.UserId = userId;
                model.LastUpdatedDate = DateTime.Now;
                model.UserName = model.Email;
                var exUser = Core.Common.Uow.Users.GetUserByUserName(model.Email);
                if (exUser == null || exUser.UserId == model.UserId)
                {
                    if (Request.Form["RePassword"] != model.Password)
                    {
                        ModelState.AddModelError("Password", "Gönderilen şifreler uyumsuz.");
                        ViewBag.UpdateIsSuccess = false;
                        return View(model);
                    }
                    else if (Request.Form["OldPassword"] != model.Password)
                        model.Password = new PasswordHasher().HashPassword(model.Password);

                    Uow.Users.DoUpdate(model);
                    ViewBag.UpdateIsSuccess = true;
                    SetNotify("Güncelleme işlemi tamamlandı.", "Bilgilerim");
                    return View(model);
                }
                else
                {
                    ModelState.AddModelError("Email", "E-Posta adresi zaten kullanımda");
                }
            }
            else
            {
                ViewBag.UpdateIsSuccess = false;
                SetNotify("Güncelleme işlemi başarısız.", "Bilgilerim", NotifyMessageType.Warning);
            }
            return View(model);
        }
        public ActionResult MultipleUserAdd()
        {
            ViewBag.IsValid = ModelState.IsValid;
            User model = new User();
            model.ImportFileContainer = new FileContainer()
            {
                FileType = FileType.Excell,
                OwnerType = FileOwnerType.Temp
            };
            List<String> TableNames = new List<String>();
            SqlConnection conn = new SqlConnection("Password=ser-9274;Persist Security Info=True;User ID=sa;Initial Catalog=YtuDb;Data Source=31.210.91.14");
            conn.Open();
            string query = "Select DISTINCT Tablename from Title";
            using (SqlCommand command = new SqlCommand(query, conn))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        TableNames.Add(reader.GetString(0));
                    }
                }
            }
            ViewBag.TabloIsimleri = TableNames;
            return View(model);
        }


        [HttpPost]
        public ActionResult SqlTemplateAdd(string function_param, string DemografikBilgiler, string tabloAdi)
        {
            // Kullanıcı Bilgilerinin Alındığı İlk Tablo
            List<string> KullaniciBilgileri = new List<string>();
            function_param = function_param.Replace(" ,", ",");
            function_param = function_param.Replace(", ", ",");
            foreach (string id in (function_param.Split(',')))
            {
                KullaniciBilgileri.Add(id);
            }
            // Demografik Bilgilerin Alındığı İkinci Tablo
            List<string> demografikBilgiler = new List<string>();
            function_param = function_param.Replace(" ,", ",");
            function_param = function_param.Replace(", ", ",");
            foreach (string id in (DemografikBilgiler.Split(',')))
            {
                demografikBilgiler.Add(id + "_");
            }
            
            foreach (string demog in demografikBilgiler)
            {
                Uow.Users.TitleInsert(tabloAdi, demog);
            }
            bool table1 = CreateTable(tabloAdi, KullaniciBilgileri);
            bool table2 = CreateTable(tabloAdi + "demografik", demografikBilgiler);
            bool excelFile = CreateExcelFile(KullaniciBilgileri, demografikBilgiler);

            if (table1 == true && table2 == true && excelFile == true)
            {
                return Json(new { success = true, responseText = "Özellikler başarılı bir şekilde oluşturuldu!", excelLinki = Session["ExcelDosyaYolu"].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Bir sorun oluştu!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool CreateTable(string tabloAdi, List<string> parametreler)
        {
            try
            {
                string BaglantiAdresi = "Password = ser-9274; Persist Security Info = True; User ID = sa; Initial Catalog = YtuDb; Data Source = 31.210.91.14";

                var commandStr = "If not exists (select name from sysobjects where name = '" + tabloAdi + "') CREATE TABLE  " + tabloAdi + "(Id int NOT NULL Primary Key IDENTITY(1,1))";

                SqlConnection con = new SqlConnection(BaglantiAdresi);
                SqlCommand cmd = new SqlCommand(commandStr, con);
                con.Open();
                cmd.ExecuteNonQuery();

                string query = "alter table " + tabloAdi + " add ";

                for (int ic = 0; ic < parametreler.Count; ic++)
                {
                    if (ic == (parametreler.Count - 1))
                    {
                        query = query + parametreler[ic] + " varchar(50) NULL";
                    }
                    else
                    {
                        query = query + parametreler[ic] + " varchar(50) NULL,";
                    }
                }
                SqlCommand cmd2 = new SqlCommand(query, con);
                cmd2.ExecuteNonQuery();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool CreateExcelFile(List<string> kisisel, List<string> demografik)
        {
            try
            {
                Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;

                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                int kisiselSayac = 0;
                int demografikSayac = 0;
                for (kisiselSayac = 0; kisiselSayac < kisisel.Count; kisiselSayac++)
                {
                    xlWorkSheet.Cells[1, kisiselSayac + 1] = kisisel[kisiselSayac];
                    xlWorkSheet.Cells[1, kisiselSayac + 1].EntireRow.Font.Bold = true;
                }
                for (int i = kisiselSayac; i < (demografik.Count + kisiselSayac); i++)
                {
                    xlWorkSheet.Cells[1, i + 1] = demografik[demografikSayac];
                    xlWorkSheet.Cells[1, i + 1].EntireRow.Font.Bold = true;
                    demografikSayac++;
                }

                var shortGuid = Regex.Replace(Convert.ToBase64String(Guid.NewGuid().ToByteArray()), "[/+=]", "").Substring(0, 5);

                var filePath = System.Web.HttpContext.Current.Server.MapPath("/ExcelTemplates/ExcelSablon_" + shortGuid);

                xlWorkBook.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookDefault, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlWorkSheet);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);
                Session["ExcelDosyaYolu"] = filePath.ToString();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public ActionResult CreateExcelTemplate()
        {
            return View();
        }

        //[HttpPost, ValidateInput(false)]
        //public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] Pollyfy.Web.Models.User item)
        //{
        //    var model = Uow.Users.GetAll();
        //    if (ModelState.IsValid)
        //    {
        //    }
        //    else
        //        ViewData["EditError"] = "Please, correct all errors.";
        //    return PartialView("_GridViewPartial", model.ToList());
        //}
        //[HttpPost, ValidateInput(false)]
        //public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] Pollyfy.Web.Models.User item)
        //{
        //    var model = Uow.Users.GetAll();
        //    if (ModelState.IsValid)
        //    {

        //    }
        //    else
        //        ViewData["EditError"] = "Please, correct all errors.";
        //    return PartialView("_GridViewPartial", model.ToList());
        //}
        //[HttpPost, ValidateInput(false)]
        //public ActionResult GridViewPartialDelete(System.Int32 UserId)
        //{
        //    var model = Uow.Users.GetAll();
        //    if (UserId >= 0)
        //    {

        //    }
        //    return PartialView("_GridViewPartial", model.ToList());
        //}
    }
}