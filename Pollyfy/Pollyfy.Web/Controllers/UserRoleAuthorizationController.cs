﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Newtonsoft.Json;
using Pollyfy.Data.Interfaces;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Web.Controllers
{
    [System.Web.Mvc.Authorize]
    public class UserRoleAuthorizationController : BaseController
    {
        public UserRoleAuthorizationController(IUow uow) : base(uow)
        {
        }

        [System.Web.Mvc.HttpGet]
        public string GetList()
        {
            RedirectForUnauthorized();

            var model = Uow.UserRoleAuthorizations.GetAll();

            return JsonConvert.SerializeObject(model);
        }

        [System.Web.Mvc.HttpPost]
        public bool Post([FromBody] List<UserRoleAuthorization> item)
        {
            try
            {
                if (item == null || item.Count < 1)
                    return false;

                var userRoleAuthorizations = Uow.UserRoleAuthorizations.GetAll().Where(p => p.UserRoleId == item.FirstOrDefault().UserRoleId);

                foreach (var userRoleAuthorizationItem in item)
                {
                    var dbUserRoleAuthorization =userRoleAuthorizations.FirstOrDefault(p => p.Id == userRoleAuthorizationItem.Id);

                    if (dbUserRoleAuthorization == null)
                        Uow.UserRoleAuthorizations.DoInsert(userRoleAuthorizationItem);
                }

                foreach (var userRoleAuthorization in userRoleAuthorizations.Where(p => !item.Select(q => q.Id).ToArray().Contains(p.Id)))
                    Uow.UserRoleAuthorizations.DoDelete(userRoleAuthorization.Id);
            }
            catch (Exception)
            {
                // ignored
            }

            return true;
        }
    }
}