﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DevExpress.CodeParser;
using Pollyfy.Models;
using Pollyfy.Data.Interfaces;
using Pollyfy.Web.Core;
using Pollyfy.Core.Extensions;
using Pollyfy.Models.ViewModel;
using Newtonsoft.Json;
using Pollyfy.Models.DataModel;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using Pollyfy.Services;

namespace Pollyfy.Web.Controllers
{
    public class SurveyController : BaseController
    {
        public SurveyController(IUow uow) : base(uow) { }

        private Survey FillPublishStatus(Survey survey, bool reset = false, bool forceUpdate = false)
        {
            if (survey != null)
            {
                PublishStatus? status = null;
                if (survey.QuestionCount > survey.RealQuestionCount)
                    status = PublishStatus.QuestionRequired;
                else if (survey.QuestionCount < survey.RealQuestionCount)
                    status = PublishStatus.QuestionOver;
                else if (survey.PublishStatus == PublishStatus.QuestionRequired || survey.PublishStatus == PublishStatus.QuestionOver)
                    status = PublishStatus.None;

                if ((status.HasValue && status.Value == PublishStatus.None) || survey.PublishStatus == PublishStatus.None)
                    status = PublishStatus.Ready;

                if (status.HasValue)
                    survey.PublishStatus = status.Value;
                else if (reset && (CurrentUser.Types != UserType.SystemAdmin || forceUpdate))
                {
                    survey.PublishStatus = PublishStatus.None;
                    survey.StatusText = "";
                }

            }
            return survey;
        }

        private void UpdatePublishStatus(int surveyId)
        {
            if (surveyId > 0)
            {
                Survey survey = Uow.Surveys.GetById(surveyId);
                if (survey != null)
                {
                    var temp = Uow.Surveys.GetForAll(surveyId);
                    temp = FillPublishStatus(temp, true);
                    //else if (CurrentUser.Types != UserType.SystemAdmin && )
                    //    status = PublishStatus.None;

                    survey.PublishStatus = temp.PublishStatus;
                    Uow.Commit();
                }
            }
        }

        public ActionResult Index()
        {
            RedirectForUnauthorized();

            IEnumerable<Survey> surveys = new List<Survey>();

            if (CurrentUser.Types == UserType.SystemAdmin)
                surveys = Uow.Surveys.GetForAll();
            else if (CurrentUser.Types == UserType.CompanyAdmin && CurrentUser.CompanyId.HasValue)
                surveys = Uow.Surveys.GetByCompanyId(CurrentUser.CompanyId.Value);
            else if (CurrentUser.Types == UserType.CompanyUser)
                surveys = Uow.Surveys.GetByUserId(CurrentUser.UserId);

            foreach (Survey survey in surveys)
            {
                var ts = FillPublishStatus(survey);
                survey.PublishStatus = ts.PublishStatus;
            }

            return View(surveys);
        }

        public ActionResult New()
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = ModelState.IsValid;
            PrepareViewParams();
            GetTableNameList();
            Survey model = new Survey();
            return View(model);
        }

        [HttpPost]
        public ActionResult New(Survey model)
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = ModelState.IsValid;
            if (ModelState.IsValid)
            {
                model.ActiveStatus = ActiveStatus.Passive;
                model.CreatedByUserId = CurrentUser.UserId;
                model.CompanyId = 1;
                int surveyId = Uow.Surveys.DoInsert(model);
                if (surveyId > 0)
                {
                    model.SurveyId = surveyId;
                    if (model.FormScopes != null)
                    {
                        int[] scopeIds = model.FormScopes.Select(m => int.Parse(m)).ToArray();
                        Uow.Surveys.InsertScopes(surveyId, scopeIds);
                    }
                    if (model.FormConstraints != null)
                    {
                        int[] constraintIds = model.FormConstraints.Select(m => int.Parse(m)).ToArray();
                        Uow.Surveys.InsertConstraints(surveyId, constraintIds);
                    }
                    if (model.FormReportConstraints != null)
                    {
                        int[] constraintIds = model.FormReportConstraints.Select(m => int.Parse(m)).ToArray();
                        Uow.Surveys.InsertReportConstraints(surveyId, constraintIds);
                    }
                    if (!string.IsNullOrEmpty(model.FormUsers))
                    {
                        int[] userIds = model.FormUsers.Split(',').Select(id => int.Parse(id)).ToArray();
                        Uow.Surveys.InsertUsers(surveyId, userIds);
                        model.InCompanyUsage = userIds.Length > 0;
                    }
                    else
                        model.InCompanyUsage = true;

                    SetNotify("Yeni anketiniz tanımlandı.", "Anket");

                    if (SurveyManager.ImportPromotion(model.CurrentPromotion, surveyId))
                        SetNotify("Kodları tanımlandı.", "Kampanya");
                    else if (model.UsageType == UsageType.CampaignSurvey)
                        SetNotify("Kodları tanımlanmadı.", "Kampanya", NotifyMessageType.Warning);

                    if (model.QuestionGroups != null && model.QuestionGroups.Count > 0)
                    {
                        foreach (QuestionGroup g in model.QuestionGroups.Where(m => !string.IsNullOrEmpty(m.Name) && m.ActiveStatus == ActiveStatus.Active))
                        {
                            g.SurveyId = surveyId;
                            g.GroupType = g.BeginScore.HasValue && g.BeginScore.Value >= 0 && g.EndScore.HasValue && g.EndScore.Value > 0 ? GroupType.ScoreRange : GroupType.Featured;
                            Uow.QuestionGroups.DoInsert(g);
                        }
                    }

                    ViewBag.InsertIsSuccess = true;
                    UpdatePublishStatus(surveyId);
                    if (surveyId > 0 && Request.Form["MoveQuestions"] != null)
                        return RedirectToAction("Questions", "Survey", new { id = surveyId.GetIndirectRef(typeof(Survey)) });
                    else
                        return RedirectToAction("Edit", "Survey", new { id = surveyId.GetIndirectRef(typeof(Survey)) });
                }
                else
                {
                    ViewBag.InsertIsSuccess = false;
                    SetNotify("Tanımlanırken sorun oluştu.", "Anket", NotifyMessageType.Warning);
                }
            }
            PrepareViewParams();
            GetTableNameList();
            return View(model);
        }

        public ActionResult Edit(string id)
        {
            RedirectForUnauthorized();
            int surveyId = id.GetDirectRefForInt(typeof(Survey));
            if (surveyId <= 0) return RedirectToAction("Index", "Survey");

            Survey model = Uow.Surveys.GetByKey(surveyId);
            if (model == null) return RedirectToAction("Index", "Survey");
            var scopes = Uow.Surveys.SelectScopes(surveyId);
            model.FormScopes = scopes.Select(m => m.ToString()).ToArray();
            var consts = Uow.Surveys.SelectConstraints(surveyId);
            model.FormConstraints = consts.Select(m => m.ToString()).ToArray();
            var reportConsts = Uow.Surveys.SelectReportConstraints(surveyId);
            model.FormReportConstraints = reportConsts.Select(m => m.ToString()).ToArray();
            var users = Uow.Surveys.SelectUsers(surveyId);
            model.FormUsers = string.Join(",", users);
            TempData["SurveyUserIds"] = model.FormUsers;
            model.PromotionCount = Uow.Surveys.GetPromotionCount(surveyId);
            model.QuestionGroups = Uow.QuestionGroups.GetBySurveyId(surveyId).ToList();
            PrepareViewParams();
            GetTableNameList();
            return View(model);
        }

        // todo: Ankete özel resim eklemecek
        [HttpPost]
        public ActionResult Edit(Survey model)
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = ModelState.IsValid;
            int surveyId = Request.Form["SurveyIdRef"] != null ? Request.Form["SurveyIdRef"].ToString().GetDirectRefForInt(typeof(Survey)) : 0;
            if (ModelState.IsValid && surveyId > 0)
            {
                var dbSurvey = Uow.Surveys.GetByKey(surveyId);
                model.SurveyId = surveyId;
                if (CurrentUser.Types != UserType.SystemAdmin)
                    model.ActiveStatus = ActiveStatus.Passive;

                model.CreatedByUserId = dbSurvey.CreatedByUserId;
                if (model.CompanyId <= 0)
                    model.CompanyId = dbSurvey.CompanyId;
                else if (model.CompanyId <= 0)
                {
                    ModelState.AddModelError("CompanyId", "Lütfen şirket seçiniz.");
                    PrepareViewParams();
                    GetTableNameList();
                    return View(model);
                }
                Uow.Surveys.DoUpdate(model);

                Uow.Surveys.DeleteScopes(model.SurveyId, null, true);
                Uow.Surveys.DeleteConstraints(model.SurveyId, null, true);
                Uow.Surveys.DeleteReportConstraints(model.SurveyId, null, true);
                Uow.Surveys.DeleteUsers(model.SurveyId, null, true);
                if (model.FormScopes != null)
                {
                    int[] scopeIds = model.FormScopes.Select(m => int.Parse(m)).ToArray();
                    Uow.Surveys.InsertScopes(model.SurveyId, scopeIds);
                }
                if (model.FormConstraints != null)
                {
                    int[] constraintIds = model.FormConstraints.Select(m => int.Parse(m)).ToArray();
                    Uow.Surveys.InsertConstraints(model.SurveyId, constraintIds);
                }
                if (model.FormReportConstraints != null)
                {
                    int[] constraintIds = model.FormReportConstraints.Select(m => int.Parse(m)).ToArray();
                    Uow.Surveys.InsertReportConstraints(model.SurveyId, constraintIds);
                }
                if (!string.IsNullOrEmpty(model.FormUsers))
                {
                    TempData["SurveyUserIds"] = model.FormUsers;
                    int[] userIds = model.FormUsers.Split(',').Select(id => int.Parse(id)).ToArray();
                    Uow.Surveys.InsertUsers(surveyId, userIds);
                }
                else
                    TempData["SurveyUserIds"] = null;

                ViewBag.UpdateIsSuccess = true;
                UpdatePublishStatus(surveyId);

                SetNotify("Güncelleme işlemi tamamlandı.", "Anket");
                model.PromotionCount = Uow.Surveys.GetPromotionCount(surveyId);
                if (SurveyManager.ImportPromotion(model.CurrentPromotion, surveyId))
                    SetNotify("Kodları tanımlandı.", "Kampanya");
                else if (model.UsageType == UsageType.CampaignSurvey && model.PromotionCount == 0)
                    SetNotify("Kodları tanımlanmadı.", "Kampanya", NotifyMessageType.Warning);

                if (model.QuestionGroups.Count > 0)
                {
                    foreach (QuestionGroup g in model.QuestionGroups.Where(m => m.GroupId > 0 && m.ActiveStatus != ActiveStatus.Active))
                        Uow.QuestionGroups.DoDelete(g);
                    model.QuestionGroups = model.QuestionGroups.Where(m => !string.IsNullOrEmpty(m.Name) && m.ActiveStatus == ActiveStatus.Active).ToList();
                    foreach (QuestionGroup g in model.QuestionGroups)
                    {
                        g.SurveyId = surveyId;
                        g.GroupType = g.BeginScore.HasValue && g.BeginScore.Value >= 0 && g.EndScore.HasValue && g.EndScore.Value > 0 ? GroupType.ScoreRange : GroupType.Featured;
                        if (g.GroupId > 0)
                            Uow.QuestionGroups.DoUpdate(g);
                        else
                            Uow.QuestionGroups.DoInsert(g);

                    }
                }
                else
                    Uow.QuestionGroups.DeleteBySurveyId(surveyId);

                if (Request.Form["MoveQuestions"] != null && Request.Form["MoveQuestions"].ToString() == "true")
                    return RedirectToAction("Questions", "Survey", new { id = model.SurveyId.GetIndirectRef(typeof(Survey)) });
            }
            else
            {
                model.SurveyId = surveyId;
                model.PromotionCount = Uow.Surveys.GetPromotionCount(surveyId);
                ViewBag.UpdateIsSuccess = false;
                SetNotify("Güncelleme işlemi başarısız.", "Anket", NotifyMessageType.Warning);
            }

            PrepareViewParams();
            GetTableNameList();
            return View(model);
        }

        public ActionResult Delete(string id)
        {
            RedirectForUnauthorized();
            int surveyId = id.GetDirectRefForInt(typeof(Survey));
            if (surveyId > 0 && Uow.Surveys.UpdateStatus(surveyId, ActiveStatus.Deleted))
                SetNotify("Silme işlemi tamamlandı.", "Anket");
            else
                SetNotify("Silinirken sorun oluştu.", "Anket", NotifyMessageType.Warning);

            return RedirectToAction("Index", "Survey");
        }

        [HttpPost]
        public ActionResult PublishStatusUpdate(string surveyId, string status, string publish)
        {
            int sId = surveyId.GetDirectRefForInt(typeof(Survey));
            if (sId > 0)
            {
                Survey survey = Uow.Surveys.GetById(sId);
                if (survey != null)
                {
                    survey.PublishStatus = publish == "1" ? PublishStatus.Published : PublishStatus.ApprovalDenied;
                    survey.ActiveStatus = publish == "1" ? ActiveStatus.Active : ActiveStatus.Passive;
                    survey.StatusText = status;
                    survey.FirstPublishDate = survey.FirstPublishDate.HasValue ? survey.FirstPublishDate.Value : DateTime.Now;
                    survey.LastPublishDate = publish == "1" ? DateTime.Now : survey.LastPublishDate;
                    Uow.Commit();
                    if (publish == "1")
                        SetNotify("Yayına alındı.", "Anket");
                    else
                        SetNotify("Durumu güncellendi.", "Anket");
                }
            }
            else
            {
                SetNotify("Güncellenirken sorun oluştu.", "Anket", NotifyMessageType.Error);
            }
            return RedirectToAction("Index", "Survey");
        }

        public ActionResult PublishToggle(string id)
        {
            RedirectForUnauthorized();
            int surveyId = id.GetDirectRefForInt(typeof(Survey));
            if (surveyId > 0)
            {
                Survey survey = Uow.Surveys.GetById(surveyId);
                //survey = FillPublishStatus(survey);
                if (survey != null)
                {
                    if (survey.PublishStatus == PublishStatus.Published)
                    {
                        survey.PublishStatus = PublishStatus.UnPublished;
                        survey.ActiveStatus = ActiveStatus.Passive;
                        survey.LastUnPublishDate = DateTime.Now;
                        SetNotify("Yayından kaldırıldı.", "Anket");
                    }
                    else
                    {
                        if (CurrentUser.CompanyId.HasValue)
                        {
                            if (survey.PublishStatus == PublishStatus.Ready ||
                                survey.PublishStatus == PublishStatus.None ||
                                survey.PublishStatus == PublishStatus.UnPublished)
                            {
                                survey.PublishStatus = PublishStatus.WaitingApproval;
                                SetNotify("Yayın onayı için gönderildi.", "Anket");
                            }
                            else if (survey.PublishStatus == PublishStatus.WaitingApproval)
                            {
                                SetNotify("Zaten onay bekleniyor", "Anket");
                            }
                            else
                            {
                                SetNotify("Düzenleyerek hazır hale getirin.", "Anket");
                            }

                        }
                        else
                        {
                            survey.PublishStatus = PublishStatus.Published;
                            survey.ActiveStatus = ActiveStatus.Active;
                            survey.FirstPublishDate = survey.FirstPublishDate.HasValue ? survey.FirstPublishDate.Value : DateTime.Now;
                            survey.LastPublishDate = DateTime.Now;
                            SetNotify("Yayına alındı.", "Anket");

                            //if (survey.PublishStatus == PublishStatus.Ready ||
                            //    survey.PublishStatus == PublishStatus.WaitingApproval ||
                            //    survey.PublishStatus == PublishStatus.ApprovalDenied ||
                            //    survey.PublishStatus == PublishStatus.None ||
                            //    survey.PublishStatus == PublishStatus.UnPublished)
                            //{
                            //    survey.PublishStatus = PublishStatus.Published;
                            //    survey.ActiveStatus = ActiveStatus.Active;
                            //    survey.FirstPublishDate = survey.FirstPublishDate.HasValue ? survey.FirstPublishDate.Value : DateTime.Now;
                            //    survey.LastPublishDate = DateTime.Now;
                            //    SetNotify("Yayına alındı.", "Anket");
                            //}
                            //else
                            //{
                            //    SetNotify("Düzenleyerek hazır hale getirin.", "Anket");
                            //}
                        }
                    }
                    Uow.Commit();
                }
                else
                    SetNotify("İşlemi gerçekleşmedik!", "Anket", NotifyMessageType.Warning);

            }
            else
                SetNotify("Hatalı parametre!", "Anket", NotifyMessageType.Warning);

            return RedirectToAction("Index", "Survey");
        }

        public void PrepareViewParams()
        {
            ViewBag.SurveyConstraints = Uow.HelperCategories.GetByType(CategoryType.SurveyProperties).ToList();
        }
        //Zencefil
        public void GetTableNameList()
        {
            IEnumerable<string> TableName = Uow.Users.GetTableName();
            ViewBag.Tablename = TableName;
        }
       
        public ActionResult Questions(string id)
        {
            RedirectForUnauthorized();
            int surveyId = id.GetDirectRefForInt(typeof(Survey));
            IEnumerable<Question> questions = new List<Question>();
            if (surveyId > 0)
            {
                if (surveyId <= 0) return RedirectToAction("Index", "Survey");
                Survey survey = Uow.Surveys.GetByKey(surveyId);
                if (survey != null)
                {
                    ViewBag.SurveyName = survey.Name;
                    ViewBag.SurveyType = (int)survey.SurveyType;
                }
                questions = Uow.Questions.GetBySurveyId(surveyId);
            }
            else
                return RedirectToAction("Index", "Survey");
            ViewBag.SurveyId = id;
            return View(questions);
        }

        private void PrepareQuestionViewBag(Survey survey)
        {
            if (survey != null)
            {
                ViewBag.SurveyId = survey.SurveyId.GetIndirectRef(typeof(Survey));
                ViewBag.SurveyName = survey.Name;
                ViewBag.SurveyType = (int)survey.SurveyType;
            }
            else
            {
                ViewBag.SurveyName = "";
                ViewBag.SurveyType = 0;
            }
        }

        public ActionResult Question(string id, string queId, QuestionType type = QuestionType.SingleSelect)
        {
            RedirectForUnauthorized();
            Question model = new Question();
            model.QuestionType = type;
            int surveyId = id.GetDirectRefForInt(typeof(Survey));
            int questionId = queId.GetDirectRefForInt(typeof(Question));

            if (surveyId <= 0) return RedirectToAction("Index", "Survey");
            Survey survey = Uow.Surveys.GetByKey(surveyId);
            if (survey != null)
                model.Survey = survey;

            PrepareQuestionViewBag(survey);
            if (questionId > 0)
            {
                ViewBag.Title = "Soru Düzenle";
                model = Uow.Questions.GetByKey(questionId);
                if (model != null)
                {
                    model.Choices = Uow.Choices.GetByQuestionId(questionId).ToList();
                    var choiceGroups = Uow.QuestionGroups.GetByQuestionId(questionId).ToList();
                    foreach (Choice c in model.Choices)
                    {
                        c.FormIncludeGroups = choiceGroups.Where(m => m.ChoiceId == c.ChoiceId && m.Route == 1).Select(m => m.GroupId).ToArray();
                        c.FormExcludeGroups = choiceGroups.Where(m => m.ChoiceId == c.ChoiceId && m.Route == -1).Select(m => m.GroupId).ToArray();
                    }
                }
                if (survey != null)
                    model.Survey = survey;
                model = PrepareQuestionChoices(model);
            }
            else
            {
                ViewBag.Title = "Yeni Soru Ekle";
                model = PrepareQuestionChoices(model);
            }
            model.Groups = Uow.QuestionGroups.GetBySurveyId(surveyId).Where(m => m.GroupType == GroupType.Featured).ToList();
            ViewBag.SurveyId = id;
            return View(model);
        }

        public Question PrepareQuestionChoices(Question model)
        {
            if (model.Survey != null && model.Survey.SurveyType == SurveyType.Enneagram)
            {
                model.IsRequired = true;
                List<Choice> defaults = new List<Choice>();
                defaults.Add(new Choice() { ChoiceId = -1, PassQuestion = 0, Score = 1, Text = "Bana hiç uymuyor" });
                defaults.Add(new Choice() { ChoiceId = -1, PassQuestion = 0, Score = 2, Text = "Bana uymuyor" });
                defaults.Add(new Choice() { ChoiceId = -1, PassQuestion = 0, Score = 3, Text = "Bana oldukça uyuyor" });
                defaults.Add(new Choice() { ChoiceId = -1, PassQuestion = 0, Score = 4, Text = "Bana tam olarak uyuyor" });

                if (model.Choices.Count == 0)
                    model.Choices = defaults;
                else if (model.Choices.Count > defaults.Count)
                    model.Choices = model.Choices.Take(4).ToList();
                else if (model.Choices.Count < defaults.Count)
                {
                    var temp = model.Choices.ToList();
                    temp.AddRange(defaults.Take(defaults.Count - model.Choices.Count));
                    model.Choices = temp;
                }
            }
            else if (model.Choices.Count == 0)
            {
                model.Choices.Add(new Choice() { ChoiceId = -1, PassQuestion = 0, Text = "" });
                model.Choices.Add(new Choice() { ChoiceId = -1, PassQuestion = 0, Text = "" });
            }
            return model;
        }

        [HttpPost]
        public ActionResult Question(Question model)
        {
            RedirectForUnauthorized();
            string surveyIdRef = Request.Form["SurveyIdRef"] != null ? Request.Form["SurveyIdRef"].ToString() : "";
            string questionIdRef = Request.Form["QuestionIdRef"] != null ? Request.Form["QuestionIdRef"].ToString() : "";
            int surveyId = surveyIdRef.GetDirectRefForInt(typeof(Survey));
            int questionId = questionIdRef.GetDirectRefForInt(typeof(Question));
            Survey survey = Uow.Surveys.GetById(surveyId);
            if (ModelState.IsValid && surveyId > 0 && survey != null)
            {
                model.SurveyId = surveyId;
                model.CreatedByUserId = CurrentUser.UserId;
                model.CompanyId = survey.CompanyId;
                if (questionId > 0)
                {
                    model.QuestionId = questionId;
                    //Uow.Questions.Update(model);
                    if (model.QuestionType != QuestionType.OpenEnded)
                    {
                        model.Choices = Uow.Choices.GetByQuestionId(model.QuestionId).ToList();
                        List<Choice> choices = new List<Choice>();
                        if (model.FormChoices != null)
                        {
                            for (int i = 0; i < model.FormChoices.Count(); i++)
                            {
                                if (model.FormChoices[i].ActiveStatus != ActiveStatus.Active || string.IsNullOrEmpty(model.FormChoices[i].Text)) continue;

                                Choice choice = model.FormChoices[i];
                                int choiceId = choice.ChoiceId;
                                if (choiceId > 0)
                                {
                                    Choice same = model.Choices.FirstOrDefault(m => m.ChoiceId == choiceId);
                                    if (same != null) model.Choices.Remove(same);
                                }
                                choice.QuestionId = questionId;

                                if (choice.ChoiceId == 0)
                                    choice.ChoiceId = Uow.Choices.DoInsert(choice);
                                else
                                    Uow.Choices.DoUpdate(choice);

                                if (choice.ChoiceId > 0)
                                {
                                    Uow.QuestionGroups.DeleteByChoiceId(choice.ChoiceId);
                                    List<ChoiceGroup> choiceGroups = new List<ChoiceGroup>();
                                    if (choice.FormIncludeGroups != null)
                                        foreach (int groupId in choice.FormIncludeGroups)
                                            choiceGroups.Add(new ChoiceGroup() { QuestionId = questionId, ChoiceId = choice.ChoiceId, Route = 1, GroupId = groupId });

                                    if (choice.FormExcludeGroups != null)
                                        foreach (int groupId in choice.FormExcludeGroups)
                                            choiceGroups.Add(new ChoiceGroup() { QuestionId = questionId, ChoiceId = choice.ChoiceId, Route = -1, GroupId = groupId });

                                    if (choiceGroups.Count > 0)
                                        Uow.QuestionGroups.DoInsert<ChoiceGroup>(choiceGroups);
                                }
                                choices.Add(choice);
                            }
                        }
                        if (choices.Any(m => m.Score.HasValue))
                            model.MaxScore = choices.Where(m => m.Score.HasValue).Max(m => m.Score);
                    }
                    Uow.Questions.DoUpdate(model);
                    foreach (Choice c in model.Choices)
                    {
                        Uow.Choices.UpdateStatus(c.ChoiceId, ActiveStatus.Deleted);
                        Uow.QuestionGroups.DeleteByChoiceId(c.ChoiceId);
                    }
                    SetNotify("Güncelleme işlemi başarılı.", "Soru");
                }
                else
                {
                    model.QuestionId = Uow.Questions.DoInsert(model);
                    if (model.QuestionType != QuestionType.OpenEnded)
                    {
                        List<Choice> choices = new List<Choice>();
                        if (model.QuestionId > 0 && model.FormChoices != null)
                        {
                            for (int i = 0; i < model.FormChoices.Count(); i++)
                            {
                                if (model.FormChoices[i].ActiveStatus != ActiveStatus.Active || string.IsNullOrEmpty(model.FormChoices[i].Text)) continue;
                                Choice choice = model.FormChoices[i];
                                choice.QuestionId = model.QuestionId;
                                choice.ChoiceId = Uow.Choices.DoInsert(choice);
                                if (choice.ChoiceId > 0)
                                {
                                    Uow.QuestionGroups.DeleteByChoiceId(choice.ChoiceId);
                                    List<ChoiceGroup> choiceGroups = new List<ChoiceGroup>();
                                    if (choice.FormIncludeGroups != null)
                                        foreach (int groupId in choice.FormIncludeGroups)
                                            choiceGroups.Add(new ChoiceGroup() { QuestionId = model.QuestionId, ChoiceId = choice.ChoiceId, Route = 1, GroupId = groupId });

                                    if (choice.FormExcludeGroups != null)
                                        foreach (int groupId in choice.FormExcludeGroups)
                                            choiceGroups.Add(new ChoiceGroup() { QuestionId = model.QuestionId, ChoiceId = choice.ChoiceId, Route = -1, GroupId = groupId });

                                    if (choiceGroups.Count > 0)
                                        Uow.QuestionGroups.DoInsert<ChoiceGroup>(choiceGroups);
                                }

                                choices.Add(choice);
                            }
                            SetNotify("Ekleme işlemi başarılı.", "Soru");
                        }
                        if (choices.Any(m => m.Score.HasValue))
                        {
                            model.MaxScore = choices.Where(m => m.Score.HasValue).Max(m => m.Score);
                            Uow.Questions.DoUpdate(model);
                        }
                    }
                    else
                        SetNotify("Ekleme işlemi başarılı.", "Soru");
                }
                UpdatePublishStatus(surveyId);
            }
            else
            {
                PrepareQuestionViewBag(survey);
                model.Survey = survey;
                model.Groups = Uow.QuestionGroups.GetBySurveyId(surveyId).Where(m => m.GroupType == GroupType.Featured).ToList();
                ViewBag.IsValid = false;
                return View(model);
            }
            return RedirectToAction("Questions", "Survey", new { id = surveyIdRef });
        }
        [HttpGet]
        public ActionResult ConstaintsAdd(string id)
        {
            ViewBag.Id = id;
            GetTableNameList();
            return View();
        }

        [HttpPost]
        public ActionResult ConstaintsAdd(string Tablename, string id)
        {
            Session["SurveyId"] = id;
            Session["TabloAdi"] = Tablename;
            Tablename = Tablename + "demografik";
            var Liste = new List<ConstraintsModel>();
            GetTableNameList();
            Liste = GetByConstraint(Tablename);
            return View(Liste);
        }

        public List<ConstraintsModel> GetByConstraint(string Tablename)
        {
            IEnumerable<string> ConstraintList;
            Tablename = Tablename.Replace("demografik", "");
            ConstraintList = Uow.Users.GetByConstaint(Tablename).ToList();
            IEnumerable<string> Values;
            var Liste = new List<ConstraintsModel>();
            Tablename = Tablename + "demografik";
            foreach (var constraint in ConstraintList)
            {
                Values = Uow.Users.GetValuesByConstaint(constraint, Tablename);
                Liste.Add(new ConstraintsModel
                {
                    Consname = constraint,
                    Consvalue = Values,
                });

            }
            Session["kisitlar"] = true;
            return Liste;
        }
        public ActionResult SelectEmail(FormCollection frm)
        {
            string tabloAdi = Session["TabloAdi"].ToString();
            List<string> Cons = new List<string>();
            List<string> ConsName = new List<string>();
            var Liste = new List<ConstraintsModel>();
            string sql = "";
            if(frm.Keys.Count >= 3)
            {
                for (int i = 0; i < (frm.Keys.Count - 2); i++)
                {
                    List<string> Ids = new List<string>();
                    string sql_lite = "";
                    int sayac = 0;
                    Liste.Add(new ConstraintsModel
                    {
                        Consname = frm.AllKeys[i].ToString(),
                        Consvalue = Ids,
                    });

                    String sutunName = "[" + Liste[i].Consname + "]";

                    foreach (string id in frm[i].Split(','))
                    {
                        if (sayac == 0)
                            sql_lite += sutunName + " = " + "'" + id + "'";
                        else
                            sql_lite += " or " + sutunName + " = " + "'" + id + "'";

                        sayac++;

                        Ids.Add(id);
                    }
                    sql_lite = "(" + sql_lite + ")";
                    if (i == 0)
                        sql = sql_lite;
                    else
                    {
                        sql += " and " + sql_lite;
                    }
                   
                }
                sql = "WHERE" + sql;
            }
            else
            {
                sql = "";
            }
            tabloAdi = tabloAdi + "demografik";
            IEnumerable<string> Id = Uow.Users.GetEmail(sql, tabloAdi);
            string Email = null;
            int j = 0;
            tabloAdi = tabloAdi.Replace("demografik", "");
            //string sql = "SELECT [EMail] FROM [" + Tablename + "] WHERE Id = '" + Id + "'";
            string BaglantiAdresi = "Password = ser-9274; Persist Security Info = True; User ID = sa; Initial Catalog = YtuDb; Data Source = 31.210.91.14";
            SqlConnection con = new SqlConnection(BaglantiAdresi);
            con.Open();
            SqlCommand komut = new SqlCommand();
            komut.Connection = con;      
            foreach (var id in Id)
            {
                komut.CommandText = "SELECT * FROM [" + tabloAdi + "] WHERE Id = '" + id + "'";
                SqlDataReader dr = komut.ExecuteReader();
                if (dr.Read())
                {
                    if(j == 0)
                    {
                        Email = dr["EMail"].ToString();
                    }
                    else
                    {
                        Email = "," + dr["EMail"].ToString();
                    }

                    j++;
                }    
            }
            Session["gonderilecekler"] = Email.ToString();
            return Redirect("/Survey/MailPage");
            
        }
        public ActionResult QuestionDelete(string id)
        {
            RedirectForUnauthorized();
            int questionId = id.GetDirectRefForInt(typeof(Question));
            if (questionId > 0)
            {
                Question question = Uow.Questions.GetByKey(questionId);
                if (question != null)
                {
                    Uow.Questions.UpdateStatus(questionId, ActiveStatus.Deleted);
                    SetNotify("Silme işlemi başarılı.", "Soru");
                    UpdatePublishStatus(question.SurveyId);
                    return RedirectToAction("Questions", "Survey", new { id = question.SurveyId.GetIndirectRef(typeof(Survey)) });
                }
                else
                    SetNotify("Silme işlemi yapılamadı.", "Soru", NotifyMessageType.Warning);
            }
            else
                SetNotify("Silme işlemi yapılamadı.", "Soru", NotifyMessageType.Warning);

            return RedirectToAction("Index", "Survey");
        }

        public ActionResult Preview(string id)
        {
            RedirectForUnauthorized();
            int surveyId = id.GetDirectRefForInt(typeof(Survey));
            if (surveyId > 0)
            {
                Survey model = Uow.Surveys.GetSurveyIncludes(surveyId);
                if (model != null)
                {
                    model.Questions = Uow.Questions.GetWithChoicesBySurveyId(surveyId).ToList();
                    model.Company = Uow.Companies.GetByKey(model.CompanyId);
                    if (model.Company != null)
                    {
                        model.Company.FileContainer = new FileContainer()
                        {
                            CompanyId = model.CompanyId,
                            FileName = model.Company.TrackingGuid.ToString() + ".png",
                            OwnerId = model.CompanyId,
                            OwnerType = FileOwnerType.Company,
                            FileType = FileType.Image
                        };
                        FileUploadSettings fus = new FileUploadSettings();
                        model.ImageUrl = fus.GetFileLink(model.Company.FileContainer, true);
                    }
                }
                else
                {
                    SetNotify("İşleminiz gerçekleştirilemedi", "Anket", NotifyMessageType.Warning);
                    return RedirectToAction("Index", "Survey");
                }

                return View(model);
            }
            return RedirectToAction("Index", "Survey");
        }

        [ValidateInput(false)]
        public ActionResult GetUserList()
        {
            RedirectForUnauthorized();

            var model = Uow.Users.GetList().ToList();

            return PartialView("_SurveyUserList", model);
        }

        [HttpGet]
        public string GetList()
        {
            RedirectForUnauthorized();

            IEnumerable<Survey> surveys = new List<Survey>();

            if (CurrentUser.Types == UserType.SystemAdmin)
                surveys = Uow.Surveys.GetForAll();
            else if (CurrentUser.Types == UserType.CompanyAdmin && CurrentUser.CompanyId.HasValue)
                surveys = Uow.Surveys.GetByCompanyId(CurrentUser.CompanyId.Value);
            else if (CurrentUser.Types == UserType.CompanyUser)
                surveys = Uow.Surveys.GetByUserId(CurrentUser.UserId);

            return JsonConvert.SerializeObject(surveys);
        }

        [HttpGet]
        public string GetBySurveyIds(string id)
        {
            RedirectForUnauthorized();

            if (string.IsNullOrEmpty(id))
                return null;

            var enrollments = Uow.Enrollments.GetBySurveyIds(id, ActiveStatus.Active);

            return JsonConvert.SerializeObject(enrollments);
        }
        public ActionResult MailPage()
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = ModelState.IsValid;
            PrepareViewParams();
            return View();
        }
        [HttpPost]
        public ActionResult MailPage(string MailBasligi, string MailContent)
        {
            RedirectForUnauthorized();
            string link = "http://ytu.pollyfy.com/Sorular/Sorular/" + Session["SurveyId"].ToString();
            StreamReader reader = new StreamReader(Server.MapPath("/Models/mailTemplate.txt"));

            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$Body$$", MailContent);
            myString = myString.Replace("$$Link$$", link);
            MessagePackage mailMessage = new MessagePackage();
            var gon = Session["gonderilecekler"];
            List<string> Mail = new List<string>();
            foreach (string id in gon.ToString().Split(','))
            {
                Mail.Add(id);

            }
            foreach(var mail in Mail)
            {
                mailMessage.Body = myString.ToString();
                mailMessage.Subject = MailBasligi;
                mailMessage.ToEmail = mail.ToString();
                SMTPService smtpService = new SMTPService();
                smtpService.Send(mailMessage);
            }
               
            return View();
        }
    }
}