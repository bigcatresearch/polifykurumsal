﻿#region

using System;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using Pollyfy.Core.Extensions;
using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using Pollyfy.Models.DataModel;
using Pollyfy.Models.Enum;

#endregion

namespace Pollyfy.Web.Controllers
{
    [Authorize]
    public class UserRoleController : BaseController
    {
        public UserRoleController(IUow uow) : base(uow) { }

        public ActionResult Index()
        {
            RedirectForUnauthorized();

            SyncAuthorizations();

            return View();
        }

        [ValidateInput(false)]
        public ActionResult GetUserRoleList()
        {
            RedirectForUnauthorized();

            var model = Uow.UserRoles.GetAll();

            return PartialView("_UserRoleList", model);
        }

        [HttpGet]
        public string GetList()
        {
            RedirectForUnauthorized();

            var model = Uow.UserRoles.GetAll();

            return JsonConvert.SerializeObject(model);
        }

        [HttpGet]
        public ActionResult New()
        {
            RedirectForUnauthorized();

            ViewBag.IsValid = ModelState.IsValid;

            return View();
        }

        [HttpPost]
        public ActionResult New(UserRole userRole)
        {
            RedirectForUnauthorized();

            ViewBag.IsValid = ModelState.IsValid;

            if (ModelState.IsValid)
            {
                var model = Uow.UserRoles.DoInsert(userRole);

                if (model > 0)
                {
                    ViewBag.InsertIsSuccess = true;
                    SetNotify("Ekleme işlemi tamamlandı.", "Kullanıcı Rol");
                    return RedirectToAction("Edit", "UserRole", new { id = model.GetIndirectRef(typeof(UserRole)) });
                }
                else
                    ViewBag.InsertIsSuccess = false;
            }

            return View();
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            RedirectForUnauthorized();

            ViewBag.IsValid = ModelState.IsValid;

            var userRoleId = id.GetDirectRefForInt(typeof(UserRole));

            UserRole userRole = null;

            if (userRoleId > 0)
            {
                userRole = Uow.UserRoles.GetByKey(userRoleId);

                if (userRole == null)
                {
                    SetNotify("Düzenlenemez!", "Kullanıcı Rol", NotifyMessageType.Warning);
                    return RedirectToAction("Index", "UserRole");
                }
            }
            else
            {
                SetNotify("Hatalı parametre!", "Kullanıcı Rol", NotifyMessageType.Warning);
                if (userRole == null) return RedirectToAction("Index", "UserRole");
            }

            return View(userRole);
        }

        [HttpPost]
        public ActionResult Edit(UserRole model)
        {
            RedirectForUnauthorized();

            ViewBag.IsValid = ModelState.IsValid;

            if (ModelState.IsValid && model.Id > 0)
            {
                Uow.UserRoles.DoUpdate(model);

                ViewBag.UpdateIsSuccess = true;
                SetNotify("Güncelleme işlemi tamamlandı.", "Kullanıcı Rol");

                return RedirectToAction("Index", "UserRole");
            }
            else
            {
                ViewBag.UpdateIsSuccess = false;
                SetNotify("Güncelleme işlemi başarısız.", "Kullanıcı Rol", NotifyMessageType.Warning);
            }
            return View(model);
        }

        public bool Delete(int id)
        {
            RedirectForUnauthorized();

            if (id > 0 && Uow.UserRoles.UpdateStatus(id, ActiveStatus.Deleted))
                SetNotify("Silme işlemi tamamlandı.", "Kullanıcı Rol");
            else
            {
                SetNotify("Silinirken sorun oluştu.", "Kullanıcı Rol", NotifyMessageType.Warning);
                return false;
            }

            return true;
        }

        public void SyncAuthorizations()
        {
            var permissions = Enum.GetValues(typeof(Authorizations)).Cast<Authorizations>().ToList();

            var authorizationsKeys = Uow.Authorizations.GetAll().Select(p => p.Key).ToList();

            foreach (var persmission in permissions.ToArray())
            {
                if (!authorizationsKeys.Contains(persmission.ToString()))
                {
                    Uow.Authorizations.DoInsert(new Authorization
                    {
                        Name = persmission.ToString(),
                        Key = persmission.ToString()
                    });
                }
            }
        }
    }
}