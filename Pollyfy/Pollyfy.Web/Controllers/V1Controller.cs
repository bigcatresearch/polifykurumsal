﻿#region

using Pollyfy.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Pollyfy.Models.ViewModel;
using System.Web;
using DevExpress.DataProcessing;
using DevExpress.Utils.Extensions;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using NLog;
using Pollyfy.Web.Core;
using Pollyfy.Models;
using Pollyfy.Web.Validator;
using Pollyfy.Core.Extensions;
using Pollyfy.Web.Providers;
using Pollyfy.Models.DataModel;
using Pollyfy.Data.Helpers;
using Common = Pollyfy.Web.Core.Common;

#endregion

namespace Pollyfy.Web.Controllers
{
    [EnableCors("*", "*", "*")]
    public class V1Controller : BaseApiController
    {
        #region Fields

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        #endregion

        public V1Controller(IUow uow) : base(uow)
        {

        }

        #region Properties

        protected ApplicationSignInManager SignInManager
        {
            get
            {
                if (_signInManager == null) _signInManager = HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
                return _signInManager;
            }
            private set
            {
                _signInManager = value;
            }
        }
        protected ApplicationUserManager UserManager
        {
            get
            {
                if (_userManager == null) _userManager = HttpContext.Current.GetOwinContext().Get<ApplicationUserManager>();
                return _userManager;
            }
            private set
            {
                _userManager = value;
            }
        }

        #endregion

        #region Http Mehods

        [HttpPost]
        public async Task<HttpResponseMessage> Ping(QueryModel model)
        {
            int userId = model != null && !string.IsNullOrEmpty(model.UserId) ? model.UserId.GetDirectRefForInt(typeof(User)) : 0;
            if (userId > 0)
            {
                await Uow.Users.UpdateAccessTimeAsync(userId);
            }
            return ApiResponseMessage(true); ;
        }

        [HttpPost]
        public async Task<HttpResponseMessage> LoginUser([FromBody] LoginModel model)
        {
            if (model != null)
            {
                LoginModelValidator validator = new LoginModelValidator();
                var val = validator.Validate(model);

                if (val.IsValid)
                {
                    SignInStatus result = !model.IsExternalLogin ? await SignInManager.PasswordSignInAsync(model.Username, model.Password, false, shouldLockout: false) : SignInStatus.Success;


                    if (result == SignInStatus.Success)
                    {
                        User user = Uow.Users.GetUserByUserName(model.Username);

                        if (user != null && user.Types == UserType.MobileUser)
                        {
                            AuthManager.SignIn(CurrentUser.FillClaimsIdentity(user));

                            UserModel response = new UserModel(user);
                            response.UserId = user.UserId.GetIndirectRef(typeof(User));
                            if (model.Device != null || !string.IsNullOrEmpty(model.Imei))
                            {
                                if (model.Device == null) model.Device = new DeviceModel();
                                if (string.IsNullOrEmpty(model.Device.Imei) && !string.IsNullOrEmpty(model.Imei))
                                    model.Device.Imei = model.Imei;

                                Device device = Uow.Devices.Find(m => m.Imei == model.Device.Imei && (m.Owner.ActiveStatus == ActiveStatus.Deleted || m.UserId == user.UserId)).FirstOrDefault();
                                if (device == null)
                                {
                                    device = model.Device.ToDevice();
                                    device.UserId = user.UserId;
                                    device.CreatedDate = DateTime.Now;
                                    device.LastAccessDate = DateTime.Now;
                                    user.LastActiveDeviceId = Uow.Devices.DoInsert(device);
                                    if (user.LastActiveDeviceId.HasValue && user.LastActiveDeviceId.Value <= 0)
                                        user.LastActiveDeviceId = null;
                                }
                                else
                                {
                                    device.UserId = user.UserId;
                                    device.LastAccessDate = DateTime.Now;
                                    user.LastActiveDeviceId = device.DeviceId;
                                    Uow.Devices.DoUpdate(device);
                                }
                            }
                            user.LastLoginDate = DateTime.Now;
                            Uow.Users.DoUpdate(user);
                            return ApiResponseMessage(true, response);
                        }
                        else
                            return ApiResponseMessage(false, null, ErrorCode.LOGIN_FAILLED);
                    }
                    else
                    {
                        return ApiResponseMessage(false, null, ErrorCode.LOGIN_FAILLED);
                    }
                }
                else if (val.Errors.Count > 0)
                {
                    var errors = val.Errors.Select(m => new { m.ErrorMessage, m.PropertyName }).ToList();
                    return ApiResponseMessage(false, null, ErrorCode.ERROR_VALIDATION, errors);
                }
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage RegisterUser([FromBody] UserModel model)
        {
            if (model != null)
            {
                UserModelValidator validator = new UserModelValidator();

                var val = validator.Validate(model);

                if (val.IsValid)
                {
                    var DbUser = model.ToUser();

                    var exUser = Uow.Users.GetUserByUserName(DbUser.Email);

                    if (exUser == null && ((model.Device != null && !string.IsNullOrEmpty(model.Device.Imei)) || !string.IsNullOrEmpty(model.Imei)))
                    {
                        if (model.Device == null)
                            model.Device = new DeviceModel();

                        if (string.IsNullOrEmpty(model.Device.Imei) && !string.IsNullOrEmpty(model.Imei))
                            model.Device.Imei = model.Imei;

                        //exUser = Uow.Users.GetUserByImei(model.Device.Imei);
                        Device device = Uow.Devices.Find(m => m.Imei == model.Device.Imei && m.Owner.ActiveStatus != ActiveStatus.Deleted).FirstOrDefault();

                        if (device != null) return ApiResponseMessage(false, null, ErrorCode.USER_IMEI_ALREADY_EXISTS);
                    }

                    if (exUser == null)
                    {
                        DbUser.Password = new PasswordHasher().HashPassword(model.Password);
                        DbUser.ActiveStatus = ActiveStatus.Active;
                        DbUser.Types = UserType.MobileUser;

                        var counter = CounterHelper.GetCounter(Pollyfy.Models.Enum.CounterIdentifier.DOCUMENT_SEQUENCE, "PF");

                        DbUser.MyReferanceNumber = string.Concat("PF", counter.ToString("D3"));

                        //todo: Geleninin de (ReferanceNumber) puanını artır.
                        //if (!string.IsNullOrEmpty(model.ReferanceNumber))
                        //{
                        //    DbUser.ExPoint += 5;
                        //}

                        DbUser.UserId = Uow.Users.DoInsert(DbUser);

                        if (DbUser.UserId > 0)
                        {
                            if (!string.IsNullOrEmpty(model.ReferanceNumber))
                            {
                                var user = Uow.Users.GetUserByReferanceNumber(model.ReferanceNumber);

                                user.ExPoint += 10;

                                Uow.Users.DoUpdate(user);
                            }

                            DbUser = SurveyManager.UpdateUserProperties(DbUser);

                            if (model.Device != null)
                            {
                                Device newDevice = Uow.Devices.Find(m => m.Imei == model.Imei).FirstOrDefault();

                                if (newDevice == null)
                                {
                                    newDevice = model.Device.ToDevice();
                                    newDevice.UserId = DbUser.UserId;
                                    newDevice.CreatedDate = DateTime.Now;
                                    newDevice.LastAccessDate = DateTime.Now;
                                    DbUser.LastActiveDeviceId = Uow.Devices.DoInsert(newDevice);

                                    if (DbUser.LastActiveDeviceId > 0)
                                        Uow.Users.DoUpdate(DbUser);
                                }
                                else
                                {
                                    newDevice.UserId = DbUser.UserId;
                                    newDevice.LastAccessDate = DateTime.Now;
                                    DbUser.LastActiveDeviceId = newDevice.DeviceId;
                                    Uow.Devices.DoUpdate(newDevice);
                                }
                            }

                            //Core.Common.SendEmail(DbUser, EmailType.NewUser);

                            UserModel response = new UserModel(DbUser);

                            response.UserId = DbUser.UserId.GetIndirectRef(typeof(User));

                            return ApiResponseMessage(true, response);
                        }
                        else
                        {
                            return ApiResponseMessage(false, null, ErrorCode.DATABASE_INSERT_FAILED);
                        }
                    }
                    else
                        return ApiResponseMessage(false, null, ErrorCode.USER_EMAIL_ALREADY_EXISTS);
                }
                else if (val.Errors.Count > 0)
                {
                    var errors = val.Errors.Select(m => new { m.ErrorMessage, m.PropertyName }).ToList();

                    return ApiResponseMessage(false, null, ErrorCode.ERROR_VALIDATION, errors);
                }
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage RegisterOrganization([FromBody] OrganizationModel organization)
        {
            if (organization != null)
            {
                UserModel model = new UserModel(organization);
                UserModelValidator validator = new UserModelValidator();
                var val = validator.Validate(model);
                if (val.IsValid)
                {
                    var DbUser = model.ToUser();
                    var exUser = Uow.Users.GetUserByUserName(DbUser.Email);
                    if (exUser == null && ((model.Device != null && !string.IsNullOrEmpty(model.Device.Imei)) || !string.IsNullOrEmpty(model.Imei)))
                    {
                        if (model.Device == null) model.Device = new DeviceModel();
                        if (string.IsNullOrEmpty(model.Device.Imei) && !string.IsNullOrEmpty(model.Imei))
                            model.Device.Imei = model.Imei;

                        //exUser = Uow.Users.GetUserByImei(model.Device.Imei);
                        Device device = Uow.Devices.Find(m => m.Imei == model.Device.Imei && m.Owner.ActiveStatus != ActiveStatus.Deleted).FirstOrDefault();
                        if (device != null) return ApiResponseMessage(false, null, ErrorCode.USER_IMEI_ALREADY_EXISTS);
                    }
                    if (exUser == null)
                    {
                        DbUser.Password = new PasswordHasher().HashPassword(model.Password);
                        DbUser.ActiveStatus = ActiveStatus.Active;
                        DbUser.Types = UserType.MobileUser;
                        DbUser.CompanyId = 16; // EP
                        DbUser.UserId = Uow.Users.DoInsert(DbUser);
                        if (DbUser.UserId > 0)
                        {
                            DbUser = SurveyManager.UpdateUserProperties(DbUser);
                            if (model.Device != null)
                            {
                                Device newDevice = Uow.Devices.Find(m => m.Imei == model.Imei).FirstOrDefault();
                                if (newDevice == null)
                                {
                                    newDevice = model.Device.ToDevice();
                                    newDevice.UserId = DbUser.UserId;
                                    newDevice.CreatedDate = DateTime.Now;
                                    newDevice.LastAccessDate = DateTime.Now;
                                    DbUser.LastActiveDeviceId = Uow.Devices.DoInsert(newDevice);
                                    if (DbUser.LastActiveDeviceId > 0)
                                        Uow.Users.DoUpdate(DbUser);
                                }
                                else
                                {
                                    newDevice.UserId = DbUser.UserId;
                                    newDevice.LastAccessDate = DateTime.Now;
                                    DbUser.LastActiveDeviceId = newDevice.DeviceId;
                                    Uow.Devices.DoUpdate(newDevice);
                                }
                            }
                            Core.Common.SendEmail(DbUser, EmailType.NewUser);
                            UserModel response = new UserModel(DbUser);
                            response.UserId = DbUser.UserId.GetIndirectRef(typeof(User));
                            return ApiResponseMessage(true, response);
                        }
                        else
                        {
                            return ApiResponseMessage(false, null, ErrorCode.DATABASE_INSERT_FAILED);
                        }
                    }
                    else
                        return ApiResponseMessage(false, null, ErrorCode.USER_EMAIL_ALREADY_EXISTS);
                }
                else if (val.Errors.Count > 0)
                {
                    var errors = val.Errors.Select(m => new { m.ErrorMessage, m.PropertyName }).ToList();
                    return ApiResponseMessage(false, null, ErrorCode.ERROR_VALIDATION, errors);
                }
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage RegisterDevice([FromBody] DeviceModel model)
        {
            int userId = model != null && !string.IsNullOrEmpty(model.UserId) ? model.UserId.GetDirectRefForInt(typeof(User)) : 0;
            User DbUser = null;
            if (userId <= 0 && model != null && !string.IsNullOrEmpty(model.Email))
            {
                DbUser = Uow.Users.GetUserByUserName(model.Email);
                if (DbUser != null) userId = DbUser.UserId;
            }
            if (model != null && (userId > 0) && !string.IsNullOrEmpty(model.Imei))
            {
                if (DbUser == null) DbUser = Uow.Users.GetByKey(userId);
                if (DbUser != null)
                {
                    Device device = Uow.Devices.Find(m => m.Imei == model.Imei && (m.Owner.ActiveStatus == ActiveStatus.Deleted || m.UserId == userId)).FirstOrDefault();
                    if (device == null)
                    {
                        device = model.ToDevice();
                        device.UserId = userId;
                        device.CreatedDate = DateTime.Now;
                        device.LastAccessDate = DateTime.Now;
                        device.DeviceId = Uow.Devices.DoInsert(device);
                    }
                    else
                    {
                        device.UserId = userId;
                        device.Imei = model.Imei;
                        device.Manufacturer = model.Manufacturer;
                        device.Model = model.Model;
                        device.Name = model.Name;
                        device.NotifyKey = model.NotifyKey;
                        device.Operator = model.Operator;
                        device.Platform = model.Platform;
                        device.Uuid = model.Uuid;
                        device.Version = model.Uuid;
                        device.LastAccessDate = DateTime.Now;
                        Uow.Devices.DoUpdate(device);
                    }
                    DbUser.LastActiveDeviceId = device.DeviceId;
                    if (DbUser.LastActiveDeviceId > 0) Uow.Users.DoUpdate(DbUser);

                    return ApiResponseMessage(true);
                }
                else
                    return ApiResponseMessage(false, null, ErrorCode.NOTFOUND);
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public async Task<HttpResponseMessage> UpdateUserPassword([FromBody] QueryModel query)
        {
            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;
            if (query != null && (!string.IsNullOrEmpty(query.Email) || userId > 0) &&
                !string.IsNullOrEmpty(query.CurrentPassword) && !string.IsNullOrEmpty(query.NewPassword))
            {
                User user = null;
                if (userId > 0)
                    user = Uow.Users.GetUserByUserId(userId);
                else
                    user = Uow.Users.GetUserByUserName(query.Email);
                if (user != null)
                {
                    LoginModel model = new LoginModel();
                    model.Username = user.Email;
                    model.Password = query.NewPassword;
                    LoginModelValidator validator = new LoginModelValidator();
                    var val = validator.Validate(model);
                    if (val.IsValid)
                    {
                        var result = await SignInManager.PasswordSignInAsync(model.Username, query.CurrentPassword, false, shouldLockout: false);
                        if (result == SignInStatus.Success)
                        {
                            user.Password = new PasswordHasher().HashPassword(query.NewPassword);
                            Uow.Users.DoUpdate(user);
                            return ApiResponseMessage(true);

                        }
                        else
                            return ApiResponseMessage(false, null, ErrorCode.ERROR_VALIDATION);
                    }
                    else if (val.Errors.Count > 0)
                    {
                        var errors = val.Errors.Select(m => new { m.ErrorMessage, m.PropertyName }).ToList();
                        return ApiResponseMessage(false, null, ErrorCode.ERROR_VALIDATION, errors);
                    }


                }
                else
                    return ApiResponseMessage(false, null, ErrorCode.NOTFOUND);
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage RecoveryPassword([FromBody] QueryModel query)
        {
            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;
            if (query != null && (!string.IsNullOrEmpty(query.Email) || userId > 0))
            {
                User user = null;
                if (userId > 0)
                    user = Uow.Users.GetUserByUserId(userId);
                else
                    user = Uow.Users.GetUserByUserName(query.Email);
                if (user != null)
                {
                    Random r = new Random();
                    string password = r.Next(10000000, 999999999).ToString();
                    string hashedPassword = new PasswordHasher().HashPassword(password);
                    user.Password = hashedPassword;
                    Uow.Users.DoUpdate(user);
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add("@password", password);
                    if (Core.Common.SendEmail(user, EmailType.PasswordRecovery, parameters))
                        return ApiResponseMessage(true);
                    else
                        return ApiResponseMessage(false, null, ErrorCode.EMAIL_SEND_FAILED);

                }
                else
                    return ApiResponseMessage(false, null, ErrorCode.NOTFOUND);
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage UserByEmail([FromBody] QueryModel query)
        {
            if (query != null && !string.IsNullOrEmpty(query.Email))
            {
                var user = Uow.Users.GetUserByUserName(query.Email);
                if (user != null)
                {
                    UserModel response = new UserModel(user);
                    response.UserId = user.UserId.GetIndirectRef(typeof(User));
                    var completedSurveys = SurveyManager.GetFinishedSurveysForRemoteUser(user.UserId).ToList();
                    if (query.AddCompletedSurveys.HasValue && query.AddCompletedSurveys.Value)
                        response.CompletedSurveys = completedSurveys;
                    response.CompletedCount = completedSurveys.Count;
                    response.ResponseCount = Uow.Users.GetTotalResponseCount(user.UserId);

                    return ApiResponseMessage(true, response);
                }
                else
                    return ApiResponseMessage(false, null, ErrorCode.NOTFOUND);

            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage UserById([FromBody] QueryModel query)
        {
            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;
            if (userId > 0)
            {
                var user = Uow.Users.GetUserByUserId(userId);

                if (user != null)
                {
                    UserModel response = new UserModel(user);

                    response.UserId = query.UserId;

                    var completedSurveys = SurveyManager.GetFinishedSurveysForRemoteUser(user.UserId).ToList();

                    if (query.AddCompletedSurveys.HasValue && query.AddCompletedSurveys.Value)
                        response.CompletedSurveys = completedSurveys;

                    response.CompletedCount = completedSurveys.Count;

                    response.ResponseCount = Uow.Users.GetTotalResponseCount(user.UserId);

                    return ApiResponseMessage(true, response);
                }
                else
                    return ApiResponseMessage(false, null, ErrorCode.NOTFOUND);

            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage UserProperties([FromBody] QueryModel query)
        {
            IEnumerable<SimpleCategory> result = new List<SimpleCategory>();
            var categories = Uow.HelperCategories.GetVisiblesByType(CategoryType.SurveyProperties);

            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;
            if (categories != null && categories.Count() > 0)
            {
                IEnumerable<UserPropertyModel> userProps = new List<UserPropertyModel>();
                if (userId > 0)
                    userProps = Uow.Users.GetProperties(userId);
                var catIds = userProps.Select(m => m.CategoryId).ToList();
                result = categories.Where(c => !catIds.Contains(c.CategoryId.ToString()))
                    .Select(m => new SimpleCategory()
                    {
                        CategoryId = m.CategoryId.GetIndirectRef(typeof(HelperCategory)),
                        Caption = m.Text,
                        IsMultiple = m.IsMultiple,
                        Items = m.Values.Select(v => new SimpleItem()
                        {
                            ValueId = v.ValueId.GetIndirectRef(typeof(HelperValue)),
                            Text = v.Text
                        })
                    });
            }
            return ApiResponseMessage(true, result);
        }

        [HttpPost]
        public HttpResponseMessage UserPropertiesByUserId([FromBody] QueryModel query)
        {
            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;
            if (userId > 0)
            {
                var properties = Uow.Users.GetProperties(userId);
                foreach (UserPropertyModel p in properties)
                {
                    p.UserId = query.UserId;
                    p.ValueId = p.ValueId.GetIndirectRef(typeof(HelperValue));
                    p.CategoryId = p.CategoryId.GetIndirectRef(typeof(HelperCategory));
                }
                return ApiResponseMessage(true, properties);
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage AddUserProperties([FromBody] QueryModel query)
        {
            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;

            if (userId > 0 && !string.IsNullOrEmpty(query.ValueId))
            {
                foreach (var valueId in query.ValueId.Split(','))
                {
                    var currentValue = !string.IsNullOrEmpty(valueId) ? valueId.GetDirectRefForInt(typeof(HelperValue)) : 0;

                    if (currentValue > 0)
                    {
                        Uow.Users.AddUserProperty(userId, currentValue);
                    }
                }
                return ApiResponseMessage(true);
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage AddUserProperty([FromBody] QueryModel query)
        {
            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;

            int valueId = query != null && !string.IsNullOrEmpty(query.ValueId) ? query.ValueId.GetDirectRefForInt(typeof(HelperValue)) : 0;

            if (userId > 0 && valueId > 0)
            {
                if (!string.IsNullOrEmpty(query.ValueId))
                    foreach (var valueID in query.ValueId.Split(','))
                    {
                        Uow.Users.AddUserProperty(userId, valueID.GetDirectRefForInt(typeof(HelperValue)));
                    }
                return ApiResponseMessage(true);
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage GetSurveys([FromBody] QueryModel query)
        {
            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;
            if (userId > 0)
            {
                var sectors = Core.Common.GetCategorySelectList("cat_sector");
                var companies = Core.Common.GetCompanySelectList();
                var surveys = SurveyManager.GetSurveysForRemoteUser(userId, query.Latitude, query.Longitude);
                IEnumerable<object> response = new List<SurveyGroup>();
                if (surveys != null && surveys.Count() > 0)
                {
                    var generalSurveys = surveys.Where(p => p.SurveyType == SurveyType.Generic);

                    var user = Core.Common.Uow.Users.GetById(userId);

                    if (string.IsNullOrEmpty(user.CustomProperty1))
                    {
                        user.CustomProperty1 = DateTime.Now.ToString();

                        Core.Common.Uow.Users.DoUpdate(user);
                    }

                    var date = DateTime.Now.Subtract(Convert.ToDateTime(user.CustomProperty1)).TotalDays;

                    surveys = surveys.Where(p => p.SurveyType != SurveyType.Generic);

                    if (date > 1 && generalSurveys.Count() > 0)
                    {
                        try
                        {
                            var random = date % 10;

                            var generalSurvey = generalSurveys.ToList().ElementAt((int)random);

                            surveys = surveys.Concat(generalSurvey.ToArray());

                            //user.CustomProperty1 = DateTime.Now.ToString();

                            //Core.Common.Uow.Users.DoUpdate(user);
                        }
                        catch (Exception e)
                        {
                            var generalSurvey = generalSurveys.ToList().ElementAt(0);

                            surveys = surveys.Concat(generalSurvey.ToArray());
                        }
                    }

                    if (string.IsNullOrEmpty(query.GroupBy))
                    {
                        response = surveys.OrderByDescending(p => p.CreatedDate).Select(m => new SurveyModel()
                        {
                            SurveyId = m.SurveyId.GetIndirectRef(typeof(Survey)),
                            Description = m.Description ?? "",
                            Name = m.Name,
                            QuestionCount = m.QuestionCount,
                            SpecialNumber = m.SpecialNumber,
                         //   Reward = m.Reward.ToString(),
                            ExPoint = m.ExPoint.ToString(),
                            Scopes = m.Scopes.Count > 0 ? string.Join(",", m.Scopes.Select(s => s.Text)) : "Tüm Türkiye",
                          //  Sector = sectors.Any(s => s.Value == m.SectorId.ToString()) ? sectors.First(s => s.Value == m.SectorId.ToString()).Text : "",
                            SurveyType = (int)m.SurveyType,
                            SurveyTypeText = Core.Common.GetSurveyTypeText(m.SurveyType),
                            UsageType = (int)m.UsageType,
                            UsageTypeText = WebUtils.GetUsageTypeText(m.UsageType),
                            InCompanyUsage = m.InCompanyUsage,
                            CompanyName = companies.Any(c => c.Value == m.CompanyId.ToString()) ? companies.First(c => c.Value == m.CompanyId.ToString()).Text : "",
                            ImageUrl = m.ImageUrl,
                            FullingTime = m.FullingTime,
                            SurveyUrl = m.SurveyUrl,
                            StartDate = m.StartDate,
                            FinishDate = m.FinishDate,
                            FinalMessage = m.FinalMessage ?? "",
                            PrivacyQuestion = m.PrivacyQuestion ?? "",
                            Promotion = m.Promotions.Select(p => new PromotionModel()
                            {
                                Code = "*******",
                                CompanyName = p.CompanyName,
                                ContactUrl = p.ContactUrl ?? "",
                                Description = p.Description,
                                Discount = p.Discount ?? "",
                                FinishDate = p.FinishDate,
                                ImageUrl = p.ImageUrl ?? "",
                                StartDate = p.StartDate,
                                Title = p.Title
                            }).FirstOrDefault()
                        });
                    }
                    else if (query.GroupBy == "usagetype")
                    {
                        response = surveys.OrderByDescending(p => p.CreatedDate).GroupBy(k => k.UsageType).Select(v => new SurveyGroup()
                        {
                            GroupId = (int)v.Key,
                            GroupName = WebUtils.GetUsageTypeText(v.Key),
                            ColorHex = WebUtils.GetUsageTypeColor(v.Key),
                            Surveys = v.Select(m => new SurveyModel()
                            {
                                SurveyId = m.SurveyId.GetIndirectRef(typeof(Survey)),
                                Description = m.Description,
                                Name = m.Name,
                                QuestionCount = m.QuestionCount,
                              //  Reward = m.Reward.ToString(),
                                ExPoint = m.ExPoint.ToString(),
                                Scopes = m.Scopes.Count > 0 ? string.Join(",", m.Scopes.Select(s => s.Text)) : "Tüm Türkiye",
                             //   Sector = sectors.Any(s => s.Value == m.SectorId.ToString()) ? sectors.First(s => s.Value == m.SectorId.ToString()).Text : "",
                                SurveyType = (int)m.SurveyType,
                                SurveyTypeText = Core.Common.GetSurveyTypeText(m.SurveyType),
                                UsageType = (int)m.UsageType,
                                UsageTypeText = WebUtils.GetUsageTypeText(m.UsageType),
                                InCompanyUsage = m.InCompanyUsage,
                                CompanyName = companies.Any(c => c.Value == m.CompanyId.ToString()) ? companies.First(c => c.Value == m.CompanyId.ToString()).Text : "",
                                ImageUrl = m.ImageUrl,
                                FullingTime = m.FullingTime,
                                SurveyUrl = m.SurveyUrl,
                                StartDate = m.StartDate,
                                FinishDate = m.FinishDate,
                                FinalMessage = m.FinalMessage,
                                PrivacyQuestion = m.PrivacyQuestion ?? "",
                                Promotion = m.Promotions.Select(p => new PromotionModel()
                                {
                                    Code = "*******",
                                    CompanyName = p.CompanyName,
                                    ContactUrl = p.ContactUrl ?? "",
                                    Description = p.Description,
                                    Discount = p.Discount ?? "",
                                    FinishDate = p.FinishDate,
                                    ImageUrl = p.ImageUrl ?? "",
                                    StartDate = p.StartDate,
                                    Title = p.Title
                                }).FirstOrDefault()
                            })
                        });
                    }
                }
                return ApiResponseMessage(true, response);
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage GetCompletedSurveys([FromBody] QueryModel query)
        {
            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;
            IEnumerable<object> response = new List<SurveyGroup>();
            if (userId > 0)
            {
                var surveys = SurveyManager.GetFinishedSurveysForRemoteUser(userId);
                if (string.IsNullOrEmpty(query.GroupBy))
                {
                    response = surveys;
                }
                else if (query.GroupBy == "usagetype")
                {
                    response = surveys.GroupBy(k => k.UsageType).Select(v => new SurveyGroup()
                    {
                        GroupId = v.Key,
                        GroupName = WebUtils.GetUsageTypeText((UsageType)v.Key),
                        ColorHex = WebUtils.GetUsageTypeColor((UsageType)v.Key),
                        Surveys = v.ToList()
                    });
                }
                return ApiResponseMessage(true, response);
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage GetLocation([FromBody] QueryModel query)
        {
            if (query != null && !string.IsNullOrEmpty(query.Latitude) && !string.IsNullOrEmpty(query.Longitude))
            {
                HelperValue location = SurveyManager.GetScopeFromCoords(query.Latitude, query.Longitude);
                if (location != null)
                    return ApiResponseMessage(true, new
                    {
                        ValueId = location.ValueId.GetIndirectRef(typeof(HelperValue)),
                        CategoryId = location.CategoryId.GetIndirectRef(typeof(HelperCategory)),
                        Text = location.Text
                    });
                else
                    return ApiResponseMessage(false, null, ErrorCode.NOTFOUND);
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage GetSurveyQuestions([FromBody] QueryModel query)
        {
            int surveyId = query != null && !string.IsNullOrEmpty(query.SurveyId) ? query.SurveyId.GetDirectRefForInt(typeof(Survey)) : 0;
            if (surveyId > 0)
            {
                IEnumerable<QuestionModel> results = new List<QuestionModel>();

                var questions = Uow.Questions.GetWithChoicesBySurveyId(surveyId);

                if (questions != null && questions.Count() > 0)
                    results = questions.Select(q => new QuestionModel()
                    {
                        QuestionId = q.QuestionId.GetIndirectRef(typeof(Question)),
                        SurveyId = q.SurveyId.GetIndirectRef(typeof(Survey)),
                        QuestionText = q.QuestionText,
                        Description = q.Description,
                        IsImage = q.IsImage,
                        DescribeIsImage = q.DescribeIsImage,
                        IsChoiceImage = q.IsChoiceImage,
                        QuestionType = (int)q.QuestionType,
                        IsRequired = q.IsRequired,
                        ImageUrl = "",
                        Choices = q.Choices.Select(c => new ChoiceModel()
                        {
                            ChoiceId = c.ChoiceId.GetIndirectRef(typeof(Choice)),
                            Text = c.Text,
                            ChoiceDescribe = c.ChoiceDescribe,
                            PassQuestion = c.PassQuestion ?? 0
                        })
                    });
                return ApiResponseMessage(true, results);
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage GetQuestionChoices([FromBody] QueryModel query)
        {
            int questionId = query != null && !string.IsNullOrEmpty(query.QuestionId) ? query.QuestionId.GetDirectRefForInt(typeof(Question)) : 0;
            if (questionId > 0)
            {
                QuestionModel result = null;
                Question question = Uow.Questions.GetWithChoicesByQuestionId(questionId);
                if (question != null)
                {
                    result = new QuestionModel()
                    {
                        QuestionId = question.QuestionId.GetIndirectRef(typeof(Question)),
                        SurveyId = question.SurveyId.GetIndirectRef(typeof(Survey)),
                        QuestionText = question.QuestionText,
                        Description = question.Description,
                        QuestionType = (int)question.QuestionType,
                        IsRequired = question.IsRequired,
                        ImageUrl = "",
                        IsImage = question.IsImage,
                        DescribeIsImage = question.DescribeIsImage,
                        Choices = question.Choices.Select(c => new ChoiceModel()
                        {
                            ChoiceId = c.ChoiceId.GetIndirectRef(typeof(Choice)),
                            Text = c.Text,
                            ChoiceDescribe = c.ChoiceDescribe,
                            PassQuestion = c.PassQuestion ?? 0
                        })
                    };
                    return ApiResponseMessage(true, result);
                }
                else
                    return ApiResponseMessage(false, null, ErrorCode.NOTFOUND);

            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage SaveAnswer([FromBody] QueryModel query)
        {
            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;
            int surveyId = query != null && !string.IsNullOrEmpty(query.SurveyId) ? query.SurveyId.GetDirectRefForInt(typeof(Survey)) : 0;
            int questionId = query != null && !string.IsNullOrEmpty(query.QuestionId) ? query.QuestionId.GetDirectRefForInt(typeof(Question)) : 0;
            if (query != null && userId > 0 && surveyId > 0 && questionId > 0)
            {
                int choiceId = 0;
                List<int> choiceIds = new List<int>();
                if (!string.IsNullOrEmpty(query.ChoiceId))
                    foreach (string id in query.ChoiceId.Split(','))
                    {
                        choiceId = id.GetDirectRefForInt(typeof(Choice));
                        if (choiceId > 0)
                            choiceIds.Add(choiceId);
                    }

                Uow.Surveys.SaveAnswer(surveyId, userId, questionId, string.Join(",", choiceIds), query.AnswerText);
                return ApiResponseMessage(true);

            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage FinishSurvey([FromBody] QueryModel query)
        {
            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;

            int surveyId = query != null && !string.IsNullOrEmpty(query.SurveyId) ? query.SurveyId.GetDirectRefForInt(typeof(Survey)) : 0;

            bool allowedInfoShare = query != null && !string.IsNullOrEmpty(query.AllowedInfoShare) && query.AllowedInfoShare.ToLower().Trim() == "true" ? true : false;

            if (query != null && userId > 0 && surveyId > 0)
            {
                var survey = Uow.Surveys.GetById(surveyId);

                var weeklyLeader = Uow.WeeklyLeaders.GetByUserId(userId);

                if (weeklyLeader == null)
                {
                    Uow.WeeklyLeaders.DoInsert(new WeeklyLeader
                    {
                        ExPoint = (int)survey.ExPoint,
                        UserId = userId
                    });
                }
                else
                {
                    weeklyLeader.ExPoint += (int)survey.ExPoint;

                    Uow.WeeklyLeaders.DoUpdate(weeklyLeader);
                }

                int enrollId = Uow.Surveys.FinishSurvey(surveyId, userId, allowedInfoShare);

                if (enrollId > 0)
                {
                    var logger = LogManager.GetLogger("SystemLog");

                    logger.Info("Yeni bir enrollmenta anket eklendi : {0}", surveyId);

                    Uow.TemporaryExpoints.DoInsert(new TemporaryExpoint
                    {
                        UserId = userId,
                        Expoint = (int)survey.ExPoint
                    });

                    var enrollmentCount = Uow.Enrollments.GetBySurveyId(surveyId).Count();

                    if (enrollmentCount <= 50)
                    {
                        var user = Uow.Users.GetById(userId);

                        user.ExPoint += (int)(survey.ExPoint * 0.2);

                        Uow.Users.DoUpdate(user);
                    }
                    //else if (enrollmentCount > 100)
                    //{
                    //    var user = Uow.Users.GetById(userId);

                    //    user.ExPoint -= (int)(survey.ExPoint * 0.2);

                    //    Uow.Users.DoUpdate(user);
                    //}


                    List<QuestionGroup> groups = Uow.QuestionGroups.GetUserGroups(surveyId, userId, enrollId).ToList();

                    if (groups != null)
                    {
                        var allowedGroups = groups.Where(m => m.Route == 1 && !groups.Any(e => e.GroupId == m.GroupId && e.Route == -1));

                        foreach (var allowedGroup in allowedGroups)
                        {
                            Uow.Groups.DoInsert(new Group
                            {
                                UserId = userId,
                                SurveyId = allowedGroup.SurveyId,
                                GroupId = allowedGroup.GroupId
                            });
                        }

                        return ApiResponseMessage(true, allowedGroups.Select(m => new { m.Name, m.Description, m.Score }));
                    }
                }

                return ApiResponseMessage(true);
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage ReportProblem([FromBody] QueryModel query)
        {
            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;
            int surveyId = query != null && !string.IsNullOrEmpty(query.SurveyId) ? query.SurveyId.GetDirectRefForInt(typeof(Survey)) : 0;
            int questionId = query != null && !string.IsNullOrEmpty(query.QuestionId) ? query.QuestionId.GetDirectRefForInt(typeof(Question)) : 0;
            if (query != null && userId > 0 && !string.IsNullOrEmpty(query.Message))
            {
                Helpdesk ticket = new Helpdesk();
                ticket.UserId = userId;
                ticket.Message = query.Message;
                ticket.SurveyId = surveyId > 0 ? surveyId : ticket.SurveyId;
                ticket.QuestionId = questionId > 0 ? questionId : ticket.QuestionId;
                Uow.Helpdesks.DoInsert(ticket);
                return ApiResponseMessage(true);
            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        public async Task<HttpResponseMessage> FacebookLogin([FromBody] UserModel model)
        {
            if (model != null && (!string.IsNullOrEmpty(model.FacebookToken) || !string.IsNullOrEmpty(model.AccessToken)))
            {
                FacebookClient fbClient = new FacebookClient();

                IDictionary<string, string> userDatas = null;

                if (!string.IsNullOrEmpty(model.FacebookToken))
                    userDatas = fbClient.GetUserData(model.FacebookToken, "http://app.pollyfy.com/Account/Login");
                else if (!string.IsNullOrEmpty(model.AccessToken))
                    userDatas = fbClient.GetUserData(model.AccessToken);

                if (userDatas == null || userDatas.Count == 0 || !userDatas.ContainsKey("id"))
                    return ApiResponseMessage(false, null, ErrorCode.FACEBOOK_AUTH_FAILED);

                User user = Uow.Users.GetUserByFacebookId(userDatas["id"]);

                if (user == null)
                {
                    //UserModel newUser = new UserModel();
                    model.UserName = userDatas["id"];
                    model.FacebookId = userDatas["id"];
                    model.Email = userDatas.ContainsKey("email") ? userDatas["email"] : "";
                    model.Password = "p_" + userDatas["id"];
                    model.FirstName = userDatas.ContainsKey("name") ? userDatas["name"].Split(' ').First() : "Anonim Facebook";
                    model.LastName = userDatas.ContainsKey("name") ? userDatas["name"].Split(' ').Last() : "Kullanıcısı";
                    model.Gender = userDatas.ContainsKey("gender") ? userDatas["gender"] == "male" ? "Erkek" : "Kadın" : model.Gender;
                    //DateTime birthDate;
                    //if (userDatas.ContainsKey("user_birthday") && DateTime.TryParse(userDatas["user_birthday"], out birthDate))
                    //    model.BirthDate = birthDate;
                    var register_response = RegisterUser(model);
                    user = Uow.Users.GetUserByFacebookId(userDatas["id"]);
                    if (user == null)
                    {
                        List<object> errors = new List<object>();
                        errors.Add(new { PropertyName = "Email", errorMessage = "E-Posta adresiniz Facebook üzerinden sağlanamadı. Üye olmak için lütfen kayıt formunu kullanın." });
                        return ApiResponseMessage(false, null, ErrorCode.ERROR_VALIDATION, errors);
                        //return register_response;
                    }
                }

                LoginModel login = new LoginModel();
                login.IsExternalLogin = true;
                login.Username = userDatas["id"];
                login.Password = "p_" + userDatas["id"];
                return await LoginUser(login);

            }
            return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);
        }

        [HttpPost]
        public HttpResponseMessage GetLeaders([FromBody]QueryModel query)
        {
            var leaders = new List<LeaderModel>();

            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;

            var users = Uow.Users.GetAll();

            var surveyLeaders = users.OrderByDescending(p => p.ExPoint).ToList();

            var lastUser = surveyLeaders.FirstOrDefault(p => p.UserId == userId);

            var sequenceNo = 1;

            foreach (var user in surveyLeaders.Take(20))
            {
                leaders.Add(new LeaderModel
                {
                    UserFullName = user.GetFullName(),
                    UserId = user.UserId.GetIndirectRef(typeof(User)),
                    Point = user.ExPoint,
                    Type = 0,
                    SequenceNo = sequenceNo,
                });

                ++sequenceNo;
            }

            var lastLeader = leaders.FindIndex(p => p.UserId == query.UserId);

            if (lastLeader == -1)
                leaders.Add(new LeaderModel
                {
                    UserFullName = lastUser.GetFullName(),
                    UserId = lastUser.UserId.GetIndirectRef(typeof(User)),
                    Point = lastUser.ExPoint,
                    Type = 0,
                    SequenceNo = surveyLeaders.FindIndex(p => p.UserId == userId)
                });

            return ApiResponseMessage(true, leaders);
        }

        [HttpPost]
        public HttpResponseMessage GetWeeklyLeaders([FromBody]QueryModel query)
        {
            if (string.IsNullOrEmpty(query?.UserId))
                return ApiResponseMessage(false, ErrorCode.NOTFOUND);

            int userId = query.UserId.GetDirectRefForInt(typeof(User));

            var currentUser = Uow.Users.GetUserByUserId(userId);

            var leaders = new List<LeaderModel>();

            var weeklyLeaders = Uow.WeeklyLeaders.GetAll().ToList();

            var sequenceNo = 1;

            foreach (var weeklyLeader in weeklyLeaders.Take(20))
            {
                var user = Uow.Users.GetById(weeklyLeader.UserId);

                leaders.Add(new LeaderModel
                {
                    UserFullName = user.GetFullName(),
                    Point = weeklyLeader.ExPoint,
                    Type = 1,
                    UserId = user.UserId.GetIndirectRef(typeof(User)),
                    SequenceNo = sequenceNo
                });

                ++sequenceNo;
            }

            var lastLeader = leaders.FindIndex(p => p.UserId == query.UserId);
            var userInWeeklyLeaders = weeklyLeaders.FindIndex(p => p.UserId == userId);

            if (userInWeeklyLeaders == -1)
            {
                leaders.Add(new LeaderModel
                {
                    UserFullName = currentUser.GetFullName(),
                    UserId = query.UserId,
                    Point = 0,
                    Type = 1,
                    SequenceNo = 0
                });
            }
            else if (lastLeader == -1 && userInWeeklyLeaders != -1)
            {
                leaders.Add(new LeaderModel
                {
                    UserFullName = currentUser.GetFullName(),
                    UserId = query.UserId,
                    Point = weeklyLeaders.FirstOrDefault(p => p.UserId == userId).ExPoint,
                    Type = 1,
                    SequenceNo = weeklyLeaders.FindIndex(p => p.UserId == userId)
                });
            }

            return ApiResponseMessage(true, leaders);
        }

        [HttpPost]
        public HttpResponseMessage GetCampaigns(string userId)
        {
            var campaigns = Uow.Campaigns.GetAll().Where(p => p.ActiveStatus == ActiveStatus.Active).ToList();

            int userID = userId != null && !string.IsNullOrEmpty(userId) ? userId.GetDirectRefForInt(typeof(User)) : 0;

            var campaignIds = Uow.EarnCampaigns.GetAll().Where(p => p.UserId == userID).Select(p => p.CampaignId).ToArray();

            var campaignList = campaigns.Where(p => campaignIds.Contains(p.Id)).ToList();

            foreach (var campaign in campaignList)
            {
                if (campaigns.IndexOf(campaign) != -1)
                    campaigns[campaigns.IndexOf(campaign)].ActiveStatus = ActiveStatus.Passive;
            }

            return ApiResponseMessage(true, campaigns);
        }

        [HttpPost]
        public HttpResponseMessage AddEarnCampaign([FromBody]QueryModel query)
        {
            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;

            int campaignId = query != null ? Convert.ToInt32(query.CampaignId) : 0;

            if (query != null && userId > 0 && campaignId > 0)
            {
                var user = Uow.Users.GetById(userId);
                var campaign = Uow.Campaigns.GetById(campaignId);

                if (campaign != null && campaign.ActiveStatus == ActiveStatus.Active && user.ExPoint >= campaign.Point)
                {
                    Uow.EarnCampaigns.DoInsert(new EarnCampaign
                    {
                        CampaignId = campaignId,
                        UserId = userId,
                        Note = query.Note
                    });

                    user.ExPoint -= campaign.Point;
                    campaign.Count = campaign.Count - 1;

                    if (campaign.Count == 0)
                    {
                        campaign.ActiveStatus = ActiveStatus.Passive;
                    }

                    Uow.Users.DoUpdate(user);
                    Uow.Campaigns.DoUpdate(campaign);
                }
                else
                    return ApiResponseMessage(false, null, ErrorCode.UNDER_EXPOINT_LIMIT);
            }
            else
                return ApiResponseMessage(false, null, ErrorCode.INVALID_PARAMETER);

            return ApiResponseMessage(true);
        }

        [HttpPost]
        public HttpResponseMessage SharePointUpdate([FromBody] QueryModel query)
        {
            int userId = query != null && !string.IsNullOrEmpty(query.UserId) ? query.UserId.GetDirectRefForInt(typeof(User)) : 0;

            if (query != null && userId > 0)
            {
                var user = Uow.Users.GetById(userId);

                user.ExPoint += 10;

                Uow.Users.DoUpdate(user);

                return ApiResponseMessage(true);
            }

            return ApiResponseMessage(false);
        }
        #endregion
    }
}