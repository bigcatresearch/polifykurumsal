﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;
using System.Web.UI;
using System.IO;
using Pollyfy.Core.Extensions;
using Pollyfy.Web.Core;
using Pollyfy.Models.ViewModel;

namespace Pollyfy.Web.Controllers
{
    [Authorize]
    public class CompanyController : BaseController
    {
        public CompanyController(IUow uow) : base(uow) { }

        // GET: Company
        public ActionResult Index()
        {
            RedirectForUnauthorized();
            return View();
        }
        [ValidateInput(false)]
        public ActionResult GetCompanyList()
        {
            RedirectForUnauthorized();
            var model = Uow.Companies.GetAll();
            return PartialView("_CompanyList", model);
        }

        [HttpGet]
        public ActionResult New()
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = true;
            return View();
        }
        [HttpPost]
        public ActionResult New(Company model)
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = ModelState.IsValid;
            if (ModelState.IsValid)
            {
                int companyId = Uow.Companies.DoInsert(model);
                if (companyId > 0)
                {
                    ViewBag.InsertIsSuccess = true;
                    SetNotify("Ekleme işlemi tamamlandı.", "Şirket");
                    return RedirectToAction("Edit", "Company", new { id = companyId.GetIndirectRef(typeof(Company)) });
                }
                else
                    ViewBag.InsertIsSuccess = false;
            }
            return View();
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = true;
            int companyId = id.GetDirectRefForInt(typeof(Company));
            Company model = null;
            if (companyId > 0)
            {
                model = Uow.Companies.GetByKey(companyId);
                if (model == null)
                {
                    SetNotify("Düzenlenemez!", "Şirket", NotifyMessageType.Warning);
                    return RedirectToAction("Index", "Company");
                }
            }
            else
            {
                SetNotify("Hatalı parametre!", "Şirket", NotifyMessageType.Warning);
                if (model == null) return RedirectToAction("Index", "Company");
            }
            model.FileContainer = new FileContainer()
            {
                CompanyId = model.CompanyId,
                FileName = model.TrackingGuid.ToString() + ".png",
                OwnerId = model.CompanyId,
                OwnerType = FileOwnerType.Company,
                FileType = FileType.Image
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Company model)
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = ModelState.IsValid;
            int companyId = Request.Form["CompanyIdRef"] != null ? Request.Form["CompanyIdRef"].ToString().GetDirectRefForInt(typeof(Company)) : 0;
            if (ModelState.IsValid && companyId > 0)
            {
                model.CompanyId = companyId;
                Uow.Companies.DoUpdate(model);

                ViewBag.UpdateIsSuccess = true;
                SetNotify("Güncelleme işlemi tamamlandı.", "Şirket");
                return RedirectToAction("Index", "Company");
            }
            else
            {
                ViewBag.UpdateIsSuccess = false;
                SetNotify("Güncelleme işlemi başarısız.", "Şirket", NotifyMessageType.Warning);
            }
            model.FileContainer = new FileContainer();
            return View(model);
        }

        [HttpPost]
        public bool Delete(int id)
        {
            RedirectForUnauthorized();

            if (id > 0 && Uow.Companies.UpdateStatus(id, ActiveStatus.Deleted))
                SetNotify("Silme işlemi tamamlandı.", "Firma");
            else
            {
                SetNotify("Silinirken sorun oluştu.", "Firma", NotifyMessageType.Warning);
                return false;
            }

            return true;
        }
    }
}