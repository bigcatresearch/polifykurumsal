﻿using System.Web.Mvc;
using Newtonsoft.Json;
using Pollyfy.Core.Extensions;
using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using Pollyfy.Models.DataModel;

namespace Pollyfy.Web.Controllers
{
    [Authorize]
    public class CampaignController : BaseController
    {
        public CampaignController(IUow uow) : base(uow)
        {
        }

        public ActionResult Index()
        {
            RedirectForUnauthorized();

            return View();
        }

        [HttpGet]
        public ActionResult New()
        {
            RedirectForUnauthorized();

            ViewBag.IsValid = ModelState.IsValid;

            return View();
        }

        [ValidateInput(false)]
        public ActionResult GetCampaignList()
        {
            RedirectForUnauthorized();

            var model = Uow.Campaigns.GetAll();

            return PartialView("_CampaignList", model);
        }

        [HttpPost]
        public ActionResult New(Campaign campaign)
        {
            RedirectForUnauthorized();

            ViewBag.IsValid = ModelState.IsValid;

            if (ModelState.IsValid)
            {
                var model = Uow.Campaigns.DoInsert(campaign);

                if (model > 0)
                {
                    ViewBag.InsertIsSuccess = true;
                    SetNotify("Ekleme işlemi tamamlandı.", "Kampanya");
                    return RedirectToAction("Edit", "Campaign", new { id = model.GetIndirectRef(typeof(Campaign)) });
                }
                else
                    ViewBag.InsertIsSuccess = false;
            }

            return View();
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            RedirectForUnauthorized();

            ViewBag.IsValid = ModelState.IsValid;

            var campaignId = id.GetDirectRefForInt(typeof(Campaign));

            Campaign campaign = null;

            if (campaignId > 0)
            {
                campaign = Uow.Campaigns.GetByKey(campaignId);

                if (campaign == null)
                {
                    SetNotify("Düzenlenemez!", "Kampanya", NotifyMessageType.Warning);
                    return RedirectToAction("Index", "Campaign");
                }
            }
            else
            {
                SetNotify("Hatalı parametre!", "Kullanıcı Rol", NotifyMessageType.Warning);
                if (campaign == null) return RedirectToAction("Index", "Campaign");
            }

            return View(campaign);
        }

        [HttpPost]
        public ActionResult Edit(Campaign model)
        {
            RedirectForUnauthorized();

            ViewBag.IsValid = ModelState.IsValid;

            if (ModelState.IsValid && model.Id > 0)
            {
                Uow.Campaigns.DoUpdate(model);

                ViewBag.UpdateIsSuccess = true;
                SetNotify("Güncelleme işlemi tamamlandı.", "Kampanya");

                return RedirectToAction("Index", "Campaign");
            }
            else
            {
                ViewBag.UpdateIsSuccess = false;
                SetNotify("Güncelleme işlemi başarısız.", "Kampanya", NotifyMessageType.Warning);
            }
            return View(model);
        }

        [HttpGet]
        public string GetList()
        {
            RedirectForUnauthorized();

            var model = Uow.Campaigns.GetAll();

            return JsonConvert.SerializeObject(model);
        }

        [HttpPost]
        public bool Delete(int id)
        {
            RedirectForUnauthorized();

            if (id > 0 && Uow.Campaigns.UpdateStatus(id, ActiveStatus.Deleted))
                SetNotify("Silme işlemi tamamlandı.", "Kampanya");
            else
            {
                SetNotify("Silinirken sorun oluştu.", "Kampanya", NotifyMessageType.Warning);
                return false;
            }

            return true;
        }
    }
}