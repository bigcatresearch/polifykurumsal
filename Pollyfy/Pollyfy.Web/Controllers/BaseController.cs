﻿using Pollyfy.Web.Core;
using Pollyfy.Data.Interfaces;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net;

namespace Pollyfy.Web.Controllers
{
    public class BaseController : Controller
    {
        protected IUow Uow;
        public BaseController(IUow uow)
        {
            this.Uow = uow;
            if (Request != null)
                Core.Common.AbsoluteUri = Request.Url.AbsoluteUri;
        }


        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        protected void RedirectForUnauthorized()
        {
            if (Request.IsAuthenticated)
                RedirectToAction("Index", "Home");
        }
        protected void SetNotify(string message, string title = "", NotifyMessageType messageType = NotifyMessageType.Success)
        {
            List<string> notifications = TempData["Notifications"] != null ? (List<string>)TempData["Notifications"] ?? new List<string>() : new List<string>();
            notifications.Add("common.Notify('" + title + "','" + message + "','" + messageType.ToString().ToLowerInvariant() + "');");
            TempData["Notifications"] = notifications;
        }
        protected AppUser CurrentUser
        {
            get
            {
                var user = new AppUser(User as ClaimsPrincipal);
                return user;
            }
        }
        protected IAuthenticationManager AuthManager
        {
            get
            {

                var ctx = Request.GetOwinContext();
                return ctx.Authentication;
            }
        }

    }
    public enum NotifyMessageType
    {
        Success,
        Info,
        Warning,
        Error
    }
}
