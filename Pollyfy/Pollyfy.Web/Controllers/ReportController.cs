﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using Pollyfy.Web.Core;
using Pollyfy.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pollyfy.Models.ViewModel;
using Pollyfy.Web.Providers;
using System.IO;
using System.Data;
using Pollyfy.Data.Migrations;

namespace Pollyfy.Web.Controllers
{
    [Authorize]
    public class ReportController : BaseController
    {
        public ReportController(IUow uow) : base(uow) { }

        public ActionResult Index()
        {
            RedirectForUnauthorized();
            IEnumerable<Survey> surveys = new List<Survey>();
            if (CurrentUser.Types == UserType.SystemAdmin)
                surveys = Uow.Surveys.GetForAll();
            else if (CurrentUser.Types == UserType.CompanyAdmin && CurrentUser.CompanyId.HasValue)
                surveys = Uow.Surveys.GetByCompanyId(CurrentUser.CompanyId.Value);
            else if (CurrentUser.Types == UserType.CompanyUser)
                surveys = Uow.Surveys.GetByUserId(CurrentUser.UserId);
            return View(surveys);
        }
        public ActionResult View(string id)
        {
            RedirectForUnauthorized();
            int surveyId = id.GetDirectRefForInt(typeof(Survey));
            SurveyReportModel model = null;
            if (surveyId > 0)
            {
                model = Uow.Surveys.GetSummaryReport(surveyId);
                if (model != null)
                {
                    foreach (ChartDataModel item in model.ParticipationChartSeries)
                        item.UnixDate = (long)(item.Date.Date.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                    model.SerieStartDate = DateTime.Now.AddMonths(-1).Date;
                    model.SerieEndDate = DateTime.Now.Date;
                    model.SerieStartUnixDate = (long)(model.SerieStartDate.Value.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                    model.SerieEndUnixDate = (long)(model.SerieEndDate.Value.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                    model.ParticipatCoords = Uow.Enrollments.GetParticipantCoords(surveyId).ToList();
                    return View(model);
                }
            }

            SetNotify("Verileri bulunamadı.", "Rapor", NotifyMessageType.Warning);
            return RedirectToAction("Index", "Report");
        }

        // http://localhost:9658/Report/QuestionReport/2CAA599E22E369B5568FEC244145663B
        // Tamamlanmamış yada yarım kalmış anketlerin sonuçlarının silinmesi
        public ActionResult QuestionReport(string id)
        {
            RedirectForUnauthorized();
            int surveyId = id.GetDirectRefForInt(typeof(Survey));
            Survey model = null;
            if (surveyId > 0)
            {
                model = Uow.Surveys.GetSurveyIncludes(surveyId);
                if (model != null)
                {
                    model.Questions = Uow.Questions.GetWithChoicesBySurveyId(surveyId).ToList();
                    var enrollments = Uow.Enrollments.GetBySurveyId(surveyId).Where(m => m.Status == EnrollmentStatus.Finished).ToList();
                    var answers = Uow.Answers.GetBySurveyId(surveyId).ToList();
                    model.Answers = answers.Where(a => enrollments.Any(e => e.EnrollmentId == a.EnrollmentId)).ToList();
                    model.Enrollments = enrollments;
                    return View(model);
                }

            }
            SetNotify("Verileri bulunamadı.", "Rapor", NotifyMessageType.Warning);
            return RedirectToAction("Index", "Report");
        }
        public ActionResult ParticipantReport(string id, EnrollmentStatus? status)
        {
            RedirectForUnauthorized();
            ViewBag.SurveyId = id;
            ViewBag.Status = status;
            return View();
        }
        [ValidateInput(false)]
        public ActionResult GetUserList(string id, EnrollmentStatus? status)
        {
            RedirectForUnauthorized();
            int surveyId = id.GetDirectRefForInt(typeof(Survey));
            IEnumerable<ParticipantModel> results = new List<ParticipantModel>();
            if (surveyId > 0 && status.HasValue)
            {
                ViewBag.SurveyId = id;
                ViewBag.Status = status;
                Survey survey = Uow.Surveys.GetByKey(surveyId);
                if (survey != null)
                {
                    results = Uow.Enrollments.GetParticipants(surveyId, status.Value);
                    if (results != null && results.Count() > 0)
                    {
                        if (!survey.InCompanyUsage && survey.UsageType != UsageType.ApplicationForm && survey.UsageType != UsageType.ActivityForm && survey.UsageType != UsageType.InCompanySurvey)
                            foreach (ParticipantModel m in results)
                            {
                                if (m.AllowedInfoShare) continue;
                                m.FirstName = ReplaceInfo(m.FirstName);
                                m.LastName = ReplaceInfo(m.LastName);
                                m.Email = ReplaceInfo(m.Email);
                                m.IdentityNumber = ReplaceInfo(m.IdentityNumber);
                                m.Imei = ReplaceInfo(m.Imei);
                                m.PhoneNumber = ReplaceInfo(m.PhoneNumber);
                            }
                        int rowNumber = 0;
                        foreach (ParticipantModel m in results)
                        {
                            m.RowNumber = ++rowNumber;
                            DateTime finishDate = m.FinishDate.HasValue ? m.FinishDate.Value : DateTime.Now;
                            m.Duration = m.StartDate.HasValue ? (int)(finishDate - m.StartDate.Value).TotalSeconds : 0;
                        }
                    }
                }
            }
            return PartialView("_UserList", results);
        }
        private string ReplaceInfo(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                int lastIndex = value.Length - 1;
                if (value.Contains("@")) lastIndex = value.IndexOf("@");
                if (lastIndex > 1)
                {
                    string info = value.Substring(1, lastIndex - 1);
                    value = value.Replace(info, string.Concat(Enumerable.Repeat("*", info.Length)));
                }
            }
            return value;
        }

        public ActionResult ConstraintReport(string id)
        {
            RedirectForUnauthorized();
            int surveyId = id.GetDirectRefForInt(typeof(Survey));
            Survey survey = null;
            ViewBag.SurveyId = id;
            if (surveyId > 0)
            {
                survey = Uow.Surveys.GetByKey(surveyId);
                if (survey != null)
                {
                    ViewBag.Name = survey.Name;
                    IEnumerable<ConstraintModel> constraints = new List<ConstraintModel>();
                    constraints = Uow.Enrollments.GetConstraintCounts(surveyId);
                    return View(constraints);
                }
            }
            SetNotify("Verileri bulunamadı.", "Rapor", NotifyMessageType.Warning);
            return RedirectToAction("Index", "Report");
        }

        // todo: Gruplama yaptık
        public ActionResult GroupReport(string id)
        {
            RedirectForUnauthorized();

            int surveyId = id.GetDirectRefForInt(typeof(Survey));

            Survey survey = null;

            ViewBag.SurveyId = id;

            if (surveyId > 0)
            {
                survey = Uow.Surveys.GetByKey(surveyId);

                if (survey != null)
                {
                    ViewBag.Name = survey.Name;
                    
                    var groupReport = (from g in Uow.Groups.GetBySurveyId(surveyId)

                                       let user = (from user in Uow.Users.GetAll()
                                                   where user.UserId == g.UserId
                                                   select new
                                                   {
                                                       user.UserId,
                                                       user.Gender,
                                                   }).Take(1).FirstOrDefault()

                                       let questionGroup = (from questionGroup in Uow.QuestionGroups.GetAll()
                                                            where questionGroup.GroupId == g.GroupId
                                                            select new
                                                            {
                                                                questionGroup.GroupId,
                                                                questionGroup.Name,
                                                                questionGroup.Description
                                                            }).Take(1).FirstOrDefault()

                                       select new
                                       {
                                           GroupId = questionGroup.GroupId,
                                           GroupName = questionGroup.Name,
                                           GroupDescription = questionGroup.Description,
                                           SurveyId = g.SurveyId,
                                           UserId = user.UserId,
                                           Gender = user.Gender,
                                           UserCount = 1
                                       }).GroupBy(g => new { g.GroupId, g.Gender }).Select(g => new GroupModel
                                       {
                                           GroupId = g.FirstOrDefault().GroupId,
                                           GroupName = g.FirstOrDefault().GroupName,
                                           GroupDescription = g.FirstOrDefault().GroupDescription,
                                           SurveyId = g.FirstOrDefault().SurveyId,
                                           UserId = g.FirstOrDefault().UserId,
                                           Gender = g.FirstOrDefault().Gender,
                                           UserCount = g.Sum(p => g.FirstOrDefault().UserCount)
                                       });

                    return View(groupReport);
                }
            }

            SetNotify("Verileri bulunamadı.", "Rapor", NotifyMessageType.Warning);

            return RedirectToAction("Index", "Report");
        }

        [HttpGet]
        public ActionResult ExportToExcell(string id)
        {
            RedirectForUnauthorized();
            int surveyId = id.GetDirectRefForInt(typeof(Survey));
            Survey survey = null;
            if (surveyId > 0)
            {
                survey = Uow.Surveys.GetByKey(surveyId);
                if (survey == null)
                {
                    SetNotify("Verileri bulunamadı.", "Rapor", NotifyMessageType.Warning);
                    return RedirectToAction("Index", "Report");
                }



                var participants = Uow.Enrollments.GetParticipants(surveyId, EnrollmentStatus.Finished);
                if (participants != null && participants.Count() > 0)
                {
                    if (!survey.InCompanyUsage && survey.UsageType != UsageType.ApplicationForm && survey.UsageType != UsageType.ActivityForm && survey.UsageType != UsageType.InCompanySurvey)
                        foreach (ParticipantModel m in participants)
                        {
                            if (m.AllowedInfoShare) continue;
                            m.FirstName = ReplaceInfo(m.FirstName);
                            m.LastName = ReplaceInfo(m.LastName);
                            m.Email = ReplaceInfo(m.Email);
                            m.IdentityNumber = ReplaceInfo(m.IdentityNumber);
                            m.Imei = ReplaceInfo(m.Imei);
                            m.PhoneNumber = ReplaceInfo(m.PhoneNumber);
                        }
                    int rowNumber = 0;
                    foreach (ParticipantModel m in participants)
                    {
                        m.RowNumber = ++rowNumber;
                        DateTime finishDate = m.FinishDate.HasValue ? m.FinishDate.Value : DateTime.Now;
                        m.Duration = m.StartDate.HasValue ? (int)(finishDate - m.StartDate.Value).TotalSeconds : 0;
                    }
                }

                survey.Questions = Uow.Questions.GetWithChoicesBySurveyId(surveyId).ToList();
                var enrollments = Uow.Enrollments.GetBySurveyId(surveyId).Where(m => m.Status == EnrollmentStatus.Finished).ToList();
                var answers = Uow.Answers.GetBySurveyId(surveyId).ToList();
                survey.Answers = answers.Where(a => enrollments.Any(e => e.EnrollmentId == a.EnrollmentId)).ToList();
                survey.Enrollments = enrollments;

                var constraints = Uow.Enrollments.GetUserConstraints(surveyId);

                DataTable table = new DataTable();
                table.Columns.Add("Sıra");
                table.Columns.Add("Id");
                table.Columns.Add("Ad");
                table.Columns.Add("Soyad");
                table.Columns.Add("E-Posta");
                table.Columns.Add("Şehir");
                table.Columns.Add("Süre(sn)");
                table.Columns.Add("Doldurma Süresi(sn)");
                table.Columns.Add("Cihaz Adı");
                table.Columns.Add("Platform");
                table.Columns.Add("Operatör");
                table.Columns.Add("Marka");
                foreach (string cname in constraints.Select(m => m.CategoryName).Distinct().OrderBy(m => m))
                    table.Columns.Add(cname);


                int qCount = 0;
                foreach (Question q in survey.Questions)
                {
                    qCount++;
                    table.Columns.Add(string.Format("{0}. {1}", qCount, q.QuestionText));
                }
                foreach (ParticipantModel p in participants)
                {
                    List<object> row = new List<object>();
                    row.Add(p.RowNumber);
                    row.Add(p.UserId);
                    row.Add(p.FirstName);
                    row.Add(p.LastName);
                    row.Add(p.Email);
                    row.Add(p.Location);
                    row.Add(p.Duration);
                    row.Add(p.FullingTime);
                    row.Add(p.DeviceName);
                    row.Add(p.Platform);
                    row.Add(p.Operator);
                    row.Add(p.Manufacturer);

                    foreach (string cname in constraints.Select(m => m.CategoryName).Distinct().OrderBy(m => m))
                    {
                        ConstraintModel uc = constraints.Where(m => m.CategoryName == cname && m.UserId.ToString() == p.UserId).FirstOrDefault();
                        if (uc != null)
                            row.Add(uc.ValueName);
                        else
                            row.Add("");
                    }

                    foreach (Question q in survey.Questions)
                    {
                        var responses = survey.Answers.Where(m => m.QuestionId == q.QuestionId && m.UserId.ToString() == p.UserId).ToList();
                        if (responses != null && responses.Count > 0)
                        {
                            foreach (Answer r in responses)
                                r.Choice = q.Choices.Where(m => m.ChoiceId == r.ChoiceId).FirstOrDefault();
                            row.Add(string.Join(";", responses.Select(m => m.Choice != null ? m.Choice.Text : m.ResponseText ?? "")));
                        }
                        else
                            row.Add("");
                    }

                    table.Rows.Add(row.ToArray());
                }

                string fileName = "Pollyfy-SurveyReport-" + DateTime.Now.ToLocalTime().ToString("yyMMMdd-HHmm-") + Guid.NewGuid().ToString() + ".xlsx";
                FileContainer file = new FileContainer()
                {
                    FileName = fileName,
                    FileType = FileType.Excell,
                    OwnerType = FileOwnerType.Temp,
                };
                FileUploadSettings uploader = new FileUploadSettings();
                string fileUrl = uploader.GetPhysicalFileLink(file, false);
                Response.ContentEncoding = System.Text.Encoding.UTF8;
                ExcellProvider excel = new ExcellProvider();
                excel.ExportToExcell(fileUrl, table);
                return File(fileUrl, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);

                //model = Uow.Surveys.GetSurveyIncludes(surveyId);
                //if (model != null)
                //{
                //    model.Questions = Uow.Questions.GetWithChoicesBySurveyId(surveyId).ToList();
                //    var enrollments = Uow.Enrollments.GetBySurveyId(surveyId).Where(m => m.Status == EnrollmentStatus.Finished).ToList();
                //    var answers = Uow.Answers.GetBySurveyId(surveyId).ToList();
                //    model.Answers = answers.Where(a => enrollments.Any(e => e.EnrollmentId == a.EnrollmentId)).ToList();
                //    model.Enrollments = enrollments;
                //    return View(model);
                //}

            }

            SetNotify("Verileri bulunamadı.", "Rapor", NotifyMessageType.Warning);
            return RedirectToAction("Index", "Report");
        }

    }
}