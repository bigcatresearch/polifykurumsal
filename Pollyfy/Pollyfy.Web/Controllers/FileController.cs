﻿using DevExpress.Web.Mvc;
using Pollyfy.Data.Interfaces;
using Pollyfy.Models.ViewModel;
using Pollyfy.Web.Core;
using System.Web.Mvc;
using Pollyfy.Core.Extensions;
using Pollyfy.Models;

namespace Pollyfy.Web.Controllers
{
    [Authorize]
    public class FileController : BaseController
    {
        public FileController(IUow uow) : base(uow) { }
        // GET: File
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Upload(FileContainer container)
        {
            if (container != null)
            {
                container.CompanyId = container.CompanyIdRef.GetDirectRefForInt(typeof(Company));
                container.OwnerId = container.OwnerIdRef.GetDirectRefForInt(typeof(FileContainer));
                FileUploadSettings uploadSettings = new FileUploadSettings();

                UploadControlExtension.GetUploadedFiles("UploadControl", uploadSettings.UploadValidationSettings(container), uploadSettings.FileUploadComplete);
            }
            return null;
        }
        public ActionResult FileContainer()
        {
            return PartialView("_FileContainer");
        }
    }
}
