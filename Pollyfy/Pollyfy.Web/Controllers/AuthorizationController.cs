﻿#region 

using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using Pollyfy.Data.Interfaces;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Web.Controllers
{
    [Authorize]
    public class AuthorizationController : BaseController
    {
        public AuthorizationController(IUow uow) : base(uow)
        {
        }

        [HttpGet]
        public string GetList()
        {
            RedirectForUnauthorized();

            var model = Uow.Authorizations.GetAll();

            return JsonConvert.SerializeObject(model);
        }
    }
}