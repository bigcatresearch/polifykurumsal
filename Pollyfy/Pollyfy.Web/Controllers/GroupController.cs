﻿using System.Web.Mvc;
using Newtonsoft.Json;
using Pollyfy.Data.Interfaces;
using Pollyfy.Models;

namespace Pollyfy.Web.Controllers
{
    public class GroupController : BaseController
    {
        public GroupController(IUow uow) : base(uow)
        {

        }

        [HttpGet]
        public string GetQuestionGroupList()
        {
            RedirectForUnauthorized();

            var questionGroups = Uow.QuestionGroups.GetAll();

            return JsonConvert.SerializeObject(questionGroups);
        }

        [HttpGet]
        public string GetByGorupIds(string id)
        {
            RedirectForUnauthorized();

            if (string.IsNullOrEmpty(id))
                return null;

            var gorups = Uow.Groups.GetByGorupIds(id, ActiveStatus.Active);

            return JsonConvert.SerializeObject(gorups);
        }
    }
}