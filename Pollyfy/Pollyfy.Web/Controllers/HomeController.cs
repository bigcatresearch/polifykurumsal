﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;
using Pollyfy.Models;
using Pollyfy.Data.Interfaces;
using Pollyfy.Web.Core;
using Pollyfy.Models.ViewModel;

namespace Pollyfy.Web.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public HomeController(IUow uow) : base(uow) { }

        public ActionResult Index()
        {
            RedirectForUnauthorized();
            DashboardModel model = Uow.Users.GetMainDashboardData(CurrentUser.CompanyId, CurrentUser.Types);
            if (model == null) model = new DashboardModel();
            foreach (ChartDataModel item in model.ParticipationChartSeries)
                item.UnixDate = (long)(item.Date.Date.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            model.SerieStartDate = DateTime.Now.AddMonths(-1).Date;
            model.SerieEndDate = DateTime.Now.Date;
            model.SerieStartUnixDate = (long)(model.SerieStartDate.Value.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            model.SerieEndUnixDate = (long)(model.SerieEndDate.Value.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            var model = Uow.Users.GetAll();
            return PartialView("_GridViewPartial", model);
        }
    }
}