﻿using Pollyfy.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pollyfy.Web.Controllers
{
    public class HelpController : BaseController
    {
        public HelpController(IUow uow) : base(uow) { }
        public ActionResult Index()
        {
            return View();
        }
    }
}