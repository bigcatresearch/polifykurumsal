﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DevExpress.CodeParser;
using Pollyfy.Models;
using Pollyfy.Data.Interfaces;
using Pollyfy.Web.Core;
using Pollyfy.Core.Extensions;
using Pollyfy.Models.ViewModel;
using Newtonsoft.Json;
using System.Web.Http;

namespace Pollyfy.Web.Controllers
{
    public class SorularController : BaseController
    {
        public SorularController(IUow uow) : base(uow) { }

        [System.Web.Http.HttpGet]
        public ActionResult Sorular(string Id)
        {
            
            IEnumerable<QuestionModel> results = new List<QuestionModel>();     
            RedirectForUnauthorized();
            int surveyId = Id.GetDirectRefForInt(typeof(Survey));
            Survey anket = Uow.Surveys.GetForAll(surveyId);
            Session["SurveyId"] = surveyId;
            ViewBag.Surveyname = anket.Name;
            if (surveyId > 0)
            {
                var questions = Uow.Questions.GetWithChoicesBySurveyId(surveyId);

                if (questions != null && questions.Count() > 0)
                    results = questions.Select(q => new QuestionModel()
                    {
                        QuestionId = q.QuestionId.GetIndirectRef(typeof(Question)),
                        SurveyId = q.SurveyId.GetIndirectRef(typeof(Survey)),
                        QuestionText = q.QuestionText,
                        Description = q.Description,
                        IsImage = q.IsImage,
                        DescribeIsImage = q.DescribeIsImage,
                        IsChoiceImage = q.IsChoiceImage,
                        QuestionType = (int)q.QuestionType,
                        IsRequired = q.IsRequired,
                        ImageUrl = "",
                        Choices = q.Choices.Select(c => new ChoiceModel()
                        {
                            ChoiceId = c.ChoiceId.GetIndirectRef(typeof(Choice)),
                            Text = c.Text,
                            ChoiceDescribe = c.ChoiceDescribe,
                            PassQuestion = c.PassQuestion ?? 0
                        })
                    });

                return View(results);
            }
            else
            {
                return View(results);
            }
        }
        
        [System.Web.Http.HttpPost]
        public void SoruKaydet(string[] function_param)
        {
            RedirectForUnauthorized();
            ViewBag.IsValid = ModelState.IsValid;
            int surveyId = Convert.ToInt32(Session["SurveyId"]);
            string User = "F58938507C67F57FE73B46B6D5B33BDE";
            int userId = User.GetDirectRefForInt(typeof(User));
            var questions = Uow.Questions.GetWithChoicesBySurveyId(surveyId);
            int[] questionId = new int[questions.Count()];
            string[] questionText = new string[questions.Count()];
            if (userId > 0 && surveyId > 0)
            {
                int j = 0;
                foreach (var q in questions)
                {
                    questionId[j] = q.QuestionId;
                    questionText[j] = q.QuestionText;
                    j++;
                }
            } 
            for (int i = 0; i < function_param[i].Count(); i++)
            {
                int choiceId = 0;
                List<int> choiceIds = new List<int>();
                function_param[i] = function_param[i].Replace("null,", "");
                function_param[i] =  function_param[i].Replace(",null", "");
                foreach (string id in (function_param[i].Split(',')))
                    {
                        choiceId = id.GetDirectRefForInt(typeof(Choice));
                        if (choiceId > 0)
                            choiceIds.Add(choiceId);
                    }
                Uow.Surveys.SaveAnswer(surveyId, userId, questionId[i], string.Join(",", choiceIds), questionText[i]);   
            }
        }
    }
}