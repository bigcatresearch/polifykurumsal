﻿using Microsoft.Owin.Security;
using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using Pollyfy.Models.ViewModel;
using Pollyfy.Web;
using Pollyfy.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Pollyfy.Web.Controllers
{
    public class BaseApiController : ApiController
    {
        protected IUow Uow;
        public BaseApiController(IUow uow)
        {
            this.Uow = uow;
            if (Request != null)
                Core.Common.AbsoluteUri = Request.RequestUri.AbsoluteUri;
        }
        protected HttpResponseMessage ApiResponseMessage(bool success, object data = null, ErrorCode? code = ErrorCode.NONE, object error = null)
        {
            ApiResponse response = new ApiResponse();
            response.Success = success;
            response.Data = data;
            response.Error = error;
            response.Code = code != null && code.HasValue ? code.Value.ToString() : "";
            return Request.CreateResponse(HttpStatusCode.OK, response);

        }
        protected AppUser CurrentUser
        {
            get
            {
                var user = new AppUser(User as ClaimsPrincipal);
                return user;
            }
        }

        protected IAuthenticationManager AuthManager
        {
            get
            {

                var ctx = Request.GetOwinContext();
                return ctx.Authentication;
            }
        }
    }
}
