﻿using Pollyfy.Data.Interfaces;
using System;
using System.Linq;
using System.Web.Security;

namespace Pollyfy.Web.Providers
{
    public class UserRoleProvider : RoleProvider
    {
        private static IUow _uow;
        public static IUow Uow
        {
            get
            {
                if (_uow == null || _uow.DbConnection == null || string.IsNullOrEmpty(_uow.DbConnection.ConnectionString))
                    _uow = (IUow)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUow));
                return _uow;
            }
        }

        public override string ApplicationName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            //var user = Uow.Users.GetUserByUserName(username);

            //if (user.ActiveStatus != Pollyfy.Models.ActiveStatus.Active) return null;

            //var userRoleAuthorizations = Uow.UserRoleAuthorizations.GetByUserRoleId((int)user.UserRoleId).Select(p => p.AuthorizationId).ToArray();

            //var permissions = Uow.Authorizations.GetAll().Where(p => userRoleAuthorizations.Contains(p.Id)).Select(p => p.Key);

            //return permissions.ToArray();

            return null;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}