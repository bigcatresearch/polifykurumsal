﻿using Microsoft.AspNet.Identity;
using OfficeOpenXml;
using Pollyfy.Models;
using Pollyfy.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace Pollyfy.Web.Providers
{
    public class ExcellProvider
    {
        public ExcellProvider()
        {

        }
        public bool ImportData(String ImportFile, string tabloAdi)
        {
            FileInfo existingFile = new FileInfo(ImportFile);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {

                #region

                try
                {
                    OfficeProperties prp = package.Workbook.Properties;
                    ExcelWorkbook workBook = package.Workbook;
                    ExcelWorksheets sheets = workBook.Worksheets;
                    if (workBook == null || workBook.Worksheets.Count == 0) return false;

                    ExcelWorksheet ws1 = workBook.Worksheets[1];
                    var sutunSayisi = ws1.Dimension.End.Column;
                    using (SqlConnection connection = new SqlConnection("Password=ser-9274;Persist Security Info=True;User ID=sa;Initial Catalog=YtuDb;Data Source=31.210.91.14"))
                    {
                        try
                        {
                            string KisiselParametreler = "";
                            string DemografikParametreler = "";
                            string KisiselDegerler = "";
                            string DemografikDegerler = "";
                            int kisiselSayac = 0;

                            for (int i = 1; i <= sutunSayisi; i++)
                            {
                                //Kişisel ve demografik parametreleri ayırdık.
                                #region
                                if (ws1.Cells[1, i].Text.Contains('_'))
                                {
                                    DemografikParametreler = DemografikParametreler + ws1.Cells[1, i].Text + ",";
                                    DemografikDegerler = DemografikDegerler + "@" + ws1.Cells[1, i].Text + ","; ;
                                }
                                else
                                {
                                    KisiselParametreler = KisiselParametreler + ws1.Cells[1, i].Text + ",";
                                    KisiselDegerler = KisiselDegerler + "@" + ws1.Cells[1, i].Text + ","; ;
                                }
                                #endregion
                            }
                            KisiselParametreler = KisiselParametreler.Substring(0, KisiselParametreler.Length - 1);
                            DemografikParametreler = DemografikParametreler.Substring(0, DemografikParametreler.Length - 1);
                            KisiselDegerler = KisiselDegerler.Substring(0, KisiselDegerler.Length - 1);
                            DemografikDegerler = DemografikDegerler.Substring(0, DemografikDegerler.Length - 1);
                            using (SqlCommand command = new SqlCommand())
                            {
                                //Kisisel degerleri ekledik.
                                #region

                                for (int j = 2; j <= ws1.Dimension.End.Row; j++)
                                {
                                    string komut = "INSERT into " + tabloAdi + " (" + KisiselParametreler + ") VALUES (" + KisiselDegerler + ")";
                                    command.Parameters.Clear();
                                    for (kisiselSayac = 1; kisiselSayac <= (sutunSayisi - DemografikParametreler.Split(',').Count()); kisiselSayac++)
                                    {
                                        command.Connection = connection;            // <== lacking
                                        command.CommandType = CommandType.Text;
                                        command.CommandText = komut;
                                        // command.Parameters.Clear();
                                        command.Parameters.AddWithValue("@" + ws1.Cells[1, kisiselSayac].Text, ws1.Cells[j, kisiselSayac].Text);

                                    }
                                    connection.Open();
                                    command.ExecuteNonQuery();
                                    connection.Close();
                                }

                                #endregion

                                //Demografik değerleri ekledik.
                                #region
                                for (int j = 2; j <= ws1.Dimension.End.Row; j++)
                                {
                                    string komut = "INSERT into " + tabloAdi + "demografik" + " (" + DemografikParametreler + ") VALUES (" + DemografikDegerler + ")";
                                    command.Parameters.Clear();
                                    for (int i = kisiselSayac; i <= (kisiselSayac + DemografikParametreler.Split(',').Count()); i++)
                                    {
                                        command.Connection = connection;            // <== lacking
                                        command.CommandType = CommandType.Text;
                                        command.CommandText = komut;
                                        // command.Parameters.Clear();
                                        command.Parameters.AddWithValue("@" + ws1.Cells[1, i].Text, ws1.Cells[j, i].Text);

                                    }
                                    connection.Open();
                                    command.ExecuteNonQuery();
                                    connection.Close();
                                }
                                #endregion
                            }

                        }
                        catch (SqlException)
                        {
                            // error here
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }
                catch (Exception)
                {
                    package.Dispose();
                    throw;
                }
                #endregion

                package.Save();
                package.Dispose();
            }
            return true;
        }
        public List<PromotionImportModel> ImportPromotions(String ImportFile)
        {
            List<PromotionImportModel> promotions = new List<PromotionImportModel>();
            FileInfo existingFile = new FileInfo(ImportFile);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                #region using

                try
                {
                    OfficeProperties prp = package.Workbook.Properties;
                    ExcelWorkbook workBook = package.Workbook;
                    ExcelWorksheets sheets = workBook.Worksheets;

                    if (workBook == null || workBook.Worksheets.Count == 0) return promotions;

                    ExcelWorksheet ws1 = workBook.Worksheets[1];

                    for (int j = 2; j <= ws1.Dimension.End.Row; j++)
                    {
                        PromotionImportModel promo = new PromotionImportModel();
                        bool IsValid = true;
                        if (!string.IsNullOrEmpty(ws1.Cells[j, 1].Text))
                            promo.Code = ws1.Cells[j, 1].Text;
                        else
                            IsValid = false;
                        if (!string.IsNullOrEmpty(ws1.Cells[j, 2].Text))
                            promo.Discount = ws1.Cells[j, 2].Text;
                        else
                            promo.Discount = "";


                        if (IsValid && !promotions.Any(m => m.Code.Trim() == promo.Code.Trim()))
                            promotions.Add(promo);
                    }

                }
                catch (Exception)
                {

                    package.Dispose();
                    throw;
                }
                #endregion

                package.Save();
                package.Dispose();
            }
            return promotions;
        }
        public void ExportToExcell(String exportFile, DataTable table)
        {
            FileInfo newFile = new FileInfo(exportFile);
            #region using
            using (ExcelPackage excelPackage = new ExcelPackage(newFile))
            {


                try
                {
                    ExcelWorksheet ws = excelPackage.Workbook.Worksheets.Add("Rapor");

                    int col = 1;
                    foreach (DataColumn c in table.Columns)
                    {
                        ws.Cells[1, col].Value = c.ColumnName;
                        col++;
                    }
                    int row = 2;
                    foreach (DataRow r in table.Rows)
                    {
                        for (int c = 0; c < table.Columns.Count; c++)
                        {
                            ws.Cells[row, c + 1].Value = r[c];
                        }
                        row++;
                    }
                    //List<string> added = new List<string>();
                    //if (SheetHelpers != null)
                    //{
                    //    ExcelWorksheet hws = excelPackage.Workbook.Worksheets.Add("Helper");
                    //    foreach (var sheet in SheetDatas)
                    //    {
                    //        ExcelWorksheet ws = excelPackage.Workbook.Worksheets[sheet.Key];
                    //        foreach (DataColumn header in sheet.Value.Columns)
                    //        {
                    //            String addres = "ABCDEFGHIJKLMNOPRST";
                    //            String valAddres = addres[added.Count * 2].ToString();
                    //            String keyAddres = addres[added.Count * 2 + 1].ToString();
                    //            if (SheetHelpers.ContainsKey(header.Caption))
                    //            {
                    //                if (!added.Contains(header.Caption))
                    //                {
                    //                    int j = 1;
                    //                    hws.Cells[valAddres + "1"].AddComment("hws" + valAddres, "right");
                    //                    foreach (KeyValuePair<object, string> pair in SheetHelpers[header.Caption])
                    //                    {
                    //                        hws.Cells[valAddres + j.ToString()].Value = pair.Value;
                    //                        hws.Cells[keyAddres + j.ToString()].Value = pair.Key;
                    //                        j++;
                    //                    }
                    //                    hws.Names.Add("hws" + valAddres, hws.Cells[valAddres + ":" + valAddres]);
                    //                    added.Add(header.Caption);
                    //                }
                    //                String targetAddres = addres[header.Ordinal].ToString();
                    //                String currentAddres = addres[added.FindIndex(i => i == header.Caption) * 2].ToString();
                    //                var val = ws.DataValidations.AddListValidation(targetAddres + ":" + targetAddres);
                    //                val.Formula.ExcelFormula = "=" + hws.Name + "!" + "hws" + currentAddres;

                    //                val.ShowErrorMessage = true;
                    //                val.ErrorTitle = "Invalid Value : " + header.Caption;
                    //                val.Error = "Please select or enter a valid value for this cell. : " + header.Caption;

                    //            }
                    //        }
                    //    }
                    //}

                    excelPackage.Save();
                    excelPackage.Dispose();

                }
                catch (Exception)
                {
                    excelPackage.Dispose();
                    throw;
                }
            }

            #endregion
        }
    }
}