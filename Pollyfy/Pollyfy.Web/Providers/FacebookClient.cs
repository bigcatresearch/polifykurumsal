﻿using DotNetOpenAuth.AspNet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

namespace Pollyfy.Web.Providers
{
    public class FacebookClient : IAuthenticationClient
    {
        private string _appId = "183654335372817";
        private string _appSecret = "caf854eb0fe05e449442b737d4008fce";
        private string _scope;

        private const string baseUrl = "https://www.facebook.com/dialog/oauth?client_id=";
        public const string graphApiToken = "https://graph.facebook.com/oauth/access_token?";
        public const string graphApiMe = "https://graph.facebook.com/me?";

        public FacebookClient()
        {

        }
        private static string GetHTML(string URL)
        {
            string connectionString = URL;

            try
            {
                System.Net.HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(connectionString);
                myRequest.Credentials = CredentialCache.DefaultCredentials;
                //// Get the response
                WebResponse webResponse = myRequest.GetResponse();
                Stream respStream = webResponse.GetResponseStream();
                ////
                StreamReader ioStream = new StreamReader(respStream);
                string pageContent = ioStream.ReadToEnd();
                //// Close streams
                ioStream.Close();
                respStream.Close();
                return pageContent;
            }
            catch (Exception)
            {
            }
            return null;
        }

        public IDictionary<string, string> GetUserData(string accessCode, string redirectURI)
        {
            string token = GetHTML(graphApiToken + "client_id=" + appId + "&redirect_uri=" + HttpUtility.UrlEncode(redirectURI) + "&client_secret=" + appSecret + "&code=" + accessCode);
            if (token == null || token == "")
            {
                return null;
            }
            string access_token = token.Substring(token.IndexOf("access_token="), token.IndexOf("&"));
            IDictionary<string, string> userData = new Dictionary<string, string>();
            string data = GetHTML(graphApiMe + "fields=id,name,email,gender,link&" + access_token);

            if (data != null)
                userData = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            return userData;
        }
        public IDictionary<string, string> GetUserData(string accessCode)
        {
            IDictionary<string, string> userData = new Dictionary<string, string>();
            string data = GetHTML(graphApiMe + "fields=id,name,email,gender,link&access_token=" + accessCode);

            if (data != null)
                userData = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            return userData;
        }
        public FacebookClient(string appId, string appSecret, string scope)
        {
            this._appId = appId;
            this._appSecret = appSecret;
            this._scope = scope;
        }

        public string ProviderName
        {
            get { return "facebook"; }
        }

        public void RequestAuthentication(System.Web.HttpContextBase context, Uri returnUrl)
        {
            string url = baseUrl + appId + "&redirect_uri=" + HttpUtility.UrlEncode(returnUrl.ToString()) + "&scope=" + scope;
            context.Response.Redirect(url, false);
            context.ApplicationInstance.CompleteRequest();
        }

        public AuthenticationResult VerifyAuthentication(System.Web.HttpContextBase context)
        {
            string code = context.Request.QueryString["code"];

            string rawUrl = context.Request.Url.OriginalString;
            //From this we need to remove code portion
            rawUrl = rawUrl.Replace(":80", "");
            rawUrl = Regex.Replace(rawUrl, "&code=[^&]*", "");

            IDictionary<string, string> userData = GetUserData(code, rawUrl);

            if (userData == null)
                return new AuthenticationResult(false, ProviderName, null, null, null);

            string id = ""; ;
            string username = "";
            if (userData.ContainsKey("id"))
            {
                id = userData["id"];
                username = id;
                userData.Remove("id");
            }

            AuthenticationResult result = new AuthenticationResult(true, ProviderName, id, username, userData);
            return result;
        }
        public string appId
        {
            get
            {
                return _appId;
            }
            set { _appId = value; }
        }

        public string appSecret
        {
            get
            {
                return _appSecret;
            }
            set { _appSecret = value; }
        }

        public string scope
        {
            get { return ""; }
            set { _scope = value; }
        }
    }
}