﻿using Microsoft.AspNet.Identity;
using Pollyfy.Web.Core;
using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace Pollyfy.Web.Providers
{
    public class SimpleUser : IUser
    {
        public string Id { get; set; }
        public int UserId { get; set; }
        public int? CompanyId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserType Types { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<SimpleUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }

        public override string ToString()
        {
            return string.Format("Id={0} PasswordHash={1}", Id, (PasswordHash == null ? string.Empty : PasswordHash));
        }
    }

    /// <summary>
    /// Works with SimpleUser objects stored in an XML file
    /// </summary>
    public class LoginProvider : IUserStore<SimpleUser>, IUserPasswordStore<SimpleUser>, IUserLockoutStore<SimpleUser, object>
    {
        //protected XmlDocument m_doc;
        protected IUow Uow { private set; get; }
        public LoginProvider(IUow uow)
        {
            this.Uow = uow;
        }

        #region IUserStore implementation

        public Task<SimpleUser> FindByIdAsync(string userId)
        {

            if (string.IsNullOrEmpty(userId)) return Task.FromResult<SimpleUser>(null);
            var user = Core.Common.Uow.Users.GetUserByUserName(userId);
            if (user == null) return Task.FromResult<SimpleUser>(null);
            Core.Common.Uow.Users.UpdateAccessTime(user.UserId);
            SimpleUser u = new SimpleUser()
            {
                Id = user.Email,
                UserId = user.UserId,
                CompanyId = user.CompanyId,
                UserName = user.UserName,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PasswordHash = user.Password,
                Types = user.Types
            };
            return Task.FromResult<SimpleUser>(u);
        }

        public Task<SimpleUser> FindByNameAsync(string userName)
        {
            return FindByIdAsync(userName);

        }

        public Task CreateAsync(SimpleUser user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(SimpleUser user)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(SimpleUser user)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IUserPasswordStore implementation

        public Task<string> GetPasswordHashAsync(SimpleUser user)
        {
            string hash = user.PasswordHash;
            return Task.FromResult<string>(hash);
        }

        public Task<bool> HasPasswordAsync(SimpleUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetPasswordHashAsync(SimpleUser user, string passwordHash)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IUserLockoutStore implementation

        public Task<int> GetAccessFailedCountAsync(SimpleUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> GetLockoutEnabledAsync(SimpleUser user)
        {
            return Task.FromResult<bool>(false);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(SimpleUser user)
        {
            throw new NotImplementedException();
        }

        public Task<int> IncrementAccessFailedCountAsync(SimpleUser user)
        {
            throw new NotImplementedException();
        }

        public Task ResetAccessFailedCountAsync(SimpleUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEnabledAsync(SimpleUser user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEndDateAsync(SimpleUser user, DateTimeOffset lockoutEnd)
        {
            throw new NotImplementedException();
        }

        public Task<SimpleUser> FindByIdAsync(object userId)
        {
            throw new NotImplementedException();
        }

        #endregion

        public void Dispose()
        {
        }
    }
}