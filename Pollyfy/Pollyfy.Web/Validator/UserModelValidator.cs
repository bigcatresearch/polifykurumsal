﻿using FluentValidation;
using Pollyfy.Models;
using Pollyfy.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Web.Validator
{
    public class UserModelValidator : AbstractValidator<UserModel>
    {
        public UserModelValidator()
        {
            
            //RuleFor(model => model.UserName)
            //    .NotEmpty().WithMessage("Kullanıcı adı bilgisi zorunludur.")
            //    .WithName("Kullanıcı Adı");
            
            RuleFor(model => model.FirstName)
                .NotEmpty().WithMessage("Ad bilgisi zorunludur.")
                .WithName("Ad");

            RuleFor(model => model.LastName)
                .NotEmpty().WithMessage("Soyad bilgisi zorunludur.")
                .WithName("Soyad");

            RuleFor(model => model.Password)
                .NotEmpty().WithMessage("Şifre bilgisi zorunludur.")
                .Length(8, 250).WithMessage("Şifre en az 8 en fazla 250 karakter olmalıdır.")
                .WithName("Şifre");

            RuleFor(model => model.Email)
                .NotEmpty().WithMessage("E-Posta bilgisi zorunludur.")
                .Length(1, 250).WithMessage("E-Posta adresi en fazla 200 karakter olmalıdır.")
                .EmailAddress().WithMessage("E-Posta adresi geçerli değil.")
                .WithName("E-Posta");

            RuleFor(model => model.BirthDate)
                //.NotNull().WithMessage("Doğum Tarihi bilgisi zorunludur.")
                .LessThan(DateTime.Now.AddYears(-13)).WithMessage("Onüç yaşından küçükler için uygun değildir.")
                .WithName("Doğum Tarihi");

            //RuleFor(model => model.Gender)
            //   .NotEmpty().WithMessage("Cinsiyet bilgisi zorunludur.")
            //   .Length(1, 20).WithMessage("Cinsiyet bilgisi fazla 20 karakter olmalıdır.")
            //   .WithName("Cinsiyet");


        }
    }
}
