﻿using FluentValidation;
using Pollyfy.Models;
using Pollyfy.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Web.Validator
{
    public class LoginModelValidator : AbstractValidator<LoginModel>
    {
        public LoginModelValidator()
        {
            RuleFor(model => model.Username)
                .NotEmpty().WithMessage("Kullanıcı adı zorunludur.")
                .WithName("Kullanıcı Adı");

            RuleFor(model => model.Password)
                .NotEmpty().WithMessage("Şifre bilgisi zorunludur.")
                .Length(6, 250).WithMessage("Şifre en az 6, en fazla 250 karakter olmalıdır.")
                .WithName("Şifre");
        }
    }
}
