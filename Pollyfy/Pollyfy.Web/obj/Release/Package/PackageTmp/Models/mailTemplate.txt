
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title></title>
</head>
<body>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="format-detection" content="date=no" />
  <meta name="format-detection" content="address=no" />
  <meta name="format-detection" content="telephone=no" />
  <title>Email Template</title>
  <style type="text/css" media="screen">
    /* Linked Styles */
      		body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#f0efeb; -webkit-text-size-adjust:none }
      		a { color:#f47d31; text-decoration:none }
      		p { padding:0 !important; margin:0 !important } 
      
      		/* Mobile styles */
      		@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) { 
      			div[class='mobile-br-5'] { height: 5px !important; }
      			div[class='mobile-br-10'] { height: 10px !important; }
      			div[class='mobile-br-15'] { height: 15px !important; }
      			div[class='mobile-br-20'] { height: 20px !important; }
      			div[class='mobile-br-25'] { height: 25px !important; }
      			div[class='mobile-br-30'] { height: 30px !important; }
      			div[class='mobile-br-40'] { height: 40px !important; }
      
      			th[class='m-td'], 
      			td[class='m-td'], 
      			div[class='hide-for-mobile'], 
      			span[class='hide-for-mobile'] { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }
      
      			span[class='mobile-block'] { display: block !important; }
      
      
      			div[class='wgmail'] img { min-width: 320px !important; width: 320px !important; }
      
      			div[class='img-m-center'] { text-align: center !important; }
      
      			div[class='fluid-img'] img,
      			td[class='fluid-img'] img { width: 100% !important; max-width: 480px !important; height: auto !important; }
      
      			table[class='mobile-shell'] { width: 100% !important; min-width: 100% !important; }
      			td[class='td'] { width: 100% ! important; min-width: 100% !important; }
      			
      			table[class='center'] { margin: 0 auto; }
      			
      			td[class='column'],
      			th[class='column'] { float: left !important; width: 100% !important; display: block !important; }
      
      			td[class='column-white'],
      			th[class='column-white'] { float: left !important; width: 100% !important; display: block !important; background: #ffffff !important; }
      
      			div[class='text-footer2'] br { display: none; }
      			img[class="image-mobile"] { display: inline-block !important; width: 100% !important; height: auto !important; }
      
      			img[class="btn-mobile"] { display: inline-block !important; width: 120px !important; height: 42px !important; }
      			img[class="btn-mobile-2"] { display: inline-block !important; width: 120px !important; height: 36px !important; }
      
      			div[class='text-header'] { text-align: center !important; }
      
      			td[class='content-spacing'] { width: 20px !important; }
      
      			div[class="h2-white-hidden"] { color: #4d4d4d !important; font-size: 24px !important; line-height: 32px !important; display: block !important; }
      			
      			a[class="button-1"] { background: none !important; background-color: none !important; width: 100% !important; text-decoration: underline !important; color: #f47d31 !important; }
      			a[class="button-2"] { background: #64b9de !important; background-color: #64b9de !important; color: #ffffff !important; }
      			a[class="button-mobile"] { width: 270px !important; }
      			div[class='h2a'] { color: #61605e !important; font-size: 24px !important; line-height: 28px !important; }
      			div[class='h2'],
      			div[class='h3'] { font-size: 24px !important; line-height: 28px !important; }
      			table[class='section-1'] { background: #ffffff !important; }
      		}
  </style>
</head>

<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#f0efeb; -webkit-text-size-adjust:none">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f0efeb">
    <tr>
      <td align="center" valign="top">
        <!-- Header -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
          <tr>
            <td align="center">
              <table width="630" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                <tr>
                  <td class="td" style="font-size:0pt; line-height:0pt; padding:0; margin:0; Margin:0; font-weight:normal; min-width:630px; width:630px" width="630">
                    <div class="hide-for-mobile">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                        <tr>
                          <td height="20" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                        </tr>
                      </table>
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                      <tr>
                        <td height="35" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                      </tr>
                    </table>
                    <div class="img-center" style="font-size:0pt; line-height:0pt; text-align:center">
                      <a href="https://www.ytu.pollyfy.com" target="_blank">
                        <img border="0" src="http://ytu.pollyfy.com/Content/images/ytuLogo.jpg" alt="" width="208" height="44" />
                      </a>
                    </div>
                    <div class="hide-for-mobile">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                        <tr>
                          <td height="20" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                        </tr>
                      </table>
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                      <tr>
                        <td height="35" class="spacer" stle="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <!-- END Header -->
        <!-- Section 1 -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" class="img" style="font-size:0pt; line-height:0pt; text-align:left">
              <div style="font-size:0pt; line-height:0pt; height:50px; background:#ffffff; ">
                <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="50" style="height:50px" alt="" />
              </div>
              <div style="font-size:0pt; line-height:0pt; height:50px; background:#ffffff; ">
                <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="50" style="height:50px" alt="" />
              </div>
              <div class="hide-for-mobile">
                <div style="font-size:0pt; line-height:0pt; height:50px; background:#ffffff; ">
                  <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="50" style="height:50px" alt="" />
                </div>
                <div style="font-size:0pt; line-height:0pt; height:25px; background:#ffffff; ">
                  <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="25" style="height:25px" alt="" />
                </div>
              </div>
            </td>
            <td width="630" class="td" style="font-size:0pt; line-height:0pt; padding:0; margin:0; Margin:0; font-weight:normal; min-width:630px; width:630px">
              <table width="630" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                <tr>
                  <td class="td" style="font-size:0pt; line-height:0pt; padding:0; margin:0; Margin:0; font-weight:normal; min-width:630px; width:630px" width="630">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top" class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="15">
                          <div style="font-size:0pt; line-height:0pt; height:50px; background:#ffffff; ">
                            <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="50" style="height:50px" alt="" />
                          </div>
                          <div style="font-size:0pt; line-height:0pt; height:50px; background:#ffffff; ">
                            <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="50" style="height:50px" alt="" />
                          </div>
                          <div class="hide-for-mobile">
                            <div style="font-size:0pt; line-height:0pt; height:50px; background:#ffffff; ">
                              <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="50" style="height:50px" alt="" />
                            </div>
                            <div style="font-size:0pt; line-height:0pt; height:25px; background:#ffffff; ">
                              <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="25" style="height:25px" alt="" />
                            </div>
                          </div>
                        </td>
                        <td>
                          <!-- Section -->
                          <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#fdd48b" class="section-1">
                            <tr>
                              <td class="m-td" style="font-size:0pt; line-height:0pt; text-align:left" width="30"> </td>
                              <td align="center">
                                <!--[if !mso]>
                                    <!-->
                                <div class="img" style="font-size:0pt; line-height:0pt; text-align:left">
                                  <a href="https://www.research.net/r/LXS7M9B">
                                    <img src="http://hosting.fyleio.com/35437/public/Surveys/Mobile_survey_email_image1.png" width="0" height="0" alt="" border="0" style="display:none" class="image-mobile" />
                                  </a>
                                </div>
                                <div style="font-size:0pt; line-height:0pt;" class="mobile-br-25"></div>
                                <div class="h2-white-hidden" style="color:transparent; font-family:Arial, sans-serif; font-size:0px; line-height:0px; text-align:center; font-weight:bold; display:none">Goruslerinize ihtiyacimiz var!</div>
                                <!--<![endif]-->
                                <div class="hide-for-mobile">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                                    <tr>
                                      <td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                                    </tr>
                                  </table>
                                </div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                                  <tr>
                                    <td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                                  </tr>
                                </table>
                                <div class="hide-for-mobile">
                                  <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                      <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="0"> </td>
                                      <td align="center">
                                        <div class="h2-white" style="color:#ffffff; font-family:Arial, sans-serif; font-size:28px; line-height:32px; text-align:center; font-weight:bold">Goruslerinize ihtiyacimiz var!</div>
                                      </td>
                                      <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="0"> </td>
                                    </tr>
                                  </table>
                                </div>
                                <div class="hide-for-mobile">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                                    <tr>
                                      <td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                                    </tr>
                                  </table>
                                </div>
                                <div class="hide-for-mobile"></div>
                              </td>
                              <td class="m-td" style="font-size:0pt; line-height:0pt; text-align:left" width="30"> </td>
                            </tr>
                          </table>
                          <div class="hide-for-mobile">
                            <div class="fluid-img" style="font-size:0pt; line-height:0pt; text-align:left">
                              <a href="https://www.research.net/r/LXS7M9B">
                                <img border="0" src="http://hosting.fyleio.com/35437/public/Surveys/survey_desktop_2.gif" alt="" />
                              </a>
                            </div>
                          </div>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                            <tr>
                              <td>
                                <!-- Content -->
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="m-td" style="font-size:0pt; line-height:0pt; text-align:left" width="25"></td>
                                    <td>
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                                        <tr>
                                          <td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                                        </tr>
                                      </table>
                                      <!-- Button -->
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td align="center">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                              <tr>
                                                <td align="center">
                                                  <div>
                                                    <!--[if mso]>
                                                        <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word"
                                                        href="http://members.headspace.com/t/l?ssid=35437&subscriber_id=brrixumhmcackqxrinbpnwvsnsbybmi&delivery_id=aphxevnlgptvtquufabozmhyjyembkn&td=NJTaMP2Hwgni_3J517VZEQB3t2UbwwgBh0SasAKOSj8kV_EK1nwgrLX6g-TONX2uUk9GbOv_WM9hB8mu7tJykUGrFtluIyVnFLw5xX_Ve0JuRUFvW8iO0nlxN9VMYKW0TZ-oqJ57Raq4vFmXDbkM2k5tfx_caKwJbKl3SfGV2POEFDIvFAEfYLfSzscP3j8PVpHjHu__FU_eS31pB0WABciQOuGjwLYxYUS10qvzYlHpT5tAUXUkvQleGqxHITFrQ0vj0VabjgeMTYKtBIKEdku8v35srrPYtj"
                                                        style="height:46px;v-text-anchor:middle;width:185px; font-size:17px;" arcsize="7%"
                                                        stroke="f" fillcolor="#64bade">
                                                          <w:anchorlock/>
                                                          <center>
                                                          <![endif]-->
                                                    <a class="button-2" href="$$Link$$" style="background-color:#64bade;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:17px;font-weight:bold;line-height:46px;text-align:center;text-decoration:none;width:218px;-webkit-text-size-adjust:none;">Anketi Doldur</a>
                                                    <!--[if mso]>
                                                          </center>
                                                        </v:roundrect>
                                                      <![endif]-->
                                                  </div>
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
                                          <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                                              <tr>
                                                <td height="20" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                                              </tr>
                                            </table>
                                            <div class="text-left" style="color:#4d4d4d; font-family:Arial; font-size:16px; line-height:28px; text-align:center">$$Body$$</div>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                                              <tr>
                                                <td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                                              </tr>
                                            </table>
                                            <!-- Button -->
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr>
                                                <td align="center">
                                                  <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                      <td align="center"></td>
                                                    </tr>
                                                  </table>
                                                </td>
                                              </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                                              <tr>
                                                <td height="35" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                                              </tr>
                                            </table>
                                            <div class="img-center" style="font-size:0pt; line-height:0pt; text-align:center">
                                              <img border="0" src="http://hosting.fyleio.com/35437/public/Gift%20Emails/Sendgrid%20Emails/ico_heart.jpg" alt="" width="14" height="13" />
                                            </div>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                                              <tr>
                                                <td height="35" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                                              </tr>
                                            </table>
                                            <div class="text-center" style="color:#4d4d4d; font-family:Arial; font-size:16px; line-height:28px; text-align:center">YTU Istatistik Uygulama ve Arastirma Merkezi</div>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                                              <tr>
                                                <td height="35" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                                              </tr>
                                            </table>
                                          </td>
                                          <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="1"></td>
                                        </tr>
                                      </table>
                                    </td>
                                    <td class="m-td" style="font-size:0pt; line-height:0pt; text-align:left" width="25"></td>
                                  </tr>
                                </table>
                                <!-- END Content -->
                              </td>
                            </tr>
                          </table>
                          <!-- End Section -->
                          <div class="hide-for-mobile">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                              <tr>
                                <td height="9" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                              </tr>
                            </table>
                          </div>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                            <tr>
                              <td height="15" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                            </tr>
                          </table>
                        </td>
                        <td valign="top" class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="15">
                          <div style="font-size:0pt; line-height:0pt; height:50px; background:#ffffff; ">
                            <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="50" style="height:50px" alt="" />
                          </div>
                          <div style="font-size:0pt; line-height:0pt; height:50px; background:#ffffff; ">
                            <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="50" style="height:50px" alt="" />
                          </div>
                          <div class="hide-for-mobile">
                            <div style="font-size:0pt; line-height:0pt; height:50px; background:#ffffff; ">
                              <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="50" style="height:50px" alt="" />
                            </div>
                            <div style="font-size:0pt; line-height:0pt; height:25px; background:#ffffff; ">
                              <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="25" style="height:25px" alt="" />
                            </div>
                          </div>
                        </td>
                      </tr>
                    </table>
                    <div class="wgmail" style="font-size:0pt; line-height:0pt; text-align:center">
                      <img src="http://hosting.fyleio.com/35437/public/Gift%20Emails/Sendgrid%20Emails/gmail_fix.gif" width="630" height="1" style="min-width:630px" alt="" border="0" />
                    </div>
                  </td>
                </tr>
              </table>
            </td>
            <td valign="top" class="img" style="font-size:0pt; line-height:0pt; text-align:left">
              <div style="font-size:0pt; line-height:0pt; height:50px; background:#ffffff; ">
                <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="50" style="height:50px" alt="" />
              </div>
              <div style="font-size:0pt; line-height:0pt; height:50px; background:#ffffff; ">
                <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="50" style="height:50px" alt="" />
              </div>
              <div class="hide-for-mobile">
                <div style="font-size:0pt; line-height:0pt; height:50px; background:#ffffff; ">
                  <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="50" style="height:50px" alt="" />
                </div>
                <div style="font-size:0pt; line-height:0pt; height:25px; background:#ffffff; ">
                  <img src="http://hosting.fyleio.com/35437/public/Onboarding%20Emails/New%20Welcome%20Emails/spacer.gif" width="1" height="25" style="height:25px" alt="" />
                </div>
              </div>
            </td>
          </tr>
        </table>
        <!-- END Section 1 -->
        <table width="630" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
          <tr>
            <td class="td" style="font-size:0pt; line-height:0pt; padding:0; margin:0; Margin:0; font-weight:normal; min-width:630px; width:630px" width="630">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="15"></td>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                      <tr>
                        <td height="15" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                      </tr>
                    </table>
                    <div class="hide-for-mobile">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                        <tr>
                          <td height="9" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                        </tr>
                      </table>
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">
                      <tr>
                        <td height="24" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> </td>
                      </tr>
                    </table>
                    <!-- Socials -->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="center">
                          <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="63" class="img-center" style="font-size:0pt; line-height:0pt; text-align:center">
                                <a href="https://www.instagram.com/headspace/" target="_blank">
                                  <img border="0" src="http://hosting.fyleio.com/35437/public/Header%20and%20Footer%20Assets/ico_instagram.gif" alt="" width="50" height="50" />
                                </a>
                              </td>
                              <td width="63" class="img-center" style="font-size:0pt; line-height:0pt; text-align:center">
                                <a href="https://plus.google.com/+headspaceofficial/posts" target="_blank">
                                  <img border="0" src="http://hosting.fyleio.com/35437/public/Header%20and%20Footer%20Assets/ico_gplus.gif" alt="" width="50" height="50" />
                                </a>
                              </td>
                              <td width="63" class="img-center" style="font-size:0pt; line-height:0pt; text-align:center">
                                <a href="https://twitter.com/Headspace" target="_blank">
                                  <img border="0" src="http://hosting.fyleio.com/35437/public/Header%20and%20Footer%20Assets/ico_twitter.gif" alt="" width="50" height="50" />
                                </a>
                              </td>
                              <td width="63" class="img-center" style="font-size:0pt; line-height:0pt; text-align:center">
                                <a href="https://www.facebook.com/HeadspaceOfficial" target="_blank">
                                  <img border="0" src="http://hosting.fyleio.com/35437/public/Header%20and%20Footer%20Assets/ico_facebook.gif" alt="" width="50" height="50" />
                                </a>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    <div style="font-size:0pt; line-height:0pt; height:32px">
                      <img src="http://hosting.fyleio.com/35437/public/Header%20and%20Footer%20Assets/spacer.gif" width="1" height="32" style="height:32px" alt="" />
                    </div>
                    <!-- END Socials -->
                    <!-- Footer -->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="10"></td>
                        <td>
                          
                          <div style="font-size:0pt; line-height:0pt; height:26px">
                            <img src="http://hosting.fyleio.com/35437/public/Header%20and%20Footer%20Assets/spacer.gif" width="1" height="26" style="height:26px" alt="" />
                          </div>
                        </td>
                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="10"></td>
                      </tr>
                    </table>
                    <!-- END Footer -->
                  </td>
                  <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="15"></td>
                </tr>
              </table>
              <div class="wgmail" style="font-size:0pt; line-height:0pt; text-align:center">
                <img src="http://hosting.fyleio.com/35437/public/Gift%20Emails/Sendgrid%20Emails/gmail_fix.gif" width="630" height="1" style="min-width:630px" alt="" border="0" />
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

</body>

</html>
  
  
</body>
</html>
