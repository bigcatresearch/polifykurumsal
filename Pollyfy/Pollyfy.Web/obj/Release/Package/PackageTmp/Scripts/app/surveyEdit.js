﻿/// <reference path="~/Scripts/linq-vsdoc.js" />
/// <reference path="~/Scripts/app/baseQuery.js" />

var dataSource = null;
var groupUserIds = [];
var userIdSurveys = [];

$(document).ready(function () {

    GetList("/Survey/GetList").done(function (resultSurvey, dataSurvey) {

        if (resultSurvey) {

            $("#getFormSurveys").dxTagBox({
                dataSource: dataSurvey,
                valueExpr: 'SurveyId',
                displayExpr: 'Name',
                onValueChanged: function (e) {

                    if (e.value) {

                        GetById(e.value.toString(), "/Survey/GetBySurveyIds").done(function (resultSurvey, dataSurvey) {

                            if (resultSurvey) {

                                groupUserIds = [];
                                userIdSurveys = [];
                                window.selectedUserIds = [];

                                userIdSurveys = Enumerable.from(dataSurvey).select(p => p.UserId).toArray();

                                for (var userId in userIdSurveys) {
                                    if (userIdSurveys.hasOwnProperty(userId)) {

                                        if (!window.selectedUserIds.includes(userIdSurveys[userId])) {

                                            window.selectedUserIds.push(userIdSurveys[userId]);
                                        }
                                    }
                                }

                                if ($("#getFormGroups").dxTagBox("instance").option("value"))
                                    GetById($("#getFormGroups").dxTagBox("instance").option("value").toString(), "/Group/GetByGorupIds").done(function (resultGroup, dataGroup) {

                                        if (resultGroup) {

                                            groupUserIds = Enumerable.from(dataGroup).select(p => p.UserId).toArray();

                                            for (var userId in groupUserIds) {
                                                if (groupUserIds.hasOwnProperty(userId)) {

                                                    if (!window.selectedUserIds.includes(groupUserIds[userId])) {

                                                        window.selectedUserIds.push(groupUserIds[userId]);
                                                    }
                                                }
                                            }
                                        }
                                    });
                            }
                        });
                    }
                }
            });
        }
    });

    GetList("/Group/GetQuestionGroupList").done(function (resultQuestionGroup, dataQuestionGroup) {

        if (resultQuestionGroup) {

            $("#getFormGroups").dxTagBox({
                dataSource: dataQuestionGroup,
                valueExpr: 'GroupId',
                displayExpr: 'Name',
                onValueChanged: function (e) {

                    if (e.value) {

                        GetById(e.value.toString(), "/Group/GetByGorupIds").done(function (resultGroup, dataGroup) {

                            if (resultGroup) {

                                groupUserIds = [];
                                userIdSurveys = [];
                                window.selectedUserIds = [];

                                groupUserIds = Enumerable.from(dataGroup).select(p => p.UserId).toArray();

                                for (var userId in groupUserIds) {
                                    if (groupUserIds.hasOwnProperty(userId)) {

                                        if (!window.selectedUserIds.includes(groupUserIds[userId])) {

                                            window.selectedUserIds.push(groupUserIds[userId]);
                                        }
                                    }
                                }

                                if ($("#getFormSurveys").dxTagBox("instance").option("value"))
                                    GetById($("#getFormSurveys").dxTagBox("instance").option("value").toString(), "/Survey/GetBySurveyIds").done(function (resultSurvey, dataSurvey) {

                                        if (resultSurvey) {

                                            userIdSurveys = Enumerable.from(dataSurvey).select(p => p.UserId).toArray();

                                            for (var userId in userIdSurveys) {
                                                if (userIdSurveys.hasOwnProperty(userId)) {

                                                    if (!window.selectedUserIds.includes(userIdSurveys[userId])) {

                                                        window.selectedUserIds.push(userIdSurveys[userId]);
                                                    }
                                                }
                                            }
                                        }
                                    });
                            }
                        });
                    }
                }
            });
        }
    });
});

$("#SaveSelections").click(function () {

});