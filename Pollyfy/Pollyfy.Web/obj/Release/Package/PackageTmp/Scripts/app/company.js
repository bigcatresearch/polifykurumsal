﻿/// <reference path="~/Scripts/linq-vsdoc.js" />
/// <reference path="~/Scripts/app/baseQuery.js" />

function DeleteItem(sender) {

    var selectedItem = $(sender).data();

    if (selectedItem === null)
        return;

    swal({
        title: "Emin misin?",
        text: "silinecek!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "İptal",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Evet, Sil"

    }).then(function (isConfirm) {

        if (isConfirm) {

            DeleteCompanyById(parseInt(selectedItem.id));
        }
    });
}

function DeleteCompanyById(id) {

    DeleteById(id, "/Company/Delete").done(function (result, data) {

        if (result && data) {
            window.location.reload();
        }
    });
}