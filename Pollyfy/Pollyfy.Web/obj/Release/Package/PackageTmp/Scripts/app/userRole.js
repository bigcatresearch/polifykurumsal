﻿/// <reference path="~/Scripts/linq-vsdoc.js" />
/// <reference path="~/Scripts/app/baseQuery.js" />

var dataSource = null;
var getUserRoleAuthorizations = [];
var getAuthorizations = [];

$(document).ready(function () {


    GetList("/UserRoleAuthorization/GetList").done(function (resultUserRoleAuthorization, dataUserRoleAuthorization) {

        if (resultUserRoleAuthorization) {

            getUserRoleAuthorizations = dataUserRoleAuthorization;

            GetList("/Authorization/GetList").done(function (resultAuthorization, authorizationData) {

                if (resultAuthorization) {

                    getAuthorizations = authorizationData;

                    dataSource = new DevExpress.data.DataSource({
                        store: new DevExpress.data.ArrayStore({
                            key: "Id",
                            data: getAuthorizations
                        }),
                        searchOperation: "contains",
                        searchExpr: "Name",
                        paginate: false
                    });

                    GetAuthorizationSelectionList();
                }
            });
        }
    });
});

function UserTypeValueChanged(value) {

    if (value !== "4" || value !== "2") {

        //var permissions = getAuthorizations;

        //var index = permissions.indexOf(Enumerable.from(permissions).firstOrDefault(p => p.Key === "Administrator"));

        //if (index > -1) {
        //    permissions.splice(index, 1);

        //    $("#AuthorizationSelectionList").dxList("instance").option("dataSource", permissions);
        //} else {

        //    $("#AuthorizationSelectionList").dxList("instance").option("dataSource", permissions);
        //}
    }
}

function DeleteItem(sender) {

    var idAtt = $(sender).attr("data-id");
    var selectedItem = $(sender).data();

    if (selectedItem === null)
        return;

    swal({
        title: "Emin misin?",
        text: "silinecek!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "İptal",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Evet, Sil"

    }).then(function (isConfirm) {

        if (isConfirm) {

            DeleteUserRoleById(parseInt(selectedItem.id));
        }
    });
}

function DeleteUserRoleById(id) {

    DeleteById(id, "/UserRole/Delete").done(function (result, data) {

        if (result && data) {
            window.location.reload();
        }
    });
}

function GetAuthorizationSelectionList() {
    $("#AuthorizationSelectionList").dxList({
        dataSource: dataSource,
        height: 200,
        showSelectionControls: true,
        selectionMode: "multiple",
        itemTemplate: function (data) {
            return $("<div>").text(data.Name);
        }
    }).dxList("instance");

    $("#SearchBox").dxTextBox({
        valueChangeEvent: "keyup",
        placeholder: "Yetki Arayınız yada listeden seçiniz",
        onValueChanged: function (args) {
            dataSource.searchValue(args.value);
            dataSource.load();
        },
        mode: "search"
    });

    GetUserRoleValueChanged(parseFloat($("#UserRoleId").val()));
}

function GetUserRoleValueChanged(userRoleId) {

    var authorizationIds = DevExpress.data.query(getUserRoleAuthorizations)
        .filter(["UserRoleId", "=", userRoleId]).select("AuthorizationId").toArray();

    var permissions = [];

    for (var i = 0; i < authorizationIds.length; i++) {

        var permissionItems = DevExpress.data.query($("#AuthorizationSelectionList").dxList("instance")
            .option("dataSource")._store._array)
            .filter(["Id", "=", authorizationIds[i].AuthorizationId]).toArray();

        if (authorizationIds || authorizationIds.length > 0) {

            permissions.push(permissionItems[0]);
        }
    }

    setTimeout(function () {

        $("#AuthorizationSelectionList").dxList("instance").option("selectedItems", permissions);

    },
        2000);
}

$('#form').on('submit', function (event) {

    event.preventDefault();
    $.ajax({
        url: $(this).attr('action'),
        type: 'post',
        data: $(this).serialize(),
        success: function (data, textStatus, jqXHR) {

            if (textStatus === 'success') {

                var item = {};
                var userRoleAuthorization = [];

                item["UserRoleId"] = parseFloat($("#UserRoleId").val());
                var guid = new DevExpress.data.Guid();

                var selectedPermissions = $("#AuthorizationSelectionList").dxList("instance").option("selectedItems");

                for (var i = 0; i < selectedPermissions.length; i++) {

                    var userRoleAuthorizationData = Enumerable.from(getUserRoleAuthorizations)
                        .firstOrDefault(p => p.UserRoleId === item["UserRoleId"] &&
                            p.AuthorizationId === selectedPermissions[i].Id);

                    userRoleAuthorization.push({
                        "Id": !userRoleAuthorizationData ? guid._value : userRoleAuthorizationData.Id,
                        "UserRoleId": item["UserRoleId"],
                        "AuthorizationId": selectedPermissions[i].Id
                    });
                }

                AddUserRoleAuthorization(userRoleAuthorization);
            }
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 0 || jqXHR == 302) {

        } else {

        }
    });
});

function AddUserRoleAuthorization(item) {

    Add(item, "/UserRoleAuthorization/Post").done(function (result, data) {

        if (result) {
            window.location.pathname = "/UserRole/Index";
        }
    });
}