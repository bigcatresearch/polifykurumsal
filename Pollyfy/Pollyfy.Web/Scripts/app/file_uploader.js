﻿var file_uploader = (function ($) {
    var pub = {};
    var _onFileUploadCompleteCallback = null;
    var _useFileDelete = false;
    pub.init = function (useFileDelete, onFileUploadCompleteCallback) {
        if (useFileDelete && useFileDelete == true)
            _useFileDelete = useFileDelete;
        _onFileUploadCompleteCallback = onFileUploadCompleteCallback;
    };
    pub.OnFileUploadComplete = function (s, e) {
        console.log(e.callbackData);
        if (e.callbackData !== "") {
            
            var container = JSON.parse(e.callbackData);
            if (_onFileUploadCompleteCallback) _onFileUploadCompleteCallback(container);
            common.Notify('Yükleme', 'Dosyanız yüklendi..', 'info');
            if (container.hasOwnProperty('Link'))
                $('.upload-control-img').attr('src', container.Link + '?' + Math.random());
            if (_useFileDelete) {
                file_uploader.hide();
                $('.file-container').show();
            }
            $('.file-container-body').html('<tr><td>' + container.FileName + '</td><td style="width: 40px;"><a class="btn btn-danger btn-sm" style="margin: 0px;" href="javascript:file_uploader.deleteFile();"><i class="fa fa-trash"></i></a></td></tr>');
        }
        else {
            if (_useFileDelete) {
                $('.file-container').hide();
                file_uploader.show();
            }
            common.Notify('Yükleme', 'Başarısız..', 'warning');
        }
    };
    pub.deleteFile = function () {
        $('.file-container').hide();
        $('#UploadControl').show();
        $('#FileName').val('');
    };
    pub.hide = function () {
        $('#UploadControl').hide();
        $('.UploadControl').hide();
    };
    pub.show = function () {
        $('#UploadControl').show();
        $('.UploadControl').show();
    };

    return pub;
}(jQuery));