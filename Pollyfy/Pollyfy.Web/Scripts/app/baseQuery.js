﻿function BaseRequest(url, id, item, type, callback) {
    window.jQuery.ajax({
        url: url,
        type: type,
        data: { id, item },
        success: function (data) {

            if (data === "True") {

                callback(true, true);
            } else {

                try {

                    callback(true, JSON.parse(data));

                } catch (e) {
                    callback(true, data);
                } 
            }
        },
        error: function (error) {

            if (error["status"] === 401 && error["statusText"] === "Unauthorized") {
            }

            callback(false, error.toString());
        },
        beforeSend: function () {

        },
        complete: function () {

        }
    });
}

function GetById(id, url) {

    var defered = $.Deferred();

    BaseRequest(url,
        id,
        null,
        "GET",
        function (result, datas) {

            defered.resolve(result, datas);
        });

    return defered.promise();
}

function GetList(url) {

    var defered = $.Deferred();

    BaseRequest(url,
        null,
        null,
        "GET",
        function (result, datas) {

            defered.resolve(result, datas);
        });

    return defered.promise();
}

function Add(item, url) {

    var defered = $.Deferred();

    BaseRequest(url,
        null,
        item,
        "POST",
        function (result, datas) {

            defered.resolve(result, datas);
        });

    return defered.promise();
}

function AddParameters(item, url) {

    var defered = $.Deferred();

    BaseRequest(url,
        null,
        item,
        "POST",
        function (result, datas) {

            defered.resolve(result, datas);
        });

    return defered.promise();
}

function UpdateById(id, item, url) {

    var defered = $.Deferred();

    BaseRequest(url,
        id,
        item,
        "POST",
        function (result, datas) {

            defered.resolve(result, datas);
        });

    return defered.promise();
}

function DeleteById(id, url) {

    var defered = $.Deferred();

    BaseRequest(url,
        id,
        null,
        "POST",
        function (result, datas) {

            defered.resolve(result, datas);
        });

    return defered.promise();
}