﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pollyfy.Web.Startup))]
namespace Pollyfy.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
