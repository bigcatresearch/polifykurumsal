﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pollyfy.Web.Core
{
    public class WebUtils
    {
        public static IEnumerable<SelectListItem> GetGenderSelect(string selected = "")
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Value = "Erkek", Text = "Erkek", Selected = selected == "Erkek" ? true : false });
            items.Add(new SelectListItem() { Value = "Kadın", Text = "Kadın", Selected = selected == "Kadın" ? true : false });
            return items;
        }
        public static IEnumerable<SelectListItem> GetUserTypeSelect(int? selected = null, bool forAdmin = false)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Value = "0", Text = "Uygulama Kullanıcısı", Selected = selected == 0 ? true : false });
            items.Add(new SelectListItem() { Value = "1", Text = "Şirket Kullanıcısı", Selected = selected == 1 ? true : false });
            items.Add(new SelectListItem() { Value = "4", Text = "Şirket Yöneticisi", Selected = selected == 4 ? true : false });
            items.Add(new SelectListItem() { Value = "3", Text = "Rapor Kullanıcısı", Selected = selected == 3 ? true : false });
            if (forAdmin)
                items.Add(new SelectListItem() { Value = "2", Text = "Sistem Yöneticisi", Selected = selected == 2 ? true : false });
            return items;
        }
        public static IEnumerable<SelectListItem> GetStatusSelect(int? selected = null)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Value = "0", Text = "Pasif", Selected = selected == 0 ? true : false });
            items.Add(new SelectListItem() { Value = "1", Text = "Aktif", Selected = selected == 1 ? true : false });
            items.Add(new SelectListItem() { Value = "-1", Text = "Silindi", Selected = selected == -1 ? true : false });
            return items;
        }
        public static IEnumerable<SelectListItem> GetCampaignKindSelect(int? selected = null)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Value = "0", Text = "Açıklama", Selected = selected == 0 ? true : false });
            items.Add(new SelectListItem() { Value = "1", Text = "İndirim Kodu", Selected = selected == 1 ? true : false });
            return items;
        }
        public static IEnumerable<SelectListItem> GetUsageTypeSelect(int? selected = null)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            //items.Add(new SelectListItem() { Value = "0", Text = "", Selected = selected == 0 ? true : false });
            items.Add(new SelectListItem() { Value = "1", Text = "Başvuru Formu", Selected = selected == 1 ? true : false });
            items.Add(new SelectListItem() { Value = "2", Text = "Etkinlik Formu", Selected = selected == 2 ? true : false });
            items.Add(new SelectListItem() { Value = "3", Text = "Kampanyalı Anket", Selected = selected == 3 ? true : false });
            items.Add(new SelectListItem() { Value = "4", Text = "Kazançlı Anket", Selected = selected == 4 ? true : false });
            items.Add(new SelectListItem() { Value = "5", Text = "Akademik Anket", Selected = selected == 5 ? true : false });
            items.Add(new SelectListItem() { Value = "6", Text = "Kurum İçi", Selected = selected == 6 ? true : false });
            return items;
        }
        public static IEnumerable<SelectListItem> GetGroupSelect(List<QuestionGroup> groups, int[] selects = null)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (selects == null) selects = new int[] { };
            foreach (QuestionGroup g in groups)
                items.Add(new SelectListItem() { Value = g.GroupId.ToString(), Text = g.Name, Selected = selects.Contains(g.GroupId) });
            return items;
        }
        public static string GetUsageTypeText(UsageType type)
        {
            string text = "";
            switch ((int)type)
            {
                case 0: text = ""; break;
                case 1: text = "Başvuru"; break;
                case 2: text = "Etkinlik"; break;
                case 3: text = "Kampanya"; break;
                case 4: text = "Kazanç"; break;
                case 5: text = "Akademik"; break;
                case 6: text = "Kurum İçi"; break;
                default: text = "Tanımsız"; break;
            }
            return text;
        }
        public static string GetUsageTypeColor(UsageType type)
        {
            string text = "";
            switch ((int)type)
            {
                case 0: text = "7f97a2"; break;
                case 1: text = "da4264"; break;
                case 2: text = "ce6fac"; break;
                case 3: text = "42444e"; break;
                case 4: text = "89a9ee"; break;
                case 5: text = "0087cc"; break;
                case 6: text = "ff5722"; break;
                default: text = "9ac9cb"; break;
            }
            return text;
        }
        public static string GetSurveyTypeText(SurveyType type)
        {
            string text = "";
            switch ((int)type)
            {
                case 0: text = "Genel"; break;
                case 1: text = "Enneagram"; break;
                case 2: text = "Akademik"; break;
                default: text = "Tanımsız"; break;
            }
            return text;
        }
        public static string GetPublishStatusText(PublishStatus type)
        {
            string text = "";
            switch ((int)type)
            {
                case 0: text = ""; break;
                case 1: text = "Onay Verilmedi"; break;
                case 2: text = "Onay Bekliyor"; break;
                case 3: text = "Onay Verildi"; break;
                case 4: text = "Planlandı"; break;
                case 5: text = "Yayınlandı"; break;
                case 6: text = "Yayında Değil"; break;
                case 7: text = "Hazır"; break;
                case 8: text = "Soru Ekleyin"; break;
                case 9: text = "Fazla Soru Tanımlı"; break;
                default: text = "Tanımsız"; break;
            }
            return text;
        }
    }
}

