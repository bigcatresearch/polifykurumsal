﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pollyfy.Web.Core.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class AuthorizeFilter : System.Web.Mvc.AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //bool IsAuthenticated = false;
            //if (HttpContext.Current != null && HttpContext.Current.Request != null)
            //{
            //    IsAuthenticated = HttpContext.Current.Request.IsAuthenticated;
            //}
            //if (httpContext != null && httpContext.Request != null)
            //{
            //    IsAuthenticated = httpContext.Request.IsAuthenticated;
            //}
            //if (IsAuthenticated && (Common.CurrentUser == null || Common.CurrentUser.UserId == 0))
            //    return false;
            return base.AuthorizeCore(httpContext);
        }
    }
}