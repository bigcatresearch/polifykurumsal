﻿using Newtonsoft.Json.Linq;
using Pollyfy.Models;
using Pollyfy.Models.ViewModel;
using Pollyfy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pollyfy.Core.Extensions;
using Newtonsoft.Json;
using Pollyfy.Web.Providers;

namespace Pollyfy.Web.Core
{
    public static class SurveyManager
    {
        public static IEnumerable<Survey> GetSurveysForRemoteUser(int userId, string latitude, string longitude)
        {
            IList<Survey> results = new List<Survey>();
            User user = userId > 0 ? Common.Uow.Users.GetWithProperties(userId) : null;
            if (user == null) return results;
            IEnumerable<Survey> surveys = Common.Uow.Surveys.GetForRemoteUser(userId);
            if (surveys.Count() == 0) return results;
            user = UpdateUserSelfProperties(user);

            if (!string.IsNullOrEmpty(latitude) && !string.IsNullOrEmpty(longitude))
            {
                string city = GetCityFromCoords(latitude, longitude);
                string newLocation = !string.IsNullOrEmpty(city) ? city : user.Location;
                Common.Uow.Users.UpdateCoordinate(userId, latitude, longitude, newLocation);
            }
            foreach (Survey s in surveys)
            {
                bool IsValid = true;
                foreach (int constCatId in s.Constraints.Select(m => m.CategoryId).Distinct())
                {
                    if (!user.Properties.Any(m => s.Constraints.Where(c => c.CategoryId == constCatId).Any(v => v.ValueId == m.ValueId))) IsValid = false;
                    if (IsValid == false) break;
                }
                if (IsValid && s.Scopes.Count > 0)
                {
                    if (string.IsNullOrEmpty(latitude) || string.IsNullOrEmpty(longitude))
                    {
                        latitude = user.Latitude;
                        longitude = user.Longitude;
                    }
                    HelperValue scope = GetScopeFromCoords(latitude, longitude);
                    if (scope == null || !s.Scopes.Any(m => m.ValueId == scope.ValueId)) IsValid = false;
                }

                if (IsValid)
                    results.Add(s);
            }
            if (results.Count > 0)
            {
                foreach (Survey s in results)
                {
                    var FileContainer = new FileContainer()
                    {
                        CompanyId = s.CompanyId,
                        FileName = s.CompanyId.GetIndirectRef(typeof(Company)) + ".png",
                        OwnerId = s.CompanyId,
                        OwnerType = FileOwnerType.Company,
                        FileType = FileType.Image
                    };
                    FileUploadSettings fus = new FileUploadSettings();
                    s.ImageUrl = fus.GetFileLink(FileContainer, true);
                }
            }

            return results;
        }
        public static HelperValue GetScopeFromCoords(string latitude, string longitude)
        {
            HelperValue scope = null;
            if (!string.IsNullOrEmpty(latitude) || !string.IsNullOrEmpty(longitude))
            {
                string url = string.Format("http://maps.googleapis.com/maps/api/geocode/json?latlng={0},{1}&component=administrative_area?key=AIzaSyCpmJnZdsvhjORUl7HGieuEDbLtAAfR6dE", latitude, longitude);
                IWebSuplement web = new WebSuplement();
                JObject data = web.WebRequestAsJson(url);
                List<string> cities = new List<string>();
                if (data != null && data["status"] != null && data["status"].ToString() == "OK")
                {
                    if (data["results"] != null)
                        foreach (var item in data["results"])
                        {
                            if (item["address_components"] == null) continue;
                            foreach (var adress in item["address_components"])
                            {
                                if (adress["types"] != null && adress["types"].ToArray().Contains("administrative_area_level_1"))
                                {
                                    cities.Add(adress["long_name"].ToString().ToLower().Trim());
                                    cities.Add(adress["short_name"].ToString().ToLower().Trim());
                                }
                            }
                        }
                }
                if (cities.Count > 0)
                {
                    var scopes = Common.Uow.HelperValues.GetByCategoryCode("cat_scope");
                    if (scopes != null && scopes.Count() > 0)
                        scope = scopes.Where(m => cities.Distinct().Contains(m.Text.ToLower().Trim())).FirstOrDefault();
                }
            }
            return scope;
        }

        public static string GetCityFromCoords(string latitude, string longitude)
        {
            string city = "";
            if (!string.IsNullOrEmpty(latitude) || !string.IsNullOrEmpty(longitude))
            {
                string url = string.Format("http://maps.googleapis.com/maps/api/geocode/json?latlng={0},{1}&component=administrative_area?key=AIzaSyCpmJnZdsvhjORUl7HGieuEDbLtAAfR6dE", latitude, longitude);
                IWebSuplement web = new WebSuplement();
                JObject data = web.WebRequestAsJson(url);
                List<string> cities = new List<string>();
                if (data != null && data["status"] != null && data["status"].ToString() == "OK")
                {
                    if (data["results"] != null)
                        foreach (var item in data["results"])
                        {
                            if (item["address_components"] == null) continue;
                            foreach (var adress in item["address_components"])
                            {
                                if (adress["types"] != null && adress["types"].ToArray().Contains("administrative_area_level_1"))
                                {
                                    cities.Add(adress["long_name"].ToString().ToLower().Trim());
                                    cities.Add(adress["short_name"].ToString().ToLower().Trim());
                                }
                            }
                        }
                }
                if (cities.Count > 0)
                    city = cities.FirstOrDefault();
            }
            return city;
        }
        public static User UpdateUserSelfProperties(User user)
        {
            if (user == null) return null;
            user = UpdateUserProperties(user);
            return Common.Uow.Users.GetWithProperties(user.UserId);
        }
        public static User UpdateUserProperties(User user)
        {
            if (user == null) return null;
            if (!string.IsNullOrEmpty(user.Gender))
            {
                var genders = Common.Uow.HelperValues.GetByCategoryCode("cat_gender");
                if (genders != null && genders.Count() > 0)
                {
                    var gender = genders.Where(m => m.Value == user.Gender).FirstOrDefault();
                    if (gender != null)
                    {
                        foreach (HelperValue g in user.Properties.Where(m => m.CategoryId == gender.CategoryId && m.ValueId != gender.ValueId))
                            Common.Uow.Users.RemoveUserProperty(user.UserId, g.ValueId);
                        if (!user.Properties.Any(m => m.CategoryId == gender.CategoryId && m.ValueId == gender.ValueId))
                            Common.Uow.Users.AddUserProperty(user.UserId, gender.ValueId);
                    }
                }
            }
            if (user.BirthDate.HasValue)
            {
                var ranges = Common.Uow.HelperValues.GetByCategoryCode("cat_agerange");
                if (ranges != null && ranges.Count() > 0)
                {
                    HelperValue age = null;
                    foreach (HelperValue range in ranges)
                    {
                        var values = range.Value.Split(':');
                        if (values.Length != 2) continue;
                        int start = 0;
                        int end = 0;
                        if (int.TryParse(values[0], out start) && int.TryParse(values[1], out end))
                            if ((DateTime.Now.Year - user.BirthDate.Value.Year) >= start && (DateTime.Now.Year - user.BirthDate.Value.Year) <= end)
                            {
                                age = range;
                                break;
                            }
                    }
                    if (age != null)
                    {
                        foreach (HelperValue a in user.Properties.Where(m => m.CategoryId == age.CategoryId && m.ValueId != age.ValueId))
                            Common.Uow.Users.RemoveUserProperty(user.UserId, a.ValueId);
                        if (!user.Properties.Any(m => m.CategoryId == age.CategoryId && m.ValueId == age.ValueId))
                            Common.Uow.Users.AddUserProperty(user.UserId, age.ValueId);
                    }
                }
            }
            return user;
        }

        public static IEnumerable<Survey> GetFinishedSurveys(int userId)
        {
            User user = Common.Uow.Users.GetByKey(userId);
            if (user == null) return new List<Survey>();
            IEnumerable<Survey> surveys = Common.Uow.Surveys.GetFinishedSurveys(userId);

            if (surveys.Count() > 0)
            {
                foreach (Survey s in surveys)
                {
                    var FileContainer = new FileContainer()
                    {
                        CompanyId = s.CompanyId,
                        FileName = s.CompanyId.GetIndirectRef(typeof(Company)) + ".png",
                        OwnerId = s.CompanyId,
                        OwnerType = FileOwnerType.Company,
                        FileType = FileType.Image
                    };
                    FileUploadSettings fus = new FileUploadSettings();
                    s.ImageUrl = fus.GetFileLink(FileContainer, true);
                }
            }

            return surveys;
        }

        public static IEnumerable<SurveyModel> GetFinishedSurveysForRemoteUser(int userId)
        {
            var sectors = Common.GetCategorySelectList("cat_sector");
            var companies = Common.GetCompanySelectList();
            var surveys = SurveyManager.GetFinishedSurveys(userId);
            var response = surveys.Select(m => new SurveyModel()
            {
                SurveyId = m.SurveyId.GetIndirectRef(typeof(Survey)),
                Description = m.Description ?? "",
                Name = m.Name,
                QuestionCount = m.QuestionCount,
              //  Reward = m.Reward.ToString(),
                ExPoint = m.ExPoint.ToString(),
                Scopes = m.Scopes.Count > 0 ? string.Join(",", m.Scopes.Select(s => s.Text)) : "Tüm Türkiye",
                //Sector = sectors.Any(s => s.Value == m.SectorId.ToString()) ? sectors.First(s => s.Value == m.SectorId.ToString()).Text : "",
                SurveyType = (int)m.SurveyType,
                SurveyTypeText = Common.GetSurveyTypeText(m.SurveyType),
                UsageType = (int)m.UsageType,
                UsageTypeText = WebUtils.GetUsageTypeText(m.UsageType),
                InCompanyUsage = m.InCompanyUsage,
                CompanyName = companies.Any(c => c.Value == m.CompanyId.ToString()) ? companies.First(c => c.Value == m.CompanyId.ToString()).Text : "",
                ImageUrl = m.ImageUrl,
                FinalMessage = m.FinalMessage ?? "",
                Promotion = m.Promotions.Select(p => new PromotionModel()
                {
                    Code = p.Code,
                    CompanyName = p.CompanyName,
                    ContactUrl = p.ContactUrl ?? "",
                    Description = p.Description,
                    Discount = p.Discount ?? "",
                    FinishDate = p.FinishDate,
                    ImageUrl = p.ImageUrl ?? "",
                    StartDate = p.StartDate,
                    Title = p.Title
                }).FirstOrDefault()
            });
            return response;
        }
        public static bool ImportPromotion(string value, int surveyId)
        {
            List<Promotion> promotions = new List<Promotion>();
            if (!string.IsNullOrEmpty(value))
            {
                PromotionImportModel model = JsonConvert.DeserializeObject<PromotionImportModel>(value);
                if (model != null)
                {
                    FileContainer file = new FileContainer()
                    {
                        CompanyId = model.CompanyIdRef.GetDirectRefForInt(typeof(Company)),
                        CompanyIdRef = model.CompanyIdRef,
                        FileName = model.FileName,
                        OwnerType = model.OwnerType,
                        FileType = model.FileType
                    };
                    FileUploadSettings uploader = new FileUploadSettings();
                    string fileUrl = uploader.GetPhysicalFileLink(file);
                    if (!string.IsNullOrEmpty(fileUrl))
                    {
                        ExcellProvider excell = new ExcellProvider();
                        var promoCodes = excell.ImportPromotions(fileUrl);
                        foreach (PromotionImportModel p in promoCodes)
                        {
                            Promotion promo = new Promotion();
                            promo.Code = p.Code;
                            promo.CompanyName = model.CompanyName;
                            promo.ContactUrl = model.ContactUrl;
                            promo.Description = model.Description;
                            promo.Discount = p.Discount;
                            promo.FinishDate = model.FinishDate;
                            promo.ImageUrl = model.ImageUrl;
                            promo.StartDate = model.StartDate;
                            promo.Title = model.Title;
                            promo.SurveyId = surveyId;
                            promotions.Add(promo);
                        }
                        if (promotions.Count > 0)
                            Common.Uow.Promotions.MultipleInsert(promotions);

                    }
                }
            }
            return promotions.Count > 0;
        }
    }
}