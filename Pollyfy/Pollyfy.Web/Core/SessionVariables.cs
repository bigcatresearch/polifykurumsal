﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pollyfy.Web.Core
{
    public class SessionVariables1
    {
        public static void RemoveAll()
        {
            HttpContext.Current.Session.RemoveAll();
        }
        internal static void SetSessionVariable(string key, object value)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null) HttpContext.Current.Session[key] = value;
        }
        internal static object GetSessionVariable(string key)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
                return HttpContext.Current.Session[key];
            return null;
        }
        internal static T GetSessionVariable<T>(string key)
        {
            T rVal = default(T);
            if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[key] != null) rVal = (T)HttpContext.Current.Session[key];
            return rVal;
        }
        public static int? CompanyId { get; set; }
        public static int UserId { get; set; }
        public static User CurrentUser
        {
            get
            {
                return SessionVariables1.GetSessionVariable<User>("CurrentUser");
            }
            set
            {
                User user = value ?? new User();
                CompanyId = user.CompanyId;
                UserId = user.UserId;
                SessionVariables1.SetSessionVariable("CurrentUser", user);
            }
        }
    }
}