﻿using Pollyfy.Models;
using Pollyfy.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using Pollyfy.Core.Extensions;
using Newtonsoft.Json;
using System.Configuration;

namespace Pollyfy.Web.Core
{
    public class FileUploadSettings
    {
        private string RepoDirectory = "..\\FileRepo";
        private string VirtualDirectory = "\\Files";
        private string _cdnAddress = "";
        private string CdnAddress
        {
            get
            {
                if (string.IsNullOrEmpty(_cdnAddress))
                    _cdnAddress = ConfigurationManager.AppSettings["pollyfy:cdn"];
                //if (!_cdnAddress.EndsWith("/")) _cdnAddress += "/";
                return _cdnAddress;
            }
        }
        private FileContainer Container { get; set; }
        public DevExpress.Web.UploadControlValidationSettings UploadValidationSettings(FileContainer container)
        {
            Container = container;
            string[] allowedFileExtensions = new string[] { };
            if (container.FileType == FileType.Image)
                allowedFileExtensions = new string[] { ".jpg", ".jpeg", ".png" };
            else if (container.FileType == FileType.Excell)
                allowedFileExtensions = new string[] { ".xls", ".xlsx" };
            return new DevExpress.Web.UploadControlValidationSettings()
            {
                AllowedFileExtensions = allowedFileExtensions,
                MaxFileSize = 1048576
            };
        }
        public void FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                string path = GetPhysicalPath(Container);
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                if (string.IsNullOrEmpty(Container.FileName))
                    Container.FileName = e.UploadedFile.FileName;
                string resultFilePath = Path.Combine(path, Container.FileName);
                e.UploadedFile.SaveAs(resultFilePath, true);
                if (Container.OwnerType == FileOwnerType.Company)
                {
                    string copyPath = Path.Combine(path, Container.OwnerId.GetIndirectRef(typeof(Company)) + ".png");
                    e.UploadedFile.SaveAs(copyPath, true);
                }
                IUrlResolutionService urlResolver = sender as IUrlResolutionService;
                if (urlResolver != null)
                {
                    Container.Link = GetFileLink(Container);
                    e.CallbackData = JsonConvert.SerializeObject(Container);
                }
            }
        }
        public string GetPhysicalPath(FileContainer container)
        {
            string repo = Path.Combine(System.Web.HttpContext.Current.Request.PhysicalApplicationPath, RepoDirectory);
            string path = Path.Combine(repo, GetPartialPath(container));
            return path;
        }
        public string GetPartialPath(FileContainer container)
        {
            string path = "";
            if (container.OwnerType == FileOwnerType.Temp)
                path = Path.Combine(container.OwnerType.ToString());
            else
                path = Path.Combine(container.CompanyId.ToString(), container.OwnerType.ToString());
            return path;
        }
        public string GetFileLink(FileContainer container, bool IfExists = false)
        {
            if (container == null || string.IsNullOrEmpty(container.FileName)) return "";
            string path = "";
            if (IfExists == false || FileExists(container))
                path = Path.Combine(VirtualDirectory, GetPartialPath(container), container.FileName);
            path = CdnAddress + path.Replace("\\", "/");
            return path;
        }
        public string GetPhysicalFileLink(FileContainer container, bool IfExists = true)
        {
            if (container == null || string.IsNullOrEmpty(container.FileName)) return "";
            string path = Path.Combine(GetPhysicalPath(container), container.FileName);
            if (IfExists == true && !File.Exists(path)) path = "";
            return path;
        }
        public bool FileExists(FileContainer container)
        {
            return File.Exists(GetPhysicalFileLink(container));
        }
    }
}
