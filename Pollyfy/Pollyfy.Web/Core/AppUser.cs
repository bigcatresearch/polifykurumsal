﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace Pollyfy.Web
{
    public class AppUser : ClaimsPrincipal
    {
        public AppUser(ClaimsPrincipal principal) : base(principal) { }
        public int UserId
        {
            get
            {
                int userId = 0;
                int.TryParse(GetValue("UserId"), out userId);
                return userId;
            }
        }
        public int? CompanyId
        {
            get
            {
                int orgId = 0;
                int.TryParse(GetValue("CompanyId"), out orgId);
                if (orgId > 0)
                    return orgId;
                else
                    return null;
            }
        }
        public string UserName
        {
            get
            {
                return GetValue("UserName");
            }
        }

        public string Email
        {
            get
            {
                return GetValue("Email");
            }
        }
        public string FirstName
        {
            get
            {
                return GetValue("FirstName");
            }
        }

        public string LastName
        {
            get
            {
                return GetValue("LastName");
            }
        }
        public UserType Types
        {
            get
            {
                int type = (int)UserType.DashboardUser;
                int.TryParse(GetValue("Types"), out type);
                return (UserType)type;
            }
        }
        private string GetValue(string claimType)
        {
            var claim = this.FindFirst(claimType);
            return claim == null ? "" : claim.Value;
        }

        public ClaimsIdentity FillClaimsIdentity(User user)
        {
            var identity = new ClaimsIdentity(new[] {
                    new Claim("UserId", user.UserId.ToString()),
                    new Claim("CompanyId", user.CompanyId.ToString()),
                    new Claim("UserName", user.UserName),
                    new Claim("FirstName", user.FirstName),
                    new Claim("LastName", user.LastName),
                    new Claim("Email", user.Email),
                    new Claim("Types", ((int)user.Types).ToString())
                }, "ApplicationCookie");
            return identity;
        }

        public string GetFullName()
        {
            return string.Format("{0} {1}", FirstName, LastName);
        }

    }
}