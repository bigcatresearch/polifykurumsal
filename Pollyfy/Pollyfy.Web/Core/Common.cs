﻿using Microsoft.AspNet.Identity;
using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using Pollyfy.Models.ViewModel;
using Pollyfy.Services;
using Pollyfy.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Pollyfy.Web.Core
{
    public class Common
    {
        private static IUow _uow;
        public static IUow Uow
        {
            get
            {
                if (_uow == null || _uow.DbConnection == null || string.IsNullOrEmpty(_uow.DbConnection.ConnectionString))
                    _uow = (IUow)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUow));
                return _uow;
            }
        }
        public static AppUser CurrentUser
        {
            get
            {
                AppUser user = null;
                if (HttpContext.Current != null && HttpContext.Current.User != null)
                {
                    user = new AppUser(HttpContext.Current.User as ClaimsPrincipal);
                }
                return user;
            }
        }

        public static User LoginUser(LoginViewModel model)
        {
            User user = Uow.Users.GetUser(model.Email, model.Password);
            return user;
        }
        public static List<CategoryModel> GetCategoryView()
        {
            List<CategoryModel> model = new List<CategoryModel>();
            var categories = Uow.HelperCategories.GetAll();
            if (categories != null && categories.Count() > 0)
            {
                model = categories.Select(m => new CategoryModel()
                {
                    Id = "c:" + m.CategoryId,
                    Caption = m.Text,
                    IsVisible = m.IsVisible,
                    IsMultiple = m.IsMultiple,
                    CategoryType = (int)m.Types,
                    AlwaysShow = m.AlwaysShow,
                    ParentId = null
                }).ToList();
            }
            var values = Uow.HelperValues.GetAll();
            if (values != null && values.Count() > 0)
            {
                model.AddRange(values.Where(m => categories.Any(c => c.CategoryId == m.CategoryId)).Select(m => new CategoryModel()
                {
                    Id = "v:" + m.ValueId,
                    Caption = m.Text,
                    IsVisible = true,
                    IsMultiple = true,
                    CategoryType = 0,
                    ParentId = "c:" + m.CategoryId
                }).ToList());
            }
            return model;
        }
        public static IEnumerable<SelectListItem> GetCategorySelectList(IEnumerable<HelperCategory> constraints)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (constraints != null && constraints.Count() > 0)
                foreach (HelperCategory c in constraints)
                    items.Add(new SelectListItem() { Value = c.CategoryId.ToString(), Text = c.Text });
            return items;
        }
        public static IEnumerable<SelectListItem> GetCategorySelectList(string code, int? selectedId = null)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (!string.IsNullOrEmpty(code))
            {
                var values = Uow.HelperValues.GetByCategoryCode(code);
                if (values != null && values.Count() > 0)
                    foreach (HelperValue v in values)
                        items.Add(new SelectListItem() { Value = v.ValueId.ToString(), Text = v.Text, Selected = selectedId.HasValue && selectedId.Value == v.ValueId ? true : false });
            }
            return items;
        }

        public static IEnumerable<SelectListItem> GetSurveySelectList(int? selectedId = null)
        {
            List<SelectListItem> items = new List<SelectListItem>();

                IEnumerable<Survey> surveys = new List<Survey>();

                if (CurrentUser.Types == UserType.SystemAdmin)
                    surveys = Uow.Surveys.GetForAll();
                else if (CurrentUser.Types == UserType.CompanyAdmin && CurrentUser.CompanyId.HasValue)
                    surveys = Uow.Surveys.GetByCompanyId(CurrentUser.CompanyId.Value);
                else if (CurrentUser.Types == UserType.CompanyUser)
                    surveys = Uow.Surveys.GetByUserId(CurrentUser.UserId);

                if (surveys != null && surveys.Count() > 0)

                    foreach (var survey in surveys)
                        items.Add(new SelectListItem() { Value = survey.SurveyId.ToString(), Text = survey.Name, Selected = selectedId.HasValue && selectedId.Value == survey.SurveyId ? true : false });

            return items;
        }

        public static IEnumerable<SelectListItem> GetCategorySelectList(int categoryId, int? selectedId = null)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (categoryId > 0)
            {
                var values = Uow.HelperValues.GetByCategoryId(categoryId);
                if (values != null && values.Count() > 0)
                    foreach (HelperValue v in values)
                        items.Add(new SelectListItem() { Value = v.ValueId.ToString(), Text = v.Text, Selected = selectedId.HasValue && selectedId.Value == v.ValueId ? true : false });
            }
            return items;
        }
        public static IEnumerable<SelectListItem> GetCompanySelectList(int? selectedId = null)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var values = Uow.Companies.GetAll(ActiveStatus.Active);
            if (values != null && values.Count() > 0)
                foreach (Company v in values)
                    items.Add(new SelectListItem() { Value = v.CompanyId.ToString(), Text = v.CompanyName, Selected = selectedId.HasValue && selectedId.Value == v.CompanyId ? true : false });
            return items;
        }
        public static string GetSurveyTypeText(SurveyType type)
        {
            string text = "";
            switch ((int)type)
            {
                case 0: text = "Genel"; break;
                case 1: text = "Kişilik Testi"; break;
                case 2: text = "Akademik"; break;
                case 3: text = "Vitrin"; break;
                case 4: text = "Pollyfy Araştırma"; break;
                default: text = "Tanımsız"; break;
            }
            return text;
        }
        public static string GetEnneagramTypeText(EnneagramType type)
        {
            string text = "";
            switch ((int)type)
            {
                case 1: text = "Mükemmelliyetçilik"; break;
                case 2: text = "Yardımseverlik"; break;
                case 3: text = "Başarı Odaklılık"; break;
                case 4: text = "Özgünkük"; break;
                case 5: text = "Araştırmacı"; break;
                case 6: text = "Sorgulayıcı"; break;
                case 7: text = "Maceracı"; break;
                case 8: text = "Meydan Okuyan"; break;
                case 9: text = "Barışçı"; break;
                default: text = "Tanımsız"; break;
            }
            return text;
        }
        public static string GetQuestionTypeText(QuestionType type)
        {
            string text = "";
            switch ((int)type)
            {
                case 0: text = "Tek Seçmeli"; break;
                case 1: text = "Çok Seçmeli"; break;
                case 2: text = "Açık Uçlu"; break;
                case 3: text = "Resimli"; break;
                case 4: text = "Progres"; break;
                case 5: text = "Emoji"; break;

                default: text = "Tanımsız"; break;
            }
            return text;
        }
        public static string UpdateUserPassword(int? userId, string email = "", string password = "")
        {
            User user = null;
            if (userId.HasValue && userId.Value > 0)
                user = Uow.Users.GetUserByUserId(userId.Value);
            else if (!string.IsNullOrEmpty(email))
                user = Uow.Users.GetUserByUserName(email); ;
            if (user != null)
            {
                if (string.IsNullOrEmpty(password))
                {
                    Random r = new Random();
                    password = r.Next(100000, 999999).ToString();
                }
                string hashedPassword = new PasswordHasher().HashPassword(password);
                user.Password = hashedPassword;
                Uow.Users.DoUpdate(user);
            }
            else
                password = "";
            return password;
        }

        public static bool SendEmail(User toUser, EmailType emailType, Dictionary<string, string> parameters = null)
        {
            //return true;
            if (parameters == null) parameters = new Dictionary<string, string>();
            MessagePackage package = new MessagePackage();
            package.ToEmail = toUser.Email;
            parameters.Add("@fullname", toUser.GetFullName());
            if (emailType == EmailType.PasswordRecovery)
            {
                package.Subject = "Pollyfy - Şifre Sıfırlama";
                #region Template
                package.Body = @"    <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><title>@subject</title><style type='text/css'>
      body {
       padding-top: 0 !important;
       padding-bottom: 0 !important;
       padding-top: 0 !important;
       padding-bottom: 0 !important;
       margin:0 !important;
       width: 100% !important;
       -webkit-text-size-adjust: 100% !important;
       -ms-text-size-adjust: 100% !important;
       -webkit-font-smoothing: antialiased !important;
     }
     .tableContent img {
       border: 0 !important;
       display: block !important;
       outline: none !important;
     }
     a{
      color:#382F2E;
    }

    p, h1{
      color:#382F2E;
      margin:0;
    }
 p{
      text-align:left;
      color:#999999;
      font-size:14px;
      font-weight:normal;
      line-height:19px;
    }

    a.link1{
      color:#382F2E;
    }
    a.link2{
      font-size:16px;
      text-decoration:none;
      color:#ffffff;
    }

    h2{
      text-align:left;
       color:#222222; 
       font-size:19px;
      font-weight:normal;
    }
    div,p,ul,h1{
      margin:0;
    }

    .bgBody{
      background: #ffffff;
    }
    .bgItem{
      background: #ffffff;
    }
	
@media only screen and (max-width:480px)
		
{
		
table[class='MainContainer'], td[class='cell'] 
	{
		width: 100% !important;
		height:auto !important; 
	}
td[class='specbundle'] 
	{
		width:100% !important;
		float:left !important;
		font-size:13px !important;
		line-height:17px !important;
		display:block !important;
		padding-bottom:15px !important;
	}
		
td[class='spechide'] 
	{
		display:none !important;
	}
	    img[class='banner'] 
	{
	          width: 100% !important;
	          height: auto !important;
	}
		td[class='left_pad'] 
	{
			padding-left:15px !important;
			padding-right:15px !important;
	}
		 
}
	
@media only screen and (max-width:540px) 

{
		
table[class='MainContainer'], td[class='cell'] 
	{
		width: 100% !important;
		height:auto !important; 
	}
td[class='specbundle'] 
	{
		width:100% !important;
		float:left !important;
		font-size:13px !important;
		line-height:17px !important;
		display:block !important;
		padding-bottom:15px !important;
	}
		
td[class='spechide'] 
	{
		display:none !important;
	}
	    img[class='banner'] 
	{
	          width: 100% !important;
	          height: auto !important;
	}
	.font {
		font-size:18px !important;
		line-height:22px !important;
		
		}
		.font1 {
		font-size:18px !important;
		line-height:22px !important;
		
		}
}

    </style><script type='colorScheme' class='swatch active'>
{
    'name':'Default',
    'bgBody':'ffffff',
    'link':'382F2E',
    'color':'999999',
    'bgItem':'ffffff',
    'title':'222222'
}
</script></head><body paddingwidth='0' paddingheight='0'   style='text-align:left !important; padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;' offset='0' toppadding='0' leftpadding='0'><table bgcolor='#ffffff' width='100%' border='0' cellspacing='0' cellpadding='0' class='tableContent' align='center'  style='font-family:Helvetica, Arial,serif;'><tbody><tr><td><table width='600' border='0' cellspacing='0' cellpadding='0' align='center' bgcolor='#ffffff' class='MainContainer'><tbody><tr><td><table width='100%' border='0' cellspacing='0' cellpadding='0'><tbody><tr><td valign='top' width='40'>&nbsp;</td><td><table width='100%' border='0' cellspacing='0' cellpadding='0'><tbody><tr><td height='10' class='spechide'></td></tr><tr><td class='movableContentContainer ' valign='top'><div class='movableContent' style='border: 0px; padding-top: 0px; position: relative;'></div><div class='movableContent' style='border: 0px; padding-top: 0px; position: relative;'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'><tr><td height='55'></td></tr><tr><td align='left'><div class='contentEditableContainer contentTextEditable'><div class='contentEditable' align='center'><h2>Sifreniz sıfırlandı!</h2></div></div></td></tr><tr><td height='15'></td></tr><tr><td align='left'><div class='contentEditableContainer contentTextEditable'><div class='contentEditable' align='center'><p >
                                    Sayın <b>@fullname</b>,
									<br>
									Yeni şifreniz <b>@password</b> olarak tanımlandı. Artık eski şifrenizi kullanamazsınız.
                                    <br><br>
                                    Her türlü istek ve şikayetlerinizi iletişim kanallarımızdan bize ulaştırabilirsiniz.
                                    <br><br>
                                    Saygılarımızla,
                                    <span style='color:#222222;'>Pollyfy.com</span></p></div></div></td></tr><tr><td height='55'></td></tr><tr><td height='20'></td></tr></table></div><div class='movableContent' style='border: 0px; padding-top: 0px; position: relative;'><table width='100%' border='0' cellspacing='0' cellpadding='0'><tbody><tr><td height='65'></tr><tr><td  style='border-bottom:1px solid #DDDDDD;'></td></tr><tr><td height='25'></td></tr><tr><td></td></tr><tr><td height='88'></td></tr></tbody></table></div></td></tr></tbody></table></td><td valign='top' width='40'>&nbsp;</td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></body></html>


";
                #endregion
            }
            if (emailType == EmailType.NewUser)
            {
                package.Subject = "Pollyfy'a Hoşgeldiniz";
                #region Template
                package.Body = @"    <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><title>@subject</title><style type='text/css'>
      body {
       padding-top: 0 !important;
       padding-bottom: 0 !important;
       padding-top: 0 !important;
       padding-bottom: 0 !important;
       margin:0 !important;
       width: 100% !important;
       -webkit-text-size-adjust: 100% !important;
       -ms-text-size-adjust: 100% !important;
       -webkit-font-smoothing: antialiased !important;
     }
     .tableContent img {
       border: 0 !important;
       display: block !important;
       outline: none !important;
     }
     a{
      color:#382F2E;
    }

    p, h1{
      color:#382F2E;
      margin:0;
    }
 p{
      text-align:left;
      color:#999999;
      font-size:14px;
      font-weight:normal;
      line-height:19px;
    }

    a.link1{
      color:#382F2E;
    }
    a.link2{
      font-size:16px;
      text-decoration:none;
      color:#ffffff;
    }

    h2{
      text-align:left;
       color:#222222; 
       font-size:19px;
      font-weight:normal;
    }
    div,p,ul,h1{
      margin:0;
    }

    .bgBody{
      background: #ffffff;
    }
    .bgItem{
      background: #ffffff;
    }
	
@media only screen and (max-width:480px)
		
{
		
table[class='MainContainer'], td[class='cell'] 
	{
		width: 100% !important;
		height:auto !important; 
	}
td[class='specbundle'] 
	{
		width:100% !important;
		float:left !important;
		font-size:13px !important;
		line-height:17px !important;
		display:block !important;
		padding-bottom:15px !important;
	}
		
td[class='spechide'] 
	{
		display:none !important;
	}
	    img[class='banner'] 
	{
	          width: 100% !important;
	          height: auto !important;
	}
		td[class='left_pad'] 
	{
			padding-left:15px !important;
			padding-right:15px !important;
	}
		 
}
	
@media only screen and (max-width:540px) 

{
		
table[class='MainContainer'], td[class='cell'] 
	{
		width: 100% !important;
		height:auto !important; 
	}
td[class='specbundle'] 
	{
		width:100% !important;
		float:left !important;
		font-size:13px !important;
		line-height:17px !important;
		display:block !important;
		padding-bottom:15px !important;
	}
		
td[class='spechide'] 
	{
		display:none !important;
	}
	    img[class='banner'] 
	{
	          width: 100% !important;
	          height: auto !important;
	}
	.font {
		font-size:18px !important;
		line-height:22px !important;
		
		}
		.font1 {
		font-size:18px !important;
		line-height:22px !important;
		
		}
}

    </style><script type='colorScheme' class='swatch active'>
{
    'name':'Default',
    'bgBody':'ffffff',
    'link':'382F2E',
    'color':'999999',
    'bgItem':'ffffff',
    'title':'222222'
}
</script></head><body paddingwidth='0' paddingheight='0'   style='text-align:left !important; padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;' offset='0' toppadding='0' leftpadding='0'><table bgcolor='#ffffff' width='100%' border='0' cellspacing='0' cellpadding='0' class='tableContent' align='center'  style='font-family:Helvetica, Arial,serif;'><tbody><tr><td><table width='600' border='0' cellspacing='0' cellpadding='0' align='center' bgcolor='#ffffff' class='MainContainer'><tbody><tr><td><table width='100%' border='0' cellspacing='0' cellpadding='0'><tbody><tr><td valign='top' width='40'>&nbsp;</td><td><table width='100%' border='0' cellspacing='0' cellpadding='0'><tbody><tr><td height='10' class='spechide'></td></tr><tr><td class='movableContentContainer ' valign='top'><div class='movableContent' style='border: 0px; padding-top: 0px; position: relative;'></div><div class='movableContent' style='border: 0px; padding-top: 0px; position: relative;'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'><tr><td height='55'></td></tr><tr><td align='left'><div class='contentEditableContainer contentTextEditable'><div class='contentEditable' align='center'><h2>Pollyfy'a hoşgeldiniz!</h2></div></div></td></tr><tr><td height='15'></td></tr><tr><td align='left'><div class='contentEditableContainer contentTextEditable'><div class='contentEditable' align='center'><p >
                                    Sayın <b>@fullname</b>,
									<br>
									Pollyfy ailesine katıldığınız için teşekkür ederiz.
                                    <br><br>
                                    Her türlü istek ve şikayetlerinizi iletişim kanallarımızdan bize ulaştırabilirsiniz.
                                    <br><br>
                                    Saygılarımızla,
                                    <span style='color:#222222;'>Pollyfy.com</span></p></div></div></td></tr><tr><td height='55'></td></tr><tr><td height='20'></td></tr></table></div><div class='movableContent' style='border: 0px; padding-top: 0px; position: relative;'><table width='100%' border='0' cellspacing='0' cellpadding='0'><tbody><tr><td height='65'></tr><tr><td  style='border-bottom:1px solid #DDDDDD;'></td></tr><tr><td height='25'></td></tr><tr><td></td></tr><tr><td height='88'></td></tr></tbody></table></div></td></tr></tbody></table></td><td valign='top' width='40'>&nbsp;</td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></body></html>

";
                #endregion
            }
            parameters.Add("@subject", package.Subject);
            foreach (var item in parameters)
                package.Body = package.Body.Replace(string.Format("{0}", item.Key), item.Value);

            IEmailService emailService = new SMTPService();
            return emailService.Send(package);
        }
        public static string AbsoluteUri { get; set; }
    }
}