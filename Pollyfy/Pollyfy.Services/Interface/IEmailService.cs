﻿using Newtonsoft.Json.Linq;
using Pollyfy.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Services
{
    public interface IEmailService
    {
        bool Send(MessagePackage mailMessage);
        Task<bool> SendAsync(MessagePackage mailMessage);
    }
}
