﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Services
{
    public interface IWebSuplement
    {
        string WebRequest(string url);
        Task<string> WebRequestAsync(string url);
        JObject WebRequestAsJson(string url);
        Task<JObject> WebRequestAsJsonAsync(string url);
    }
}
