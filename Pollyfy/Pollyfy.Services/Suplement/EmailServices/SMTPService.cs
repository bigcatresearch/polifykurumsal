﻿using Pollyfy.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Services
{
    public class SMTPService : IEmailService
    {
        public bool Send(MessagePackage mailMessage)
        {
            try
            {
                SmtpClient smtp = new SmtpClient("smtp.gmail.com") { Port = 587 };
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential("pollyfy@gmail.com", "Pollyfy09.");
                smtp.EnableSsl = true;
                MailMessage ePosta = new MailMessage();
                ePosta.From = new MailAddress("pollyfy@gmail.com", "Pollyfy");
                ePosta.To.Add(mailMessage.ToEmail);
                ePosta.Subject = mailMessage.Subject;
                ePosta.IsBodyHtml = true;
                ePosta.Body = mailMessage.Body;
                smtp.Send(ePosta);
                return true;
            }
            catch
            {
                return false;
            }

        }
        public async Task<bool> SendAsync(MessagePackage mailMessage)
        {
            return true;
        }
    }
}