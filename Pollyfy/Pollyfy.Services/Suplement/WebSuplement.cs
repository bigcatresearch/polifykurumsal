﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Services
{
    public class WebSuplement : IWebSuplement
    {
        public string WebRequest(string url)
        {
            string content = "";
            HttpWebRequest myRequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
            myRequest.Credentials = CredentialCache.DefaultCredentials;
            using (WebResponse webResponse = myRequest.GetResponse())
            using (Stream respStream = webResponse.GetResponseStream())
            using (StreamReader ioStream = new StreamReader(respStream))
            {
                content = ioStream.ReadToEnd();
                ioStream.Close();
                respStream.Close();
            }
            return content;
        }
        public async Task<string> WebRequestAsync(string url)
        {
            string content = "";
            HttpWebRequest myRequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
            myRequest.Credentials = CredentialCache.DefaultCredentials;
            using (WebResponse webResponse = await myRequest.GetResponseAsync())
            using (Stream respStream = webResponse.GetResponseStream())
            using (StreamReader ioStream = new StreamReader(respStream))
            {
                content = await ioStream.ReadToEndAsync();
                ioStream.Close();
                respStream.Close();
            }
            return content;
        }
        public JObject WebRequestAsJson(string url)
        {
            string content = "";
            HttpWebRequest myRequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
            myRequest.Credentials = CredentialCache.DefaultCredentials;
            using (WebResponse webResponse = myRequest.GetResponse())
            using (Stream respStream = webResponse.GetResponseStream())
            using (StreamReader ioStream = new StreamReader(respStream))
            {
                content = ioStream.ReadToEnd();
                ioStream.Close();
                respStream.Close();
            }
            JObject data = JObject.Parse(content);
            return data;
        }
        public async Task<JObject> WebRequestAsJsonAsync(string url)
        {
            string content = "";
            HttpWebRequest myRequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
            myRequest.Credentials = CredentialCache.DefaultCredentials;
            using (WebResponse webResponse = await myRequest.GetResponseAsync())
            using (Stream respStream = webResponse.GetResponseStream())
            using (StreamReader ioStream = new StreamReader(respStream))
            {
                content = await ioStream.ReadToEndAsync();
                ioStream.Close();
                respStream.Close();
            }
            JObject data = JObject.Parse(content);
            return data;
        }
    }
}
