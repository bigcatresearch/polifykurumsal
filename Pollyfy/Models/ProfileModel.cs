﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pollyfy.Models
{
    public class ProfileModel
    {
        public int ID { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public int AnketSayisi { get; set; }
        public int KazancTutari { get; set; }
        public string Base64Image { get; set; }
    }
}