﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pollyfy.Models
{
    public class SurveyModel
    {
        public string Firma_ismi { get; set; }
        public string Aciklama { get; set; }
        public string Lokasyon { get; set; }
        public int SoruSayisi { get; set; }
        public int Ucret { get; set; }
        public string Kategori { get; set; }
        public string FirmaFoto { get; set; }
        public string AnketAdi { get; set; }
        public int BitisZamani { get; set; }
        public int BitisSayisi { get; set; }
        public int CinsiyetKapsam { get; set; }
        public int YasKapsam { get; set; }
    }

}