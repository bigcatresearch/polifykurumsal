﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;
namespace Pollyfy.Models
{
    public class PollyfyMemberShipProvider:MembershipProvider
    {
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool EnablePasswordReset
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations", Justification = "none")]
        public override bool EnablePasswordRetrieval
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { return 6; }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations", Justification = "none")]
        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { return false; }
        }

        public static string GetMD5Hash(string value)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sbuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sbuilder.Append(data[i].ToString("x2"));
            }

            return sbuilder.ToString();
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            MemberUserManager userRep = new MemberUserManager();
            int result = userRep.ChangePassword(username, GetMD5Hash(oldPassword), GetMD5Hash(newPassword));
            if (result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            MemberUserManager userRep = new MemberUserManager();
            User user = userRep.GetAllUsers().FirstOrDefault(u => u.Email == username);

            if (user != null)
            {
                MembershipUser memUser = new MembershipUser(
                                                            "DtasUserMembershipProvider",
                                                            username,
                                                            user.ID,
                                                            string.Empty,
                                                            string.Empty,
                                                            string.Empty,
                                                            true,
                                                            false,
                                                            DateTime.MinValue,
                                                            DateTime.MinValue,
                                                            DateTime.MinValue,
                                                            DateTime.Now,
                                                            DateTime.Now);
                return memUser;
            }

            return null;
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            try
            {
                ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, password, true);
                this.OnValidatingPassword(args);

                if (args.Cancel)
                {
                    status = MembershipCreateStatus.InvalidPassword;
                    return null;
                }

                if (this.RequiresUniqueEmail && this.GetUserNameByEmail(email) != string.Empty)
                {
                    status = MembershipCreateStatus.DuplicateEmail;
                    return null;
                }

                MembershipUser user = this.GetUser(username, true);

                if (user == null)
                {
                    status = MembershipCreateStatus.Success;
                    return this.GetUser(username, true);
                }
                else
                {
                    status = MembershipCreateStatus.DuplicateUserName;
                }

                return null;
            }
            catch (Exception)
            {
                status = MembershipCreateStatus.ProviderError;
                return null;
            }
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password)
        {
            if (username != null & password != null)
            {
                string sha1Pswd = GetMD5Hash(password);
                MemberUserManager user = new MemberUserManager();
                User userObj = user.GetUserObjByUserName(username, sha1Pswd);
                if (userObj != null)
                {
                    return true;
                }
            }

            return false;
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        private string StringiNullDanKurtar(string veri)
        {
            string yanıt = string.Empty;
            if (veri != null)
            {
                yanıt = veri;
            }

            return yanıt;
        }
    }
}