﻿#region 

using System.Collections.Generic;
using Pollyfy.Models;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Data.Interfaces
{
    public interface IUserRoleAuthorizationRepository : IRepository<UserRoleAuthorization>
    {
        IEnumerable<UserRoleAuthorization> GetAll(ActiveStatus? status = null);

        IEnumerable<UserRoleAuthorization> GetByUserRoleId(int userRoleId);
    }
}