﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Interfaces
{
    public interface IChoiceRepository : IRepository<Choice>
    {
        IEnumerable<Choice> GetByQuestionId(int questionId);
        bool UpdateStatus(int choiceId, ActiveStatus status);
    }
}
