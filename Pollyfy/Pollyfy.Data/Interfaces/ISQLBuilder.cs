﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Interfaces
{
    public interface ISQLBuilder
    {
        string InsertSqlTemplate<T>(bool IdentityInsertAllowed = false);
        string UpdateSqlTemplate<T>();
        string DeleteSqlTemplate<T>();
        string SelectSqlTemplate<T>();
        string SelectSqlTemplate<T>(Expression<Func<T, object>> predicate);
        string SelectByKeyTemplate<T>(int key);
        IDictionary<string, string> GetFields<T>(out string primaryKey, out string tableName);
        IDictionary<string, object> GetParameterValues(object obj);
        IDictionary<string, string> FindPrimaryKey<T>();
        string UpdateStatusSqlTemplate<T>();
    }
}
