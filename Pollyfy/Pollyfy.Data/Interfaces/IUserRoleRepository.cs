﻿#region 

using System.Collections.Generic;
using Pollyfy.Models;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Data.Interfaces
{
    public interface IUserRoleRepository : IRepository<UserRole>
    {
        IEnumerable<UserRole> GetAll(ActiveStatus? activeStatus = null);

        bool UpdateStatus(int surveyId, ActiveStatus status);
    }
}
