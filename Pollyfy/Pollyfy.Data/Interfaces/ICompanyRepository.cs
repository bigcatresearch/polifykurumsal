﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Interfaces
{
    public interface ICompanyRepository : IRepository<Company>
    {
        IEnumerable<Company> GetAll(ActiveStatus? status = null);

        bool UpdateStatus(int companyId, ActiveStatus status);
    }
}
