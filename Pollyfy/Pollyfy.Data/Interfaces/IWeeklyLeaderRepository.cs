﻿#region

using Pollyfy.Models;
using Pollyfy.Models.DataModel;
using System.Collections.Generic;

#endregion

namespace Pollyfy.Data.Interfaces
{
    public interface IWeeklyLeaderRepository : IRepository<WeeklyLeader>
    {
        IEnumerable<WeeklyLeader> GetAll(ActiveStatus? status = null);

        WeeklyLeader GetByUserId(int userId);

        bool DeleteAll();
    }
}