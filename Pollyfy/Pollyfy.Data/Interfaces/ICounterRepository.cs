﻿#region

using Pollyfy.Models.DataModel;
using System.Collections.Generic;

#endregion

namespace Pollyfy.Data.Interfaces
{
    public interface ICounterRepository : IRepository<Counter>
    {
        IEnumerable<Counter> GetAll();

        Counter GetByParams(string key, string param1, string param2, string param3);
    }
}