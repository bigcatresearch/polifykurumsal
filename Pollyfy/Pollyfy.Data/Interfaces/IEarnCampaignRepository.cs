﻿using Pollyfy.Models;
using Pollyfy.Models.DataModel;
using System.Collections.Generic;

namespace Pollyfy.Data.Interfaces
{
    public interface IEarnCampaignRepository : IRepository<EarnCampaign>
    {
        IEnumerable<EarnCampaign> GetAll(ActiveStatus? status = null);
    }
}