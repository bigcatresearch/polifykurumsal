﻿using Pollyfy.Models;
using Pollyfy.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Interfaces
{
    public interface IHelperCategoryRepository : IRepository<HelperCategory>
    {
        IEnumerable<HelperCategory> GetAll();
        IEnumerable<HelperCategory> GetVisiblesByType(CategoryType types);
        IEnumerable<HelperCategory> GetByType(CategoryType types);
        bool UpdateStatus(int categoryId, ActiveStatus status);
    }
}
