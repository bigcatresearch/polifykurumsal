﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Interfaces
{
    public interface IQuestionGroupRepository : IRepository<QuestionGroup>
    {
        IEnumerable<QuestionGroup> GetBySurveyId(int surveyId);
        void DeleteBySurveyId(int surveyId);

        IEnumerable<ChoiceGroup> GetByQuestionId(int questionId);
        void DeleteByChoiceId(int choiceId);
        IEnumerable<QuestionGroup> GetUserGroups(int surveyId, int userId, int enrollmentId);
    }
}
