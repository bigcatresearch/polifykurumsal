﻿using Pollyfy.Models;
using Pollyfy.Models.DataModel;
using System.Collections.Generic;

namespace Pollyfy.Data.Interfaces
{
    public interface ICampaignRepository : IRepository<Campaign>
    {
        IEnumerable<Campaign> GetAll(ActiveStatus? status = null);

        bool UpdateStatus(int campaignId, ActiveStatus status);
    }
}