﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Interfaces
{
    public interface IRepositoryBase<T> : IDisposable where T : class
    {
        IUow Uow { get; set; }
        //IList<T> GetAll();
        int DoInsert(T model, bool identityInsert = false);
        Task<int> DoInsertAsync(T model, bool identityInsert = false);
        int DoInsert(IEnumerable<T> models, bool identityInsert = false);
        int DoInsert<T1>(IEnumerable<T1> models, bool identityInsert = false);
        int DoUpdate(T model);
        int DoUpdate(IEnumerable<T> models);
        int DoDelete(T model);
        int DoDelete(IEnumerable<T> models);
        int DoDelete(IEnumerable<int> Ids);
        int DoDelete(int Id);
        T GetByKey(int id);
        bool CanDispose { get; set; }
        void Dispose(bool force);
    }
}

