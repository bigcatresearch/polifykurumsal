﻿using Pollyfy.Models;
using Pollyfy.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Interfaces
{
    public interface IEnrollmentRepository : IRepository<Enrollment>
    {
        IEnumerable<Enrollment> GetBySurveyId(int surveyId);
        IEnumerable<ParticipantModel> GetParticipants(int surveyId, EnrollmentStatus status);
        IEnumerable<ConstraintModel> GetConstraintCounts(int surveyId);
        IEnumerable<Coord> GetParticipantCoords(int surveyId);
        IEnumerable<ConstraintModel> GetUserConstraints(int surveyId);
        IEnumerable<Enrollment> GetBySurveyIds(string surveyIds, ActiveStatus activeStatus);
    }
}
