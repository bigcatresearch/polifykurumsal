﻿using Pollyfy.Models;
using Pollyfy.Models.DataModel;
using Pollyfy.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        IEnumerable<User> GetAll(int? companyId);
        IEnumerable<User> GetList(ActiveStatus? activeStatus = null);
        User GetUser(string userName, string password);
        User GetUserByUserName(string userName);
        User GetUserByFacebookId(string facebookId);
        User GetUserByImei(string imei);
        User GetUserByReferanceNumber(string referanceNumber);
        User GetUserByUserId(int userId);
        IEnumerable<UserPropertyModel> GetProperties(int userId, CategoryType types = CategoryType.SurveyProperties);
        int AddUserProperty(int userId, int valueId);
        int RemoveUserProperty(int userId, int valueId);
        User GetWithProperties(int userId);
        int MultipleInsert(IEnumerable<User> models);
        void UpdateCoordinate(int userId, string latitude, string longitude, string location);
        Task UpdateAccessTimeAsync(int userId);
        void UpdateAccessTime(int userId);
        DashboardModel GetMainDashboardData(int? companyId, UserType userType);
        int GetTotalResponseCount(int userId);
        IEnumerable<string> GetByConstaint(string tablename);
        int TitleInsert(string Tablename, string Titlename);
        IEnumerable<string> GetValuesByConstaint(string Constaint, string TableName);
        IEnumerable<string> GetTableName();
        IEnumerable<string> GetEmail(string sql, string Tablename);
    }
}

