﻿#region 

using System.Collections.Generic;
using Pollyfy.Models;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Data.Interfaces
{
    public interface IAuthorizationRepository : IRepository<Authorization>
    {
        IEnumerable<Authorization> GetAll(ActiveStatus? status = null);
    }
}