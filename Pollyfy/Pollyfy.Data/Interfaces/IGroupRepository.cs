﻿#region

using Pollyfy.Models;
using Pollyfy.Models.DataModel;
using System.Collections.Generic;

#endregion

namespace Pollyfy.Data.Interfaces
{
    public interface IGroupRepository : IRepositoryBase<Group>
    {
        IEnumerable<Group> GetAll(ActiveStatus? status = null);

        IEnumerable<Group> GetBySurveyId(int surveyId);

        IEnumerable<Group> GetByGorupIds(string goupIds, ActiveStatus activeStatus);
    }
}