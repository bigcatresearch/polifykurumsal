﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Interfaces
{
    public interface IRepository<T> : IRepositoryBase<T> where T : class
    {
        IQueryable<T> GetAll();
        T GetById(int id);
        void Add(T entity);

        Task AddAsync(T entity);
        void AddWithoutCommit(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(int id);
        IQueryable<T> Find(Expression<Func<T, bool>> predicate);
        IQueryable<T> QueryWithEagerLoad(Expression<Func<T, bool>> filter, params Expression<Func<T, object>>[] children);
        IQueryable<T> GetAllEagerLoad(params Expression<Func<T, object>>[] children);
        //IBlmsUow Uow { get; set; }

        void LoadCollectionsExplicity(T entity, params string[] collections);
        void LoadReferencesExplicity(T entity, params string[] references);
        void LoadCollectionsExplicity(T entity, Expression<Func<T, ICollection<object>>> collection, Expression<Func<object, bool>> criteriaExpression);
    }
}
