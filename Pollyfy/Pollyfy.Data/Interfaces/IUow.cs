﻿using System;
using System.Data;
using System.Threading.Tasks;

namespace Pollyfy.Data.Interfaces
{
    /// <summary>
    /// Interface for the Code Camper "Unit of Work"
    /// </summary>
    public interface IUow : IDisposable
    {
        IDbConnection DbConnection { get; }
        ISQLBuilder SQLBuilder { get; }
        void Commit();
        Task CommitAsync();

        ILogRepository Logs { get; }
        IUserRepository Users { get; }
        IHelperCategoryRepository HelperCategories { get; }
        IHelperValueRepository HelperValues { get; }
        ISurveyRepository Surveys { get; }
        IEnrollmentRepository Enrollments { get; }
        IQuestionRepository Questions { get; }
        IChoiceRepository Choices { get; }
        IAnswerRepository Answers { get; }
        IHelpdeskRepository Helpdesks { get; }
        ICompanyRepository Companies { get; }
        IDeviceRepository Devices { get; }
        IPromotionRepository Promotions { get; }
        IQuestionGroupRepository QuestionGroups { get; }
        IUserRoleRepository UserRoles { get; }
        IAuthorizationRepository Authorizations { get; }
        IUserRoleAuthorizationRepository UserRoleAuthorizations { get; }
        ICampaignRepository Campaigns { get; }
        IEarnCampaignRepository EarnCampaigns { get; }
        IWeeklyLeaderRepository WeeklyLeaders { get; }
        IGroupRepository Groups { get; }
        ICounterRepository Counters { get; }
        ITemporaryExpointRepository TemporaryExpoints { get; }
    }
}