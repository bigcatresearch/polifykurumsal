﻿#region 

using System.Collections.Generic;
using Pollyfy.Models;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Data.Interfaces
{
    public interface ITemporaryExpointRepository : IRepository<TemporaryExpoint>
    {
        IEnumerable<TemporaryExpoint> GetAll(ActiveStatus? status = null);

        TemporaryExpoint GetByUserId(int userId);

        bool DeleteAll();
    }
}