﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Interfaces
{
    public interface IHelperValueRepository : IRepository<HelperValue>
    {
        IEnumerable<HelperValue> GetAll();
        IEnumerable<HelperValue> GetByCategoryId(int categoryId);
        IEnumerable<HelperValue> GetByCategoryCode(string categoryCode);
        bool UpdateStatus(int valueId, ActiveStatus status);
    }
}
