﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Interfaces
{
    public interface IQuestionRepository : IRepository<Question>
    {
        IEnumerable<Question> GetBySurveyId(int surveyId);
        IEnumerable<Question> GetWithChoicesBySurveyId(int surveyId);
        Question GetWithChoicesByQuestionId(int questionId);
        bool UpdateStatus(int questionId, ActiveStatus status);
    }
}
