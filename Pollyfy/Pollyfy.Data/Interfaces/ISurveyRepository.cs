﻿using Pollyfy.Models;
using Pollyfy.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Interfaces
{
    public interface ISurveyRepository : IRepository<Survey>
    {
        IEnumerable<Survey> GetForAll();
        Survey GetForAll(int surveyId);
        bool InsertScopes(int surveyId, int[] valueIds);
        IEnumerable<int> SelectScopes(int surveyId);
        bool InsertConstraints(int surveyId, int[] valueIds);
        IEnumerable<int> SelectConstraints(int surveyId);
        int InsertUsers(int surveyId, int[] userIds);
        IEnumerable<int> SelectUsers(int surveyId);
        IEnumerable<Survey> GetByUserId(int userId);
        IEnumerable<Survey> GetByCompanyId(int companyId);
        bool UpdateStatus(int surveyId, ActiveStatus status);
        bool DeleteConstraints(int surveyId, int?[] valueIds = null, bool? deleteAll = false);
        bool DeleteScopes(int surveyId, int?[] valueIds = null, bool? deleteAll = false);
        int DeleteUsers(int surveyId, int?[] userIds = null, bool? deleteAll = false);
        Survey GetWithQuestions(int surveyId);
        IEnumerable<Survey> GetForRemoteUser(int userId);
        Survey GetSurveyIncludes(int surveyId);
        bool SaveAnswer(int surveyId, int userId, int questionId, string choiceIds, string responseText);
        int FinishSurvey(int surveyId, int userId, bool allowedInfoShare = false);
        IEnumerable<Survey> GetFinishedSurveys(int userId);
        SurveyReportModel GetSummaryReport(int surveyId);
        int GetPromotionCount(int surveyId);
        bool InsertReportConstraints(int surveyId, int[] valueIds);
        IEnumerable<int> SelectReportConstraints(int surveyId);
        bool DeleteReportConstraints(int surveyId, int?[] valueIds = null, bool? deleteAll = false);
    }
}
