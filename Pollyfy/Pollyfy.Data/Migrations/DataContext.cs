﻿#region 

using Pollyfy.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Pollyfy.Data.Configuration;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Data.Migrations
{
    public class DataContext : DbContext
    {
        public DataContext() : base("db") { }

        public DbSet<Log> Logs { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<HelperCategory> HelperCategories { get; set; }
        public DbSet<HelperValue> HelperValues { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Choice> Choices { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<TypeDescription> TypeDescriptions { get; set; }
        public DbSet<Helpdesk> Helpdesks { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Promotion> Promotions { get; set; }
        public DbSet<QuestionGroup> QuestionGroups { get; set; }
        public DbSet<ChoiceGroup> ChoiceGroups { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Authorization> Authorizations { get; set; }
        public DbSet<UserRoleAuthorization> UserRoleAuthorizations { get; set; }
        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<EarnCampaign> EarnCampaigns { get; set; }
        public DbSet<WeeklyLeader> WeeklyLeaders { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Counter> Counters { get; set; }
        //public DbSet<TemporaryExpoint> TemporaryExpoints { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new LogConfiguration());
            modelBuilder.Configurations.Add(new HelperCategoryConfiguration());
            modelBuilder.Configurations.Add(new HelperValueConfiguration());
            modelBuilder.Configurations.Add(new SurveyConfiguration());
            modelBuilder.Configurations.Add(new QuestionConfiguration());
            modelBuilder.Configurations.Add(new ChoiceConfiguration());
            modelBuilder.Configurations.Add(new EnrollmentConfiguration());
            modelBuilder.Configurations.Add(new AnswerConfiguration());
            modelBuilder.Configurations.Add(new TypeDescriptionConfiguration());
            modelBuilder.Configurations.Add(new HelpdeskConfiguration());
            modelBuilder.Configurations.Add(new CompanyConfiguration());
            modelBuilder.Configurations.Add(new DeviceConfiguration());
            modelBuilder.Configurations.Add(new PromotionConfiguration());
            modelBuilder.Configurations.Add(new QuestionGroupConfiguration());
            modelBuilder.Configurations.Add(new ChoiceGroupConfiguration());
            modelBuilder.Configurations.Add(new AuthorizationConfiguration());
            modelBuilder.Configurations.Add(new UserRoleConfiguration());
            modelBuilder.Configurations.Add(new UserRoleAuthorizationConfiguration());
            modelBuilder.Configurations.Add(new CampaignConfiguration());
            modelBuilder.Configurations.Add(new EarnCampaignConfiguration());
            modelBuilder.Configurations.Add(new WeeklyLeaderConfiguration());
            modelBuilder.Configurations.Add(new GroupConfiguration());
            modelBuilder.Configurations.Add(new CounterConfiguration());
            //modelBuilder.Configurations.Add(new TemporaryExpointConfiguration());

        }

        public void RefreshAll()
        {
            ObjectContext ctx = ((IObjectContextAdapter)this).ObjectContext;
            this.ChangeTracker.DetectChanges();

            IEnumerable<ObjectStateEntry> changes = ctx.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Deleted | EntityState.Modified);

            var refreshableObjects = (from entry in ctx.ObjectStateManager.GetObjectStateEntries(
                                                       EntityState.Added
                                                      | EntityState.Deleted
                                                      | EntityState.Modified
                                                      | EntityState.Unchanged)
                                      where entry.EntityKey != null
                                      select entry.Entity);

            ctx.Refresh(RefreshMode.StoreWins, refreshableObjects);
        }
    }
}