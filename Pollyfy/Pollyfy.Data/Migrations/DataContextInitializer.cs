﻿#region 

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

#endregion

namespace Pollyfy.Data.Migrations
{
    public class DataContextInitializer : CreateDatabaseIfNotExists<DataContext>
    {
        protected override void Seed(DataContext context)
        {
            base.Seed(context);

            var contextType = context.GetType();

            foreach (var contextTypeProperty in contextType.GetProperties())
            {
                if (contextTypeProperty.PropertyType.IsGenericType && contextTypeProperty.PropertyType.Name.Contains("DbSet"))
                {
                    var entityType = contextTypeProperty.PropertyType.GetGenericArguments()[0];

                    var entityTypeTableAttributes = entityType.GetCustomAttributes(typeof(TableAttribute), true);

                    var tableName = entityTypeTableAttributes.Length > 0 ? ((TableAttribute)entityTypeTableAttributes[0]).Name : contextTypeProperty.Name;

                    foreach (var entityTypeProperty in entityType.GetProperties())
                    {
                        if (entityTypeProperty.GetCustomAttributes(typeof(UniqueAttribute), true).Length == 0)
                            continue;

                        var fieldName = entityTypeProperty.Name;

                        try
                        {
                            context.Database.ExecuteSqlCommand("DROP INDEX [UX_" + fieldName + "] ON [" + tableName + "]");
                        }
                        catch
                        {
                            // ignored
                        }
                        context.Database.ExecuteSqlCommand("CREATE UNIQUE INDEX [UX_" + fieldName + "] ON [" + tableName + "] ([" + fieldName + "])");
                    }
                }
            }
        }
    }
}