﻿#region 

using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

#endregion

namespace Pollyfy.Data.Migrations
{
    internal sealed class MigrationConfiguration : DbMigrationsConfiguration<DataContext>
    {
        public MigrationConfiguration()
        {
            AutomaticMigrationsEnabled = true;

            AutomaticMigrationDataLossAllowed = true;

            MigrationsDirectory = "Migrations\\ExplicitMigrations";

        }

        protected override void Seed(DataContext context)
        {
            List<User> SystemUsers = new List<User>();
            User sysadmin = new User()
            {
                ActiveStatus = ActiveStatus.Active,
                Balance = 0,
                CreatedDate = DateTime.Now,
                Email = "admin@pollyfy.com",
                FirstName = "System",
                LastName = "Admin",
                Password = "AOeYp4Ge20O8112Q9Aev8RrdFeEns3Ia8YwT3qY9MUVKJl1ekL9HAKvw9qixg1k9UA==",
                TrackingGuid = Guid.NewGuid(),
                Types = UserType.SystemAdmin,
                UserName = "padmin",
                IsEmailValidated = false,
                IsPhoneValidated = false,
                Gender = "Erkek",
                IsOnline = false
            };
            SystemUsers.Add(sysadmin);

            bool hasAdd = false;

            var dbUsers = context.Users.ToList();
            foreach (User usr in SystemUsers)
            {
                if (!dbUsers.Any(m => m.Email == usr.Email))
                {
                    hasAdd = true;
                    context.Users.Add(usr);
                }
            }
            if (hasAdd)
                context.SaveChanges();

        }
    }
}