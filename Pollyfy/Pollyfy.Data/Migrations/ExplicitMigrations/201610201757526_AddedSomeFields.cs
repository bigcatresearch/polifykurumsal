namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSomeFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Question", "CopiedId", c => c.Int());
            AddColumn("dbo.Survey", "DaylyJoinLimit", c => c.Int());
            AddColumn("dbo.Survey", "PublishStatus", c => c.Int(nullable: false));
            AddColumn("dbo.Survey", "StatusText", c => c.String());
            AddColumn("dbo.Survey", "UseAccuracy", c => c.Boolean(nullable: false));
            AddColumn("dbo.Survey", "AccuracyQueCount", c => c.Int());
            AddColumn("dbo.Survey", "LastUnPublishDate", c => c.DateTime());
            AddColumn("dbo.Survey", "StartDate", c => c.DateTime());
            AddColumn("dbo.Survey", "FinishDate", c => c.DateTime());
            AddColumn("dbo.Survey", "InCompanyUsage", c => c.Boolean(nullable: false));
            AddColumn("dbo.Enrollment", "Accuracy", c => c.Decimal(precision: 18, scale: 2));
            CreateIndex("dbo.Question", "CopiedId");
            AddForeignKey("dbo.Question", "CopiedId", "dbo.Question", "QuestionId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Question", "CopiedId", "dbo.Question");
            DropIndex("dbo.Question", new[] { "CopiedId" });
            DropColumn("dbo.Enrollment", "Accuracy");
            DropColumn("dbo.Survey", "InCompanyUsage");
            DropColumn("dbo.Survey", "FinishDate");
            DropColumn("dbo.Survey", "StartDate");
            DropColumn("dbo.Survey", "LastUnPublishDate");
            DropColumn("dbo.Survey", "AccuracyQueCount");
            DropColumn("dbo.Survey", "UseAccuracy");
            DropColumn("dbo.Survey", "StatusText");
            DropColumn("dbo.Survey", "PublishStatus");
            DropColumn("dbo.Survey", "DaylyJoinLimit");
            DropColumn("dbo.Question", "CopiedId");
        }
    }
}
