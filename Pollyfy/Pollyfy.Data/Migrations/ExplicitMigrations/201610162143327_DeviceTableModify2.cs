namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeviceTableModify2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Device", "NotifyKey", c => c.String());
            AddColumn("dbo.Device", "Operator", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Device", "Operator");
            DropColumn("dbo.Device", "NotifyKey");
        }
    }
}
