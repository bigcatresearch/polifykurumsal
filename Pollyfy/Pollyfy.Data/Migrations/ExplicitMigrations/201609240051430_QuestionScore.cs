namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class QuestionScore : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Question", "MaxScore", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Question", "Score", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Question", "Score");
            DropColumn("dbo.Question", "MaxScore");
        }
    }
}
