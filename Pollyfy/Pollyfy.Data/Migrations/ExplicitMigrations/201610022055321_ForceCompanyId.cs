namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForceCompanyId : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Question", "CompanyId", c => c.Int(nullable: false));
            AlterColumn("dbo.Survey", "CompanyId", c => c.Int(nullable: false));
            CreateIndex("dbo.Survey", "CompanyId");
            AddForeignKey("dbo.Survey", "CompanyId", "dbo.Company", "CompanyId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Survey", "CompanyId", "dbo.Company");
            DropIndex("dbo.Survey", new[] { "CompanyId" });
            AlterColumn("dbo.Survey", "CompanyId", c => c.Int());
            AlterColumn("dbo.Question", "CompanyId", c => c.Int());
        }
    }
}
