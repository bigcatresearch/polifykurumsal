namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SurveyPrivacyQue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Survey", "PrivacyQuestion", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Survey", "PrivacyQuestion");
        }
    }
}
