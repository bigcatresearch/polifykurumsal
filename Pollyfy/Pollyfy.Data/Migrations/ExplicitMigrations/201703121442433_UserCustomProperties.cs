namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserCustomProperties : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "CustomProperty6", c => c.String());
            AddColumn("dbo.User", "CustomProperty7", c => c.String());
            AddColumn("dbo.User", "CustomProperty8", c => c.String());
            AddColumn("dbo.User", "CustomProperty9", c => c.String());
            AddColumn("dbo.User", "CustomProperty10", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "CustomProperty10");
            DropColumn("dbo.User", "CustomProperty9");
            DropColumn("dbo.User", "CustomProperty8");
            DropColumn("dbo.User", "CustomProperty7");
            DropColumn("dbo.User", "CustomProperty6");
        }
    }
}
