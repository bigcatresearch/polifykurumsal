namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TypeDescriptionTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TypeDescription",
                c => new
                    {
                        DescId = c.Int(nullable: false, identity: true),
                        IsForUser = c.Boolean(nullable: false),
                        IsForCompany = c.Boolean(nullable: false),
                        Type1 = c.String(nullable: false),
                        Type2 = c.String(nullable: false),
                        Type3 = c.String(nullable: false),
                        Type4 = c.String(nullable: false),
                        Type5 = c.String(nullable: false),
                        Type6 = c.String(nullable: false),
                        Type7 = c.String(nullable: false),
                        Type8 = c.String(nullable: false),
                        Type9 = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.DescId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TypeDescription");
        }
    }
}
