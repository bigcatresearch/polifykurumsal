namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SurveyUsageType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Survey", "UsageType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Survey", "UsageType");
        }
    }
}
