namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GroupTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QuestionGroup",
                c => new
                    {
                        GroupId = c.Int(nullable: false, identity: true),
                        SurveyId = c.Int(nullable: false),
                        ActiveStatus = c.Int(nullable: false),
                        GroupType = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        BeginScore = c.Int(),
                        EndScore = c.Int(),
                    })
                .PrimaryKey(t => t.GroupId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.QuestionGroup");
        }
    }
}
