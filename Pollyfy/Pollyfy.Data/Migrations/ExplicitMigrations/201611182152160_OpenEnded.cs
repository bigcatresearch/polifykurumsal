namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OpenEnded : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Answer", new[] { "ChoiceId" });
            AddColumn("dbo.Answer", "ResponseText", c => c.String());
            AlterColumn("dbo.Answer", "ChoiceId", c => c.Int());
            CreateIndex("dbo.Answer", "ChoiceId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Answer", new[] { "ChoiceId" });
            AlterColumn("dbo.Answer", "ChoiceId", c => c.Int(nullable: false));
            DropColumn("dbo.Answer", "ResponseText");
            CreateIndex("dbo.Answer", "ChoiceId");
        }
    }
}
