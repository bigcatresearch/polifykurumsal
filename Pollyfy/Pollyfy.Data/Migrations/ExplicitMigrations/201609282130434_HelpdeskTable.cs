namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HelpdeskTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Helpdesk",
                c => new
                    {
                        HelpdeskId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ActiveStatus = c.Int(nullable: false),
                        Message = c.String(nullable: false),
                        SurveyId = c.Int(),
                        QuestionId = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        TrackingGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.HelpdeskId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Helpdesk");
        }
    }
}
