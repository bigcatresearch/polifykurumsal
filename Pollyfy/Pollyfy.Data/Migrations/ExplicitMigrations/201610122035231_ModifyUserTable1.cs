namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyUserTable1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.User", "Latitude", c => c.String(maxLength: 50));
            AlterColumn("dbo.User", "Longitude", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.User", "Longitude", c => c.Decimal(precision: 10, scale: 6));
            AlterColumn("dbo.User", "Latitude", c => c.Decimal(precision: 10, scale: 6));
        }
    }
}
