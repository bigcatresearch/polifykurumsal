namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Promotions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Promotion",
                c => new
                    {
                        PromotionId = c.Int(nullable: false, identity: true),
                        SurveyId = c.Int(nullable: false),
                        UserId = c.Int(),
                        ActiveStatus = c.Int(nullable: false),
                        CompanyName = c.String(),
                        ContactUrl = c.String(),
                        ImageUrl = c.String(),
                        Title = c.String(),
                        Description = c.String(),
                        Discount = c.String(),
                        Code = c.String(),
                        StartDate = c.DateTime(),
                        FinishDate = c.DateTime(),
                        AssignedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                        TrackingGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.PromotionId)
                .ForeignKey("dbo.User", t => t.UserId)
                .ForeignKey("dbo.Survey", t => t.SurveyId)
                .Index(t => t.SurveyId)
                .Index(t => t.UserId);
            
            AddColumn("dbo.Survey", "ExPoint", c => c.Int());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Promotion", "SurveyId", "dbo.Survey");
            DropForeignKey("dbo.Promotion", "UserId", "dbo.User");
            DropIndex("dbo.Promotion", new[] { "UserId" });
            DropIndex("dbo.Promotion", new[] { "SurveyId" });
            DropColumn("dbo.Survey", "ExPoint");
            DropTable("dbo.Promotion");
        }
    }
}
