namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CompanyTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Company",
                c => new
                    {
                        CompanyId = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(nullable: false),
                        Email = c.String(),
                        AuthorizedPerson = c.String(),
                        ActiveStatus = c.Int(nullable: false),
                        SectorId = c.Int(nullable: false),
                        LicenseType = c.Int(nullable: false),
                        MaxUserCount = c.Int(),
                        MaxSurveyCount = c.Int(),
                        MaxQuestionCount = c.Int(),
                        Balance = c.Decimal(precision: 18, scale: 2),
                        BillingDate = c.DateTime(),
                        PhoneNumber = c.String(),
                        Country = c.String(),
                        City = c.String(),
                        Address = c.String(),
                        Culture = c.String(),
                        Currency = c.String(),
                        LogoId = c.Int(),
                        CustomProperty1 = c.String(),
                        CustomProperty2 = c.String(),
                        CustomProperty3 = c.String(),
                        CustomProperty4 = c.String(),
                        CustomProperty5 = c.String(),
                        Comment = c.String(),
                        ExpiryDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                        LastUpdatedDate = c.DateTime(),
                        TrackingGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.CompanyId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Company");
        }
    }
}
