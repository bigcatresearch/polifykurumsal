namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChoiceGroups : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChoiceGroup",
                c => new
                    {
                        GroupId = c.Int(nullable: false),
                        ChoiceId = c.Int(nullable: false),
                        Route = c.Int(nullable: false),
                        QuestionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GroupId, t.ChoiceId, t.Route });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ChoiceGroup");
        }
    }
}
