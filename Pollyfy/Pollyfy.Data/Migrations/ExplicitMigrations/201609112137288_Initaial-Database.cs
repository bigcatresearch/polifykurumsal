namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitaialDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Answer",
                c => new
                    {
                        AnswerId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        EnrollmentId = c.Int(nullable: false),
                        SurveyId = c.Int(nullable: false),
                        QuestionId = c.Int(nullable: false),
                        ChoiceId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AnswerId)
                .ForeignKey("dbo.Choice", t => t.ChoiceId)
                .ForeignKey("dbo.Question", t => t.QuestionId)
                .ForeignKey("dbo.Survey", t => t.SurveyId)
                .ForeignKey("dbo.User", t => t.UserId)
                .ForeignKey("dbo.Enrollment", t => t.EnrollmentId)
                .Index(t => t.UserId)
                .Index(t => t.EnrollmentId)
                .Index(t => t.SurveyId)
                .Index(t => t.QuestionId)
                .Index(t => t.ChoiceId);
            
            CreateTable(
                "dbo.Choice",
                c => new
                    {
                        ChoiceId = c.Int(nullable: false, identity: true),
                        QuestionId = c.Int(nullable: false),
                        Text = c.String(nullable: false),
                        Score = c.Decimal(precision: 18, scale: 2),
                        PassQuestion = c.Int(),
                        ActiveStatus = c.Int(nullable: false),
                        FileId = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        TrackingGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ChoiceId)
                .ForeignKey("dbo.Question", t => t.QuestionId)
                .Index(t => t.QuestionId);
            
            CreateTable(
                "dbo.Question",
                c => new
                    {
                        QuestionId = c.Int(nullable: false, identity: true),
                        SurveyId = c.Int(nullable: false),
                        QuestionType = c.Int(nullable: false),
                        QuestionText = c.String(nullable: false),
                        Description = c.String(),
                        IsRequired = c.Boolean(nullable: false),
                        ActiveStatus = c.Int(nullable: false),
                        FileId = c.Int(),
                        CompanyId = c.Int(),
                        CreatedByUserId = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        TrackingGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.QuestionId)
                .ForeignKey("dbo.Survey", t => t.SurveyId)
                .Index(t => t.SurveyId);
            
            CreateTable(
                "dbo.Survey",
                c => new
                    {
                        SurveyId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 500),
                        Description = c.String(),
                        QuestionCount = c.Int(nullable: false),
                        JoinLimit = c.Int(),
                        SectorId = c.Int(nullable: false),
                        Reward = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SurveyType = c.Int(nullable: false),
                        ActiveStatus = c.Int(nullable: false),
                        JoinStatus = c.Int(),
                        FileId = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        FirstPublishDate = c.DateTime(),
                        LastPublishDate = c.DateTime(),
                        CompanyId = c.Int(),
                        CreatedByUserId = c.Int(),
                        TrackingGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.SurveyId)
                .ForeignKey("dbo.User", t => t.CreatedByUserId)
                .ForeignKey("dbo.HelperValue", t => t.SectorId)
                .Index(t => t.SectorId)
                .Index(t => t.CreatedByUserId);
            
            CreateTable(
                "dbo.HelperValue",
                c => new
                    {
                        ValueId = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(),
                        CategoryId = c.Int(nullable: false),
                        CompanyId = c.Int(),
                        Code = c.String(maxLength: 200),
                        Value = c.String(maxLength: 2000),
                        Text = c.String(maxLength: 2000),
                        ActiveStatus = c.Int(nullable: false),
                        IsVisible = c.Boolean(nullable: false),
                        IsDeleteAllowed = c.Boolean(nullable: false),
                        DisplayOrder = c.Int(nullable: false),
                        IsLevelAllowed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ValueId)
                .ForeignKey("dbo.HelperCategory", t => t.CategoryId)
                .ForeignKey("dbo.HelperValue", t => t.ParentId)
                .Index(t => t.ParentId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.HelperCategory",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 200),
                        Text = c.String(maxLength: 200),
                        ParentId = c.Int(),
                        IsVisible = c.Boolean(nullable: false),
                        IsMultiple = c.Boolean(nullable: false),
                        IsDeleteAllowed = c.Boolean(nullable: false),
                        CompanyId = c.Int(),
                        Types = c.Int(nullable: false),
                        ActiveStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CategoryId)
                .ForeignKey("dbo.HelperCategory", t => t.ParentId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 200),
                        Email = c.String(nullable: false, maxLength: 200),
                        Password = c.String(nullable: false, maxLength: 250),
                        FirstName = c.String(nullable: false, maxLength: 400),
                        LastName = c.String(nullable: false, maxLength: 400),
                        CompanyId = c.Int(),
                        CompanyName = c.String(maxLength: 500),
                        Types = c.Int(nullable: false),
                        ActiveStatus = c.Int(nullable: false),
                        IsOnline = c.Boolean(),
                        IdentityNumber = c.String(),
                        PhoneNumber = c.String(maxLength: 20),
                        PhoneAcivationCode = c.String(maxLength: 400),
                        EmailAcivationCode = c.String(),
                        BirthDate = c.DateTime(),
                        Gender = c.String(maxLength: 20),
                        Comment = c.String(maxLength: 2000),
                        Culture = c.String(),
                        Currency = c.String(),
                        Balance = c.Decimal(precision: 18, scale: 4),
                        FacebookId = c.String(maxLength: 500),
                        GoogleId = c.String(maxLength: 500),
                        TwitterId = c.String(maxLength: 500),
                        OpenId = c.String(maxLength: 500),
                        IsEmailValidated = c.Boolean(),
                        IsPhoneValidated = c.Boolean(),
                        ExpiryDate = c.DateTime(),
                        LastAccessTime = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                        LastUpdatedDate = c.DateTime(),
                        ProfileImageId = c.Int(),
                        LoginChannel = c.String(maxLength: 500),
                        Country = c.String(maxLength: 100),
                        Location = c.String(maxLength: 400),
                        Address = c.String(maxLength: 500),
                        Sector = c.String(maxLength: 500),
                        Latitude = c.Decimal(precision: 10, scale: 6),
                        Longitude = c.Decimal(precision: 10, scale: 6),
                        CustomProperty1 = c.String(),
                        CustomProperty2 = c.String(),
                        CustomProperty3 = c.String(),
                        CustomProperty4 = c.String(),
                        CustomProperty5 = c.String(),
                        TrackingGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Enrollment",
                c => new
                    {
                        EnrollmentId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        SurveyId = c.Int(nullable: false),
                        Score = c.Decimal(precision: 18, scale: 2),
                        RepeatId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(),
                        FinishDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                        TrackingGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.EnrollmentId)
                .ForeignKey("dbo.User", t => t.UserId)
                .ForeignKey("dbo.Survey", t => t.SurveyId)
                .Index(t => t.UserId)
                .Index(t => t.SurveyId);
            
            CreateTable(
                "dbo.Log",
                c => new
                    {
                        LogId = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Thread = c.String(),
                        Level = c.String(),
                        Logger = c.String(),
                        Assembly = c.String(),
                        MachineName = c.String(),
                        Message = c.String(),
                        Exception = c.String(),
                        StackTrace = c.String(),
                    })
                .PrimaryKey(t => t.LogId);
            
            CreateTable(
                "dbo.UserProperties",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        ValueId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.ValueId })
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.HelperValue", t => t.ValueId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.ValueId);
            
            CreateTable(
                "dbo.SurveyConstraints",
                c => new
                    {
                        SurveyId = c.Int(nullable: false),
                        ValueId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SurveyId, t.ValueId })
                .ForeignKey("dbo.Survey", t => t.SurveyId, cascadeDelete: true)
                .ForeignKey("dbo.HelperValue", t => t.ValueId, cascadeDelete: true)
                .Index(t => t.SurveyId)
                .Index(t => t.ValueId);
            
            CreateTable(
                "dbo.SurveyScopes",
                c => new
                    {
                        SurveyId = c.Int(nullable: false),
                        ValueId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SurveyId, t.ValueId })
                .ForeignKey("dbo.Survey", t => t.SurveyId, cascadeDelete: true)
                .ForeignKey("dbo.HelperValue", t => t.ValueId, cascadeDelete: true)
                .Index(t => t.SurveyId)
                .Index(t => t.ValueId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Survey", "SectorId", "dbo.HelperValue");
            DropForeignKey("dbo.SurveyScopes", "ValueId", "dbo.HelperValue");
            DropForeignKey("dbo.SurveyScopes", "SurveyId", "dbo.Survey");
            DropForeignKey("dbo.Question", "SurveyId", "dbo.Survey");
            DropForeignKey("dbo.Enrollment", "SurveyId", "dbo.Survey");
            DropForeignKey("dbo.SurveyConstraints", "ValueId", "dbo.HelperValue");
            DropForeignKey("dbo.SurveyConstraints", "SurveyId", "dbo.Survey");
            DropForeignKey("dbo.Survey", "CreatedByUserId", "dbo.User");
            DropForeignKey("dbo.UserProperties", "ValueId", "dbo.HelperValue");
            DropForeignKey("dbo.UserProperties", "UserId", "dbo.User");
            DropForeignKey("dbo.Enrollment", "UserId", "dbo.User");
            DropForeignKey("dbo.Answer", "EnrollmentId", "dbo.Enrollment");
            DropForeignKey("dbo.Answer", "UserId", "dbo.User");
            DropForeignKey("dbo.HelperValue", "ParentId", "dbo.HelperValue");
            DropForeignKey("dbo.HelperValue", "CategoryId", "dbo.HelperCategory");
            DropForeignKey("dbo.HelperCategory", "ParentId", "dbo.HelperCategory");
            DropForeignKey("dbo.Answer", "SurveyId", "dbo.Survey");
            DropForeignKey("dbo.Choice", "QuestionId", "dbo.Question");
            DropForeignKey("dbo.Answer", "QuestionId", "dbo.Question");
            DropForeignKey("dbo.Answer", "ChoiceId", "dbo.Choice");
            DropIndex("dbo.SurveyScopes", new[] { "ValueId" });
            DropIndex("dbo.SurveyScopes", new[] { "SurveyId" });
            DropIndex("dbo.SurveyConstraints", new[] { "ValueId" });
            DropIndex("dbo.SurveyConstraints", new[] { "SurveyId" });
            DropIndex("dbo.UserProperties", new[] { "ValueId" });
            DropIndex("dbo.UserProperties", new[] { "UserId" });
            DropIndex("dbo.Enrollment", new[] { "SurveyId" });
            DropIndex("dbo.Enrollment", new[] { "UserId" });
            DropIndex("dbo.HelperCategory", new[] { "ParentId" });
            DropIndex("dbo.HelperValue", new[] { "CategoryId" });
            DropIndex("dbo.HelperValue", new[] { "ParentId" });
            DropIndex("dbo.Survey", new[] { "CreatedByUserId" });
            DropIndex("dbo.Survey", new[] { "SectorId" });
            DropIndex("dbo.Question", new[] { "SurveyId" });
            DropIndex("dbo.Choice", new[] { "QuestionId" });
            DropIndex("dbo.Answer", new[] { "ChoiceId" });
            DropIndex("dbo.Answer", new[] { "QuestionId" });
            DropIndex("dbo.Answer", new[] { "SurveyId" });
            DropIndex("dbo.Answer", new[] { "EnrollmentId" });
            DropIndex("dbo.Answer", new[] { "UserId" });
            DropTable("dbo.SurveyScopes");
            DropTable("dbo.SurveyConstraints");
            DropTable("dbo.UserProperties");
            DropTable("dbo.Log");
            DropTable("dbo.Enrollment");
            DropTable("dbo.User");
            DropTable("dbo.HelperCategory");
            DropTable("dbo.HelperValue");
            DropTable("dbo.Survey");
            DropTable("dbo.Question");
            DropTable("dbo.Choice");
            DropTable("dbo.Answer");
        }
    }
}
