namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class alwaysshow : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HelperCategory", "AlwaysShow", c => c.Boolean(nullable: false, defaultValue: false));
        }

        public override void Down()
        {
            DropColumn("dbo.HelperCategory", "AlwaysShow");
        }
    }
}
