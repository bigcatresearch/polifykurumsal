namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReportingConstraits : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SurveyReportConstraints",
                c => new
                    {
                        SurveyId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SurveyId, t.CategoryId })
                .ForeignKey("dbo.Survey", t => t.SurveyId, cascadeDelete: true)
                .ForeignKey("dbo.HelperCategory", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.SurveyId)
                .Index(t => t.CategoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SurveyReportConstraints", "CategoryId", "dbo.HelperCategory");
            DropForeignKey("dbo.SurveyReportConstraints", "SurveyId", "dbo.Survey");
            DropIndex("dbo.SurveyReportConstraints", new[] { "CategoryId" });
            DropIndex("dbo.SurveyReportConstraints", new[] { "SurveyId" });
            DropTable("dbo.SurveyReportConstraints");
        }
    }
}
