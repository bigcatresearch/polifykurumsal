namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnrollmentPromo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Enrollment", "PromoId", c => c.Int());
            CreateIndex("dbo.Enrollment", "PromoId");
            AddForeignKey("dbo.Enrollment", "PromoId", "dbo.Promotion", "PromotionId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Enrollment", "PromoId", "dbo.Promotion");
            DropIndex("dbo.Enrollment", new[] { "PromoId" });
            DropColumn("dbo.Enrollment", "PromoId");
        }
    }
}
