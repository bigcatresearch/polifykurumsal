namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Question_EnneagramType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Question", "EnneagramType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Question", "EnneagramType");
        }
    }
}
