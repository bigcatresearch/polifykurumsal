namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeviceTableModify1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "LastLoginDate", c => c.DateTime());
            AddColumn("dbo.User", "LastActiveDeviceId", c => c.Int());
            AddColumn("dbo.Device", "LastAccessDate", c => c.DateTime());
            AddColumn("dbo.Enrollment", "DeviceId", c => c.Int());
            CreateIndex("dbo.Enrollment", "DeviceId");
            AddForeignKey("dbo.Enrollment", "DeviceId", "dbo.Device", "DeviceId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Enrollment", "DeviceId", "dbo.Device");
            DropIndex("dbo.Enrollment", new[] { "DeviceId" });
            DropColumn("dbo.Enrollment", "DeviceId");
            DropColumn("dbo.Device", "LastAccessDate");
            DropColumn("dbo.User", "LastActiveDeviceId");
            DropColumn("dbo.User", "LastLoginDate");
        }
    }
}
