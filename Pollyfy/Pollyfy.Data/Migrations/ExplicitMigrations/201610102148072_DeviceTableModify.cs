namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeviceTableModify : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Device", "CreatedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Device", "CreatedDate");
        }
    }
}
