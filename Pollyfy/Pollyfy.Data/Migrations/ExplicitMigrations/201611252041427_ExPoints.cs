namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExPoints : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "ExPoint", c => c.Int(nullable: false));
            AddColumn("dbo.User", "AllowedInfoShare", c => c.Boolean(nullable: false));
            AddColumn("dbo.Enrollment", "ExPoint", c => c.Int(nullable: false));
            AddColumn("dbo.Enrollment", "AllowedInfoShare", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Enrollment", "AllowedInfoShare");
            DropColumn("dbo.Enrollment", "ExPoint");
            DropColumn("dbo.User", "AllowedInfoShare");
            DropColumn("dbo.User", "ExPoint");
        }
    }
}
