namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccuracyCriteria : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Survey", "AccuracyCriteria", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Survey", "AccuracyCriteria");
        }
    }
}
