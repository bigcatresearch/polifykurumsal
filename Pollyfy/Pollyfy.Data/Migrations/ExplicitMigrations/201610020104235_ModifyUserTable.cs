namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyUserTable : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.User", "CompanyName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.User", "CompanyName", c => c.String(maxLength: 500));
        }
    }
}
