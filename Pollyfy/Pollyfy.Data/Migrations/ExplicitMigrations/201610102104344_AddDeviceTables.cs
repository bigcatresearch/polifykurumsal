namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDeviceTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Device",
                c => new
                    {
                        DeviceId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Platform = c.String(),
                        Name = c.String(),
                        Imei = c.String(),
                        Manufacturer = c.String(),
                        Model = c.String(),
                        Version = c.String(),
                        Uuid = c.String(),
                    })
                .PrimaryKey(t => t.DeviceId)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.SurveyUsers",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        SurveyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.SurveyId })
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Survey", t => t.SurveyId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.SurveyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Device", "UserId", "dbo.User");
            DropForeignKey("dbo.SurveyUsers", "SurveyId", "dbo.Survey");
            DropForeignKey("dbo.SurveyUsers", "UserId", "dbo.User");
            DropIndex("dbo.SurveyUsers", new[] { "SurveyId" });
            DropIndex("dbo.SurveyUsers", new[] { "UserId" });
            DropIndex("dbo.Device", new[] { "UserId" });
            DropTable("dbo.SurveyUsers");
            DropTable("dbo.Device");
        }
    }
}
