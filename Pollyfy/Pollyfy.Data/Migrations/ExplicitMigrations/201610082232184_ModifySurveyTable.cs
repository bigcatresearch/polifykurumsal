namespace Pollyfy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifySurveyTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Survey", "FinalMessage", c => c.String(maxLength: 4000));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Survey", "FinalMessage");
        }
    }
}
