﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Data.Utilities;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using Pollyfy.Data.Helpers;
using Pollyfy.Data;

namespace Pollyfy.Data.Ninject
{
    public class ApplicationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ISQLBuilder>().To<SQLBuilder>();
            Bind<IDbConnection>().To<SqlConnection>();
            Bind<RepositoryFactories>().To<RepositoryFactories>();
            Bind<IRepositoryProvider>().To<RepositoryProvider>();
            Bind<IUow>().To<Uow>();
            Bind<IIdentity>().To<Identity>();
        }
    }
}
