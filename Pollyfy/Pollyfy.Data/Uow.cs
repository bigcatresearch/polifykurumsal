﻿using Pollyfy.Data.Helpers;
using Pollyfy.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Configuration;
using System.Data.SqlClient;
using Pollyfy.Data.Migrations;

namespace Pollyfy.Data
{
    /// <summary>
    /// The Code Camper "Unit of Work"
    ///     1) decouples the repos from  the controllers
    ///     2) decouples the DbContext and EF from the controllers
    ///     3) manages the UoW
    /// </summary>
    /// <remarks>
    /// This class implements the "Unit of Work" pattern in which
    /// the "UoW" serves as a facade for querying and saving to the database.
    /// Querying is delegated to "repositories".
    /// Each repository serves as a container dedicated to a particular
    /// root entity type such as a <see cref="Person"/>.

    /// will offer add, update, and delete methods if those features are supported.
    /// The repositories rely on their parent UoW to provide the interface to the
    /// data layer (which is the EF DbContext in Code Camper).
    /// </remarks>
    public class Uow : IUow, IDisposable
    {

        protected IRepositoryProvider RepositoryProvider { get; set; }
        public IDbConnection DbConnection { get; set; }
        public ISQLBuilder SQLBuilder { get; set; }
        private DataContext DbContext { get; set; }
        protected IIdentity Identity { get; set; }
        public Uow(IDbConnection dbConnection, IRepositoryProvider repositoryProvider, IIdentity identity, ISQLBuilder sqlBuilder)
        {
            this.DbConnection = dbConnection;
            CreateDbConnection(identity);
            CreateDbContext(identity);
            repositoryProvider.DbConnection = dbConnection;
            repositoryProvider.DbContext = DbContext;
            this.RepositoryProvider = repositoryProvider;
            this.Identity = identity;
            this.SQLBuilder = sqlBuilder;
            Common.Uow = this;

        }
        protected void CreateDbConnection(IIdentity identity)
        {
            DbConnection.ConnectionString = ConfigurationManager.ConnectionStrings["db"].ConnectionString;
        }

        protected void CreateDbContext(IIdentity identity)
        {
            DbContext = new DataContext();

            // Do NOT enable proxied entities, else serialization fails
            DbContext.Configuration.ProxyCreationEnabled = false;

            // Load navigation properties explicitly (avoid serialization trouble)
            DbContext.Configuration.LazyLoadingEnabled = false;

            // Because Web API will perform validation, we don't need/want EF to do so
            DbContext.Configuration.ValidateOnSaveEnabled = true;

            DbContext.Configuration.AutoDetectChangesEnabled = true;

            //DbContext.Configuration.AutoDetectChangesEnabled = false;
            // We won't use this performance tweak because we don't need 
            // the extra performance and, when autodetect is false,
            // we'd have to be careful. We're not being that careful.
        }

        public ILogRepository Logs { get { var repo = GetRepo<ILogRepository>(); repo.Uow = this; return repo; } }
        public IUserRepository Users { get { var repo = GetRepo<IUserRepository>(); repo.Uow = this; return repo; } }
        public IHelperCategoryRepository HelperCategories { get { var repo = GetRepo<IHelperCategoryRepository>(); repo.Uow = this; return repo; } }
        public IHelperValueRepository HelperValues { get { var repo = GetRepo<IHelperValueRepository>(); repo.Uow = this; return repo; } }
        public ISurveyRepository Surveys { get { var repo = GetRepo<ISurveyRepository>(); repo.Uow = this; return repo; } }
        public IEnrollmentRepository Enrollments { get { var repo = GetRepo<IEnrollmentRepository>(); repo.Uow = this; return repo; } }
        public IQuestionRepository Questions { get { var repo = GetRepo<IQuestionRepository>(); repo.Uow = this; return repo; } }
        public IChoiceRepository Choices { get { var repo = GetRepo<IChoiceRepository>(); repo.Uow = this; return repo; } }
        public IAnswerRepository Answers { get { var repo = GetRepo<IAnswerRepository>(); repo.Uow = this; return repo; } }
        public ITypeDescriptionRepository TypeDescriptions { get { var repo = GetRepo<ITypeDescriptionRepository>(); repo.Uow = this; return repo; } }
        public IHelpdeskRepository Helpdesks { get { var repo = GetRepo<IHelpdeskRepository>(); repo.Uow = this; return repo; } }
        public ICompanyRepository Companies { get { var repo = GetRepo<ICompanyRepository>(); repo.Uow = this; return repo; } }
        public IDeviceRepository Devices { get { var repo = GetRepo<IDeviceRepository>(); repo.Uow = this; return repo; } }
        public IPromotionRepository Promotions { get { var repo = GetRepo<IPromotionRepository>(); repo.Uow = this; return repo; } }
        public IQuestionGroupRepository QuestionGroups { get { var repo = GetRepo<IQuestionGroupRepository>(); repo.Uow = this; return repo; } }
        public IUserRoleRepository UserRoles { get { var repo = GetRepo<IUserRoleRepository>(); repo.Uow = this; return repo; } }
        public IAuthorizationRepository Authorizations { get { var repo = GetRepo<IAuthorizationRepository>(); repo.Uow = this; return repo; } }
        public IUserRoleAuthorizationRepository UserRoleAuthorizations { get { var repo = GetRepo<IUserRoleAuthorizationRepository>(); repo.Uow = this; return repo; } }
        public ICampaignRepository Campaigns { get { var repo = GetRepo<ICampaignRepository>(); repo.Uow = this; return repo; } }
        public IEarnCampaignRepository EarnCampaigns { get { var repo = GetRepo<IEarnCampaignRepository>(); repo.Uow = this; return repo; } }
        public IWeeklyLeaderRepository WeeklyLeaders { get { var repo = GetRepo<IWeeklyLeaderRepository>(); repo.Uow = this; return repo; } }
        public IGroupRepository Groups { get { var repo = GetRepo<IGroupRepository>(); repo.Uow = this; return repo; } }
        public ICounterRepository Counters { get { var repo = GetRepo<ICounterRepository>(); repo.Uow = this; return repo; } }
        public ITemporaryExpointRepository TemporaryExpoints { get { var repo = GetRepo<ITemporaryExpointRepository>(); repo.Uow = this; return repo; } }

        public void Commit()
        {
            DbContext.SaveChanges();
        }
        public async Task CommitAsync()
        {
            await DbContext.SaveChangesAsync();
        }
        private IRepositoryBase<T> GetStandardRepo<T>() where T : class
        {
            var repo = RepositoryProvider.GetRepositoryForEntityType<T>();
            repo.Uow = this;
            return repo;
        }
        private T GetRepo<T>() where T : class
        {
            var repo = RepositoryProvider.GetRepository<T>();
            return repo;
        }

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbConnection != null)
                {
                    DbConnection.Dispose();
                }
            }
        }

        #endregion
    }
}
