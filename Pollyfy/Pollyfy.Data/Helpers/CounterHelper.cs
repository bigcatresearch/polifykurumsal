﻿#region

using Pollyfy.Models.DataModel;
using Pollyfy.Models.Enum;

#endregion

namespace Pollyfy.Data.Helpers
{
    public static class CounterHelper
    {
        public static int GetCounter(CounterIdentifier counterIdentifier, string param1 = "", string param2 = "", string param3 = "")
        {
            return GetCounter(counterIdentifier.ToString("G"), param1, param2, param3);
        }

        public static int GetCounter(string key, string param1 = "", string param2 = "", string param3 = "")
        {
            Counter counter;

            counter = Common.Uow.Counters.GetByParams(key, param1, param2, param3);

            if (counter == null)
            {
                counter = new Counter
                {
                    Key = key,
                    Param1 = param1,
                    Param2 = param2,
                    Param3 = param3
                };

                counter.Value++;

                Common.Uow.Counters.DoInsert(counter);
            }
            else
            {
                counter.Value++;

                Common.Uow.Counters.DoUpdate(counter);
            }
            return counter.Value;
        }
    }
}