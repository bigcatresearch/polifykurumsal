﻿using Pollyfy.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using Pollyfy.Data.Repositories;

namespace Pollyfy.Data.Helpers
{
    /// <summary>
    /// A maker of Code Camper Repositories.
    /// </summary>
    /// <remarks>
    /// An instance of this class contains repository factory functions for different types.
    /// Each factory function takes an EF <see cref="DbContext"/> and returns
    /// a repository bound to that DbContext.
    /// <para>
    /// Designed to be a "Singleton", configured at web application start with
    /// all of the factory functions needed to create any type of repository.
    /// Should be thread-safe to use because it is configured at app start,
    /// before any request for a factory, and should be immutable thereafter.
    /// </para>
    /// </remarks>
    public class RepositoryFactories
    {
        /// <summary>
        /// Return the runtime Code Camper repository factory functions,
        /// each one is a factory for a repository of a particular type.
        /// </summary>
        /// <remarks>
        /// </remarks>
        private IDictionary<Type, Func<DbContext, object>> GetFactories()
        {
            return new Dictionary<Type, Func<DbContext, object>>
                {
                    {typeof(ILogRepository), dbContext => new LogRepository(dbContext)},
                    {typeof(IUserRepository), dbContext => new UserRepository(dbContext)},
                    {typeof(IHelperCategoryRepository), dbContext => new HelperCategoryRepository(dbContext)},
                    {typeof(IHelperValueRepository), dbContext => new HelperValueRepository(dbContext)},
                    {typeof(ISurveyRepository), dbContext => new SurveyRepository(dbContext)},
                    {typeof(IEnrollmentRepository), dbContext => new EnrollmentRepository(dbContext)},
                    {typeof(IQuestionRepository), dbContext => new QuestionRepository(dbContext)},
                    {typeof(IChoiceRepository), dbContext => new ChoiceRepository(dbContext)},
                    {typeof(IAnswerRepository), dbContext => new AnswerRepository(dbContext)},
                    {typeof(IHelpdeskRepository), dbContext => new HelpdeskRepository(dbContext)},
                    {typeof(ITypeDescriptionRepository), dbContext => new TypeDescriptionRepository(dbContext)},
                    {typeof(ICompanyRepository), dbContext => new CompanyRepository(dbContext)},
                    {typeof(IUserRoleRepository), dbContext => new UserRoleRepository(dbContext)},
                    {typeof(IAuthorizationRepository), dbContext => new AuthorizationRepository(dbContext)},
                    {typeof(IUserRoleAuthorizationRepository), dbContext => new UserRoleAuthorizationRepository(dbContext)},
                    {typeof(IDeviceRepository), dbContext => new DeviceRepository(dbContext)},
                    {typeof(IPromotionRepository), dbContext => new PromotionRepository(dbContext)},
                    {typeof(IQuestionGroupRepository), dbContext => new QuestionGroupRepository(dbContext)},
                    {typeof(ICampaignRepository), dbContext => new CampaignRepository(dbContext)},
                    {typeof(IEarnCampaignRepository), dbContext => new EarnCampaignRepository(dbContext)},
                    {typeof(IWeeklyLeaderRepository), dbContext => new WeeklyLeaderRepository(dbContext)},
                    {typeof(IGroupRepository), dbContext => new GroupRepository(dbContext)},
                    {typeof(ICounterRepository), dbContext => new CounterRepository(dbContext)},
                    {typeof(ITemporaryExpointRepository), dbContext => new TemporaryExpointRepository(dbContext)},
                };
        }

        /// <summary>
        /// Constructor that initializes with runtime Code Camper repository factories
        /// </summary>
        public RepositoryFactories()
        {
            _repositoryFactories = GetFactories();
        }

        /// <summary>
        /// Constructor that initializes with an arbitrary collection of factories
        /// </summary>
        /// <param name="factories">
        /// The repository factory functions for this instance. 
        /// </param>
        /// <remarks>
        /// This ctor is primarily useful for testing this class
        /// </remarks>
        public RepositoryFactories(IDictionary<Type, Func<DbContext, object>> factories)
        {
            _repositoryFactories = factories;
        }

        /// <summary>
        /// Get the repository factory function for the type.
        /// </summary>
        /// <typeparam name="T">Type serving as the repository factory lookup key.</typeparam>
        /// <returns>The repository function if found, else null.</returns>
        /// <remarks>
        /// The type parameter, T, is typically the repository type 
        /// but could be any type (e.g., an entity type)
        /// </remarks>
        public Func<DbContext, object> GetRepositoryFactory<T>()
        {
            Func<DbContext, object> factory;
            _repositoryFactories.TryGetValue(typeof(T), out factory);
            return factory;
        }

        /// <summary>
        /// Get the factory for <see cref="IRepository{T}"/> where T is an entity type.
        /// </summary>
        /// <typeparam name="T">The root type of the repository, typically an entity type.</typeparam>
        /// <returns>
        /// A factory that creates the <see cref="IRepository{T}"/>, given an EF <see cref="DbContext"/>.
        /// </returns>
        /// <remarks>
        /// Looks first for a custom factory in <see cref="_repositoryFactories"/>.
        /// If not, falls back to the <see cref="DefaultEntityRepositoryFactory{T}"/>.
        /// You can substitute an alternative factory for the default one by adding
        /// a repository factory for type "T" to <see cref="_repositoryFactories"/>.
        /// </remarks>
        public Func<DbContext, object> GetRepositoryFactoryForEntityType<T>() where T : class
        {
            return GetRepositoryFactory<T>() ?? DefaultEntityRepositoryFactory<T>();
        }

        /// <summary>
        /// Default factory for a <see cref="IRepository{T}"/> where T is an entity.
        /// </summary>
        /// <typeparam name="T">Type of the repository's root entity</typeparam>
        protected virtual Func<DbContext, object> DefaultEntityRepositoryFactory<T>() where T : class
        {
            return dbContext => new EFRepository<T>(dbContext);
        }
        /// <summary>
        /// Get the dictionary of repository factory functions.
        /// </summary>
        /// <remarks>
        /// A dictionary key is a System.Type, typically a repository type.
        /// A value is a repository factory function
        /// that takes a <see cref="DbContext"/> argument and returns
        /// a repository object. Caller must know how to cast it.
        /// </remarks>
        private readonly IDictionary<Type, Func<DbContext, object>> _repositoryFactories;

    }
}

