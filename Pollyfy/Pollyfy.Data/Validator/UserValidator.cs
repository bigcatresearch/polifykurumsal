﻿using FluentValidation;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Validator
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            //RuleFor(model => model.UserName)
            //    .NotEmpty().WithMessage("Kullanıcı adı bilgisi zorunludur.")
            //    .WithName("Kullanıcı Adı");

            RuleFor(model => model.FirstName)
                .NotEmpty().WithMessage("Ad bilgisi zorunludur.")
                .WithName("Ad");

            RuleFor(model => model.LastName)
                .NotEmpty().WithMessage("Soyad bilgisi zorunludur.")
                .WithName("Soyad");

            RuleFor(model => model.Password)
                .NotEmpty().WithMessage("Şifre bilgisi zorunludur.")
                .Length(6, 250).WithMessage("Şifre en az 6, en fazla 250 karakter olmalıdır.")
                .WithName("Şifre");

            RuleFor(model => model.Email)
                .NotEmpty().WithMessage("E-Posta bilgisi zorunludur.")
                .Length(0, 250).WithMessage("E-Posta adresi en fazla 250 karakter olmalıdır.")
                .EmailAddress().WithMessage("E-Posta adresi geçerli değil.")
                .WithName("E-Posta");

            RuleFor(model => model.ActiveStatus)
                .NotEmpty().WithMessage("Durum bilgisi zorunludur.")
                .WithName("Durum");

            RuleFor(model => model.Address)
                .Length(0, 500).WithMessage("Adress bilgisi en fazla 500 karakter olmalıdır.")
                .WithName("Adres");

            //RuleFor(model => model.Sector)
            //    .Length(0, 500).WithMessage("Sektör bilgisi en fazla 500 karakter olmalıdır.")
            //    .WithName("Sektör");

            RuleFor(model => model.Comment)
                .Length(0, 2000).WithMessage("Not bilgisi en fazla 2000 karakter olmalıdır.")
                .WithName("Not");

            RuleFor(model => model.CreatedDate)
                .NotNull().WithMessage("Oluşturulma tarihi bilgisi zorunludur.")
                .WithName("Oluşturulma Tarihi");

            RuleFor(model => model.Types)
                .NotNull().WithMessage("Kullanıcı tipi zorunludur.")
                .WithName("Kullanıcı Tipi");

            //RuleFor(model => model.BirthDate)
            //    .NotNull().WithMessage("Doğum tarihi bilgisi zorunludur.")
            //    .LessThan(DateTime.Now.AddYears(-13)).WithMessage("Onüç yaşından küçükler için uygun değildir.")
            //    .WithName("Doğum Tarihi");
        }
    }
}
