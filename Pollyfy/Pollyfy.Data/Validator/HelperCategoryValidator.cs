﻿using FluentValidation;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Validator
{
    public class HelperCategoryValidator : AbstractValidator<HelperCategory>
    {
        public HelperCategoryValidator()
        {
            RuleFor(model => model.Code)
                .Length(0, 200).WithMessage("Kategori Kodu en fazla 200 karakter olmalıdır.");

            RuleFor(model => model.IsDeleteAllowed)
                .NotNull().WithMessage("Silinebilme izni zorunludur.")
                .WithName("Silinebilir");

            RuleFor(model => model.AlwaysShow)
                .NotNull().WithMessage("Her zaman gösterme izni zorunludur.")
                .WithName("Her Zaman Göster");

            RuleFor(model => model.IsMultiple)
                .NotNull().WithMessage("Çoklu seçim izni zorunludur.")
                .WithName("Çoklu Seçim");

            RuleFor(model => model.IsVisible)
                .NotEmpty().WithMessage("Görünürlük durumu bilgisi zorunludur.")
                .WithName("Görünürlük");

            RuleFor(model => model.Text)
                .Length(0, 200).WithMessage("Başlık en fazla 200 karakter olmalıdır.")
                .WithName("Başlık");

            RuleFor(model => model.Types)
                .NotNull().WithMessage("Kategori tipi zorunludur.")
                .WithName("Kategori Tipi");

        }
    }
}
