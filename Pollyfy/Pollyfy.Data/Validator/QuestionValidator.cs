﻿using FluentValidation;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Validator
{
    public class QuestionValidator : AbstractValidator<Question>
    {
        public QuestionValidator()
        {

            RuleFor(model => model.QuestionText)
                .NotEmpty().WithMessage("Soru metni zorunludur.")
                .WithName("Soru Metni");

            //RuleFor(model => model.FormChoiceTexts)
            //    .NotNull().WithMessage("Seçenekler zorunludur.");
        }
    }
}
