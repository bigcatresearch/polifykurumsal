﻿using FluentValidation;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Validator
{
    public class HelperValueValidator : AbstractValidator<HelperValue>
    {
        public HelperValueValidator()
        {
            RuleFor(model => model.Code)
                .Length(0, 200).WithMessage("Kategori Kodu en fazla 200 karakter olmalıdır.");

            RuleFor(model => model.CategoryId)
                .NotNull().WithMessage("Kategori bilgisi zorunludur.")
                .WithName("Kategori");

            RuleFor(model => model.DisplayOrder)
                .NotNull().WithMessage("Gösterim sırası zorunludur.")
                .WithName("Gösterim Sırası");

            RuleFor(model => model.Value)
                .Length(0, 2000).WithMessage("Değer en fazla 2000 karakter olmalıdır.")
                .WithName("Değer");

            RuleFor(model => model.Text)
                .Length(0, 2000).WithMessage("Başlık en fazla 2000 karakter olmalıdır.")
                .WithName("Başlık");

        }
    }
}
