﻿using FluentValidation;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data.Validator
{
    public class SurveyValidator : AbstractValidator<Survey>
    {
        public SurveyValidator()
        {
            //RuleFor(model => model.SectorId)
            //    .NotEmpty().WithMessage("Sektör bilgisi zorunludur.")
            //    .WithName("Sektör");

            RuleFor(model => model.Name)
                .NotEmpty().WithMessage("Anket ismi zorunludur.")
                .Length(1, 500).WithMessage("Anket ismi en fazla 500 karakter olmalıdır.")
                .WithName("Anket İsmi");


            RuleFor(model => model.QuestionCount)
                .GreaterThan(0).WithMessage("Soru sayısı 0'dan büyük olmalıdır.")
                .NotEmpty().WithMessage("Soru sayısı zorunludur.")
                .WithName("Soru Sayısı");

            //RuleFor(model => model.Reward)
                //.GreaterThan(-1).WithMessage("Ödül değeri 0 veya 0'dan büyük olmalıdır.")
                //.NotEmpty().WithMessage("Ödül değeri zorunludur.")
                //.WithName("Ödül");

            //RuleFor(model => model.JoinLimit)
            //    .GreaterThan(0).WithMessage("Katılımcı limiti 0'dan büyük olmalıdır.")
            //    .WithName("Katılımcı Limiti");


            //RuleFor(model => model.SurveyType)
            //    .NotNull().WithMessage("Anket tipi zorunludur.")
            //    .WithName("Anket Tipi");

            RuleFor(model => model.FinalMessage)
               .Length(0, 4000).WithMessage("Final mesajı en fazla 4000 karakter olmalıdır.")
               .WithName("Final Mesajı");

            //RuleFor(model => model.Reward).NotNull().WithMessage("Ödül değeri zorunludur");
        }
    }
}
