﻿#region 

using System.Data.Entity.ModelConfiguration;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Data.Configuration
{
    public class AuthorizationConfiguration : EntityTypeConfiguration<Authorization>
    {
        public AuthorizationConfiguration()
        {
            ToTable("Authorization");
            HasKey(p => p.Id);
            Property(p => p.Name).IsRequired().HasMaxLength(255);
            Property(p => p.Key).IsRequired().HasMaxLength(255);
            Property(p => p.Note).IsOptional().HasMaxLength(2048);
        }
    }
}