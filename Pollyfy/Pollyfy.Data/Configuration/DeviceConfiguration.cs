﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class DeviceConfiguration : EntityTypeConfiguration<Device>
    {
        public DeviceConfiguration()
        {
            ToTable("Device");
            HasKey(m => m.DeviceId);
            Property(m => m.UserId).IsRequired();
            Property(m => m.Imei).IsOptional();
            Property(m => m.Manufacturer).IsOptional();
            Property(m => m.Model).IsOptional();
            Property(m => m.Name).IsOptional();
            Property(m => m.Platform).IsOptional();
            Property(m => m.Uuid).IsOptional();
            Property(m => m.Version).IsOptional();
            Property(m => m.CreatedDate).IsRequired();
            Property(m => m.LastAccessDate).IsOptional();

            HasMany(m => m.Enrollments).WithOptional(m => m.Device).HasForeignKey(m => m.DeviceId).WillCascadeOnDelete(false);
        }

    }
}
