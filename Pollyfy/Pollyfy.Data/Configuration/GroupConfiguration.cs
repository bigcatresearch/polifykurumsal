﻿#region

using Pollyfy.Models.DataModel;
using System.Data.Entity.ModelConfiguration;

#endregion

namespace Pollyfy.Data.Configuration
{
    public class GroupConfiguration : EntityTypeConfiguration<Group>
    {
        public GroupConfiguration()
        {
            ToTable("Group");
            HasKey(p => p.Id);
            Property(m => m.SurveyId).IsRequired();
            Property(m => m.GroupId).IsRequired();
            Property(m => m.UserId).IsRequired();
        }
    }
}