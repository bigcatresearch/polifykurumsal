﻿using Pollyfy.Models.DataModel;
using System.Data.Entity.ModelConfiguration;

namespace Pollyfy.Data.Configuration
{
    public class CampaignConfiguration : EntityTypeConfiguration<Campaign>
    {
        public CampaignConfiguration()
        {
            ToTable("Campaign");
            HasKey(p => p.Id);
            Property(p => p.Name).IsRequired().HasMaxLength(128);
            Property(p => p.CompanyName).IsOptional().HasMaxLength(128);
            Property(p => p.WebSiteName).IsRequired();
            Property(p => p.Image).IsRequired();
            Property(p => p.Point).IsRequired();
            Property(p => p.CampaignKind).IsRequired();
            Property(p => p.Message).IsRequired();
            Property(p => p.Count).IsRequired();
            Property(p => p.Discount).IsOptional();
            Property(p => p.Note).IsOptional();
        }
    }
}