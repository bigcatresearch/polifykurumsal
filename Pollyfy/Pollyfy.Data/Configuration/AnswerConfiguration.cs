﻿using Pollyfy.Models;
using System.Data.Entity.ModelConfiguration;

namespace Pollyfy.Data
{
    public class AnswerConfiguration : EntityTypeConfiguration<Answer>
    {
        public AnswerConfiguration()
        {
            ToTable("Answer");
            HasKey(m => m.AnswerId);
            Property(m => m.ChoiceId).IsOptional();
            Property(m => m.EnrollmentId).IsRequired();
            Property(m => m.QuestionId).IsRequired();
            Property(m => m.SurveyId).IsRequired();
            Property(m => m.UserId).IsRequired();
            Property(m => m.CreatedDate).IsRequired();
            Property(m => m.ResponseText).IsOptional();
        }
    }
}