﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class HelperValueConfiguration : EntityTypeConfiguration<HelperValue>
    {
        public HelperValueConfiguration()
        {
            ToTable("HelperValue");
            HasKey(m => m.ValueId);
            Property(m => m.CategoryId).IsRequired();
            Property(m => m.Code).IsOptional().HasMaxLength(200);
            Property(m => m.CompanyId).IsOptional();
            Property(m => m.ParentId).IsOptional();
            Property(m => m.DisplayOrder).IsRequired();
            Property(m => m.IsLevelAllowed).IsRequired();
            Property(m => m.Text).IsOptional().HasMaxLength(2000);
            Property(m => m.Value).IsOptional().HasMaxLength(2000);
            HasMany(m => m.Owners).WithOptional(m => m.Parent).HasForeignKey(m => m.ParentId).WillCascadeOnDelete(false);
            Ignore(m => m.SurveyId);
        }
    }
}
