﻿#region 

using System.Data.Entity.ModelConfiguration;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Data.Configuration
{
    public class UserRoleConfiguration : EntityTypeConfiguration<UserRole>
    {
        public UserRoleConfiguration()
        {
            ToTable("UserRole");
            HasKey(p => p.Id);
            Property(p => p.Name).IsRequired().HasMaxLength(64);
            Property(p => p.UserType).IsRequired();
            Property(p => p.Note).IsOptional().HasMaxLength(2048);
        }
    }
}