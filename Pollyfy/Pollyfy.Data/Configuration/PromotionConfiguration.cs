﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class PromotionConfiguration : EntityTypeConfiguration<Promotion>
    {
        public PromotionConfiguration()
        {
            ToTable("Promotion");
            HasKey(m => m.PromotionId);
            Property(m => m.ActiveStatus).IsRequired();
            Property(m => m.AssignedDate).IsOptional();
            Property(m => m.Code).IsOptional();
            Property(m => m.CompanyName).IsOptional();
            Property(m => m.ContactUrl).IsOptional();
            Property(m => m.CreatedDate).IsRequired();
            Property(m => m.Description).IsOptional();
            Property(m => m.Discount).IsOptional();
            Property(m => m.FinishDate).IsOptional();
            Property(m => m.ImageUrl).IsOptional();
            Property(m => m.StartDate).IsOptional();
            Property(m => m.SurveyId).IsRequired();
            Property(m => m.Title).IsOptional();
            Property(m => m.TrackingGuid).IsRequired();
            Property(m => m.UserId).IsOptional();
        }

    }
}
