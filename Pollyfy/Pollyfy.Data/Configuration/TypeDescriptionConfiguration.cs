﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class TypeDescriptionConfiguration : EntityTypeConfiguration<TypeDescription>
    {
        public TypeDescriptionConfiguration()
        {
            ToTable("TypeDescription");
            HasKey(m => m.DescId);
            Property(m => m.IsForUser).IsRequired();
            Property(m => m.IsForCompany).IsRequired();
            Property(m => m.Type1).IsRequired();
            Property(m => m.Type2).IsRequired();
            Property(m => m.Type3).IsRequired();
            Property(m => m.Type4).IsRequired();
            Property(m => m.Type5).IsRequired();
            Property(m => m.Type6).IsRequired();
            Property(m => m.Type7).IsRequired();
            Property(m => m.Type8).IsRequired();
            Property(m => m.Type9).IsRequired();

        }

    }
}
