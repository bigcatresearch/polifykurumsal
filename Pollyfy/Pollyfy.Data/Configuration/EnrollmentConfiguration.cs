﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class EnrollmentConfiguration : EntityTypeConfiguration<Enrollment>
    {
        public EnrollmentConfiguration()
        {
            ToTable("Enrollment");
            HasKey(m => m.EnrollmentId);
            Property(m => m.FinishDate).IsOptional();
            Property(m => m.RepeatId).IsRequired();
            Property(m => m.Score).IsOptional();
            Property(m => m.StartDate).IsOptional();
            Property(m => m.Status).IsRequired();
            Property(m => m.UserId).IsRequired();
            Property(m => m.SurveyId).IsRequired();
            Property(m => m.CreatedDate).IsRequired();
            Property(m => m.TrackingGuid).IsRequired();
            Property(m => m.Accuracy).IsOptional();
            Property(m => m.ExPoint).IsRequired();
            Property(m => m.AllowedInfoShare).IsRequired();

            HasOptional(m => m.Promotion).WithMany().HasForeignKey(m => m.PromoId);
            HasMany(m => m.Answers).WithRequired(m => m.Enrollment).HasForeignKey(m => m.EnrollmentId).WillCascadeOnDelete(false);
        }

    }
}
