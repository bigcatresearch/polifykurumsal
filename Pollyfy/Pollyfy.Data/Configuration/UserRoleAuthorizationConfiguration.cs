﻿#region 

using System.Data.Entity.ModelConfiguration;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Data.Configuration
{
    public class UserRoleAuthorizationConfiguration : EntityTypeConfiguration<UserRoleAuthorization>
    {
        public UserRoleAuthorizationConfiguration()
        {
            ToTable("UserRoleAuthorization");
            HasKey(p => p.Id);
            Property(p => p.UserRoleId).IsRequired();
            Property(p => p.AuthorizationId).IsRequired();
        }
    }
}