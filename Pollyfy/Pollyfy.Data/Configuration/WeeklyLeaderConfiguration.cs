﻿#region

using Pollyfy.Models.DataModel;
using System.Data.Entity.ModelConfiguration;

#endregion

namespace Pollyfy.Data.Configuration
{
    public class WeeklyLeaderConfiguration : EntityTypeConfiguration<WeeklyLeader>
    {
        public WeeklyLeaderConfiguration()
        {
            ToTable("WeeklyLeader");
            HasKey(p => p.Id);
            Property(p => p.UserId).IsRequired();
            Property(p => p.ExPoint).IsOptional();
            Property(p => p.SequenceNo).IsOptional();
        }
    }
}