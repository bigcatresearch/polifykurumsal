﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class QuestionGroupConfiguration : EntityTypeConfiguration<QuestionGroup>
    {
        public QuestionGroupConfiguration()
        {
            ToTable("QuestionGroup");
            HasKey(m => m.GroupId);
            Property(m => m.SurveyId).IsRequired();
            Property(m => m.ActiveStatus).IsRequired();
            Property(m => m.GroupType).IsRequired();
            Property(m => m.Name).IsRequired();
            Property(m => m.Description).IsOptional();
            Property(m => m.BeginScore).IsOptional();
            Property(m => m.EndScore).IsOptional();

            Ignore(m => m.Route);
            Ignore(m => m.Score);
        }

    }
}
