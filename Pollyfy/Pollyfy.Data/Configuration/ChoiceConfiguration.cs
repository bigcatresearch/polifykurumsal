﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class ChoiceConfiguration : EntityTypeConfiguration<Choice>
    {
        public ChoiceConfiguration()
        {
            ToTable("Choice");
            HasKey(m => m.ChoiceId);
            Property(m => m.ActiveStatus).IsRequired();
            Property(m => m.PassQuestion).IsOptional();
            Property(m => m.QuestionId).IsRequired();
            Property(m => m.Score).IsOptional();
            Property(m => m.FileId).IsOptional();
            Property(m => m.Text).IsRequired();
            Property(m => m.CreatedDate).IsRequired();
            Property(m => m.TrackingGuid).IsRequired();
            Property(m => m.ChoiceDescribe).IsOptional();

            HasMany(m => m.Answers).WithOptional(m => m.Choice).HasForeignKey(m => m.ChoiceId).WillCascadeOnDelete(false);
        }

    }
}
