﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class HelperCategoryConfiguration : EntityTypeConfiguration<HelperCategory>
    {
        public HelperCategoryConfiguration()
        {
            ToTable("HelperCategory");
            HasKey(m => m.CategoryId);
            Property(m => m.ParentId).IsOptional();
            Property(m => m.Code).IsOptional().HasMaxLength(200);
            Property(m => m.CompanyId).IsOptional();
            Property(m => m.ActiveStatus).IsRequired();
            Property(m => m.IsDeleteAllowed).IsRequired();
            Property(m => m.IsMultiple).IsRequired();
            Property(m => m.IsVisible).IsRequired();
            Property(m => m.AlwaysShow).IsRequired();
            Property(m => m.Text).IsOptional().HasMaxLength(200);
            Property(m => m.Types).IsRequired();
            HasMany(m => m.Values).WithRequired(m => m.Category).HasForeignKey(m => m.CategoryId).WillCascadeOnDelete(false);
            HasMany(m => m.Owners).WithOptional(m => m.Parent).HasForeignKey(m => m.ParentId).WillCascadeOnDelete(false);
        }
    }
}
