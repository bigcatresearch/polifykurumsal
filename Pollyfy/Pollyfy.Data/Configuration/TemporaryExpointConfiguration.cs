﻿#region 

using System.Data.Entity.ModelConfiguration;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Data.Configuration
{
    public class TemporaryExpointConfiguration : EntityTypeConfiguration<TemporaryExpoint>
    {
        public TemporaryExpointConfiguration()
        {
            ToTable("TemporaryExpoints");
            HasKey(p => p.Id);
            Property(p => p.UserId).IsRequired();
            Property(p => p.Expoint).IsOptional();
        }
    }
}