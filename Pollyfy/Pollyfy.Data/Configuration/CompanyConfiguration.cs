﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class CompanyConfiguration : EntityTypeConfiguration<Company>
    {
        public CompanyConfiguration()
        {
            ToTable("Company");
            HasKey(m => m.CompanyId);
            Property(m => m.CompanyName).IsRequired();
            Property(m => m.AuthorizedPerson).IsOptional();
            Property(m => m.PhoneNumber).IsOptional();
            Property(m => m.Email).IsOptional();
            Property(m => m.Address).IsOptional();
            Property(m => m.Balance).IsOptional();
            Property(m => m.BillingDate).IsOptional();
            Property(m => m.City).IsOptional();
            Property(m => m.Comment).IsOptional();
            Property(m => m.Country).IsOptional();
            Property(m => m.Culture).IsOptional();
            Property(m => m.CreatedDate).IsRequired();
            Property(m => m.ActiveStatus).IsRequired();
            Property(m => m.Currency).IsOptional();
            Property(m => m.CustomProperty1).IsOptional();
            Property(m => m.CustomProperty2).IsOptional();
            Property(m => m.CustomProperty3).IsOptional();
            Property(m => m.CustomProperty4).IsOptional();
            Property(m => m.CustomProperty5).IsOptional();
            Property(m => m.ExpiryDate).IsOptional();
            Property(m => m.LastUpdatedDate).IsOptional();
            Property(m => m.LicenseType).IsRequired();
            Property(m => m.LogoId).IsOptional();
            Property(m => m.MaxQuestionCount).IsOptional();
            Property(m => m.MaxSurveyCount).IsOptional();
            Property(m => m.MaxUserCount).IsOptional();
         //   Property(m => m.SectorId).IsRequired();
            Property(m => m.TrackingGuid).IsRequired();
            Ignore(m => m.FileContainer);
        }

    }
}
