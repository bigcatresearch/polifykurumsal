﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class HelpdeskConfiguration : EntityTypeConfiguration<Helpdesk>
    {
        public HelpdeskConfiguration()
        {
            ToTable("Helpdesk");
            HasKey(m => m.HelpdeskId);
            Property(m => m.CreatedDate).IsRequired();
            Property(m => m.ActiveStatus).IsRequired();
            Property(m => m.Message).IsRequired();
            Property(m => m.TrackingGuid).IsRequired();
            Property(m => m.UserId).IsRequired();
            Property(m => m.SurveyId).IsOptional();
            Property(m => m.QuestionId).IsOptional();
        }

    }
}
