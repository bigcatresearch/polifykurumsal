﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("User");
            HasKey(m => m.UserId);
            Property(m => m.UserName).IsRequired().HasMaxLength(200);
            Property(m => m.ActiveStatus).IsRequired();
            Property(m => m.Address).IsOptional().HasMaxLength(500);
           // Property(m => m.Sector).IsOptional().HasMaxLength(500);
            Property(m => m.BirthDate).IsOptional();
            Property(m => m.Comment).IsOptional().HasMaxLength(2000);
            Property(m => m.CompanyId).IsOptional();
            Property(m => m.Country).IsOptional().HasMaxLength(100);
            Property(m => m.CreatedDate).IsRequired();
            Property(m => m.Culture).IsOptional();
            Property(m => m.Currency).IsOptional();
            Property(m => m.Email).IsRequired().HasMaxLength(200);
            Property(m => m.EmailAcivationCode).IsOptional();
            Property(m => m.ExpiryDate).IsOptional();
            Property(m => m.FacebookId).IsOptional().HasMaxLength(500);
            Property(m => m.FirstName).IsRequired().HasMaxLength(400);
            Property(m => m.Gender).IsOptional().HasMaxLength(20);
            Property(m => m.GoogleId).IsOptional().HasMaxLength(500);
            Property(m => m.IdentityNumber).IsOptional();
            Property(m => m.IsEmailValidated).IsOptional();
            Property(m => m.IsOnline).IsOptional();
            Property(m => m.IsPhoneValidated).IsOptional();
            Property(m => m.LastAccessTime).IsOptional();
            Property(m => m.LastName).IsRequired().HasMaxLength(400);
            Property(m => m.LastUpdatedDate).IsOptional();
            Property(m => m.Location).IsOptional().HasMaxLength(400);
            Property(m => m.LoginChannel).IsOptional().HasMaxLength(500);
            Property(m => m.OpenId).IsOptional().HasMaxLength(500);
            Property(m => m.Password).IsRequired().HasMaxLength(250);
            Property(m => m.PhoneAcivationCode).IsOptional().HasMaxLength(400);
            Property(m => m.PhoneNumber).IsOptional().HasMaxLength(20);
            Property(m => m.ProfileImageId).IsOptional();
            Property(m => m.TrackingGuid).IsRequired();
            Property(m => m.TwitterId).IsOptional().HasMaxLength(500);
            Property(m => m.Types).IsRequired();
            Property(m => m.Balance).IsOptional().HasPrecision(18, 4);
            Property(m => m.Latitude).IsOptional().HasMaxLength(50);
            Property(m => m.Longitude).IsOptional().HasMaxLength(50);
            Property(m => m.LastActiveDeviceId).IsOptional();
            Property(m => m.ExPoint).IsRequired();
            Property(m => m.AllowedInfoShare).IsRequired();

            HasMany(m => m.Properties).WithMany(m => m.Users).Map(c => { c.MapLeftKey("UserId"); c.MapRightKey("ValueId"); c.ToTable("UserProperties"); });
            HasMany(m => m.Enrollments).WithRequired(m => m.User).HasForeignKey(m => m.UserId).WillCascadeOnDelete(false);
            HasMany(m => m.Answers).WithRequired(m => m.User).HasForeignKey(m => m.UserId).WillCascadeOnDelete(false);
            HasMany(m => m.OwnedSurveys).WithOptional(m => m.CreatedUser).HasForeignKey(m => m.CreatedByUserId).WillCascadeOnDelete(false);
            HasMany(m => m.Devices).WithRequired(m => m.Owner).HasForeignKey(m => m.UserId).WillCascadeOnDelete(false);
            HasMany(m => m.AssignedSurveys).WithMany(m => m.AssignedUsers).Map(c => { c.MapLeftKey("UserId"); c.MapRightKey("SurveyId"); c.ToTable("SurveyUsers"); });
            HasMany(m => m.Promotions).WithOptional(m => m.User).HasForeignKey(m => m.UserId).WillCascadeOnDelete(false);

            Property(m => m.CustomProperty1).IsOptional();
            Property(m => m.CustomProperty2).IsOptional();
            Property(m => m.CustomProperty3).IsOptional();
            Property(m => m.CustomProperty4).IsOptional();
            Property(m => m.CustomProperty5).IsOptional();
            Property(m => m.CustomProperty6).IsOptional();
            Property(m => m.CustomProperty7).IsOptional();
            Property(m => m.CustomProperty8).IsOptional();
            Property(m => m.CustomProperty9).IsOptional();
            Property(m => m.CustomProperty10).IsOptional();


            Ignore(m => m.CompanyName);
            Ignore(m => m.ImportFileContainer);
        }

    }
}
