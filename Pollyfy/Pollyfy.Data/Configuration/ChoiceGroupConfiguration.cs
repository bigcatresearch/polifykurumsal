﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class ChoiceGroupConfiguration : EntityTypeConfiguration<ChoiceGroup>
    {
        public ChoiceGroupConfiguration()
        {
            ToTable("ChoiceGroup");
            HasKey(m => new {  m.GroupId, m.ChoiceId, m.Route });
            Property(m => m.QuestionId).IsRequired();
        }

    }
}
