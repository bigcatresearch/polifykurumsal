﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class QuestionConfiguration : EntityTypeConfiguration<Question>
    {
        public QuestionConfiguration()
        {
            ToTable("Question");
            HasKey(m => m.QuestionId);
            Property(m => m.ActiveStatus).IsRequired();
            Property(m => m.IsRequired).IsRequired();
            Property(m => m.CompanyId).IsRequired();
            Property(m => m.Description).IsOptional();
            Property(m => m.CreatedByUserId).IsOptional();
            Property(m => m.QuestionText).IsRequired();
            Property(m => m.QuestionType).IsRequired();
            Property(m => m.SurveyId).IsRequired();
            Property(m => m.FileId).IsOptional();
            Property(m => m.EnneagramType).IsRequired();
            Property(m => m.Score).IsOptional();
            Property(m => m.MaxScore).IsOptional();
            Property(m => m.CreatedDate).IsRequired();
            Property(m => m.TrackingGuid).IsRequired();
            Property(m => m.IsImage).IsOptional();
            Property(m => m.DescribeIsImage).IsOptional();
            Property(m => m.IsChoiceImage).IsOptional();

            HasOptional(m => m.CopiedQuestion).WithMany().HasForeignKey(m => m.CopiedId).WillCascadeOnDelete(false);
            HasMany(m => m.Choices).WithRequired(m => m.Question).HasForeignKey(m => m.QuestionId).WillCascadeOnDelete(false);
            HasMany(m => m.Answers).WithRequired(m => m.Question).HasForeignKey(m => m.QuestionId).WillCascadeOnDelete(false);

            Ignore(m => m.Groups);
            Ignore(m => m.FormChoices);
        }

    }
}
