﻿#region

using Pollyfy.Models.DataModel;
using System.Data.Entity.ModelConfiguration;

#endregion

namespace Pollyfy.Data.Configuration
{
    public class CounterConfiguration : EntityTypeConfiguration<Counter>
    {
        public CounterConfiguration()
        {
            ToTable("Counter");
            HasKey(p => p.Id);
            Property(p => p.Key).IsRequired();
            Property(p => p.Param1).IsOptional();
            Property(p => p.Param2).IsOptional();
            Property(p => p.Param3).IsOptional();
            Property(p => p.Value).IsRequired();
        }
    }
}
