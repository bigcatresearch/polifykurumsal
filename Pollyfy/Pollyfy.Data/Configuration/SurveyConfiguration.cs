﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class SurveyConfiguration : EntityTypeConfiguration<Survey>
    {
        public SurveyConfiguration()
        {
            ToTable("Survey");
            HasKey(m => m.SurveyId);
            Property(m => m.ActiveStatus).IsRequired();
            
           // Property(m => m.SectorId).IsOptional();
            Property(m => m.CompanyId).IsRequired();
            Property(m => m.Description).IsOptional();
            Property(m => m.FileId).IsOptional();
            Property(m => m.FirstPublishDate).IsOptional();
            Property(m => m.JoinStatus).IsOptional();
            Property(m => m.SurveyType).IsRequired();
            Property(m => m.LastPublishDate).IsOptional();
            Property(m => m.Name).IsRequired().HasMaxLength(500);
            Property(m => m.QuestionCount).IsRequired();
            Property(m => m.CreatedDate).IsRequired();
           // Property(m => m.Reward).IsRequired();
            Property(m => m.ExPoint).IsOptional();
            Property(m => m.JoinLimit).IsOptional();
            Property(m => m.TrackingGuid).IsRequired();
            Property(m => m.CreatedByUserId).IsOptional();
            Property(m => m.FinalMessage).IsOptional().HasMaxLength(4000);
            Property(m => m.UseAccuracy).IsRequired();
            Property(m => m.AccuracyQueCount).IsOptional();
            Property(m => m.LastUnPublishDate).IsOptional();
            Property(m => m.StartDate).IsOptional();
            Property(m => m.FinishDate).IsOptional();
            Property(m => m.StatusText).IsOptional();
            Property(m => m.PublishStatus).IsRequired();
            Property(m => m.DaylyJoinLimit).IsOptional();
            Property(m => m.UsageType).IsRequired();
            Property(m => m.AccuracyCriteria).IsOptional();
            Property(m => m.PrivacyQuestion).IsOptional();
            Property(m => m.FullingTime).IsOptional();
            Property(m => m.ImageUrl).IsOptional();
            Property(m => m.SurveyUrl).IsOptional();

            HasRequired(m => m.Company).WithMany(m => m.Surveys).HasForeignKey(m => m.CompanyId).WillCascadeOnDelete(false);
            HasMany(m => m.Enrollments).WithRequired(m => m.Survey).HasForeignKey(m => m.SurveyId).WillCascadeOnDelete(false);
            HasMany(m => m.Questions).WithRequired(m => m.Survey).HasForeignKey(m => m.SurveyId).WillCascadeOnDelete(false);
            HasMany(m => m.Answers).WithRequired(m => m.Survey).HasForeignKey(m => m.SurveyId).WillCascadeOnDelete(false);
            HasMany(m => m.Constraints).WithMany(m => m.ConstraintSurveys).Map(c => { c.MapLeftKey("SurveyId"); c.MapRightKey("ValueId"); c.ToTable("SurveyConstraints"); });
            HasMany(m => m.ReportConstraints).WithMany(m => m.ReportConstraintSurveys).Map(c => { c.MapLeftKey("SurveyId"); c.MapRightKey("CategoryId"); c.ToTable("SurveyReportConstraints"); });
            HasMany(m => m.Scopes).WithMany(m => m.ScopeSurveys).Map(c => { c.MapLeftKey("SurveyId"); c.MapRightKey("ValueId"); c.ToTable("SurveyScopes"); });
            //zencefil
            //HasRequired(m => m.Sector).WithMany().HasForeignKey(m => m.SectorId).WillCascadeOnDelete(false);
            HasMany(m => m.Promotions).WithRequired(m => m.Survey).HasForeignKey(m => m.SurveyId).WillCascadeOnDelete(false);
            
            Ignore(m => m.FormUsers);
            Ignore(m => m.FormSurveys);
            Ignore(m => m.RealQuestionCount);
            Ignore(m => m.CurrentPromotion);
            Ignore(m => m.PromotionCount);
            Ignore(m => m.QuestionGroups);
        }

    }
}
