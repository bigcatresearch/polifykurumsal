﻿using Pollyfy.Models.DataModel;
using System.Data.Entity.ModelConfiguration;

namespace Pollyfy.Data.Configuration
{
    public class EarnCampaignConfiguration : EntityTypeConfiguration<EarnCampaign>
    {
        public EarnCampaignConfiguration()
        {
            ToTable("EarnCampaign");
            HasKey(p => p.Id);
            Property(p => p.CampaignId).IsRequired();
            Property(p => p.CampaignId).IsRequired();
            Property(p => p.Note).IsOptional().HasMaxLength(2048);
        }
    }
}