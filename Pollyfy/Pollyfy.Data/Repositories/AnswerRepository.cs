﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.Entity;
namespace Pollyfy.Data
{
    public class AnswerRepository : EFRepository<Answer>, IAnswerRepository
    {
        public AnswerRepository(DbContext context) : base(context) { }

        public IEnumerable<Answer> GetBySurveyId(int surveyId)
        {
            IEnumerable<Answer> results = new List<Answer>();
            string sql = @"SELECT * FROM [Answer] A WHERE A.SurveyId = @surveyId
AND EXISTS(SELECT TOP 1 1 FROM  [User] U WHERE U.UserId = A.UserId AND U.ActiveStatus = 1);";
            results = WithConnection(c =>
            {
                var dbResults = c.QueryMultiple(sql, new { surveyId = surveyId });
                results = dbResults.Read<Answer>();
                return results;
            });
            return results;
        }
    }
}
