﻿#region

using Pollyfy.Models.DataModel;
using System.Collections.Generic;
using System.Data.Entity;
using Pollyfy.Data.Interfaces;
using Dapper;
using System;
using System.Linq;

#endregion

namespace Pollyfy.Data.Repositories
{
    public class CounterRepository : EFRepository<Counter>, ICounterRepository
    {
        public CounterRepository(DbContext dbContext) : base(dbContext)
        {

        }

        public Counter GetByParams(string key, string param1, string param2, string param3)
        {
            string sql = "SELECT TOP 1 * FROM [Counter] WHERE [Key] = @key AND Param1 = @param1 AND Param2 = @param2 AND Param3 = @param3;";

            return WithConnection(c =>
            {
                return c.Query<Counter>(sql, new { key = key, param1 = param1, param2 = param2, param3 = param3 }).SingleOrDefault();
            });
        }

        IEnumerable<Counter> ICounterRepository.GetAll()
        {
            string sql = "SELECT * FROM [Counter];";

            return WithConnection(c =>
            {
                return c.Query<Counter>(sql);
            });
        }
    }
}