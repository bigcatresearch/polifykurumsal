﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Pollyfy.Models.ViewModel;
using System.Data.Entity;
namespace Pollyfy.Data
{
    public class HelperCategoryRepository : EFRepository<HelperCategory>, IHelperCategoryRepository
    {
        public HelperCategoryRepository(DbContext context) : base(context) { }
        public IEnumerable<HelperCategory> GetAll()
        {
            string sql = "SELECT *  FROM [HelperCategory] WHERE [ActiveStatus] = 1;";
            return WithConnection(c =>
            {
                return c.Query<HelperCategory>(sql);
            });
        }
        public IEnumerable<HelperCategory> GetVisiblesByType(CategoryType types)
        {
            string sql = @"SELECT *  FROM [HelperCategory] WHERE [ActiveStatus] = 1 AND [IsVisible] = 1 AND [Types] = @types;

SELECT V.*  FROM [HelperValue] V 
INNER JOIN [HelperCategory] C ON C.CategoryId = V.CategoryId AND C.[ActiveStatus] = 1 AND C.[IsVisible] = 1 AND C.[Types] = @types
WHERE V.[ActiveStatus] = 1 AND V.[IsVisible] = 1;";
            IEnumerable<HelperCategory> result;
            result = WithConnection(con =>
            {
                var dbResults = con.QueryMultiple(sql, new { types = types });
                result = dbResults.Read<HelperCategory>();
                if (result != null && result.Count() > 0)
                {
                    var values = dbResults.Read<HelperValue>();
                    if (values != null && values.Count() > 0)
                        foreach (HelperCategory c in result)
                            c.Values = values.Where(m => m.CategoryId == c.CategoryId).ToList();
                }
                return result;
            });

            return result;
        }
        public IEnumerable<HelperCategory> GetByType(CategoryType types)
        {
            string sql = "SELECT * FROM [HelperCategory] WHERE [ActiveStatus] = 1 AND [Types] = @types;";
            return WithConnection(c =>
            {
                return c.Query<HelperCategory>(sql, new { types = types });
            });
        }
        public bool UpdateStatus(int categoryId, ActiveStatus status)
        {
            string sql = "UPDATE [HelperCategory] SET ActiveStatus = @status WHERE CategoryId = @categoryId;";
            if (status == ActiveStatus.Deleted)
                sql = "UPDATE [HelperCategory] SET ActiveStatus = @status WHERE [IsDeleteAllowed] = 1 AND CategoryId = @categoryId;";
            return WithConnection(c =>
            {
                return c.Execute(sql, new { categoryId = categoryId, status = (int)status }) == 1;
            });
        }
    }
}
