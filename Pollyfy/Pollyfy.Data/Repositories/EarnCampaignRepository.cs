﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models.DataModel;
using System.Collections.Generic;
using System.Data.Entity;
using Pollyfy.Models;
using Dapper;

namespace Pollyfy.Data.Repositories
{
    public class EarnCampaignRepository : EFRepository<EarnCampaign>, IEarnCampaignRepository
    {
        public EarnCampaignRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<EarnCampaign> GetAll(ActiveStatus? status = default(ActiveStatus?))
        {
            string sql = "SELECT * FROM [EarnCampaign] WHERE [ActiveStatus] != -1 ORDER BY [CreatedOn];";
            if (status.HasValue)
                sql = "SELECT * FROM [EarnCampaign] WHERE [ActiveStatus] = @activeStatus ORDER BY [CreatedOn];";
            return WithConnection(c =>
            {
                return c.Query<EarnCampaign>(sql, new { activeStatus = status.HasValue ? (int)status.Value : 1 });
            });
        }
    }
}