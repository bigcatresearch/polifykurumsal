﻿#region 

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Dapper;
using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Data.Repositories
{
    public class TemporaryExpointRepository : EFRepository<TemporaryExpoint>, ITemporaryExpointRepository
    {
        public TemporaryExpointRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<TemporaryExpoint> GetAll(ActiveStatus? status = null)
        {
            string sql = "SELECT * FROM [TemporaryExpoints] ORDER BY [Expoint] DESC;";

            return WithConnection(c =>
            {
                return c.Query<TemporaryExpoint>(sql);
            });
        }

        public TemporaryExpoint GetByUserId(int userId)
        {
            string sql = "SELECT TOP 1 * FROM [TemporaryExpoints] WHERE UserId = @userId;";

            return WithConnection(c =>
            {
                return c.Query<TemporaryExpoint>(sql, new { userId = userId }).SingleOrDefault();
            });
        }

        public bool DeleteAll()
        {
            string sql = "DELETE FROM [TemporaryExpoints]";

            return WithConnection(c =>
            {
                return c.Execute(sql) > 1;
            });
        }
    }
}