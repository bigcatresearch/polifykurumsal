﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class LogRepository : EFRepository<Log>, ILogRepository
    {
        public LogRepository(DbContext context) : base(context) { }
        
    }
}
