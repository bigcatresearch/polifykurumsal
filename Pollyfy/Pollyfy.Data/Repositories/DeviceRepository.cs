﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.Entity;
namespace Pollyfy.Data
{
    public class DeviceRepository : EFRepository<Device>, IDeviceRepository
    {
        public DeviceRepository(DbContext context) : base(context) { }
    }
}
