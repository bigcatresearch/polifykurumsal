﻿#region 

using System.Collections.Generic;
using System.Data.Entity;
using Dapper;
using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Data.Repositories
{
    public class AuthorizationRepository : EFRepository<Authorization>, IAuthorizationRepository
    {
        public AuthorizationRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<Authorization> GetAll(ActiveStatus? status = null)
        {
            string sql = "SELECT * FROM [Authorization] WHERE [ActiveStatus] != -1 ORDER BY [Name];";
            if (status.HasValue)
                sql = "SELECT * FROM [Authorization] WHERE [ActiveStatus] = @activeStatus ORDER BY [Name];";
            return WithConnection(c =>
            {
                return c.Query<Authorization>(sql, new { activeStatus = status.HasValue ? (int)status.Value : 1 });
            });
        }
    }
}