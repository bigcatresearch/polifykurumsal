﻿#region 

using System.Collections.Generic;
using System.Data.Entity;
using Dapper;
using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Data.Repositories
{
    public class UserRoleRepository : EFRepository<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<UserRole> GetAll(ActiveStatus? activeStatus = null)
        {
            var sql = "SELECT * FROM [UserRole] WHERE [ActiveStatus] != -1 ORDER BY [Name];";

            if (activeStatus.HasValue)
                sql = "SELECT * FROM [UserRole] WHERE [ActiveStatus] = @activeStatus ORDER BY [Name];";

            return WithConnection(c => c.Query<UserRole>(sql, new { activeStatus = activeStatus.HasValue ? (int)activeStatus.Value : 1 }));
        }

        public bool UpdateStatus(int userRoleId, ActiveStatus status)
        {
            string sql = "UPDATE [UserRole] SET ActiveStatus = @status WHERE Id = @userRoleId;";
            return WithConnection(c =>
            {
                return c.Execute(sql, new { userRoleId = userRoleId, status = (int)status }) == 1;
            });
        }
    }
}