﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.Entity;
using Pollyfy.Models.ViewModel;
namespace Pollyfy.Data
{
    public class EnrollmentRepository : EFRepository<Enrollment>, IEnrollmentRepository
    {
        public EnrollmentRepository(DbContext context) : base(context) { }
        public IEnumerable<Enrollment> GetBySurveyId(int surveyId)
        {
            IEnumerable<Enrollment> results = new List<Enrollment>();
            string sql = @"SELECT * FROM [Enrollment] E WHERE E.SurveyId = @surveyId AND E.RepeatId = 1
AND EXISTS(SELECT TOP 1 1 FROM  [User] U WHERE U.UserId = E.UserId AND U.ActiveStatus = 1);";
            results = WithConnection(c =>
            {
                var dbResults = c.QueryMultiple(sql, new { surveyId = surveyId });
                results = dbResults.Read<Enrollment>();
                return results;
            });

            return results;
        }

        public IEnumerable<ParticipantModel> GetParticipants(int surveyId, EnrollmentStatus status)
        {
            IEnumerable<ParticipantModel> results = new List<ParticipantModel>();
            string sql = @"SELECT U.UserId, U.FirstName, U.LastName, U.Email,U.IdentityNumber, U.PhoneNumber, S.FullingTime,
U.BirthDate, U.Gender, U.Location, D.[Platform], D.Imei, D.Operator, D.Manufacturer,
D.Model, D.Name AS 'DeviceName', E.StartDate, E.FinishDate, E.AllowedInfoShare
FROM [Survey] S
INNER JOIN [Enrollment] E ON E.SurveyId = S.SurveyId AND E.RepeatId = 1 AND E.[Status] = @status
INNER JOIN [User] U ON U.UserId = E.UserId AND U.ActiveStatus <> -1
LEFT JOIN [Device] D ON D.DeviceId = E.DeviceId
WHERE S.SurveyId = @surveyId AND S.ActiveStatus <> -1;";
            results = WithConnection(c =>
            {
                return c.Query<ParticipantModel>(sql, new { surveyId = surveyId, status = (int)status });
            });

            return results;
        }
        public IEnumerable<ConstraintModel> GetConstraintCounts(int surveyId)
        {
            IEnumerable<ConstraintModel> results = new List<ConstraintModel>();
            #region old_code
            //            string sql = @"SELECT V.ValueId, C.CategoryId, C.[Text] AS 'CategoryName', V.[Text] AS 'ValueName', COUNT(P.UserId) AS 'Count'
            //FROM [HelperCategory] C
            //INNER JOIN [HelperValue] V On V.CategoryId = C.CategoryId AND V.ActiveStatus = 1
            //LEFT JOIN (
            //	SELECT SU.UserId, SU.ValueId  FROM [dbo].[UserProperties] SU
            //	INNER JOIN [Enrollment] E ON E.SurveyId = @surveyId AND E.RepeatId = 1 AND E.[Status] = 4 AND E.UserId = SU.UserId 
            //    WHERE EXISTS(SELECT TOP 1 1 FROM  [User] U WHERE U.UserId = SU.UserId AND U.ActiveStatus = 1)
            //) P ON P.ValueId = V.ValueId
            //WHERE C.ActiveStatus = 1 AND C.[Types] = 1
            //AND NOT EXISTS( 
            //	SELECT TOP 1 1 FROM [dbo].[SurveyConstraints] SC
            //	INNER JOIN [dbo].[HelperValue] CV ON CV.ValueId = SC.ValueId AND CV.CategoryId = C.CategoryId AND CV.ActiveStatus = 1
            //	WHERE SC.SurveyId = @surveyId GROUP BY CV.CategoryId HAVING COUNT(CV.CategoryId) = 1
            //)
            //GROUP BY C.[Text], V.[Text], V.ValueId, C.CategoryId
            //ORDER BY C.[Text], V.ValueId;";
            #endregion
            string sql = @"SELECT V.ValueId, C.CategoryId, C.[Text] AS 'CategoryName', V.[Text] AS 'ValueName', COUNT(P.UserId) AS 'Count'
FROM [HelperCategory] C
INNER JOIN [HelperValue] V On V.CategoryId = C.CategoryId AND V.ActiveStatus = 1
LEFT JOIN (
	SELECT SU.UserId, SU.ValueId  FROM [dbo].[UserProperties] SU
	INNER JOIN [Enrollment] E ON E.SurveyId = @surveyId AND E.RepeatId = 1 AND E.[Status] = 4 AND E.UserId = SU.UserId 
    WHERE EXISTS(SELECT TOP 1 1 FROM  [User] U WHERE U.UserId = SU.UserId AND U.ActiveStatus = 1)
) P ON P.ValueId = V.ValueId
WHERE C.ActiveStatus = 1 AND C.[Types] = 1
AND (C.AlwaysShow = 1 OR EXISTS( 
	SELECT TOP 1 1 FROM [dbo].[SurveyReportConstraints] SC WHERE SC.SurveyId = @surveyId AND SC.CategoryId = C.CategoryId
))
GROUP BY C.[Text], V.[Text], V.ValueId, C.CategoryId
ORDER BY C.[Text], V.ValueId;";
            results = WithConnection(c =>
            {
                return c.Query<ConstraintModel>(sql, new { surveyId = surveyId });
            });

            return results;
        }
        public IEnumerable<ConstraintModel> GetUserConstraints(int surveyId)
        {
            IEnumerable<ConstraintModel> results = new List<ConstraintModel>();
            string sql = @"SELECT V.ValueId, C.CategoryId, C.[Text] AS 'CategoryName', V.[Text] AS 'ValueName', P.UserId
FROM [HelperCategory] C
INNER JOIN [HelperValue] V On V.CategoryId = C.CategoryId AND V.ActiveStatus = 1
LEFT JOIN (
	SELECT SU.UserId, SU.ValueId  FROM [dbo].[UserProperties] SU
	INNER JOIN [Enrollment] E ON E.SurveyId = @surveyId AND E.RepeatId = 1 AND E.[Status] = 4 AND E.UserId = SU.UserId 
    WHERE EXISTS(SELECT TOP 1 1 FROM  [User] U WHERE U.UserId = SU.UserId AND U.ActiveStatus = 1)
) P ON P.ValueId = V.ValueId
WHERE C.ActiveStatus = 1 AND C.[Types] = 1
AND (C.AlwaysShow = 1 OR EXISTS( 
	SELECT TOP 1 1 FROM [dbo].[SurveyReportConstraints] SC WHERE SC.SurveyId = @surveyId AND SC.CategoryId = C.CategoryId
))
ORDER BY C.[Text], V.ValueId;";
            results = WithConnection(c =>
            {
                return c.Query<ConstraintModel>(sql, new { surveyId = surveyId });
            });

            return results;
        }

        public IEnumerable<Coord> GetParticipantCoords(int surveyId)
        {
            IEnumerable<Coord> results = new List<Coord>();
            string sql = @"SELECT U.Latitude, U.Longitude FROM Enrollment E 
INNER JOIN [User] U ON U.UserId = E.UserId
WHERE E.SurveyId = @surveyId AND E.RepeatId = 1 AND E.[Status] = 4 AND U.ActiveStatus = 1
AND ISNULL(U.Latitude,'') <> '' AND U.Latitude<>'0.0';";
            results = WithConnection(c =>
            {
                return c.Query<Coord>(sql, new { surveyId = surveyId });
            });

            return results;
        }

        public IEnumerable<Enrollment> GetBySurveyIds(string surveyIds, ActiveStatus activeStatus = ActiveStatus.Active)
        {
            IEnumerable<Enrollment> results = new List<Enrollment>();

            string sql = string.Format(@"SELECT *FROM [Enrollment] E WHERE E.SurveyId in ({0})", surveyIds);

            results = WithConnection(c =>
            {
                return c.Query<Enrollment>(sql, new { activeStatus = activeStatus });
            });

            return results;
        }
    }
}
