﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models.DataModel;
using System.Collections.Generic;
using Pollyfy.Models;
using System.Data.Entity;
using Dapper;

namespace Pollyfy.Data.Repositories
{
    public class CampaignRepository : EFRepository<Campaign>, ICampaignRepository
    {
        public CampaignRepository(DbContext dbContext) : base(dbContext)
        {

        }

        public IEnumerable<Campaign> GetAll(ActiveStatus? status = default(ActiveStatus?))
        {
            string sql = "SELECT * FROM [Campaign] WHERE [ActiveStatus] != -1 ORDER BY [Name];";
            if (status.HasValue)
                sql = "SELECT * FROM [Campaign] WHERE [ActiveStatus] = @activeStatus ORDER BY [Name];";
            return WithConnection(c =>
            {
                return c.Query<Campaign>(sql, new { activeStatus = status.HasValue ? (int)status.Value : 1 });
            });
        }

        public bool UpdateStatus(int campaignId, ActiveStatus status)
        {
            string sql = "UPDATE [Campaign] SET ActiveStatus = @status WHERE Id = @campaignId;";

            return WithConnection(c =>
            {
                return c.Execute(sql, new { campaignId = campaignId, status = (int)status }) == 1;
            });
        }
    }
}