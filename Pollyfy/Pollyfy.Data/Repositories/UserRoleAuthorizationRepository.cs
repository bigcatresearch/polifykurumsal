﻿#region 

using System;
using System.Collections.Generic;
using System.Data.Entity;
using Dapper;
using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using Pollyfy.Models.DataModel;

#endregion

namespace Pollyfy.Data.Repositories
{
    public class UserRoleAuthorizationRepository : EFRepository<UserRoleAuthorization>, IUserRoleAuthorizationRepository
    {
        public UserRoleAuthorizationRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<UserRoleAuthorization> GetAll(ActiveStatus? status = null)
        {
            string sql = "SELECT * FROM [UserRoleAuthorization] WHERE [ActiveStatus] != -1 ORDER BY [CreatedOn];";
            if (status.HasValue)
                sql = "SELECT * FROM [UserRoleAuthorization] WHERE [ActiveStatus] = @activeStatus ORDER BY [CreatedOn];";
            return WithConnection(c =>
            {
                return c.Query<UserRoleAuthorization>(sql, new { activeStatus = status.HasValue ? (int)status.Value : 1 });
            });
        }

        public IEnumerable<UserRoleAuthorization> GetByUserRoleId(int userRoleId)
        {
            string sql = "SELECT * FROM [UserRoleAuthorization] Where ActiveStatus <> -1 AND UserRoleId = @userRoleId ORDER BY UserRoleId;";
            return WithConnection(c =>
            {
                return c.Query<UserRoleAuthorization>(sql, new { userRoleId = userRoleId });
            });
        }
    }
}