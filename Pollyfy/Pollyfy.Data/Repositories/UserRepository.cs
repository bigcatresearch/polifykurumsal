﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Pollyfy.Models.ViewModel;
using System.Data.Entity;
using Pollyfy.Models.DataModel;

namespace Pollyfy.Data
{
    public class UserRepository : EFRepository<User>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context) { }

        public IEnumerable<string> GetEmail(string sql , string Tablename)
        {
            string Sql_ = "SELECT [Id] FROM [" + Tablename + "] " + sql;
            return WithConnection(c =>
            {
                return c.Query<string>(Sql_);
            });
        }
  
        public IEnumerable<User> GetAll(int? companyId)
        {
            string sql = @"SELECT U.*, C.CompanyName FROM [User] U 
LEFT JOIN [Company] C ON C.CompanyId = U.CompanyId
WHERE U.[ActiveStatus] != -1 AND (ISNULL(@companyId,0) = 0 OR U.CompanyId = @companyId);";
            return WithConnection(c =>
            {
                return c.Query<User>(sql, new { companyId = companyId });
            });
        }
        public IEnumerable<string> GetTableName()
        {
            string sql = "SELECT DISTINCT [TableName] FROM [Title]";
            return WithConnection(c => c.Query<string>(sql));
        }
        public IEnumerable<string> GetByConstaint(string tableName)
        {
            string sql = "SELECT [TitleName] FROM [Title] WHERE [TableName] = @tableName";
            return WithConnection(c => c.Query<string>(sql, new { tableName = tableName }));
        }
        public IEnumerable<string> GetValuesByConstaint(string Constaint, string TableName)
        {
            string sql = "SELECT DISTINCT " + Constaint +" FROM [" + TableName + "]";
            return WithConnection(c => c.Query<string>(sql, new {Constaint = Constaint, TableName = TableName }));
        }
        public IEnumerable<User> GetList(ActiveStatus? activeStatus = null)
        {
            var sql = "SELECT * FROM [User] WHERE [ActiveStatus] != -1 ORDER BY [UserName];";

            if (activeStatus.HasValue)
                sql = "SELECT * FROM [User] WHERE [ActiveStatus] = @activeStatus ORDER BY [UserName];";

            return WithConnection(c => c.Query<User>(sql, new { activeStatus = activeStatus.HasValue ? (int)activeStatus.Value : 1 }));
        }

        public User GetUser(string userName, string password)
        {
            string sql = "SELECT TOP 1 * FROM [User] WHERE ActiveStatus = 1 AND (UserName = @username OR [Email] = @username) AND [Password] = @password;";
            return WithConnection(c =>
            {
                return c.Query<User>(sql, new { username = userName, password = password }).SingleOrDefault();
            });
        }
        public User GetUserByUserName(string userName)
        {
            string sql = "SELECT TOP 1 * FROM [User] WHERE ActiveStatus = 1 AND (UserName = @username OR [Email] = @username);";
            return WithConnection(c =>
            {
                return c.Query<User>(sql, new { username = userName }).SingleOrDefault();
            });
        }
        public User GetUserByFacebookId(string facebookId)
        {
            string sql = "SELECT TOP 1 * FROM [User] WHERE ActiveStatus = 1 AND FacebookId = @facebookId;";
            return WithConnection(c =>
            {
                return c.Query<User>(sql, new { facebookId = facebookId }).SingleOrDefault();
            });
        }

        public User GetUserByImei(string imei)
        {
            string sql = @"SELECT TOP 1 U.* FROM [User] U  INNER JOIN Device UD On UD.UserId = U.UserId AND UD.Imei = @imei WHERE U.ActiveStatus = 1;";
            return WithConnection(c =>
            {
                return c.Query<User>(sql, new { imei = imei }).SingleOrDefault();
            });
        }

        public User GetUserByReferanceNumber(string referanceNumber)
        {
            string sql = "SELECT TOP 1 * FROM [User] WHERE ActiveStatus = 1 AND MyReferanceNumber = @referanceNumber;";

            return WithConnection(c =>
            {
                return c.Query<User>(sql, new { referanceNumber = referanceNumber }).SingleOrDefault();
            });
        }

        public User GetUserByUserId(int userId)
        {
            string sql = "SELECT TOP 1 * FROM [User] WHERE ActiveStatus = 1 AND UserId = @userid;";
            return WithConnection(c =>
            {
                return c.Query<User>(sql, new { userid = userId }).SingleOrDefault();
            });
        }
        public IEnumerable<UserPropertyModel> GetProperties(int userId, CategoryType types = CategoryType.SurveyProperties)
        {
            string sql = @"SELECT DISTINCT UserId, C.CategoryId, C.[Text] AS Caption, U.ValueId, V.[Text] FROM [UserProperties] U
INNER JOIN [HelperValue] V ON V.ValueId = U.ValueId  
INNER JOIN [HelperCategory] C ON C.CategoryId = V.CategoryId WHERE C.[Types] = @types AND U.[UserId] = @userId;";
            return WithConnection(c =>
            {
                return c.Query<UserPropertyModel>(sql, new { types = types, userId = userId });
            });
        }

        public int AddUserProperty(int userId, int valueId)
        {
            string sql = @"IF NOT EXISTS (SELECT TOP 1 1 FROM [UserProperties] WHERE [ValueId] = @valueId AND [UserId] = @userId)
INSERT INTO [UserProperties]([ValueId],[UserId]) VALUES(@valueId, @userId);";
            return WithConnection(c =>
            {
                return c.Execute(sql, new { valueId = valueId, userId = userId });
            });
        }
        public int RemoveUserProperty(int userId, int valueId)
        {
            string sql = @"DELETE FROM [UserProperties] WHERE [ValueId] = @valueId AND [UserId] = @userId;";
            return WithConnection(c =>
            {
                return c.Execute(sql, new { valueId = valueId, userId = userId });
            });
        }
        public User GetWithProperties(int userId)
        {
            string sql = @"
SELECT TOP 1 * FROM [User] WHERE ActiveStatus = 1 AND UserId = @userId;
SELECT V.* FROM [UserProperties] UP 
INNER JOIN [HelperValue] V ON V.ValueId = UP.ValueId AND V.ActiveStatus = 1
WHERE UP.UserId = @userId;";
            User result = null;

            result = WithConnection(c =>
            {
                var dbResults = c.QueryMultiple(sql, new { userId = userId });
                result = dbResults.Read<User>().SingleOrDefault();
                if (result != null)
                    result.Properties = dbResults.Read<HelperValue>().ToList();
                return result;
            });

            return result;
        }
        public virtual int MultipleInsert(IEnumerable<User> models)
        {
            string queryInsert = SQLBuilder.InsertSqlTemplate<User>(false);
            string query = string.Format(@"IF NOT EXISTS(SELECT TOP 1 1 FROM [User] WHERE [Email]=@email AND [ActiveStatus]<>-1) 
            BEGIN 
            {0}
            END
            ", queryInsert);
            List<object> parameters = new List<object>();
            foreach (User model in models)
                parameters.Add(SQLBuilder.GetParameterValues(model));
            return WithConnection(c =>
            {
                return c.Execute(query, parameters.ToArray(), commandType: CommandType.Text);
            });
        }
        public void UpdateCoordinate(int userId, string latitude, string longitude, string location)
        {
            string sql = @"UPDATE [User] SET [Latitude]=@latitude, [Longitude]=@longitude, [Location]= @location WHERE [UserId] = @userId;";

            WithConnection(c =>
            {
                return c.Execute(sql, new { userId = userId, latitude = latitude, longitude = longitude, location = location });
            });
        }
        public async Task UpdateAccessTimeAsync(int userId)
        {
            string sql = @"UPDATE [User] SET [LastAccessTime]=GETDATE() WHERE [UserId] = @userId;";

            await WithConnectionAsync(async c =>
            {
                return await c.ExecuteAsync(sql, new { userId = userId });
            });
        }
        public void UpdateAccessTime(int userId)
        {
            string sql = @"UPDATE [User] SET [LastAccessTime]=GETDATE() WHERE [UserId] = @userId;";

            WithConnection(c =>
            {
                return c.Execute(sql, new { userId = userId });
            });
        }
        public DashboardModel GetMainDashboardData(int? companyId, UserType userType)
        {
            DashboardModel result = null;
            string sql = @" 
DECLARE @companyId INT = @p_companyId;
DECLARE @userType INT = @p_userType;
SELECT 
(CASE WHEN @userType IN(1,2,4) THEN (SELECT Count(*) FROM [User] WHERE ActiveStatus <> -1 AND (@companyId IS NULL OR CompanyId = @companyId)) ELSE 0 END) AS 'UserCount',
(CASE WHEN @userType IN(1,2,4) THEN (SELECT Count(*) FROM [User] WHERE ActiveStatus <> -1 AND (@companyId IS NULL OR CompanyId = @companyId) AND LastAccessTime>= DATEADD(MINUTE, -5, GETDATE())) ELSE 0 END) AS 'OnlineCount',
(CASE WHEN @userType IN(1,2,3,4) THEN (SELECT Count(*) FROM [Survey] WHERE ActiveStatus <> -1 AND (@companyId IS NULL OR CompanyId = @companyId)) ELSE 0 END) AS 'SurveyCount',
(CASE WHEN @userType IN(1,2,3,4) THEN (SELECT Count(*) FROM [Survey] WHERE ActiveStatus = 1 AND (@companyId IS NULL OR CompanyId = @companyId)) ELSE 0 END) AS 'PublishCount',
(CASE WHEN @userType IN(2) THEN (SELECT Count(*) FROM [Company] WHERE ActiveStatus <> -1) ELSE 0 END) AS 'CompanyCount';
 
SELECT CONVERT(date, E.StartDate) 'Date', COUNT(E.UserId) 'Count' FROM Enrollment E
WHERE (@companyId IS NULL OR EXISTS(SELECT TOP 1 1 FROM [Survey] WHERE SurveyId = E.SurveyId AND ActiveStatus<>-1 AND CompanyId = @companyId))
AND E.StartDate>= DATEADD(MONTH,-1,GETDATE()) 
GROUP BY CONVERT(date, E.StartDate);";
            result = WithConnection(c =>
            {
                var dbResults = c.QueryMultiple(sql, new { p_companyId = companyId, p_userType = (int)userType });
                result = dbResults.Read<DashboardModel>().SingleOrDefault();
                if (result != null)
                    result.ParticipationChartSeries = dbResults.Read<ChartDataModel>().ToList();
                return result;
            });

            return result;
        }
        public int GetTotalResponseCount(int userId)
        {
            return WithConnection(c =>
            {
                return c.Query<int>("Select COUNT(1) FROM [Answer] Where UserId = @userId", new { userId = userId }).SingleOrDefault();
            });
        }
        public int TitleInsert(string Tablename, string Titlename)
        {
            string sql = "INSERT INTO [Title]([TableName],[TitleName]) VALUES(@Tablename, @Titlename);";
            return WithConnection(c =>
            {
                return c.Execute(sql, new { TableName = Tablename, TitleName = Titlename });
            });
        }
    }
}
