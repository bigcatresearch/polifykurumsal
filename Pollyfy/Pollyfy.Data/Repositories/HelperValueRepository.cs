﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.Entity;
namespace Pollyfy.Data
{
    public class HelperValueRepository : EFRepository<HelperValue>, IHelperValueRepository
    {
        public HelperValueRepository(DbContext context) : base(context) { }

        public IEnumerable<HelperValue> GetAll()
        {
            string sql = "SELECT * FROM [HelperValue] WHERE [ActiveStatus] = 1;";
            return WithConnection(c =>
            {
                return c.Query<HelperValue>(sql);
            });
        }
        public IEnumerable<HelperValue> GetByCategoryId(int categoryId)
        {
            string sql = @"SELECT V.* FROM [HelperValue] V
INNER JOIN [HelperCategory] C ON C.ActiveStatus = 1 AND C.CategoryId = V.CategoryId AND C.CategoryId = @id
WHERE V.ActiveStatus = 1 ORDER BY V.[Text]";
            return WithConnection(c =>
            {
                return c.Query<HelperValue>(sql, new { id = categoryId });
            });
        }
        public IEnumerable<HelperValue> GetByCategoryCode(string categoryCode)
        {
            string sql = @"SELECT V.* FROM [HelperValue] V
INNER JOIN [HelperCategory] C ON C.ActiveStatus = 1 AND C.CategoryId = V.CategoryId AND C.Code = @code
WHERE V.ActiveStatus = 1 ORDER BY V.[Text]";
            return WithConnection(c =>
            {
                return c.Query<HelperValue>(sql, new { code = categoryCode });
            });
        }

        public bool UpdateStatus(int valueId, ActiveStatus status)
        {
            string sql = "UPDATE [HelperValue] SET ActiveStatus = @status WHERE CategoryId = @categoryId;";
            if (status == ActiveStatus.Deleted)
                sql = "UPDATE [HelperValue] SET ActiveStatus = @status WHERE [IsDeleteAllowed] = 1 AND ValueId = @valueId;";
            return WithConnection(c =>
            {
                return c.Execute(sql, new { valueId = valueId, status = (int)status }) == 1;
            });
        }
    }
}
