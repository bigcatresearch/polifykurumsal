﻿#region

using Pollyfy.Data.Interfaces;
using Pollyfy.Models.DataModel;
using System;
using System.Collections.Generic;
using Pollyfy.Models;
using System.Data.Entity;
using Dapper;
using System.Linq;

#endregion

namespace Pollyfy.Data.Repositories
{
    public class WeeklyLeaderRepository : EFRepository<WeeklyLeader>, IWeeklyLeaderRepository
    {
        public WeeklyLeaderRepository(DbContext dbContext) : base(dbContext)
        {

        }

        public bool DeleteAll()
        {
            string sql = "DELETE FROM [WeeklyLeader]";

            return WithConnection(c =>
            {
                return c.Execute(sql) > 1;
            });
        }

        public IEnumerable<WeeklyLeader> GetAll(ActiveStatus? status = null)
        {
            string sql = "SELECT * FROM [WeeklyLeader] ORDER BY [ExPoint] DESC;";

            return WithConnection(c =>
            {
                return c.Query<WeeklyLeader>(sql);
            });
        }

        public WeeklyLeader GetByUserId(int userId)
        {
            string sql = "SELECT TOP 1 * FROM [WeeklyLeader] WHERE UserId = @userId;";

            return WithConnection(c =>
            {
                return c.Query<WeeklyLeader>(sql, new { userId = userId }).SingleOrDefault();
            });
        }
    }
}