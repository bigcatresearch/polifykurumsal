﻿using Pollyfy.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Pollyfy.Models;
using Pollyfy.Data.Utilities;
using System.Dynamic;
using FastMember;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq.Expressions;

namespace Pollyfy.Data
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {

        protected IDbConnection DbConnection
        {
            get
            {
                return this.Uow.DbConnection;
            }
        }
        protected ISQLBuilder SQLBuilder
        {
            get
            {
                return this.Uow.SQLBuilder;
            }
        }
        public IUow Uow { get; set; }

        protected async Task<T> WithConnectionAsync<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["db"].ConnectionString))
                {
                    //await connection.OpenAsync(); // Asynchronously open a connection to the database
                    return await getData(connection); // Asynchronously execute getData, which has been passed in as a Func<IDBConnection, Task<T>>
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(String.Format("{0}.WithConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(String.Format("{0}.WithConnection() experienced a SQL exception (not a timeout)", GetType().FullName), ex);
            }
        }
        protected T WithConnection<T>(Func<IDbConnection, T> getData)
        {
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["db"].ConnectionString))
                {
                    //connection.Open();
                    return getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(String.Format("{0}.WithConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(String.Format("{0}.WithConnection() experienced a SQL exception (not a timeout)", GetType().FullName), ex);
            }
        }
        protected List<T> Query<T>(string sqltext)
        {
            return Query<T>(sqltext, null);
        }
        protected List<T> Query<T>(string sqltext, object parameters, CommandType commandType = CommandType.Text)
        {

            return WithConnection(c =>
            {
                return c.Query<T>(sqltext, parameters, commandType: commandType).ToList();
            });
        }

        //public virtual IList<T> GetAll()
        //{
        //    return Query<T>("Select * from [User];");
        //}

        public virtual int DoInsert(T model, bool identityInsert = false)
        {
            var query = SQLBuilder.InsertSqlTemplate<T>(identityInsert);
            return WithConnection(c =>
            {
                return c.Query<int>(query, SQLBuilder.GetParameterValues(model)).SingleOrDefault();
            });
        }
        public virtual async Task<int> DoInsertAsync(T model, bool identityInsert = false)
        {
            var query = SQLBuilder.InsertSqlTemplate<T>(identityInsert);
            return await WithConnectionAsync(async c =>
            {
                var result = await c.QueryAsync<int>(query, SQLBuilder.GetParameterValues(model));
                return result.FirstOrDefault();
            });
        }
        public virtual int DoInsert(IEnumerable<T> models, bool identityInsert = false)
        {
            var query = SQLBuilder.InsertSqlTemplate<T>(identityInsert);
            List<object> parameters = new List<object>();
            foreach (T model in models)
                parameters.Add(SQLBuilder.GetParameterValues(model));

            return WithConnection(c =>
            {
                return c.Execute(query, parameters.ToArray(), commandType: CommandType.Text);
            });
        }
        public virtual int DoInsert<T1>(IEnumerable<T1> models, bool identityInsert = false)
        {
            var query = SQLBuilder.InsertSqlTemplate<T1>(identityInsert);
            List<object> parameters = new List<object>();
            foreach (T1 model in models)
                parameters.Add(SQLBuilder.GetParameterValues(model));

            return WithConnection(c =>
            {
                return c.Execute(query, parameters.ToArray(), commandType: CommandType.Text);
            });
        }
        public virtual int DoUpdate(T model)
        {
            var query = SQLBuilder.UpdateSqlTemplate<T>();
            return Query<int>(query, SQLBuilder.GetParameterValues(model)).SingleOrDefault();
        }

        public virtual int DoUpdate(IEnumerable<T> models)
        {
            var query = SQLBuilder.UpdateSqlTemplate<T>();
            List<object> parameters = new List<object>();
            foreach (T model in models)
                parameters.Add(SQLBuilder.GetParameterValues(model));
            return WithConnection(c =>
            {
                return c.Execute(query, parameters.ToArray(), commandType: CommandType.Text);
            });
        }
        public virtual int DoDelete(T model)
        {
            var query = SQLBuilder.DeleteSqlTemplate<T>();
            return Query<int>(query, SQLBuilder.GetParameterValues(model)).SingleOrDefault();
        }
        public virtual int DoDelete(IEnumerable<T> models)
        {
            var query = SQLBuilder.DeleteSqlTemplate<T>();
            List<object> parameters = new List<object>();
            foreach (T model in models)
                parameters.Add(SQLBuilder.GetParameterValues(model));
            return WithConnection(c =>
            {
                return c.Execute(query, parameters.ToArray(), commandType: CommandType.Text);
            });
        }
        public virtual int DoDelete(IEnumerable<int> Ids)
        {
            var query = SQLBuilder.DeleteSqlTemplate<T>();
            List<object> parameters = new List<object>();
            var primaryKey = SQLBuilder.FindPrimaryKey<T>();
            if (primaryKey.Count == 0) return 0;
            IDictionary<string, object> p;
            foreach (int id in Ids)
            {
                p = new Dictionary<string, object>();
                p.Add(primaryKey.Values.First(), id);
                parameters.Add(p);
            }
            return WithConnection(c =>
            {
                return c.Execute(query, parameters.ToArray(), commandType: CommandType.Text);
            });
        }
        public virtual int DoDelete(int Id)
        {
            var query = SQLBuilder.DeleteSqlTemplate<T>();
            List<object> parameters = new List<object>();
            var primaryKey = SQLBuilder.FindPrimaryKey<T>();
            if (primaryKey.Count == 0) return 0;
            IDictionary<string, object> p;
            p = new Dictionary<string, object>();
            p.Add(primaryKey.Values.First(), Id);
            parameters.Add(p);
            return DbConnection.Execute(query, parameters.ToArray(), commandType: CommandType.Text);
        }
        public virtual T GetByKey(int id)
        {
            var query = SQLBuilder.SelectByKeyTemplate<T>(id);
            return WithConnection(c =>
            {
                return c.Query<T>(query).SingleOrDefault();
            });
        }

        #region IDisposable
        public bool CanDispose { get; set; }

        public void Dispose(bool force = true)
        {
            this.CanDispose = true;
            Dispose();
        }

        public void Dispose()
        {
            if (DbConnection != null && this.CanDispose)
                DbConnection.Dispose();
        }
        #endregion
    }
}
