﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.Entity;
namespace Pollyfy.Data
{
    public class PromotionRepository : EFRepository<Promotion>, IPromotionRepository
    {
        public PromotionRepository(DbContext context) : base(context) { }
        public virtual int MultipleInsert(IEnumerable<Promotion> models)
        {
            string queryInsert = SQLBuilder.InsertSqlTemplate<Promotion>(false);
            string query = string.Format(@"IF NOT EXISTS(SELECT TOP 1 1 FROM [Promotion] WHERE [Code]=@code AND [ActiveStatus]=1) 
            BEGIN 
            {0}
            END
            ", queryInsert);
            List<object> parameters = new List<object>();
            foreach (Promotion model in models)
                parameters.Add(SQLBuilder.GetParameterValues(model));
            return WithConnection(c =>
            {
                return c.Execute(query, parameters.ToArray(), commandType: CommandType.Text);
            });
        }
    }
}
