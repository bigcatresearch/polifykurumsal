﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.Entity;
using Pollyfy.Models.ViewModel;

namespace Pollyfy.Data
{
    public class SurveyRepository : EFRepository<Survey>, ISurveyRepository
    {
        public SurveyRepository(DbContext context) : base(context) { }

        public IEnumerable<Survey> GetForAll()
        {
            string sql = @"SELECT *,
(SELECT COUNT(1) FROM [Question] Q WHERE Q.SurveyId = S.SurveyId AND Q.ActiveStatus = 1 AND Q.CopiedId IS NULL) AS 'RealQuestionCount'
FROM [Survey] S WHERE S.ActiveStatus <> -1 ORDER BY S.CreatedDate DESC;";
            return WithConnection(c =>
            {
                return c.Query<Survey>(sql);
            });
        }
        public Survey GetForAll(int surveyId)
        {
            string sql = @"SELECT TOP 1 *,
(SELECT COUNT(1) FROM [Question] Q WHERE Q.SurveyId = S.SurveyId AND Q.ActiveStatus = 1 AND Q.CopiedId IS NULL) AS 'RealQuestionCount'
FROM [Survey] S WHERE S.SurveyId = @surveyId AND S.ActiveStatus <> -1 ORDER BY S.CreatedDate DESC;";
            return WithConnection(c =>
            {
                return c.Query<Survey>(sql, new { surveyId = surveyId }).SingleOrDefault();
            });
        }

        public bool InsertScopes(int surveyId, int[] valueIds)
        {
            if (valueIds.Count() == 0) return false;
            string sql = @"IF NOT EXISTS(SELECT TOP 1 1 FROM [SurveyScopes] WHERE SurveyId = @surveyId AND ValueId = @valueId) 
INSERT INTO [SurveyScopes]([SurveyId],[ValueId]) VALUES(@surveyId, @valueId)";
            List<object> parameters = new List<object>();
            foreach (int id in valueIds)
                parameters.Add(new { surveyId = surveyId, valueId = id });
            return WithConnection(c =>
            {
                return c.Execute(sql, parameters) == valueIds.Count();
            });
        }
        public bool InsertConstraints(int surveyId, int[] valueIds)
        {
            if (valueIds.Count() == 0) return false;
            string sql = @"IF NOT EXISTS(SELECT TOP 1 1 FROM [SurveyConstraints] WHERE SurveyId = @surveyId AND ValueId = @valueId)
INSERT INTO [SurveyConstraints]([SurveyId],[ValueId]) VALUES(@surveyId, @valueId)";
            List<object> parameters = new List<object>();
            foreach (int id in valueIds)
                parameters.Add(new { surveyId = surveyId, valueId = id });
            return WithConnection(c =>
            {
                return c.Execute(sql, parameters) == valueIds.Count();
            });
        }
        public bool InsertReportConstraints(int surveyId, int[] valueIds)
        {
            if (valueIds.Count() == 0) return false;
            string sql = @"IF NOT EXISTS(SELECT TOP 1 1 FROM [SurveyReportConstraints] WHERE SurveyId = @surveyId AND CategoryId = @valueId)
INSERT INTO [SurveyReportConstraints]([SurveyId],[CategoryId]) VALUES(@surveyId, @valueId)";
            List<object> parameters = new List<object>();
            foreach (int id in valueIds)
                parameters.Add(new { surveyId = surveyId, valueId = id });
            return WithConnection(c =>
            {
                return c.Execute(sql, parameters) == valueIds.Count();
            });
        }
        public int InsertUsers(int surveyId, int[] userIds)
        {
            if (userIds.Count() == 0) return 0;
            string sql = @"IF NOT EXISTS(SELECT TOP 1 1 FROM [SurveyUsers] WHERE SurveyId = @surveyId AND UserId = @userId)
INSERT INTO [SurveyUsers]([SurveyId],[UserId]) VALUES(@surveyId, @userId)";
            List<object> parameters = new List<object>();
            foreach (int id in userIds)
                parameters.Add(new { surveyId = surveyId, userId = id });
            return WithConnection(c =>
            {
                return c.Execute(sql, parameters);
            });
        }
        public IEnumerable<Survey> GetByUserId(int userId)
        {
            string sql = @"SELECT *, 
(SELECT COUNT(1) FROM [Question] Q WHERE Q.SurveyId = S.SurveyId AND Q.ActiveStatus = 1 AND Q.CopiedId IS NULL) AS 'RealQuestionCount'
FROM [Survey] S Where S.ActiveStatus <> -1 AND S.CreatedByUserId = @userId ORDER BY S.CreatedDate DESC;";
            return WithConnection(c =>
            {
                return c.Query<Survey>(sql, new { userId = userId });
            });
        }
        public IEnumerable<Survey> GetByCompanyId(int companyId)
        {
            string sql = @"SELECT *,
(SELECT COUNT(1) FROM [Question] Q WHERE Q.SurveyId = S.SurveyId AND Q.ActiveStatus = 1 AND Q.CopiedId IS NULL) AS 'RealQuestionCount'
FROM [Survey] S Where S.ActiveStatus <> -1 AND S.CompanyId = @companyId ORDER BY S.CreatedDate DESC;";
            return WithConnection(c =>
            {
                return c.Query<Survey>(sql, new { companyId = companyId });
            });
        }

        public IEnumerable<int> SelectScopes(int surveyId)
        {
            string sql = @"SELECT ValueId FROM [SurveyScopes] WHERE SurveyId = @surveyId;";
            return WithConnection(c =>
            {
                return c.Query<int>(sql, new { surveyId = surveyId });
            });
        }
        public IEnumerable<int> SelectConstraints(int surveyId)
        {
            string sql = @"SELECT ValueId FROM [SurveyConstraints] WHERE SurveyId = @surveyId;";
            return WithConnection(c =>
            {
                return c.Query<int>(sql, new { surveyId = surveyId });
            });
        }
        public IEnumerable<int> SelectReportConstraints(int surveyId)
        {
            string sql = @"SELECT CategoryId FROM [SurveyReportConstraints] WHERE SurveyId = @surveyId;";
            return WithConnection(c =>
            {
                return c.Query<int>(sql, new { surveyId = surveyId });
            });
        }
        public IEnumerable<int> SelectUsers(int surveyId)
        {
            string sql = @"SELECT UserId FROM [SurveyUsers] WHERE SurveyId = @surveyId;";
            return WithConnection(c =>
            {
                return c.Query<int>(sql, new { surveyId = surveyId });
            });
        }
        public bool UpdateStatus(int surveyId, ActiveStatus status)
        {
            string sql = "UPDATE [Survey] SET ActiveStatus = @status WHERE SurveyId = @surveyId;";
            return WithConnection(c =>
            {
                return c.Execute(sql, new { surveyId = surveyId, status = (int)status }) == 1;
            });
        }
        public bool DeleteScopes(int surveyId, int?[] valueIds = null, bool? deleteAll = false)
        {
            if (surveyId == 0) return false;
            string sql = "";
            if (deleteAll.HasValue && deleteAll.Value == true)
                sql = "DELETE FROM [SurveyScopes] WHERE SurveyId = @surveyId;";
            else
                sql = @"DELETE FROM [SurveyScopes] WHERE SurveyId = @surveyId AND ValueId = @valueId;";

            List<object> parameters = new List<object>();
            parameters.Add(new { surveyId = surveyId });
            if (deleteAll.HasValue && deleteAll.Value == false)
                foreach (int id in valueIds)
                    parameters.Add(new { surveyId = surveyId, valueId = id });
            return WithConnection(c =>
            {
                return c.Execute(sql, parameters) > 1;
            });
        }
        public bool DeleteConstraints(int surveyId, int?[] valueIds = null, bool? deleteAll = false)
        {
            if (surveyId == 0) return false;
            string sql = "";
            if (deleteAll.HasValue && deleteAll.Value == true)
                sql = "DELETE FROM [SurveyConstraints] WHERE SurveyId = @surveyId;";
            else
                sql = @"DELETE FROM [SurveyConstraints] WHERE SurveyId = @surveyId AND ValueId = @valueId;";

            List<object> parameters = new List<object>();
            parameters.Add(new { surveyId = surveyId });
            if (deleteAll.HasValue && deleteAll.Value == false)
                foreach (int id in valueIds)
                    parameters.Add(new { surveyId = surveyId, valueId = id });
            return WithConnection(c =>
            {
                return c.Execute(sql, parameters) > 1;
            });
        }
        public bool DeleteReportConstraints(int surveyId, int?[] valueIds = null, bool? deleteAll = false)
        {
            if (surveyId == 0) return false;
            string sql = "";
            if (deleteAll.HasValue && deleteAll.Value == true)
                sql = "DELETE FROM [SurveyReportConstraints] WHERE SurveyId = @surveyId;";
            else
                sql = @"DELETE FROM [SurveyReportConstraints] WHERE SurveyId = @surveyId AND CategoryId = @valueId;";

            List<object> parameters = new List<object>();
            parameters.Add(new { surveyId = surveyId });
            if (deleteAll.HasValue && deleteAll.Value == false)
                foreach (int id in valueIds)
                    parameters.Add(new { surveyId = surveyId, valueId = id });
            return WithConnection(c =>
            {
                return c.Execute(sql, parameters) > 1;
            });
        }
        public int DeleteUsers(int surveyId, int?[] userIds = null, bool? deleteAll = false)
        {
            if (surveyId == 0) return 0;
            string sql = "";
            if (deleteAll.HasValue && deleteAll.Value == true)
                sql = "DELETE FROM [SurveyUsers] WHERE SurveyId = @surveyId;";
            else
                sql = @"DELETE FROM [SurveyUsers] WHERE SurveyId = @surveyId AND UserId = @userId;";

            List<object> parameters = new List<object>();
            parameters.Add(new { surveyId = surveyId });
            if (deleteAll.HasValue && deleteAll.Value == false)
                foreach (int id in userIds)
                    parameters.Add(new { surveyId = surveyId, userId = id });
            return WithConnection(c =>
            {
                return c.Execute(sql, parameters);
            });
        }
        public Survey GetWithQuestions(int surveyId)
        {
            Survey result = null;
            string sql = @"SELECT * FROM [Survey] WHERE [ActiveStatus] <> -1 AND SurveyId = @surveyId;
SELECT * FROM [Question] WHERE [ActiveStatus] = 1 AND SurveyId = @surveyId ORDER BY [CreatedDate];
SELECT * FROM [Choice] WHERE [ActiveStatus] = 1 AND QuestionId In 
(SELECT QuestionId FROM [Question] WHERE [ActiveStatus] = 1 AND SurveyId = @surveyId);";
            result = WithConnection(c =>
            {
                var dbResults = c.QueryMultiple(sql, new { surveyId = surveyId });
                result = dbResults.Read<Survey>().SingleOrDefault();
                if (result != null)
                {
                    result.Questions = dbResults.Read<Question>().ToList();
                    var choices = dbResults.Read<Choice>().ToList();
                    if (result.Questions.Count > 0 && choices.Count > 0)
                        foreach (Question q in result.Questions)
                            q.Choices = choices.Where(m => m.QuestionId == q.QuestionId).ToList();
                }
                return result;
            });

            return result;
        }

        public IEnumerable<Survey> GetForRemoteUser(int userId)
        {
            string sql = @"
  DECLARE @SurveyIds TABLE (SurveyId INT)
  DELETE FROM @SurveyIds;
 
  INSERT @SurveyIds(SurveyId)
  SELECT S.SurveyId FROM [Survey] S WHERE S.ActiveStatus = 1 AND S.PublishStatus = 5 AND
  (NOT EXISTS(SELECT TOP 1 1 FROM [SurveyUsers] WHERE SurveyId = S.SurveyId) OR EXISTS(SELECT TOP 1 1 FROM [SurveyUsers] WHERE SurveyId = S.SurveyId AND UserId = @userId))
  AND NOT EXISTS(SELECT TOP 1 1 FROM [Enrollment] WHERE SurveyId = S.SurveyId AND UserId = @userId AND [Status] = 4)
  AND (ISNULL(S.JoinLimit,0)=0 OR S.JoinLimit>(SELECT COUNT(EnrollmentId) FROM [Enrollment] WHERE SurveyId = S.SurveyId AND RepeatId = 1 AND [Status] = 4))
  AND (ISNULL(S.DaylyJoinLimit,0)=0 OR S.DaylyJoinLimit>(SELECT COUNT(EnrollmentId) FROM [Enrollment] WHERE SurveyId = S.SurveyId AND RepeatId = 1 AND [Status] = 4 AND CONVERT(date, StartDate) = CONVERT(date, GETDATE())));

  SELECT S.* FROM [Survey] S 
  INNER JOIN @SurveyIds SI ON SI.SurveyId = S.SurveyId
  Where S.ActiveStatus = 1;

  SELECT SS.SurveyId, V.* FROM [SurveyScopes] SS
  INNER JOIN @SurveyIds SI ON SI.SurveyId = SS.SurveyId
  INNER JOIN [HelperValue] V ON V.ValueId = SS.ValueId AND V.ActiveStatus = 1;

  SELECT SC.SurveyId, V.* FROM [SurveyConstraints] SC 
  INNER JOIN @SurveyIds SI ON SI.SurveyId = SC.SurveyId
  INNER JOIN [HelperValue] V ON V.ValueId = SC.ValueId AND V.ActiveStatus = 1;

  SELECT * FROM [HelperCategory] C WHERE C.ActiveStatus = 1;
 
  SELECT * FROM [Promotion] SP
  INNER JOIN (SELECT P.SurveyId, MIN(P.PromotionId) AS 'PromotionId' FROM [Promotion] P 
  INNER JOIN @SurveyIds SI ON SI.SurveyId = P.SurveyId
  WHERE P.ActiveStatus = 1 AND P.UserId IS NULL
  GROUP BY P.SurveyId) P ON P.PromotionId = SP.PromotionId;

  DELETE FROM @SurveyIds;";
            IEnumerable<Survey> result;
            result = WithConnection(c =>
            {
                var dbResults = c.QueryMultiple(sql, new { userId = userId });
                result = dbResults.Read<Survey>();
                if (result != null && result.Count() > 0)
                {
                    var scopes = dbResults.Read<HelperValue>();
                    var constrains = dbResults.Read<HelperValue>();
                    var categories = dbResults.Read<HelperCategory>();
                    var promotions = dbResults.Read<Promotion>();
                    foreach (Survey s in result)
                    {
                        s.Scopes = scopes.Where(m => m.SurveyId.HasValue && m.SurveyId.Value == s.SurveyId).ToList();
                        if (s.Scopes != null && s.Scopes.Count > 0)
                            foreach (HelperValue v in s.Scopes)
                                v.Category = categories.FirstOrDefault(m => m.CategoryId == v.CategoryId);

                        s.Constraints = constrains.Where(m => m.SurveyId.HasValue && m.SurveyId.Value == s.SurveyId).ToList();
                        if (s.Constraints != null && s.Constraints.Count > 0)
                            foreach (HelperValue v in s.Constraints)
                                v.Category = categories.FirstOrDefault(m => m.CategoryId == v.CategoryId);

                        if (promotions != null && promotions.Count() > 0)
                            s.Promotions = promotions.Where(m => m.SurveyId == s.SurveyId).ToList();
                    }
                }
                return result;
            });

            return result;
        }
        public Survey GetSurveyIncludes(int surveyId)
        {
            string sql = @"
  SELECT TOP 1 * FROM [Survey] S WHERE S.SurveyId = @surveyId;

  SELECT SS.SurveyId, V.* FROM [SurveyScopes] SS
  INNER JOIN [HelperValue] V ON V.ValueId = SS.ValueId AND V.ActiveStatus = 1
  WHERE SS.SurveyId = @surveyId;

  SELECT SC.SurveyId, V.* FROM [SurveyConstraints] SC 
  INNER JOIN [HelperValue] V ON V.ValueId = SC.ValueId AND V.ActiveStatus = 1
  WHERE SC.SurveyId = @surveyId;

  SELECT * FROM [HelperCategory] C WHERE C.ActiveStatus = 1;
  
  
";
            //zencefil
            //SELECT TOP 1 V.* FROM [Survey] S 
            //INNER JOIN [HelperValue] V ON V.ValueId = S.SectorId AND V.ActiveStatus = 1
            //WHERE S.SurveyId = @surveyId;
            Survey survey = null;
            survey = WithConnection(c =>
            {
                var dbResults = c.QueryMultiple(sql, new { surveyId = surveyId });
                survey = dbResults.Read<Survey>().SingleOrDefault();
                if (survey != null)
                {
                    var scopes = dbResults.Read<HelperValue>();
                    var constrains = dbResults.Read<HelperValue>();
                    var categories = dbResults.Read<HelperCategory>();
                    //survey.Sector = dbResults.Read<HelperValue>().SingleOrDefault();

                    survey.Scopes = scopes.Where(m => m.SurveyId.HasValue && m.SurveyId.Value == survey.SurveyId).ToList();
                    if (survey.Scopes != null && survey.Scopes.Count > 0)
                        foreach (HelperValue v in survey.Scopes)
                            v.Category = categories.FirstOrDefault(m => m.CategoryId == v.CategoryId);

                    survey.Constraints = constrains.Where(m => m.SurveyId.HasValue && m.SurveyId.Value == survey.SurveyId).ToList();
                    if (survey.Constraints != null && survey.Constraints.Count > 0)
                        foreach (HelperValue v in survey.Constraints)
                            v.Category = categories.FirstOrDefault(m => m.CategoryId == v.CategoryId);
                }
                return survey;
            });

            return survey;
        }


        public bool SaveAnswer(int surveyId, int userId, int questionId, string choiceIds, string responseText)
        {
            string sql = @"
DECLARE @UserId INT = @p_UserId;
DECLARE @SurveyId INT = @p_SurveyId;
DECLARE @QuestionId INT = @p_QuestionId;
DECLARE @ChoiceIds NVARCHAR(MAX) = @p_ChoiceIds;
DECLARE @ResponseText NVARCHAR(MAX) = @p_ResponseText;
DECLARE @EnrollmentId INT = NULL;
DECLARE @RepeatId INT = NULL;
DECLARE @DeviceId INT = NULL;
DECLARE @ChoiceIdTable TABLE (ChoiceId INT);
DELETE FROM @ChoiceIdTable;

IF ISNULL(@ChoiceIds,'') <> ''
BEGIN
	DECLARE @X XML
	SELECT @X = CONVERT(XML,'<root><s>' + REPLACE(@ChoiceIds,',','</s><s>') + '</s></root>')
	INSERT INTO @ChoiceIdTable(ChoiceId) SELECT [Value] = T.c.value('.','VARCHAR(20)') FROM @X.nodes('/root/s') T(c);
END

SELECT TOP 1 @EnrollmentId = EnrollmentId FROM [Enrollment] WHERE [UserId] = @UserId AND [SurveyId] = @SurveyId AND [Status]<>4;

IF(ISNULL(@EnrollmentId,0) = 0)
BEGIN
	SELECT @RepeatId = COUNT(*) FROM [Enrollment] WHERE [UserId] = @UserId AND [SurveyId] = @SurveyId;
    SELECT TOP 1 @DeviceId = [LastActiveDeviceId] FROM [User] WHERE [UserId] = @UserId;
	INSERT INTO [Enrollment] ([UserId],[SurveyId],[RepeatId],[Status],[StartDate],[FinishDate],[CreatedDate],[TrackingGuid],[DeviceId],[ExPoint],[AllowedInfoShare])
	VALUES(@UserId, @SurveyId, ISNULL(@RepeatId, 0) + 1, 2, GETDATE(), NULL, GETDATE(), NEWID(), @DeviceId,0,0)
	SELECT @EnrollmentId = SCOPE_IDENTITY();
END
IF ISNULL(@EnrollmentId,0) > 0 
BEGIN
    DELETE FROM [Answer] WHERE [UserId] = @UserId AND [SurveyId] = @SurveyId AND EnrollmentId = @EnrollmentId AND QuestionId = @QuestionId 
AND ([ChoiceId] IS NULL OR [ChoiceId] NOT IN (SELECT [ChoiceId] FROM @ChoiceIdTable));
    IF EXISTS (SELECT TOP 1 1 FROM @ChoiceIdTable)
	    INSERT INTO [Answer] ([UserId],[EnrollmentId],[SurveyId],[QuestionId],[ChoiceId],[CreatedDate])
	    SELECT @UserId, @EnrollmentId, @SurveyId, @QuestionId, ChoiceId, GETDATE() FROM @ChoiceIdTable CT
	    WHERE NOT EXISTS(SELECT TOP 1 1 FROM [Answer] WHERE [UserId] = @UserId AND [EnrollmentId] = @EnrollmentId AND [ChoiceId] = CT.ChoiceId);
	ELSE
		INSERT INTO [Answer] ([UserId],[EnrollmentId],[SurveyId],[QuestionId],[ChoiceId],[CreatedDate],[ResponseText])
		VALUES (@UserId, @EnrollmentId, @SurveyId, @QuestionId, NULL, GETDATE(), @ResponseText);
END
DELETE FROM @ChoiceIdTable;
";

            return WithConnection(c =>
            {
                return c.Execute(sql, new { p_UserId = userId, p_SurveyId = surveyId, p_QuestionId = questionId, p_ChoiceIds = choiceIds, p_ResponseText = responseText }) > 1;
            });
        }
        //public void DeleteAnswer(int surveyId, int userId, int questionId)
        //{
        //    string sql = @"DELETE FROM [Answer] WHERE [UserId] = @UserId AND [SurveyId] = @SurveyId AND EnrollmentId = @EnrollmentId AND QuestionId = @QuestionId;";

        //    DbConnection.Execute(sql, new { UserId = userId, SurveyId = surveyId });
        //}
        public int FinishSurvey(int surveyId, int userId, bool allowedInfoShare = false)
        {
            string sql = @"
                          DECLARE @SurveyId INT = @p_SurveyId;
                          DECLARE @UserId INT = @p_UserId;
                          DECLARE @AllowedInfoShare BIT= @p_allowedInfoShare;
                          DECLARE @ExPoint INT = 0;
                          DECLARE @Reward DECIMAL(18,2) = 0;
                          DECLARE @GiveReward BIT = 0;
                          DECLARE @GivePromo BIT = 0;
                          DECLARE @PromoId INT = NULL;
                          DECLARE @EnrollmentId INT = 0;
                          
                          SELECT @ExPoint = ISNULL(S.ExPoint,0), @Reward = ISNULL(S.Reward,0),
                          @PromoId = (SELECT TOP 1 PromotionId FROM [Promotion] P WHERE P.SurveyId = @SurveyId AND P.ActiveStatus = 1 AND P.UserId IS NULL
                          AND NOT EXISTS(SELECT TOP 1 1 FROM [Promotion] XP WHERE XP.SurveyId = @SurveyId AND XP.UserId = @UserId) ORDER BY NEWID())
                          FROM [Survey] S WHERE S.ActiveStatus = 1 AND S.SurveyId = @SurveyId;
                          
                          SELECT TOP 1 @EnrollmentId = E.EnrollmentId FROM [Enrollment] E WHERE E.[UserId] = @UserId AND E.[SurveyId] = @SurveyId AND E.[Status]<>4;
                          
                          UPDATE E SET E.[Status]=4, E.[FinishDate]=GETDATE(), E.[ExPoint] = @ExPoint, E.[AllowedInfoShare] = @AllowedInfoShare,
                          @GiveReward = CASE WHEN ISNULL(E.Score,0)<>@Reward AND E.[RepeatId]=1 THEN 1 ELSE 0 END,
                          @GivePromo = CASE WHEN E.[PromoId] IS NULL AND @PromoId IS NOT NULL AND E.[RepeatId]=1 THEN 1 ELSE 0 END,
                          E.[Score] = CASE WHEN E.[RepeatId]=1 THEN @Reward ELSE 0 END,
                          E.[PromoId] = CASE WHEN E.[RepeatId]=1 THEN @PromoId ELSE NULL END
                          FROM [Enrollment] E WHERE E.[UserId] = @UserId AND E.[SurveyId] = @SurveyId AND E.[Status]<>4;
                          
                         UPDATE U SET U.[ExPoint] = U.[ExPoint],
                          U.[Balance] = CASE WHEN @GiveReward = 1 THEN ISNULL(U.[Balance],0) + @Reward ELSE ISNULL(U.[Balance],0) END
                          FROM [User] U  WHERE U.[UserId] = @UserId AND U.ActiveStatus = 1;
                          
                          IF (@GivePromo = 1 AND @PromoId IS NOT NULL)
                          UPDATE Promotion SET UserId = @UserId Where PromotionId = @PromoId; 
                          SELECT ISNULL(@EnrollmentId,0);";

            // UPDATE U SET U.[ExPoint] = U.[ExPoint] + @ExPoint --> UPDATE U SET U.[ExPoint] = U.[ExPoint] değiştirildi.
            return WithConnection(c =>
            {
                return c.Query<int>(sql, new { p_UserId = userId, p_SurveyId = surveyId, p_allowedInfoShare = allowedInfoShare }).SingleOrDefault();
            });
        }
        public IEnumerable<Survey> GetFinishedSurveys(int userId)
        {
            string sql = @"
  DECLARE @SurveyIds TABLE (SurveyId INT)
  DELETE FROM @SurveyIds;
 
  INSERT @SurveyIds(SurveyId)
  SELECT S.SurveyId FROM [Survey] S WHERE S.ActiveStatus <> -1 
  AND EXISTS(SELECT TOP 1 1 FROM [Enrollment] WHERE SurveyId = S.SurveyId AND UserId = @userId AND [Status] = 4);

  SELECT S.* FROM [Survey] S 
  INNER JOIN @SurveyIds SI ON SI.SurveyId = S.SurveyId
  Where S.ActiveStatus <> -1;

  SELECT SS.SurveyId, V.* FROM [SurveyScopes] SS
  INNER JOIN @SurveyIds SI ON SI.SurveyId = SS.SurveyId
  INNER JOIN [HelperValue] V ON V.ValueId = SS.ValueId AND V.ActiveStatus = 1;

  SELECT SC.SurveyId, V.* FROM [SurveyConstraints] SC 
  INNER JOIN @SurveyIds SI ON SI.SurveyId = SC.SurveyId
  INNER JOIN [HelperValue] V ON V.ValueId = SC.ValueId AND V.ActiveStatus = 1;

  SELECT * FROM [HelperCategory] C WHERE C.ActiveStatus = 1;
  SELECT * FROM [Promotion] WHERE ActiveStatus = 1 AND UserId = @userId;
  DELETE FROM @SurveyIds;";
            IEnumerable<Survey> result;
            result = WithConnection(c =>
            {
                var dbResults = c.QueryMultiple(sql, new { userId = userId });
                result = dbResults.Read<Survey>();
                if (result != null && result.Count() > 0)
                {
                    var scopes = dbResults.Read<HelperValue>();
                    var constrains = dbResults.Read<HelperValue>();
                    var categories = dbResults.Read<HelperCategory>();
                    var promotions = dbResults.Read<Promotion>();
                    foreach (Survey s in result)
                    {
                        s.Scopes = scopes.Where(m => m.SurveyId.HasValue && m.SurveyId.Value == s.SurveyId).ToList();
                        if (s.Scopes != null && s.Scopes.Count > 0)
                            foreach (HelperValue v in s.Scopes)
                                v.Category = categories.FirstOrDefault(m => m.CategoryId == v.CategoryId);

                        s.Constraints = constrains.Where(m => m.SurveyId.HasValue && m.SurveyId.Value == s.SurveyId).ToList();
                        if (s.Constraints != null && s.Constraints.Count > 0)
                            foreach (HelperValue v in s.Constraints)
                                v.Category = categories.FirstOrDefault(m => m.CategoryId == v.CategoryId);

                        if (promotions != null && promotions.Count() > 0)
                            s.Promotions = promotions.Where(m => m.SurveyId == s.SurveyId).ToList();
                    }
                }
                return result;
            });
            return result;
        }

        public SurveyReportModel GetSummaryReport(int surveyId)
        {
            SurveyReportModel result = null;
            string sql = @" SELECT S.SurveyId, S.CompanyId, S.Name, S.SurveyType, S.FirstPublishDate, S.LastPublishDate, S.QuestionCount,
(SELECT COUNT(1) FROM Enrollment E WHERE E.SurveyId = S.SurveyId AND E.RepeatId = 1 AND E.[Status] = 4 AND EXISTS(SELECT TOP 1 1 FROM  [User] U WHERE U.UserId = E.UserId AND U.ActiveStatus = 1)) 'CompletedCount',
(SELECT COUNT(1) FROM Enrollment E WHERE E.SurveyId = S.SurveyId AND E.RepeatId = 1 AND E.[Status] <> 4 AND EXISTS(SELECT TOP 1 1 FROM  [User] U WHERE U.UserId = E.UserId AND U.ActiveStatus = 1)) 'ContinuedCount'
FROM Survey S WHERE S.SurveyId = @surveyId AND S.ActiveStatus <> -1;

SELECT CONVERT(date, E.StartDate) 'Date', COUNT(E.UserId) 'Count' FROM Enrollment E
WHERE E.SurveyId = @surveyId AND RepeatId = 1 AND E.[Status] = 4  AND E.StartDate>= DATEADD(MONTH,-1,GETDATE()) 
AND EXISTS(SELECT TOP 1 1 FROM  [User] U WHERE U.UserId = E.UserId AND U.ActiveStatus = 1)
GROUP BY CONVERT(date, E.StartDate);";
            result = WithConnection(c =>
            {
                var dbResults = c.QueryMultiple(sql, new { surveyId = surveyId });
                result = dbResults.Read<SurveyReportModel>().SingleOrDefault();
                if (result != null)
                    result.ParticipationChartSeries = dbResults.Read<ChartDataModel>().ToList();
                return result;
            });

            return result;
        }

        public int GetPromotionCount(int surveyId)
        {
            string sql = @"SELECT COUNT(1) FROM [Promotion] WHERE ActiveStatus = 1 AND SurveyId = @surveyId;";
            return WithConnection(c =>
            {
                return c.Query<int>(sql, new { surveyId = surveyId }).SingleOrDefault();
            });
        }


    }
}
