﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.Entity;
namespace Pollyfy.Data
{
    public class CompanyRepository : EFRepository<Company>, ICompanyRepository
    {
        public CompanyRepository(DbContext context) : base(context) { }
        public IEnumerable<Company> GetAll(ActiveStatus? status = null)
        {
            string sql = "SELECT * FROM [Company] WHERE [ActiveStatus] != -1 ORDER BY [CompanyName];";
            if (status.HasValue)
                sql = "SELECT * FROM [Company] WHERE [ActiveStatus] = @activeStatus ORDER BY [CompanyName];";
            return WithConnection(c =>
            {
                return c.Query<Company>(sql, new { activeStatus = status.HasValue ? (int)status.Value : 1 });
            });
        }

        public bool UpdateStatus(int companyId, ActiveStatus status)
        {
            string sql = "UPDATE [Company] SET ActiveStatus = @status WHERE CompanyId = @companyId;";

            return WithConnection(c =>
            {
                return c.Execute(sql, new { CompanyId = companyId, status = (int)status }) == 1;
            });
        }
    }
}