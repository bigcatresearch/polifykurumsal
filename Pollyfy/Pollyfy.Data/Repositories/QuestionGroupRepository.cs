﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.Entity;
namespace Pollyfy.Data
{
    public class QuestionGroupRepository : EFRepository<QuestionGroup>, IQuestionGroupRepository
    {
        public QuestionGroupRepository(DbContext context) : base(context) { }
        public IEnumerable<QuestionGroup> GetBySurveyId(int surveyId)
        {
            string sql = @"SELECT * FROM [QuestionGroup] WHERE ActiveStatus = 1 AND SurveyId = @surveyId;";
            return WithConnection(c =>
            {
                return c.Query<QuestionGroup>(sql, new { surveyId = surveyId });
            });
        }
        public void DeleteBySurveyId(int surveyId)
        {
            string sql = @"DELETE FROM [QuestionGroup] WHERE SurveyId = @surveyId;";
            WithConnection(c =>
            {
                return c.Execute(sql, new { surveyId = surveyId });
            });
        }
        public IEnumerable<ChoiceGroup> GetByQuestionId(int questionId)
        {
            string sql = @"SELECT * FROM [ChoiceGroup] WHERE QuestionId = @questionId;";
            return WithConnection(c =>
            {
                return c.Query<ChoiceGroup>(sql, new { questionId = questionId });
            });
        }
        public void DeleteByChoiceId(int choiceId)
        {
            string sql = @"DELETE FROM [ChoiceGroup] WHERE ChoiceId = @choiceId;";
            WithConnection(c =>
            {
                return c.Execute(sql, new { choiceId = choiceId });
            });
        }

        public IEnumerable<QuestionGroup> GetUserGroups(int surveyId, int userId, int enrollmentId)
        {
            string sql = @"
DECLARE @SurveyId INT = @p_SurveyId;
DECLARE @UserId INT = @p_UserId;
DECLARE @EnrollmentId INT = @p_EnrollmentId;

SELECT G.*, UA.Score, 1 AS 'Route' FROM [QuestionGroup] G 
INNER JOIN (
SELECT A.UserId, A.EnrollmentId, A.SurveyId, SUM(ISNULL(C.Score,0)) AS 'Score' FROM [Answer] A
INNER JOIN [Choice] C ON C.ChoiceId = A.ChoiceId AND C.ActiveStatus = 1
WHERE  A.SurveyId = @SurveyId AND A.UserId = @UserId AND A.EnrollmentId = @EnrollmentId
GROUP BY A.UserId, A.EnrollmentId, A.SurveyId) UA ON UA.Score>= G.BeginScore AND UA.Score<= G.EndScore
WHERE G.SurveyId = @SurveyId AND G.ActiveStatus = 1 AND G.GroupType = 1
UNION ALL
(
SELECT G.*, NULL AS 'Score', CG.[Route] FROM [Answer] A
INNER JOIN [Choice] C ON C.ChoiceId = A.ChoiceId AND C.ActiveStatus = 1
INNER JOIN [ChoiceGroup] CG ON CG.ChoiceId = A.ChoiceId
INNER JOIN [QuestionGroup] G ON G.SurveyId = @SurveyId AND G.GroupId = CG.GroupId AND G.ActiveStatus = 1 AND G.GroupType = 2
WHERE A.SurveyId = @SurveyId AND A.UserId = @UserId AND A.EnrollmentId = @EnrollmentId
);";

            return WithConnection(c =>
            {
                return c.Query<QuestionGroup>(sql, new { p_UserId = userId, p_SurveyId = surveyId, p_EnrollmentId = enrollmentId });
            });
        }
    }
}
