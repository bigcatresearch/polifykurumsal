﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.Entity;
namespace Pollyfy.Data
{
    public class QuestionRepository : EFRepository<Question>, IQuestionRepository
    {
        public QuestionRepository(DbContext context) : base(context) { }
        public IEnumerable<Question> GetBySurveyId(int surveyId)
        {
            string sql = "SELECT * FROM [Question] Where ActiveStatus <> -1 AND SurveyId = @surveyId ORDER BY [CreatedDate];";
            return WithConnection(c =>
            {
                return c.Query<Question>(sql, new { surveyId = surveyId });
            });
        }
        public IEnumerable<Question> GetWithChoicesBySurveyId(int surveyId)
        {
            IEnumerable<Question> results = new List<Question>();
            string sql = @"
SELECT * FROM [Question] WHERE [ActiveStatus] = 1 AND SurveyId = @surveyId ORDER BY [CreatedDate];
SELECT * FROM [Choice] WHERE [ActiveStatus] = 1 AND QuestionId In 
(SELECT QuestionId FROM [Question] WHERE [ActiveStatus] = 1 AND SurveyId = @surveyId) ORDER BY ChoiceId;";
            results = WithConnection(c =>
            {
                var dbResults = c.QueryMultiple(sql, new { surveyId = surveyId });
                results = dbResults.Read<Question>();
                if (results != null && results.Count() > 0)
                {
                    var choices = dbResults.Read<Choice>().ToList();
                    if (choices != null && choices.Count > 0)
                        foreach (Question q in results)
                            q.Choices = choices.Where(m => m.QuestionId == q.QuestionId).OrderBy(m => m.ChoiceId).ToList();
                }
                return results;
            });

            return results;
        }
        public Question GetWithChoicesByQuestionId(int questionId)
        {
            Question result = null;
            string sql = @"
SELECT TOP 1 * FROM [Question] WHERE [ActiveStatus] = 1 AND QuestionId = @questionId ORDER BY [CreatedDate];
SELECT * FROM [Choice] WHERE [ActiveStatus] = 1 AND QuestionId = @questionId ORDER BY ChoiceId;";
            result = WithConnection(c =>
            {
                var dbResults = c.QueryMultiple(sql, new { questionId = questionId });
                result = dbResults.Read<Question>().SingleOrDefault();
                if (result != null)
                    result.Choices = dbResults.Read<Choice>().ToList();
                return result;
            });

            return result;
        }
        public bool UpdateStatus(int questionId, ActiveStatus status)
        {
            string sql = "UPDATE [Question] SET ActiveStatus = @status WHERE QuestionId = @questionId;";
            return WithConnection(c =>
            {
                return c.Execute(sql, new { questionId = questionId, status = (int)status }) == 1;
            });
        }
    }
}
