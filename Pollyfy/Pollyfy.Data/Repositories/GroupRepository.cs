﻿#region

using Pollyfy.Data.Interfaces;
using Pollyfy.Models.DataModel;
using System.Collections.Generic;
using Pollyfy.Models;
using System.Data.Entity;
using Dapper;
using System;

#endregion

namespace Pollyfy.Data.Repositories
{
    public class GroupRepository : EFRepository<Group>, IGroupRepository
    {
        public GroupRepository(DbContext dbContext) : base(dbContext)
        {

        }

        public IEnumerable<Group> GetAll(ActiveStatus? status = default(ActiveStatus?))
        {
            string sql = "SELECT * FROM [Group] WHERE [ActiveStatus] != -1 ORDER BY [CreatedOn];";
            if (status.HasValue)
                sql = "SELECT * FROM [Group] WHERE [ActiveStatus] = @activeStatus ORDER BY [CreatedOn];";
            return WithConnection(c =>
            {
                return c.Query<Group>(sql, new { activeStatus = status.HasValue ? (int)status.Value : 1 });
            });
        }

        public IEnumerable<Group> GetBySurveyId(int surveyId)
        {
            string sql = "SELECT * FROM [Group] Where ActiveStatus <> -1 AND SurveyId = @surveyId ORDER BY SurveyId;";

            return WithConnection(c =>
            {
                return c.Query<Group>(sql, new { surveyId = surveyId });
            });
        }

        public IEnumerable<Group> GetByGorupIds(string goupIds, ActiveStatus activeStatus = ActiveStatus.Active)
        {
            IEnumerable<Group> results = new List<Group>();

            string sql = string.Format(@"SELECT *FROM [Group] E WHERE E.GroupId in ({0})", goupIds);

            results = WithConnection(c =>
            {
                return c.Query<Group>(sql, new { activeStatus = activeStatus });
            });

            return results;
        }
    }
}