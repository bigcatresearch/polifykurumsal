﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.Entity;
namespace Pollyfy.Data
{
    public class ChoiceRepository : EFRepository<Choice>, IChoiceRepository
    {
        public ChoiceRepository(DbContext context) : base(context) { }
        public IEnumerable<Choice> GetByQuestionId(int questionId)
        {
            string sql = "SELECT * FROM [Choice] Where ActiveStatus <> -1 AND QuestionId = @questionId ORDER BY ChoiceId;";
            return WithConnection(c =>
            {
                return c.Query<Choice>(sql, new { questionId = questionId });
            });
        }
        public bool UpdateStatus(int choiceId, ActiveStatus status)
        {
            string sql = "UPDATE [Choice] SET ActiveStatus = @status WHERE ChoiceId = @choiceId;";
            return WithConnection(c =>
            {
                return c.Execute(sql, new { ChoiceId = choiceId, status = (int)status }) == 1;
            });
        }
    }
}
