﻿using Pollyfy.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    public class Identity : IIdentity
    {
        public int UserId { get; set; }
        public string Username { get; set; }
    }
}
