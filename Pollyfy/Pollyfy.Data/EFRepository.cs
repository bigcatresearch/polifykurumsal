﻿using Pollyfy.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Data
{
    /// <summary>
    /// The EF-dependent, generic repository for data access
    /// </summary>
    /// <typeparam name="T">Type of entity for this Repository.</typeparam>
    public class EFRepository<T> : RepositoryBase<T>, IRepository<T> where T : class
    {
        public EFRepository(DbContext dbContext)
        {
            if (dbContext == null)
                throw new ArgumentNullException("dbContext");

            DbContext = dbContext;

            bool openConnectionOnInitialization = (System.Configuration.ConfigurationManager.AppSettings["alms:Data:OpenConnectionOnInitialization"] ?? "").ToLowerInvariant() == "true";

            if (openConnectionOnInitialization && DbContext.Database.Connection.State == ConnectionState.Closed)
            {
                DbContext.Database.Connection.Open();
            }

            DbSet = DbContext.Set<T>();
        }

        //public IBlmsUow Uow { get; set; }

        protected DbContext DbContext { get; set; }

        protected DbSet<T> DbSet { get; set; }

        public virtual IQueryable<T> GetAll()
        {
            return DbSet;
        }

        public virtual T GetById(int id)
        {
            //return DbSet.FirstOrDefault(PredicateBuilder.GetByIdPredicate<T>(id));
            return DbSet.Find(id);
        }

        public virtual void Add(T entity)
        {
            //SetCreatedAuditProperties(entity);

            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }

            DbContext.SaveChanges();
        }

        public virtual Task AddAsync(T entity)
        {
            //SetCreatedAuditProperties(entity);

            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
            return DbContext.SaveChangesAsync();
        }
        public virtual void AddWithoutCommit(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
        }
        public virtual void Update(T entity)
        {
            DbEntityEntry entry = DbContext.Entry(entity);


            if (entry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
                entry.State = EntityState.Modified;
            }

            //if (entry.State == EntityState.Detached)
            //{
            //    var key = this.getPrimaryKey(entry); 
            //    var currentEntry = this.DbSet.Find(key);
            //    if (currentEntry != null)
            //    {
            //        var attachedEntry = this.DbContext.Entry(currentEntry);
            //        attachedEntry.CurrentValues.SetValues(entity);
            //    }
            //    else
            //    {
            //        this.DbSet.Attach(entity);
            //        entry.State = EntityState.Modified;
            //    }
            //}
        }

        public virtual void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Deleted)
            {
                dbEntityEntry.State = EntityState.Deleted;
            }
            else
            {
                DbSet.Attach(entity);
                DbSet.Remove(entity);
            }
        }


        public virtual void Delete(int id)
        {
            var entity = GetById(id);
            if (entity == null) return; // not found; assume already deleted.
            Delete(entity);
        }

        public virtual IQueryable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }

        public virtual IQueryable<T> QueryWithEagerLoad(Expression<Func<T, bool>> filter, params Expression<Func<T, object>>[] children)
        {
            IQueryable<T> query = DbContext.Set<T>();
            foreach (var inc in children)
            {
                query = query.Include(inc);
            }

            return query.Where(filter);

            // do not use load , it load all data
            //children.ToList().ForEach(x => DbSet.Include(x).Load());
            //return DbSet.Where(filter);
        }

        public virtual IQueryable<T> GetAllEagerLoad(params Expression<Func<T, object>>[] children)
        {

            IQueryable<T> query = DbContext.Set<T>();

            foreach (var exp in children)
            {
                query = query.Include(exp);
            }

            return query;

            // do not use load , it load all data

            //children.ToList().ForEach(x => DbSet.Include(x).Load());
            //return DbSet;
        }

        public virtual void LoadCollectionsExplicity(T entity, params string[] collections)
        {
            foreach (var col in collections)
            {
                DbContext.Entry(entity).Collection(col).Load();
            }
        }
        public virtual void LoadCollectionsExplicity(T entity, Expression<Func<T, ICollection<object>>> collection, Expression<Func<object, bool>> criteriaExpression)
        {
            DbContext.Entry(entity).Collection(collection).Query().Where(criteriaExpression).Load();
        }
        public virtual void LoadReferencesExplicity(T entity, params string[] references)
        {
            foreach (var r in references)
            {
                DbContext.Entry(entity).Reference(r).Load();
            }
        }

        public bool TryRunAction(Action action, int attemptCount, int waitDurationAfterFailure)
        {
            bool result = false;
            while (attemptCount > 0)
            {
                try
                {
                    action();
                    result = true;
                    break; // exit while
                }
                catch
                {
                    System.Threading.Thread.Sleep(500); // wait for 0.5 sec.
                }
                --attemptCount;
            }
            return result;
        }

        public async Task<bool> TryRunActionAsync(Action action, int attemptCount, int waitDurationAfterFailure)
        {
            bool result = false;
            while (attemptCount > 0)
            {
                try
                {
                    await Task.Run(action);
                    result = true;
                    break; // exit while
                }
                catch
                {
                    System.Threading.Thread.Sleep(500); // wait for 0.5 sec.
                }
                --attemptCount;
            }
            return result;
        }
    }
}
