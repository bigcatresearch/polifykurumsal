﻿using Pollyfy.Data.Interfaces;
using Pollyfy.Models;
using FastMember;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Linq.Expressions;
using System.Reflection;

namespace Pollyfy.Data.Utilities
{
    public class SQLBuilder : ISQLBuilder
    {
        private static ConcurrentDictionary<string, string> StoredTemplates;
        private static String[] NoUpdateFields = new string[] { "CreatedDate", "TrackingGuid" };
        public SQLBuilder()
        {
            StoredTemplates = new ConcurrentDictionary<string, string>();
        }
        public string InsertSqlTemplate<T>(bool IdentityInsertAllowed = false)
        {
            string key = "Insert" + (IdentityInsertAllowed ? "_Identity" : "");
            string template = GetStoredTeplate<T>(key);
            if (!string.IsNullOrEmpty(template)) return template;
            string primaryKey = "";
            string table = "";
            var fieds = GetFields<T>(out primaryKey, out table);
            string columns = string.Join(",", fieds.Where(m => IdentityInsertAllowed || m.Key != primaryKey).Select(m => "[" + m.Key + "]"));
            string parameters = string.Join(",", fieds.Where(m => IdentityInsertAllowed || m.Key != primaryKey).Select(m => m.Value));
            template = string.Format("INSERT INTO [{0}] ({1}) VALUES ({2}); SELECT ISNULL(SCOPE_IDENTITY(),-1);", table, columns, parameters);
            if (IdentityInsertAllowed) template = string.Format("SET IDENTITY_INSERT [{0}] ON; {1} SET IDENTITY_INSERT [{0}] OFF;", table, template);
            SetStoredTeplate<T>(key, template);
            return template;
        }
        public string UpdateSqlTemplate<T>()
        {
            string template = GetStoredTeplate<T>("Update");
            if (!string.IsNullOrEmpty(template)) return template;
            string primaryKey = "";
            string table = "";
            var fieds = GetFields<T>(out primaryKey, out table);
            string columns = string.Join(",", fieds.Where(m => m.Key != primaryKey && !NoUpdateFields.Contains(m.Key)).Select(m => "[" + m.Key + "]=" + m.Value));
            template = string.Format("UPDATE [{0}] SET {1} WHERE [{2}]={3};", table, columns, primaryKey, fieds[primaryKey]);
            SetStoredTeplate<T>("Update", template);
            return template;
        }
        public string DeleteSqlTemplate<T>()
        {
            string template = GetStoredTeplate<T>("Delete");
            if (!string.IsNullOrEmpty(template)) return template;
            T model = (T)Activator.CreateInstance(typeof(T));
            string primaryKey = "";
            string table = "";
            var fieds = GetFields<T>(out primaryKey, out table);
            template = string.Format("DELETE FROM [{0}] WHERE [{1}]={2};", table, primaryKey, fieds[primaryKey]);
            SetStoredTeplate<T>("Delete", template);
            return template;
        }
        public string SelectSqlTemplate<T>()
        {
            string template = GetStoredTeplate<T>("Select");
            if (!string.IsNullOrEmpty(template)) return template;
            string primaryKey = "";
            string table = "";
            var fieds = GetFields<T>(out primaryKey, out table);
            string columns = fieds.Count > 0 ? string.Join(",", fieds.Select(m => "[" + m.Key + "]")) : "*";
            template = string.Format("SELECT {0} FROM [{1}];", columns, table);
            SetStoredTeplate<T>("Select", template);
            return template;

        }
        public string SelectSqlTemplate<T>(Expression<Func<T, object>> predicate)
        {
            string table = "";
            var fieds = GetFields<T>(predicate, out table);
            string columns = fieds.Count > 0 ? string.Join(",", fieds.Select(m => "[" + m.Key + "]")) : "*";
            string template = string.Format("SELECT {0} FROM [{1}];", columns, table);
            return template;

        }
        public string SelectByKeyTemplate<T>(int key)
        {
            string template = GetStoredTeplate<T>("SelectByKey");
            if (string.IsNullOrEmpty(template))
            {
                string primaryKey = "";
                string table = "";
                var fieds = GetFields<T>(out primaryKey, out table);
                string columns = fieds.Count > 0 ? string.Join(",", fieds.Select(m => "[" + m.Key + "]")) : "*";
                template = string.Format("SELECT {0} FROM [{1}] WHERE [{2}]=", columns, table, primaryKey);
                SetStoredTeplate<T>("SelectByKey", template);
            }
            template = string.Format(template + "{0};", key);
            return template;
        }
        public string UpdateStatusSqlTemplate<T>()
        {
            string template = GetStoredTeplate<T>("UpdateStatus");
            if (!string.IsNullOrEmpty(template)) return template;
            string primaryKey = "";
            string table = "";
            var fieds = GetFields<T>(out primaryKey, out table);
            template = string.Format("UPDATE [{0}] SET [ActiveStatus] = @activestatus  WHERE [{1}]={2};", table, primaryKey, fieds[primaryKey]);
            SetStoredTeplate<T>("UpdateStatus", template);
            return template;
        }
        public IDictionary<string, string> GetFields<T>(out string primaryKey, out string tableName)
        {
            primaryKey = "";
            tableName = "";
            IDictionary<string, string> result = new Dictionary<string, string>();

            T model = (T)Activator.CreateInstance(typeof(T));
            Type tableType = model.GetType();
            var tblAttr = tableType.CustomAttributes.FirstOrDefault(m => m.AttributeType.Name == "TableName");
            if (tblAttr != null) tableName = tblAttr.ConstructorArguments[0].Value.ToString();

            TypeAccessor accessor = TypeAccessor.Create(tableType);
            foreach (Member m in accessor.GetMembers())
            {
                if (!IsDbColumn(m)) continue;
                if (string.IsNullOrEmpty(primaryKey) && m.IsDefined(typeof(PrimaryKey))) { primaryKey = m.Name; }
                result[m.Name] = "@" + m.Name.ToLowerInvariant();
            }
            if (string.IsNullOrEmpty(tableName)) tableName = tableType.Name;
            return result;
        }
        public IDictionary<string, string> GetFields<T>(Expression<Func<T, object>> predicate, out string tableName)
        {
            tableName = "";
            IDictionary<string, string> result = new Dictionary<string, string>();

            T model = (T)Activator.CreateInstance(typeof(T));
            Type tableType = model.GetType();
            var tblAttr = tableType.CustomAttributes.FirstOrDefault(m => m.AttributeType.Name == "TableName");
            if (tblAttr != null)
                tableName = tblAttr.ConstructorArguments[0].Value.ToString();
            else
                tableName = tableType.Name;

            TypeAccessor accessor = TypeAccessor.Create(predicate.Body.GetType());
            if (accessor.GetMembers().Any(m => m.Name == "Members"))
            {
                foreach (MemberInfo m in accessor[predicate.Body, "Members"] as ICollection<MemberInfo>)
                {
                    result[m.Name] = "@" + m.Name.ToLowerInvariant();
                }
            }

            return result;
        }
        public IDictionary<string, object> GetParameterValues(object obj)
        {
            IDictionary<string, object> result = new Dictionary<string, object>();
            if (obj == null) return result;

            TypeAccessor accessor = TypeAccessor.Create(obj.GetType());
            foreach (Member m in accessor.GetMembers())
            {
                if (!IsDbColumn(m)) continue;
                result["@" + m.Name.ToLowerInvariant()] = accessor[obj, m.Name];
            }
            return result;
        }
        public IDictionary<string, string> FindPrimaryKey<T>()
        {
            IDictionary<string, string> result = new Dictionary<string, string>();
            T obj = (T)Activator.CreateInstance(typeof(T));
            TypeAccessor accessor = TypeAccessor.Create(obj.GetType());
            foreach (Member m in accessor.GetMembers())
            {
                if (!IsDbColumn(m)) continue;
                if (!m.IsDefined(typeof(PrimaryKey))) continue;
                result[m.Name] = "@" + m.Name.ToLowerInvariant();
            }
            return result;
        }
        private bool IsDbColumn(Member m)
        {
            if (m.IsDefined(typeof(NoDatabaseColumn))) return false;
            return (m.Type.IsSealed && m.Type.IsPublic && m.Type.GetProperties().Where(p => p.GetGetMethod().IsVirtual).FirstOrDefault() == null);
        }
        private string GetStoredTeplate<T>(string templateType)
        {
            var modelType = typeof(T);
            string key = string.Format("{0}_{1}", templateType, modelType.Name);
            string template = "";
            if (StoredTemplates.ContainsKey(key))
                StoredTemplates.TryGetValue(key, out template);
            return template;
        }
        private void SetStoredTeplate<T>(string templateType, string template)
        {
            var modelType = typeof(T);
            string key = string.Format("{0}_{1}", templateType, modelType.Name);
            if (!StoredTemplates.ContainsKey(key))
                StoredTemplates.TryAdd(key, template);
        }
    }

}
