﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Core
{
    public static class Storage
    {
        private static CloudStorageAccount _storageAccount { get; set; }
        public static CloudStorageAccount StorageAccount
        {
            get
            {
                if (_storageAccount == null)
                {
                    _storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);
                }
                return _storageAccount;
            }
        }
        private static CloudBlobClient _blobClient { get; set; }
        public static CloudBlobClient BlobClient
        {
            get
            {
                if (_blobClient == null)
                {
                    _blobClient = StorageAccount.CreateCloudBlobClient();
                }
                return _blobClient;
            }
        }
        private static CloudQueueClient _queueClient { get; set; }
        public static CloudQueueClient QueueClient
        {
            get
            {
                if (_queueClient == null)
                {
                    _queueClient = StorageAccount.CreateCloudQueueClient();
                }
                return _queueClient;
            }
        }

        private static CloudBlobContainer _audioContainer { get; set; }
        public static CloudBlobContainer Audios
        {
            get
            {
                if (_audioContainer == null)
                {
                    _audioContainer = BlobClient.GetContainerReference(ConfigurationManager.AppSettings["AudioContainerName"]);
                    _audioContainer.CreateIfNotExists();
                }
                return _audioContainer;
            }
        }

        private static CloudQueue _audioQueue { get; set; }
        public static CloudQueue AudioQueue
        {
            get
            {
                if (_audioQueue == null)
                {
                    _audioQueue = QueueClient.GetQueueReference(ConfigurationManager.AppSettings["AudioQueueName"]);
                    _audioQueue.CreateIfNotExists();
                }
                return _audioQueue;
            }
        }

    }
}
