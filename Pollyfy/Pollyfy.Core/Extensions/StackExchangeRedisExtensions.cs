﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Web;

namespace Pollyfy.Core
{
    public static class StackExchangeRedisExtensions
    {
        public static T Get<T>(this IDatabase _cache, string key)
        {
            return Deserialize<T>(_cache.StringGet(key));
        }
        public static async Task<T> GetAsync<T>(this IDatabase _cache, string key)
        {
            return Deserialize<T>(await _cache.StringGetAsync(key));
        }
        public static string Get(this IDatabase _cache, string key)
        {
            return _cache.StringGet(key);
        }
        public static async Task<string> GetAsync(this IDatabase _cache, string key)
        {
            return await _cache.StringGetAsync(key);
        }
        public static void Set(this IDatabase _cache, string key, string value, TimeSpan expiresIn)
        {
            _cache.StringSet(key, value, expiresIn);
        }
        public static void Set<T>(this IDatabase _cache, string key, T value, TimeSpan expiresIn)
        {
            _cache.StringSet(key, Serialize(value), expiresIn);
        }
        public static void Set(this IDatabase _cache, string key, string value)
        {
            _cache.StringSet(key, value, TimeSpan.FromDays(1));
        }
        public static void Set<T>(this IDatabase _cache, string key, T value)
        {
            _cache.StringSet(key, Serialize(value), TimeSpan.FromDays(1));
        }
        public static async Task SetAsync(this IDatabase _cache, string key, string value, TimeSpan expiresIn)
        {
            await _cache.StringSetAsync(key, value, expiresIn);
        }
        public static async Task SetAsync(this IDatabase _cache, string key, string value)
        {
            await _cache.StringSetAsync(key, value, TimeSpan.FromDays(1));
        }
        public static async Task SetAsync<T>(this IDatabase _cache, string key, T value)
        {
            await _cache.StringSetAsync(key, Serialize(value), TimeSpan.FromDays(1));
        }
        public static async Task SetAsync<T>(this IDatabase _cache, string key, T value, TimeSpan expiresIn)
        {
            await _cache.StringSetAsync(key, Serialize(value), expiresIn);
        }
        public static void Remove(this IDatabase _cache, string key)
        {
            Set(_cache, key, null, TimeSpan.FromSeconds(5));
        }

        public static async Task<bool> KeyExistsAsync(this IDatabase _cache, string key)
        {
            return await _cache.KeyExistsAsync(key);
        }
        public static bool KeyExists(this IDatabase _cache, string key)
        {
            return _cache.KeyExists(key);
        }
        private static byte[] Serialize(object o)
        {
            if (o == null)
            {
                return null;
            }

            try
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    binaryFormatter.Serialize(memoryStream, o);
                    byte[] objectDataAsStream = memoryStream.ToArray();
                    return objectDataAsStream;
                }
            }
            catch (Exception ex)
            {
                var test = ex;
                return null;
            }
        }

        private static T Deserialize<T>(byte[] stream)
        {
            if (stream == null)
            {
                return default(T);
            }

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (MemoryStream memoryStream = new MemoryStream(stream))
            {
                T result = (T)binaryFormatter.Deserialize(memoryStream);
                return result;
            }
        }
    }
}