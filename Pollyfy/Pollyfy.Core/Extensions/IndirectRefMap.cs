﻿using Pollyfy.Core.Crypt;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Core.Extensions
{
    public enum ReferenceType
    {
        Public = 1,
        Session = 2,
        Shared = 4
    }

    public static class IndirectRefMap
    {
        public static string EncKey { get; set; }

        private const string SHAREDKEYMARKER = "G";
        private const string PUBLICKEYMARKER = "P";

        private static CryptEngine cryptor = new CryptEngine(CryptEngine.AlgorithmType.Rijndael)
        {
            UseHexEncoding = true
        };
        private static bool? _byPassReferenceMapping;
        public static bool ByPassReferenceMapping
        {
            get
            {
                var a = new System.Collections.Concurrent.ConcurrentDictionary<string, string>();

                if (!_byPassReferenceMapping.HasValue)
                {
                    bool byPass = false;
                    bool.TryParse(ConfigurationManager.AppSettings["ByPassReferenceMapping"], out byPass);
                    _byPassReferenceMapping = byPass;
                }
                return _byPassReferenceMapping.Value;
            }
        }

        private static string getIndirectRef(this string directRef, string objectType, ReferenceType referenceType = ReferenceType.Session)
        {

            string str = "";
            if (ByPassReferenceMapping)
            {
                str = directRef;
            }
            else
            {
                try
                {
                    str = cryptor.Encrypt(directRef, getEncryptionKey(objectType, referenceType));
                }
                catch { }
            }
            if (referenceType == ReferenceType.Shared) str += SHAREDKEYMARKER;
            else if (referenceType == ReferenceType.Public) str += PUBLICKEYMARKER;

            return str;
        }

        private static string getEncryptionKey(string objectType, ReferenceType referenceType)
        {

            if (!string.IsNullOrEmpty(EncKey)) return EncKey;
            string key = null;
            /** TODO: the following code is commented out to stop using HttpContext...
            if (referenceType == ReferenceType.Public || (HttpContext.Current != null && HttpContext.Current.User != null && !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name)))
            {
                switch (referenceType)
                {
                    case ReferenceType.Public:
                        key = "public|" + objectType;
                        break;
                    case ReferenceType.Session:
                        key = HttpContext.Current.User.Identity.Name + "|" + objectType;
                        break;
                    case ReferenceType.Shared:
                        string organizationId = HttpContext.Current.User.Identity.Name.Split(':')[1];
                        key = organizationId + "|" + objectType;
                        break;
                }
            }
            else
                key = "alms-indirectref-key" + "|" + objectType;
             */
            key = "rtoU/dm+%i)s?dNBd!d)aT" + "|" + objectType;/**/
            return key;
        }

        private static string getDirectRef(string indirectRef, string objectType)
        {
            if (String.IsNullOrEmpty(indirectRef) || String.IsNullOrEmpty(objectType)) return "";

            string plaintext = "";

            ReferenceType referenceType = ReferenceType.Session;
            try
            {
                if (indirectRef.EndsWith(SHAREDKEYMARKER))
                {
                    indirectRef = indirectRef.Substring(0, indirectRef.Length - SHAREDKEYMARKER.Length);
                    referenceType = ReferenceType.Shared;
                }
                else if (indirectRef.EndsWith(PUBLICKEYMARKER))
                {
                    indirectRef = indirectRef.Substring(0, indirectRef.Length - PUBLICKEYMARKER.Length);
                    referenceType = ReferenceType.Public;
                }

                if (ByPassReferenceMapping) return indirectRef;

                plaintext = cryptor.Decrypt(indirectRef, getEncryptionKey(objectType, referenceType));
            }
            catch
            {
            }
            return plaintext;
        }

        private static int getDirectRefForInt(string indirectRef, string objectType)
        {
            int val = 0;
            string str = getDirectRef(indirectRef, objectType);
            int.TryParse(str, out val);
            return val;
        }

        public static string GetIndirectRef(this string directRef, Type objectType, ReferenceType referenceType = ReferenceType.Session)
        {
            if (!objectType.IsArray)
            {
                return getIndirectRef(directRef, objectType.Name, referenceType);
            }
            else
            {
                string typeNameForEncryption = objectType.Name.Split('[')[0];
                string[] arr = directRef.Split(',');
                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i] = getIndirectRef(arr[i], typeNameForEncryption, referenceType);
                }
                return string.Join(",", arr);
            }
        }
        public static string GetDirectRef(this string indirectRef, Type objectType)
        {
            return getDirectRef(indirectRef, objectType.Name);
        }

        public static string GetIndirectRef(this int directRef, Type objectType, ReferenceType referenceType = ReferenceType.Session)
        {
            return GetIndirectRef(directRef.ToString(), objectType, referenceType);
        }

        public static int GetDirectRefForInt(this string indirectRef, Type objectType)
        {
            return getDirectRefForInt(indirectRef, objectType.Name);
        }
    }
}
