﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Core
{
    public class Logger
    {
        [Serializable]
        public class LogItem
        {
            public LogItem(string text, LogType logType, Exception exception, DateTime logTime)
            {
                Text = text;
                LogType = logType;
                Exception = exception;
                LogTime = logTime;
            }
            public string Text { get; set; }
            public LogType LogType { get; set; }
            public Exception Exception { get; set; }
            public DateTime LogTime { get; set; }
        }

        public enum LogType : int
        {
            Info,
            Warn,
            Debug,
            Fatal
        }

        private ILog log;

        public Logger(string name)
        {
            log4net.Config.XmlConfigurator.Configure();
            log = LogManager.GetLogger(name);
        }

        /// <summary>
        /// LogTime is discarded and automatically set to DateTime.Now
        /// </summary>
        /// <param name="log"></param>
        public void Log(LogItem log)
        {
            Log(log.Text, log.LogType, log.Exception);
        }

        public void Log(string text, LogType logType = LogType.Info, Exception ex = null)
        {
            if (logType == LogType.Info && log.IsInfoEnabled)
            {
                if (ex == null)
                    log.Info(text);
                else
                    log.Info(text, ex);
            }
            else if (logType == LogType.Debug && log.IsDebugEnabled)
            {
                if (ex == null)
                    log.Debug(text);
                else
                    log.Debug(text, ex);
            }
            else if (logType == LogType.Fatal && log.IsFatalEnabled)
            {
                if (ex == null)
                    log.Fatal(text);
                else
                    log.Fatal(text, ex);
            }
            else if (logType == LogType.Warn && log.IsWarnEnabled)
            {
                if (ex == null)
                    log.Warn(text);
                else
                    log.Warn(text, ex);
            }
        }
    }
}
