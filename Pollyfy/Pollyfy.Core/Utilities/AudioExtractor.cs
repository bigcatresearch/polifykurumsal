﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Pollyfy.Core
{
    public class Audio : IDisposable
    {
        public Audio()
        {
            Stream = new MemoryStream();
            Position = 0;
        }
        public MemoryStream Stream { get; set; }
        public long Position { get; set; }
        public void Dispose()
        {
            if (Stream != null)
                Stream.Close();
            Stream.Dispose();
            Stream = null;
        }
    }
    public class AudioExtractor
    {
        protected bool IsNotAvaliable(BinaryReader reader, long next)
        {

            bool notAvailable = false;
            if (reader == null || reader.BaseStream == null || reader.BaseStream.Position + next > reader.BaseStream.Length)
                notAvailable = true;
            return notAvailable;
        }
        protected Audio AfterExtract(System.IO.Stream source, MemoryStream output, long position, long startPosition)
        {
            source.Position = position;
            Audio audio = new Audio()
            {
                Stream = output,
                Position = startPosition
            };
            return audio;
        }
        public Audio Extract(System.IO.Stream stream, long startPosition)
        {
            MemoryStream outstream = new MemoryStream();
            var reader = new BinaryReader(stream);
            long oldPosition = reader.BaseStream.Position;
            reader.BaseStream.Position = 0;

            if (startPosition == 0)
            {
                // Is stream a Flash Video stream
                if (IsNotAvaliable(reader, 3)) return AfterExtract(stream, outstream, oldPosition, startPosition);
                if (reader.ReadChar() != 'F' || reader.ReadChar() != 'L' || reader.ReadChar() != 'V')
                    throw new IOException("The file is not a FLV file.");

                // Is audio stream exists in the video stream
                if (IsNotAvaliable(reader, 1)) return AfterExtract(stream, outstream, oldPosition, startPosition);
                var version = reader.ReadByte();
                if (IsNotAvaliable(reader, 1)) return AfterExtract(stream, outstream, oldPosition, startPosition);
                var exists = reader.ReadByte();

                if ((exists != 5) && (exists != 4))
                    throw new IOException("No Audio Stream");

                if (IsNotAvaliable(reader, 4)) return AfterExtract(stream, outstream, oldPosition, startPosition);
                reader.ReadInt32(); // data offset of header. ignoring
            }
            else
                reader.BaseStream.Position = 0;
            var output = new List<byte>();
            bool IsContinue = false;
            while (true)
            {
                try
                {
                    if (!IsContinue && startPosition > 0)
                    {
                        IsContinue = true;
                        reader.BaseStream.Position = startPosition;
                    }
                    else
                        startPosition = reader.BaseStream.Position;

                    if (IsNotAvaliable(reader, 4)) return AfterExtract(stream, outstream, oldPosition, startPosition);
                    reader.ReadInt32(); // PreviousTagSize0 skipping

                    if (reader.BaseStream.Position == reader.BaseStream.Length) break;
                    if (IsNotAvaliable(reader, 1)) return AfterExtract(stream, outstream, oldPosition, startPosition);
                    var tagType = reader.ReadByte();

                    while (tagType != 8)
                    {
                        if (IsNotAvaliable(reader, 3)) return AfterExtract(stream, outstream, oldPosition, startPosition);
                        var skip = ReadNext3Bytes(reader) + 11;
                        if (IsNotAvaliable(reader, skip)) return AfterExtract(stream, outstream, oldPosition, startPosition);
                        reader.BaseStream.Position += skip;
                        if (IsNotAvaliable(reader, 1)) return AfterExtract(stream, outstream, oldPosition, startPosition);
                        tagType = reader.ReadByte();
                    }
                    if (IsNotAvaliable(reader, 3)) return AfterExtract(stream, outstream, oldPosition, startPosition);
                    var DataSize = ReadNext3Bytes(reader);
                    if (IsNotAvaliable(reader, 4)) return AfterExtract(stream, outstream, oldPosition, startPosition);
                    reader.ReadInt32(); //skip timestamps 
                    if (IsNotAvaliable(reader, 3)) return AfterExtract(stream, outstream, oldPosition, startPosition);
                    ReadNext3Bytes(reader); // skip streamID
                    if (IsNotAvaliable(reader, 1)) return AfterExtract(stream, outstream, oldPosition, startPosition);
                    reader.ReadByte(); // skip audio header

                    if (IsNotAvaliable(reader, DataSize)) return AfterExtract(stream, outstream, oldPosition, startPosition);
                    for (int i = 0; i < DataSize - 1; i++)
                    {

                        var byt = reader.ReadByte();
                        output.Add(byt);
                        outstream.WriteByte(byt);
                        startPosition = reader.BaseStream.Position;
                    }
                }
                catch
                {
                    break;
                }
            }

            return AfterExtract(stream, outstream, oldPosition, startPosition);  //output.ToArray();
        }

        private long ReadNext3Bytes(BinaryReader reader)
        {
            try
            {
                return Math.Abs((reader.ReadByte() & 0xFF) * 256 * 256 + (reader.ReadByte() & 0xFF)
                    * 256 + (reader.ReadByte() & 0xFF));
            }
            catch
            {
                return 0;
            }
        }
    }
}