﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Pollyfy.Core
{
    public static class Utils
    {
        public static string GetLink(string inputString)
        {
            Match m;
            string HRefPattern = "href\\s*=\\s*(?:[\"'](?<1>[^\"']*)[\"']|(?<1>\\S+))";
            List<string> links = new List<string>();
            try
            {
                //m = Regex.Match(inputString, HRefPattern,
                //                RegexOptions.IgnoreCase | RegexOptions.Compiled,
                //                TimeSpan.FromSeconds(1));
                m = Regex.Match(inputString, HRefPattern,
                RegexOptions.IgnoreCase | RegexOptions.Compiled);

                while (m.Success)
                {
                    links.Add(m.Groups[1].Value);
                    m = m.NextMatch();
                }
                return links.OrderByDescending(s => s.Length).FirstOrDefault();
            }
            catch (Exception)
            {
                return "";
            }
        }
        public static string justAlpha(string str, char[] excluded = null)
        {
            string result = "";
            str = replaceChars(str.ToLower());
            if (!string.IsNullOrEmpty(str))
            {
                char[] az = Enumerable.Range('a', 'z' - 'a' + 1).Select(i => (char)i).ToArray();
                foreach (char c in str)
                {
                    if (az.Contains(c) || Char.IsNumber(c) || (excluded != null && excluded.Contains(c)))
                        result += c;
                }
            }
            return result;
        }
        public static string clearSpecialChars(string str, char[] excluded = null)
        {
            string result = "";
            if (!string.IsNullOrEmpty(str))
            {
                string original = "(*^%)@&#<>?:\\\":';=-+_.,~][{}/|";
                foreach (char c in str)
                {
                    if (!original.Contains(c) || (excluded != null && excluded.Contains(c)))
                        result += c;
                }
            }
            return result;
        }
        public static string replaceChars(string str)
        {
            str = str.Replace('ı', 'i');
            str = str.Replace('ü', 'u');
            str = str.Replace('ş', 's');
            str = str.Replace('ğ', 'g');
            str = str.Replace('ç', 'c');
            str = str.Replace('ö', 'o');

            str = str.Replace('İ', 'I');
            str = str.Replace('Ü', 'U');
            str = str.Replace('Ş', 'S');
            str = str.Replace('Ğ', 'G');
            str = str.Replace('Ç', 'C');
            str = str.Replace('Ö', 'O');
            return str;
        }

        public static byte[] getBtyes(string strSource, int? size = null)
        {
            var srcBtyes = Encoding.UTF8.GetBytes(replaceChars(strSource));
            if (size == null) size = srcBtyes.Length;
            byte[] result = new byte[srcBtyes.Length];
            if (srcBtyes.Length < size.Value) result = new byte[size.Value];
            Array.Copy(srcBtyes, 0, result, 0, srcBtyes.Length);
            return result;
        }

        public static byte[] EditID3Tag(byte[] buffer)
        {
            List<byte> result = new List<byte>();
            List<byte> frames = new List<byte>();
            byte[] cTagHead = new byte[3] { 4, 0, 0 };
            byte[] cFrmFlg = new byte[3] { 0, 0, 0 };

            byte[] tagBytes = new byte[4];

            Array.Copy(buffer, 6, tagBytes, 0, 4);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(tagBytes);
            int tagSize = BitConverter.ToInt32(tagBytes, 0);
            if (tagSize + 10 >= buffer.Length) return null;

            result.AddRange(getBtyes("ID3"));
            result.AddRange(cTagHead);

            var frameVal = getBtyes("ClubbyMIX");
            frames.AddRange(getBtyes("TIT2"));
            frames.AddRange(BitConverter.GetBytes(frameVal.Length + 1).Reverse());
            frames.AddRange(cFrmFlg);
            frames.AddRange(frameVal);

            frameVal = getBtyes("ClubbyMIX");
            frames.AddRange(getBtyes("TPE1"));
            frames.AddRange(BitConverter.GetBytes(frameVal.Length + 1).Reverse());
            frames.AddRange(cFrmFlg);
            frames.AddRange(frameVal);

            frameVal = getBtyes("ClubbyMIX");
            frames.AddRange(getBtyes("TPE2"));
            frames.AddRange(BitConverter.GetBytes(frameVal.Length + 1).Reverse());
            frames.AddRange(cFrmFlg);
            frames.AddRange(frameVal);

            frameVal = getBtyes("ClubbyIO");
            frames.AddRange(getBtyes("TALB"));
            frames.AddRange(BitConverter.GetBytes(frameVal.Length + 1).Reverse());
            frames.AddRange(cFrmFlg);
            frames.AddRange(frameVal);

            frameVal = getBtyes("MIX");
            frames.AddRange(getBtyes("TCON"));
            frames.AddRange(BitConverter.GetBytes(frameVal.Length + 1).Reverse());
            frames.AddRange(cFrmFlg);
            frames.AddRange(frameVal);

            frameVal = getBtyes("ClubbyIO");
            frames.AddRange(getBtyes("TENC"));
            frames.AddRange(BitConverter.GetBytes(frameVal.Length + 1).Reverse());
            frames.AddRange(cFrmFlg);
            frames.AddRange(frameVal);


            result.AddRange(BitConverter.GetBytes(frames.Count).Reverse());
            result.AddRange(frames);

            int start = 10 + tagSize;
            int len = buffer.Length - start;
            byte[] content = new byte[len];
            Array.Copy(buffer, start, content, 0, len);
            result.AddRange(content);

            return result.ToArray();
        }

    }
}