﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Core
{
    public static class Redis
    {
        private static IDatabase _cache { get; set; }
        private static ConnectionMultiplexer _connection { get; set; }
        private static ConnectionMultiplexer Connection
        {
            get
            {
                if (_connection == null || !_connection.IsConnected)
                    _connection = ConnectionMultiplexer.Connect(ConfigurationManager.AppSettings["RedisConnectionString"]);
                return _connection;
            }
        }
        public static IDatabase Cache
        {
            get
            {
                if (_cache == null)
                    _cache = Connection.GetDatabase();
                return _cache;
            }
        }
        public static void ClearAll()
        {
            var endpoints = Connection.GetEndPoints(true);
            foreach (var endpoint in endpoints)
            {
                var server = Connection.GetServer(endpoint);
                server.FlushAllDatabases();
            }
        }
    }
}
