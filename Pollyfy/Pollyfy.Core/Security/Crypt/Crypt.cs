
using System;
using System.Security;
using System.Security.Cryptography;
using System.IO;
using System.Text;


namespace Pollyfy.Core.Crypt
{
    public class CryptEngine
    {
        public static string ByteArrayToString(byte[] ba)
        {
            /*StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();*/
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }
        public static byte[] StringToByteArray(String hex)
        {
            int numberChars = hex.Length;
            byte[] bytes = new byte[numberChars / 2];
            for (int i = 0; i < numberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public enum AlgorithmType : int
        {
            None, SHA, SHA256, SHA384, SHA512, MD5,
            DES, RC2, Rijndael, TripleDES, BlowFish, Twofish
        }


        private AlgorithmType _algorithm = AlgorithmType.None;

        public AlgorithmType Algorithm
        {
            get { return _algorithm; }
            set
            {
                _algorithm = value;
                InitializeEngine();
            }
        }

        private bool _isHash;
        public bool IsHashAlgorithm
        {
            get { return _isHash; }
            set { }
        }
        private bool _isSymmetric;
        public bool IsSymmetricAlgorithm
        {
            get { return _isSymmetric; }
            set { }
        }

        private string _key = null;
        public string Key
        {
            get { return _key; }
            set { _key = this.formatKey(value); }
        }
        /// <summary>
        /// Instead of Base 64 encoding, use Hex encoding in string forming.
        /// </summary>
        public bool UseHexEncoding { get; set; }

        private string DefaultKey = "";

        private HashAlgorithmEngine _he;
        private SymmetricAlgorithmEngine _se;

        private string formatKey(string k)
        {
            if (k == null || k.Length == 0) return null;
            return k.Trim();
        }

        public CryptEngine()
        {
            //InitializeEngine();
            DefaultKey = "abcdefg";
            UseHexEncoding = false; // base64 encoding is default.
        }

        public CryptEngine(AlgorithmType al)
        {
            _algorithm = al;
            DefaultKey = "abcdefg";

            InitializeEngine();
        }

        public string Decrypt(string src)
        {
            string result = "";

            if (_isSymmetric)
            {
                if (_key == null)
                    result = _se.Decrypting(src, DefaultKey, UseHexEncoding);
                else result = _se.Decrypting(src, _key, UseHexEncoding);
            }

            return result;
        }

        public string Decrypt(string src, string ckey)
        {
            string result = "";

            if (_isSymmetric)
            {
                result = _se.Decrypting(src, ckey, UseHexEncoding);
            }

            return result;
        }

        #region "Implement Salt Hashing"

        public static string GenerateSalt()
        {
            string salt = String.Empty;

            var saltBytes = new byte[32];
            var provider = RandomNumberGenerator.Create();
            provider.GetNonZeroBytes(saltBytes);
            salt = Convert.ToBase64String(saltBytes);

            return salt;
        }

        public string Hash(string plainText)
        {
            string result = "";

            if (_isHash)
            {
                result = _he.Encoding(plainText, UseHexEncoding);
            }
            else
            {
                throw new Exception("The algorithm is not hashing algorithm.");
            }
            return result;
        }

        public string Hash(string plainText, string salt)
        {
            string result = "";
            var passwordAndSaltBytes = GetSaltedText(plainText, salt);

            if (_isHash)
            {
                result = _he.Encoding(passwordAndSaltBytes, UseHexEncoding);
            }
            else
            {
                throw new Exception("The algorithm is not hashing algorithm.");
            }
            return result;
        }


        private string GetSaltedText(string plainText, string salt)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            byte[] saltBytes = Encoding.UTF8.GetBytes(salt);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes = new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];

            return System.Text.Encoding.UTF8.GetString(plainTextWithSaltBytes);

        }

        public bool Verify(string plainText, string salt, string hash)
        {
            if (salt == null || salt == "")
            {
                return this.Hash(plainText) == hash;
            }
            else
            {
                return this.Hash(plainText, salt) == hash;
            }
        }



        #endregion



        public string Encrypt(string src)
        {
            string result = "";

            if (_isHash)
            {
                result = _he.Encoding(src, UseHexEncoding);
            }
            else if (_isSymmetric)
            {
                if (_key == null)
                    result = _se.Encrypting(src, DefaultKey, UseHexEncoding);
                else result = _se.Encrypting(src, _key, UseHexEncoding);
            }

            return result;
        }

        public string Encrypt(string src, string ckey)
        {
            string result = "";

            if (_isHash)
            {
                result = _he.Encoding(src, UseHexEncoding);
            }
            else if (_isSymmetric)
            {
                result = _se.Encrypting(src, ckey, UseHexEncoding);
            }

            return result;
        }

        public bool DestroyEngine()
        {
            _se = null;
            _he = null;
            return true;
        }

        public bool InitializeEngine(AlgorithmType at)
        {
            _algorithm = at;

            return InitializeEngine();
        }

        public bool InitializeEngine()
        {
            if (_algorithm == AlgorithmType.None)
                return false;

            if (_algorithm == AlgorithmType.BlowFish ||
                _algorithm == AlgorithmType.DES ||
                _algorithm == AlgorithmType.RC2 ||
                _algorithm == AlgorithmType.Rijndael ||
                _algorithm == AlgorithmType.TripleDES ||
                _algorithm == AlgorithmType.Twofish)
            {
                _isSymmetric = true;
                _isHash = false;
            }
            else
                if (_algorithm == AlgorithmType.MD5 ||
                _algorithm == AlgorithmType.SHA ||
                _algorithm == AlgorithmType.SHA256 ||
                _algorithm == AlgorithmType.SHA384 ||
                _algorithm == AlgorithmType.SHA512)
                {
                    _isSymmetric = false;
                    _isHash = true;
                }

            _se = null;
            _he = null;
            switch (_algorithm)
            {
                case AlgorithmType.BlowFish:
                    _se = new SymmetricAlgorithmEngine(SymmetricAlgorithmEngine.EncodeMethodEnum.BlowFish);
                    break;
                case AlgorithmType.DES:
                    _se = new SymmetricAlgorithmEngine(SymmetricAlgorithmEngine.EncodeMethodEnum.DES);
                    break;
                case AlgorithmType.MD5:
                    _he = new HashAlgorithmEngine(HashAlgorithmEngine.EncodeMethodEnum.MD5);
                    break;
                case AlgorithmType.RC2:
                    _se = new SymmetricAlgorithmEngine(SymmetricAlgorithmEngine.EncodeMethodEnum.RC2);
                    break;
                case AlgorithmType.Rijndael:
                    _se = new SymmetricAlgorithmEngine(SymmetricAlgorithmEngine.EncodeMethodEnum.Rijndael);
                    break;
                case AlgorithmType.SHA:
                    _he = new HashAlgorithmEngine(HashAlgorithmEngine.EncodeMethodEnum.SHA);
                    break;
                case AlgorithmType.SHA256:
                    _he = new HashAlgorithmEngine(HashAlgorithmEngine.EncodeMethodEnum.SHA256);
                    break;
                case AlgorithmType.SHA384:
                    _he = new HashAlgorithmEngine(HashAlgorithmEngine.EncodeMethodEnum.SHA384);
                    break;
                case AlgorithmType.SHA512:
                    _he = new HashAlgorithmEngine(HashAlgorithmEngine.EncodeMethodEnum.SHA512);
                    break;
                case AlgorithmType.TripleDES:
                    _se = new SymmetricAlgorithmEngine(SymmetricAlgorithmEngine.EncodeMethodEnum.TripleDES);
                    break;
                case AlgorithmType.Twofish:
                    _se = new SymmetricAlgorithmEngine(SymmetricAlgorithmEngine.EncodeMethodEnum.Twofish);
                    break;
                default:
                    return false;
            }
            return true;
        }

        public class HashAlgorithmEngine
        {
            public enum EncodeMethodEnum : int
            {
                SHA, SHA256, SHA384, SHA512, MD5
            }

            private HashAlgorithm EncodeMethod;

            public HashAlgorithmEngine(HashAlgorithm ServiceProvider)
            {
                EncodeMethod = ServiceProvider;
            }

            public HashAlgorithmEngine(EncodeMethodEnum iSelected)
            {
                switch (iSelected)
                {
                    case EncodeMethodEnum.SHA:
                        EncodeMethod = new SHA1CryptoServiceProvider();
                        break;
                    case EncodeMethodEnum.SHA256:
                        EncodeMethod = new SHA256Managed();
                        break;
                    case EncodeMethodEnum.SHA384:
                        EncodeMethod = new SHA384Managed();
                        break;
                    case EncodeMethodEnum.SHA512:
                        EncodeMethod = new SHA512Managed();
                        break;
                    case EncodeMethodEnum.MD5:
                        EncodeMethod = new MD5CryptoServiceProvider();
                        break;
                }
            }

            public string Encoding(string Source, bool useHexCoding = false)
            {
                byte[] bytIn = System.Text.ASCIIEncoding.ASCII.GetBytes(Source);
                byte[] bytOut = EncodeMethod.ComputeHash(bytIn);
                if (!useHexCoding)
                {
                    // convert into Base64 so that the result can be used in xml
                    return System.Convert.ToBase64String(bytOut, 0, bytOut.Length);
                }
                else
                {
                    return ByteArrayToString(bytOut);
                }
            }
        }

        public class SymmetricAlgorithmEngine
        {
            public enum EncodeMethodEnum : int
            {
                DES, RC2, Rijndael, TripleDES, BlowFish, Twofish
            }

            private SymmetricAlgorithm EncodeMethod;

            public SymmetricAlgorithmEngine(SymmetricAlgorithm ServiceProvider)
            {
                EncodeMethod = ServiceProvider;
            }

            public SymmetricAlgorithmEngine(EncodeMethodEnum iSelected)
            {
                switch (iSelected)
                {
                    case EncodeMethodEnum.DES:
                        EncodeMethod = new DESCryptoServiceProvider();
                        break;
                    case EncodeMethodEnum.RC2:
                        EncodeMethod = new RC2CryptoServiceProvider();
                        break;
                    case EncodeMethodEnum.Rijndael:
                        EncodeMethod = new RijndaelManaged();
                        break;
                    case EncodeMethodEnum.TripleDES:
                        EncodeMethod = new TripleDESCryptoServiceProvider();
                        break;
                    case EncodeMethodEnum.BlowFish:
                        EncodeMethod = new BlowfishAlgorithm();
                        EncodeMethod.Mode = CipherMode.CBC;
                        EncodeMethod.KeySize = 40;
                        EncodeMethod.GenerateKey();
                        EncodeMethod.GenerateIV();
                        break;

                }
            }

            private byte[] getValidKey(string Key)
            {
                string sTemp;
                if (EncodeMethod.LegalKeySizes.Length > 0)
                {
                    int lessSize = 0, moreSize = EncodeMethod.LegalKeySizes[0].MinSize;
                    // key sizes are in bits

                    while (Key.Length * 8 > moreSize &&
                        EncodeMethod.LegalKeySizes[0].SkipSize > 0 &&
                        moreSize < EncodeMethod.LegalKeySizes[0].MaxSize)
                    {
                        lessSize = moreSize;
                        moreSize += EncodeMethod.LegalKeySizes[0].SkipSize;
                    }

                    if (Key.Length * 8 > moreSize)
                        sTemp = Key.Substring(0, (moreSize / 8));
                    else
                        sTemp = Key.PadRight(moreSize / 8, ' ');
                }
                else
                    sTemp = Key;
                // convert the secret key to byte array
                return ASCIIEncoding.ASCII.GetBytes(sTemp);
            }

            private byte[] getValidIV(String InitVector, int ValidLength)
            {
                if (InitVector.Length > ValidLength)
                {
                    return ASCIIEncoding.ASCII.GetBytes(InitVector.Substring(0, ValidLength));
                }
                else
                {
                    return ASCIIEncoding.ASCII.GetBytes(InitVector.PadRight(ValidLength, ' '));
                }
            }

           
            public string Encrypting(string Source, string Key, bool useHexEncoding)
            {
                if (Source == null || Key == null || Source.Length == 0 || Key.Length == 0)
                    return null;

                if (EncodeMethod == null) return "Under Construction";

                long lLen;
                int nRead, nReadTotal;
                byte[] buf = new byte[3];
                byte[] srcData;
                byte[] encData;
                System.IO.MemoryStream sin;
                System.IO.MemoryStream sout;
                CryptoStream encStream;

                srcData = new System.Text.UTF8Encoding().GetBytes(Source);
                sin = new MemoryStream();
                sin.Write(srcData, 0, srcData.Length);
                sin.Position = 0;
                sout = new MemoryStream();

                EncodeMethod.Key = getValidKey(Key);
                EncodeMethod.IV = getValidIV(Key, EncodeMethod.IV.Length);

                encStream = new CryptoStream(sout,
                    EncodeMethod.CreateEncryptor(),
                    CryptoStreamMode.Write);
                lLen = sin.Length;
                nReadTotal = 0;
                while (nReadTotal < lLen)
                {
                    nRead = sin.Read(buf, 0, buf.Length);
                    encStream.Write(buf, 0, nRead);
                    nReadTotal += nRead;
                }
                encStream.Close();

                encData = sout.ToArray();
                return useHexEncoding ? CryptEngine.ByteArrayToString(encData) : System.Convert.ToBase64String(encData);
            }

            public string Decrypting(string Source, string Key, bool UseHexEncoding)
            {
                if (Source == null || Key == null || Source.Length == 0 || Key.Length == 0)
                    return null;

                if (EncodeMethod == null) return "Under Construction";

                long lLen;
                int nRead, nReadTotal;
                byte[] buf = new byte[3];
                byte[] decData;
                byte[] encData;
                System.IO.MemoryStream sin;
                System.IO.MemoryStream sout;
                CryptoStream decStream;

                encData = UseHexEncoding ? CryptEngine.StringToByteArray(Source) : System.Convert.FromBase64String(Source);
                sin = new MemoryStream(encData);
                sout = new MemoryStream();

                EncodeMethod.Key = getValidKey(Key);
                EncodeMethod.IV = getValidIV(Key, EncodeMethod.IV.Length);

                decStream = new CryptoStream(sin,
                    EncodeMethod.CreateDecryptor(),
                    CryptoStreamMode.Read);

                lLen = sin.Length;
                nReadTotal = 0;
                while (nReadTotal < lLen)
                {
                    nRead = decStream.Read(buf, 0, buf.Length);
                    if (0 == nRead) break;

                    sout.Write(buf, 0, nRead);
                    nReadTotal += nRead;
                }


                decStream.Close();

                decData = sout.ToArray();

                UTF8Encoding ascEnc = new UTF8Encoding();
                return ascEnc.GetString(decData);

            }

        }
    }
}
