﻿using Pollyfy.Core.Crypt;
using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;

namespace Pollyfy.Core.Security
{
    public class Encryption
    {
        /// <summary>
        /// Create a new instance of Crypt engine by using system.web/machineKey
        /// </summary>
        /// <returns></returns>
        private static CryptEngine getEngine(string key = null)
        {
            MachineKeySection section = (MachineKeySection)ConfigurationManager.GetSection("system.web/machineKey");
            CryptEngine engine = new CryptEngine(CryptEngine.AlgorithmType.Rijndael);
            engine.Key = section.DecryptionKey;
            return engine;

        }

        private static CryptEngine getHashingEngine()
        {
            CryptEngine engine = new CryptEngine(CryptEngine.AlgorithmType.SHA256);
            return engine;
        }


        /// <summary>
        /// Encrpyt value 
        /// </summary>
        /// <param name="value">Value to be encrypted</param>
        /// <returns>Returns encrypted value</returns>
        public static string Encrypt(string value, string key = null)
        {
            CryptEngine engine = getEngine(key);
            string encryptedValue = string.Empty;
            try
            {
                encryptedValue = engine.Encrypt(value);
            }
            finally
            {
                engine.DestroyEngine();
            }
            return encryptedValue;
        }

        /// <summary>
        /// Decrpty value. If fail to decrypt, returs an empty string
        /// </summary>
        /// <param name="value">Value to be descrypted</param>
        /// <returns>Decrypted value. If fail to decrypt, returs an empty string</returns>
        public static string Decrypt(string value, string key = null)
        {
            CryptEngine engine = getEngine(key);
            string decryptedValue = String.Empty;
            try
            {
                decryptedValue = engine.Decrypt(value);
            }
            finally
            {
                engine.DestroyEngine();
            }
            
            return decryptedValue;
        }

        public static string Hash(string value, string salt = null)
        {
            CryptEngine engine = getHashingEngine();
            string rVal = String.Empty;
            try
            {
                rVal = salt != null ? engine.Hash(value, salt) : engine.Hash(value);
            }
            finally
            {
                engine.DestroyEngine();
            }
            return rVal;
        }

        public static string KeyedHmac(string key, string message)
        {
            byte[] hash_code = null;
            KeyedHashAlgorithm hasher = KeyedHashAlgorithm.Create("HMACSHA1");

            hasher.Key = Encoding.UTF8.GetBytes(key);
            hash_code = hasher.ComputeHash(Encoding.UTF8.GetBytes(message));
            return Convert.ToBase64String(hash_code);
        }
    }
}
