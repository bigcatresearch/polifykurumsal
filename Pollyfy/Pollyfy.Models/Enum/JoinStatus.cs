﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    public enum JoinStatus : int
    {
        AllowedSameUsers = 0,
        NotAllowedSameUsers = 1,
        JustNewUsers = 2
    }
}
