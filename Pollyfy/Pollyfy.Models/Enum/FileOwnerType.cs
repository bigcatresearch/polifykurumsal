﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    public enum FileOwnerType : int
    {
        None = 0,
        Company = 1,
        User = 2,
        Survey = 3,
        Question = 4,
        Choice = 5,
        Temp = 6
    }
}
