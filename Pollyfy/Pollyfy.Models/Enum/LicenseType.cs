﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    public enum LicenseType : int
    {
        None = 0,
        Freebie = 1,
        Limited = 2,
        PayAsGo = 3,
        Billing = 4,
        Limitless = 5

    }
}
