﻿namespace Pollyfy.Models
{
    public enum QuestionType : int
    {
        SingleSelect = 0,
        MultipleSelect = 1,
        OpenEnded = 2,
        Image = 3,
        Progress = 4,
        Emoji = 5
    }
}