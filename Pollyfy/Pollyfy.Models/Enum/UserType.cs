﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    public enum UserType : int
    {
        MobileUser = 0,
        CompanyUser = 1,
        DashboardUser = 3,
        SystemAdmin = 2,
        CompanyAdmin = 4,

    }
}
