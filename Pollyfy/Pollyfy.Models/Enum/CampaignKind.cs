﻿namespace Pollyfy.Models.Enum
{
    public enum CampaignKind : int
    {
        Description,
        DiscountCode
    }
}