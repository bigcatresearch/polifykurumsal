﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    public enum PublishStatus : int
    {
        None = 0,
        ApprovalDenied = 1,
        WaitingApproval = 2,
        ApprovalGranted = 3,
        Scheduled = 4,
        Published = 5,
        UnPublished = 6,
        Ready = 7,
        QuestionRequired = 8,
        QuestionOver = 9,
    }
}
