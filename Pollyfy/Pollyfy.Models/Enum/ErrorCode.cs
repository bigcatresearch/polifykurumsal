﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    public enum ErrorCode
    {
        NONE,
        NOTFOUND,
        ALREADY_EXISTS,
        LOGIN_FAILLED,
        LOGIN_USER_PASSIVE,
        LOGIN_USER_DELETED,
        INVALID_PARAMETER,
        ERROR_OCCURRED,
        ERROR_VALIDATION,
        USER_EMAIL_ALREADY_EXISTS,
        USER_IMEI_ALREADY_EXISTS,
        DATABASE_INSERT_FAILED,
        EMAIL_SEND_FAILED,
        UNDER_AGE_LIMIT,
        FACEBOOK_AUTH_FAILED,
        UNDER_EXPOINT_LIMIT
    }
}
