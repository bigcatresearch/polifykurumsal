﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    public enum FileType : int
    {
        None = 0,
        Image = 1,
        Excell = 2
    }
}
