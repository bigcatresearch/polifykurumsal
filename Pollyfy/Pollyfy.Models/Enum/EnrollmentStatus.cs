﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    public enum EnrollmentStatus : int
    {
        Initial = 1,
        Started = 2,
        Finished = 4
    }
}
