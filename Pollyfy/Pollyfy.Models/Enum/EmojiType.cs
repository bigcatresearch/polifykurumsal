﻿namespace Pollyfy.Models.Enum
{
    public enum EmojiType : int
    {
        Hicbiri = 0,
        Sevincli = 1,
        Mutlu = 2,
        Umursamaz = 3,
        Dusunceli = 4,
        Huzunlu = 5,
        Sasirmis = 6,
        Sinsi = 7,
        Kirgin = 8,
        Kizgin = 9
    }
}