﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    public enum EnneagramType : int
    {
        None = 0,
        Perfectionist = 1,
        Helpfully = 2,
        AchievementOriented = 3,
        Unique = 4,
        Researcher = 5,
        Questioner = 6,
        Adventurous = 7,
        Defiant = 8,
        Pacifist = 9
    }
}
