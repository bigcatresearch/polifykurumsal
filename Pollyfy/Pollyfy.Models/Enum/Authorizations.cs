﻿using System;

namespace Pollyfy.Models.Enum
{
    [Flags]
    public enum Authorizations
    {
        None = 0,

        Administrator,

        HomeView,

        CompanyView,
        CompanyAdd,
        CompanyEdit,
        CompanyDelete,

        UserView,
        UserAdd,
        UserEdit,
        UserDelete,

        UserRoleView,
        UserRoleAdd,
        UserRoleEdit,
        UserRoleDelete,

        SurveyView,
        SurveyAdd,
        SurveyEdit,
        SurveyDelete,

        ReportView,

        SettingsView,

        HelpView
    }
}