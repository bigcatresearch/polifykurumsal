﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    public enum SurveyType : int
    {
        Generic = 0,
        Enneagram = 1,
        Academic = 2,
        Showcase = 3,
        Research = 4
    }
}
