﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    public enum CategoryType : int
    {
        None = 0,
        SurveyProperties = 1,
        SystemProperties = 2
    }
}
