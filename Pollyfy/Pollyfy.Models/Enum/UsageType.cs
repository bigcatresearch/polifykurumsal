﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    public enum UsageType : int
    {
        None = 0,
        ApplicationForm = 1,
        ActivityForm = 2,
        CampaignSurvey = 3,
        ProfitableSurvey = 4,
        AcademicSurvey = 5,
        InCompanySurvey = 6
    }
}
