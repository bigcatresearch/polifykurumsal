﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    public enum GroupType : int
    {
        None = 0,
        ScoreRange = 1,
        Featured = 2
    }
}
