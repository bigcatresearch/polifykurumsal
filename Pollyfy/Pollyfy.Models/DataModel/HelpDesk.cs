﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    [TableName("Helpdesk")]
    public class Helpdesk
    {
        public Helpdesk()
        {
            this.CreatedDate = DateTime.Now;
            this.ActiveStatus = ActiveStatus.Active;
            this.TrackingGuid = Guid.NewGuid();
        }
        [PrimaryKey]
        public int HelpdeskId { get; set; }
        public int UserId { get; set; }
        public ActiveStatus ActiveStatus { get; set; }
        public string Message { get; set; }
        public int? SurveyId { get; set; }
        public int? QuestionId { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid TrackingGuid { get; set; }
    }
}
