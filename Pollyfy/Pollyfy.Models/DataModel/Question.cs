﻿using System;
using System.Collections.Generic;

namespace Pollyfy.Models
{
    [TableName("Question")]
    public class Question
    {
        public Question()
        {
            this.Choices = new HashSet<Choice>();
            this.Answers = new HashSet<Answer>();
            this.IsRequired = true;
            this.CreatedDate = DateTime.Now;
            this.TrackingGuid = Guid.NewGuid();
            this.QuestionType = Models.QuestionType.SingleSelect;
            this.ActiveStatus = ActiveStatus.Active;
            this.EnneagramType = EnneagramType.None;
            //FormIncludeGroups = new List<string[]>();
            //FormExcludeGroups = new List<string[]>();
            Groups = new List<QuestionGroup>();
            FormChoices = new List<Choice>();
        }

        [PrimaryKey]
        public int QuestionId { get; set; }

        public int SurveyId { get; set; }

        public QuestionType QuestionType { get; set; }

        public string QuestionText { get; set; }

        public string Description { get; set; }

        public bool IsRequired { get; set; }

        public bool IsImage { get; set; }

        public string DescribeIsImage { get; set; }

        public ActiveStatus ActiveStatus { get; set; }

        public EnneagramType EnneagramType { get; set; }

        public decimal? MaxScore { get; set; }

        public decimal? Score { get; set; }

        public int? FileId { get; set; }

        public int CompanyId { get; set; }

        public int? CreatedByUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid TrackingGuid { get; set; }

        public int? CopiedId { get; set; }

        public bool IsChoiceImage { get; set; }


        public virtual Survey Survey { get; set; }

        public virtual Question CopiedQuestion { get; set; }

        public virtual ICollection<Choice> Choices { get; set; }

        public virtual ICollection<Answer> Answers { get; set; }

        //public virtual String[] FormChoiceIds { get; set; }
        //public virtual String[] FormChoiceTexts { get; set; }
        //public virtual Decimal[] FormChoiceScores { get; set; }
        //public virtual List<string[]> FormIncludeGroups { get; set; }
        //public virtual List<string[]> FormExcludeGroups { get; set; }
        //public virtual int[] FormActiveStatus { get; set; }
        //public virtual int[] FormChoicePasses { get; set; }

        public virtual List<QuestionGroup> Groups { get; set; }

        public virtual List<Choice> FormChoices { get; set; }
    }
}