﻿using System;

namespace Pollyfy.Models.DataModel
{
    public class UserRoleAuthorization : Base.EditableBase
    {
        public UserRoleAuthorization()
        {
            this.TrackingGuid = Guid.NewGuid();
            this.ActiveStatus = ActiveStatus.Active;
        }

        public Guid TrackingGuid { get; set; }

        public int UserRoleId { get; set; }

        public int AuthorizationId { get; set; }


        public virtual UserRole UserRole { get; set; }

        public virtual Authorization Authorization { get; set; }
    }
}