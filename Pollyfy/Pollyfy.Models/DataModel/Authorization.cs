﻿#region 

using System;
using System.Collections.Generic;

#endregion

namespace Pollyfy.Models.DataModel
{
    public class Authorization : Base.EditableBase
    {
        public Authorization()
        {
            this.TrackingGuid = Guid.NewGuid();
            this.ActiveStatus = ActiveStatus.Active;
        }

        public Guid TrackingGuid { get; set; }

        public string Key { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public string Note { get; set; }


        public virtual ICollection<UserRoleAuthorization> UserRoleAuthorizations { get; set; }
    }
}