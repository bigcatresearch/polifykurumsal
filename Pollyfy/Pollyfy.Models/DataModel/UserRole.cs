﻿using System;
using System.Collections.Generic;

namespace Pollyfy.Models.DataModel
{
    public class UserRole : Base.EditableBase
    {
        public UserRole()
        {
            this.TrackingGuid = Guid.NewGuid();
            this.ActiveStatus = ActiveStatus.Active;
        }

        public Guid TrackingGuid { get; set; }

        public string Name { get; set; }

        public UserType UserType { get; set; }

        public string Note { get; set; }


        public virtual ICollection<UserRoleAuthorization> UserRoleAuthorizations { get; set; }
    }
}