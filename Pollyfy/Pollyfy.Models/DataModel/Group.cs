﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pollyfy.Models.DataModel
{
    public class Group : Base.EditableBase
    {
        public Group()
        {
            this.TrackingGuid = Guid.NewGuid();
            this.ActiveStatus = ActiveStatus.Active;
        }

        public Guid TrackingGuid { get; set; }

        /// <summary>
        /// GroupId QuestionGroup'un Id'sidir.
        /// </summary>
        public int GroupId { get; set; }

        public int SurveyId { get; set; }

        public int UserId { get; set; }


        [ForeignKey("SurveyId")]
        public virtual Survey Survey { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [ForeignKey("GroupId")]
        public virtual QuestionGroup QuestionGroup { get; set; }
    }
}