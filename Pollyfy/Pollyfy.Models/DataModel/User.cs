﻿using System;
using System.Collections.Generic;
using Pollyfy.Models.ViewModel;
using Pollyfy.Models.DataModel;

namespace Pollyfy.Models
{
    [TableName("User")]
    public class User
    {
        public User()
        {
            Properties = new HashSet<HelperValue>();
            Enrollments = new HashSet<Enrollment>();
            Answers = new HashSet<Answer>();
            OwnedSurveys = new HashSet<Survey>();
            AssignedSurveys = new HashSet<Survey>();
            Promotions = new HashSet<Promotion>();
            Balance = 0;
            IsOnline = false;
            IsPhoneValidated = false;
            IsEmailValidated = false;
            TrackingGuid = Guid.NewGuid();
            CreatedDate = DateTime.Now;
            ExPoint = 0;
            AllowedInfoShare = false;
        }

        [PrimaryKey]
        public int UserId { get; set; }

        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? CompanyId { get; set; }
        public UserType Types { get; set; }
        public ActiveStatus ActiveStatus { get; set; }
        public bool? IsOnline { get; set; }
        public string IdentityNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneAcivationCode { get; set; }
        public string EmailAcivationCode { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Gender { get; set; }
        public string Comment { get; set; }
        public string Culture { get; set; }
        public string Currency { get; set; }
        public decimal? Balance { get; set; }
        public int ExPoint { get; set; }
        public string FacebookId { get; set; }
        public string GoogleId { get; set; }
        public string TwitterId { get; set; }
        public string OpenId { get; set; }
        public bool AllowedInfoShare { get; set; }
        public bool? IsEmailValidated { get; set; }
        public bool? IsPhoneValidated { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? LastAccessTime { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public int? LastActiveDeviceId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public int? ProfileImageId { get; set; }
        public string LoginChannel { get; set; }
        public string Country { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
       // public string Sector { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CustomProperty1 { get; set; }
        public string CustomProperty2 { get; set; }
        public string CustomProperty3 { get; set; }
        public string CustomProperty4 { get; set; }
        public string CustomProperty5 { get; set; }
        public string CustomProperty6 { get; set; }
        public string CustomProperty7 { get; set; }
        public string CustomProperty8 { get; set; }
        public string CustomProperty9 { get; set; }
        public string CustomProperty10 { get; set; }
        public Guid TrackingGuid { get; set; }
        public int? UserRoleId { get; set; }
        public string ReferanceNumber { get; set; }
        public string MyReferanceNumber { get; set; }

        public virtual ICollection<HelperValue> Properties { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }

        public virtual ICollection<Answer> Answers { get; set; }

        public virtual ICollection<Survey> OwnedSurveys { get; set; }

        public virtual ICollection<Device> Devices { get; set; }

        public virtual ICollection<Survey> AssignedSurveys { get; set; }

        public virtual ICollection<Promotion> Promotions { get; set; }

        [NoDatabaseColumn]
        public virtual string CompanyName { get; set; }

        [NoDatabaseColumn]
        public virtual FileContainer ImportFileContainer { get; set; }

        public virtual UserRole UserRole { get; set; }

        public string GetFullName()
        {
            return FirstName + " " + LastName;
        }
    }
}