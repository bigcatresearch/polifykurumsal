﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    [TableName("Device")]
    public class Device
    {
        public Device()
        {
            this.Enrollments = new HashSet<Enrollment>();
        }
        [PrimaryKey]
        public int DeviceId { get; set; }
        public int UserId { get; set; }
        public string Platform { get; set; }
        public string Name { get; set; }
        public string Imei { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Version { get; set; }
        public string Uuid { get; set; }
        public string NotifyKey { get; set; }
        public string Operator { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? LastAccessDate { get; set; }
        public virtual User Owner { get; set; }
        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}
