﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    [TableName("HelperValue")]
    public class HelperValue
    {
        public HelperValue()
        {
            this.Users = new HashSet<User>();
            this.Owners = new HashSet<HelperValue>();
            this.IsLevelAllowed = false;
            this.IsDeleteAllowed = true;
            this.IsVisible = true;
            this.ActiveStatus = ActiveStatus.Active;
            ScopeSurveys = new HashSet<Survey>();
            ConstraintSurveys = new HashSet<Survey>();
        }
        [PrimaryKey]
        public int ValueId { get; set; }
        public int? ParentId { get; set; }
        public int CategoryId { get; set; }
        public int? CompanyId { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }
        public ActiveStatus ActiveStatus { get; set; }
        public bool IsVisible { get; set; }
        public bool IsDeleteAllowed { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsLevelAllowed { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual HelperCategory Category { get; set; }
        public virtual HelperValue Parent { get; set; }
        public virtual ICollection<HelperValue> Owners { get; set; }
        public virtual ICollection<Survey> ConstraintSurveys { get; set; }
        public virtual ICollection<Survey> ScopeSurveys { get; set; }
        [NoDatabaseColumn]
        public virtual int? SurveyId { get; set; }
    }
}
