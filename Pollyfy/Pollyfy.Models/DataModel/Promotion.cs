﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    [TableName("Promotion")]
    public class Promotion
    {
        public Promotion()
        {
            ActiveStatus = ActiveStatus.Active;
            CreatedDate = DateTime.Now;
            TrackingGuid = Guid.NewGuid();
        }
        [PrimaryKey]
        public int PromotionId { get; set; }
        public int SurveyId { get; set; }
        public int? UserId { get; set; }
        public ActiveStatus ActiveStatus { get; set; }
        public string CompanyName { get; set; }
        public string ContactUrl { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Discount { get; set; }
        public string Code { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public DateTime? AssignedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid TrackingGuid { get; set; }
        public virtual Survey Survey { get; set; }
        public virtual User User { get; set; }
    }
}
