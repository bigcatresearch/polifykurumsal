﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    [TableName("QuestionGroup")]
    public class QuestionGroup
    {
        public QuestionGroup()
        {
            ActiveStatus = ActiveStatus.Active;
            GroupType = GroupType.None;
        }
        [PrimaryKey]
        public int GroupId { get; set; }
        public int SurveyId { get; set; }
        public ActiveStatus ActiveStatus { get; set; }
        public GroupType GroupType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? BeginScore { get; set; }
        public int? EndScore { get; set; }
        [NoDatabaseColumn]
        public decimal? Score { get; set; }
        [NoDatabaseColumn]
        public int? Route { get; set; }

    }
}
