﻿using Pollyfy.Models.Enum;
using System;
using System.Collections.Generic;

namespace Pollyfy.Models
{
    [TableName("Choice")]
    public class Choice
    {
        public Choice()
        {
            this.CreatedDate = DateTime.Now;
            this.TrackingGuid = Guid.NewGuid();
            this.Answers = new HashSet<Answer>();
            this.ActiveStatus = ActiveStatus.Active;
        }

        [PrimaryKey]
        public int ChoiceId { get; set; }

        public int QuestionId { get; set; }

        public string Text { get; set; }

        public decimal? Score { get; set; }

        public int? PassQuestion { get; set; }

        public ActiveStatus ActiveStatus { get; set; }

        public int? FileId { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid TrackingGuid { get; set; }

        public string ChoiceDescribe { get; set; }

        
        public virtual Question Question { get; set; }

        public virtual ICollection<Answer> Answers { get; set; }

        public virtual int[] FormIncludeGroups { get; set; }

        public virtual int[] FormExcludeGroups { get; set; }
    }
}