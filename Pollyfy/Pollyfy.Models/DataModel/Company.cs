﻿using Pollyfy.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    [TableName("Company")]
    public class Company
    {
        public Company()
        {
            this.Surveys = new HashSet<Survey>();
            this.CreatedDate = DateTime.Now;
            this.TrackingGuid = Guid.NewGuid();
            this.LicenseType = LicenseType.None;
            this.ActiveStatus = ActiveStatus.Active;
        }
        [PrimaryKey]
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string AuthorizedPerson { get; set; }
        public ActiveStatus ActiveStatus { get; set; }
       // public int SectorId { get; set; }
        public LicenseType LicenseType { get; set; }
        public int? MaxUserCount { get; set; }
        public int? MaxSurveyCount { get; set; }
        public int? MaxQuestionCount { get; set; }
        public decimal? Balance { get; set; }
        public DateTime? BillingDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Culture { get; set; }
        public string Currency { get; set; }
        public int? LogoId { get; set; }
        public string CustomProperty1 { get; set; }
        public string CustomProperty2 { get; set; }
        public string CustomProperty3 { get; set; }
        public string CustomProperty4 { get; set; }
        public string CustomProperty5 { get; set; }
        public string Comment { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public Guid TrackingGuid { get; set; }
        [NoDatabaseColumn]
        public virtual FileContainer FileContainer { get; set; }
        public virtual ICollection<Survey> Surveys { get; set; }
    }
}
