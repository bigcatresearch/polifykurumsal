﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    [TableName("ChoiceGroup")]
    public class ChoiceGroup
    {
        public ChoiceGroup()
        {
        }
        public int QuestionId { get; set; }
        public int GroupId { get; set; }
        public int ChoiceId { get; set; }
        public int Route { get; set; }

    }
}
