﻿#region 

using Pollyfy.Models.DataModel.Base;

#endregion

namespace Pollyfy.Models.DataModel
{
    public class TemporaryExpoint : EditableBase
    {
        public int Expoint { get; set; }

        public int UserId { get; set; }


        public virtual User User { get; set; }
    }
}