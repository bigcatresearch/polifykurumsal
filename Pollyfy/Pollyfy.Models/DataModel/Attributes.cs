﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    [System.AttributeUsage(System.AttributeTargets.All)]
    public class TableName : System.Attribute
    {
        public string name;
        public TableName()
        {

        }
        public TableName(string name)
        {
            this.name = name;
        }
    }

    [System.AttributeUsage(System.AttributeTargets.All)]
    public class PrimaryKey : System.Attribute
    {
        public string name;
        public PrimaryKey()
        {

        }
        public PrimaryKey(string name)
        {
            this.name = name;
        }
    }
    [System.AttributeUsage(System.AttributeTargets.All)]
    public class NoDatabaseColumn : System.Attribute
    {
        public string name;
        public NoDatabaseColumn()
        {

        }
        public NoDatabaseColumn(string name)
        {
            this.name = name;
        }
    }
}
