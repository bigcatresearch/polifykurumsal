﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    [TableName("Survey")]
    public class Survey
    {
        public Survey()
        {
            this.Constraints = new HashSet<HelperValue>();
            this.Scopes = new HashSet<HelperValue>();
            this.Questions = new HashSet<Question>();
            this.Enrollments = new HashSet<Enrollment>();
            this.Answers = new HashSet<Answer>();
            this.AssignedUsers = new HashSet<User>();
            this.Promotions = new HashSet<Promotion>();
            ReportConstraints = new HashSet<HelperCategory>();
            QuestionGroups = new List<QuestionGroup>();
            this.CreatedDate = DateTime.Now;
            this.TrackingGuid = Guid.NewGuid();
            this.ActiveStatus = ActiveStatus.Passive;
           // this.Reward = 0;
            this.UseAccuracy = false;
            this.AccuracyQueCount = 0;
            this.PublishStatus = PublishStatus.None;
            this.InCompanyUsage = false;
            this.ExPoint = 0;
            this.PromotionCount = 0;
        }
        [PrimaryKey]
        public int SurveyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int QuestionCount { get; set; }
        [NoDatabaseColumn]
        public int? RealQuestionCount { get; set; }
        public int? JoinLimit { get; set; }
        public int? DaylyJoinLimit { get; set; }
       // public int SectorId { get; set; }
      //  public decimal Reward { get; set; }
        public string FullingTime { get; set; }
        public int? ExPoint { get; set; }
        public SurveyType SurveyType { get; set; }
        public UsageType UsageType { get; set; }
        public ActiveStatus ActiveStatus { get; set; }
        public JoinStatus? JoinStatus { get; set; }
        public string FinalMessage { get; set; }
        public int? FileId { get; set; }
        public PublishStatus PublishStatus { get; set; }
        public string StatusText { get; set; }
        public bool UseAccuracy { get; set; }
        public int? AccuracyQueCount { get; set; }
        public int? AccuracyCriteria { get; set; }
        public string PrivacyQuestion { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? FirstPublishDate { get; set; }
        public DateTime? LastPublishDate { get; set; }
        public DateTime? LastUnPublishDate { get; set; }
        public string SurveyUrl { get; set; }

        //todo: bunlar kontrol edilecek - public ActionResult ExportToExcell(string id)
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? FinishDate { get; set; }
        public bool InCompanyUsage { get; set; }
        public int CompanyId { get; set; }
        public int? CreatedByUserId { get; set; }
        public Guid TrackingGuid { get; set; }
        public int SpecialNumber { get; set; }
       // public virtual HelperValue Sector { get; set; }
        public virtual User CreatedUser { get; set; }
        public virtual Company Company { get; set; }
        public virtual ICollection<HelperValue> Constraints { get; set; }
        public virtual ICollection<HelperCategory> ReportConstraints { get; set; }
        public virtual ICollection<HelperValue> Scopes { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<Enrollment> Enrollments { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual ICollection<User> AssignedUsers { get; set; }
        public virtual ICollection<Promotion> Promotions { get; set; }
        public virtual List<QuestionGroup> QuestionGroups { get; set; }

        [NoDatabaseColumn]
        public virtual String[] FormConstraints { get; set; }

        [NoDatabaseColumn]
        public virtual String[] FormReportConstraints { get; set; }

        public virtual String[] FormScopes { get; set; }

        [NoDatabaseColumn]
        public virtual string FormUsers { get; set; }

        [NoDatabaseColumn]
        public virtual string[] FormSurveys { get; set; }

        [NoDatabaseColumn]
        public virtual string ImageUrl { get; set; }

        [NoDatabaseColumn]
        public virtual string CurrentPromotion { get; set; }

        [NoDatabaseColumn]
        public int PromotionCount { get; set; }
    }
}