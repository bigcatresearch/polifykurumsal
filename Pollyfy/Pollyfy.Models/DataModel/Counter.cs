﻿#region

using System.ComponentModel.DataAnnotations;

#endregion

namespace Pollyfy.Models.DataModel
{
    public class Counter : Base.EntityBase
    {
        [MaxLength(64)]
        public string Key { get; set; }

        [MaxLength(64)]
        public string Param1 { get; set; }

        [MaxLength(64)]
        public string Param2 { get; set; }

        [MaxLength(64)]
        public string Param3 { get; set; }

        [Required]
        public int Value { get; set; }
    }
}