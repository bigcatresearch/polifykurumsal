﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    [TableName("TypeDescription")]
    public class TypeDescription
    {
        [PrimaryKey]
        public int DescId { get; set; }
        public bool IsForUser { get; set; }
        public bool IsForCompany { get; set; }
        public string Type1 { get; set; }
        public string Type2 { get; set; }
        public string Type3 { get; set; }
        public string Type4 { get; set; }
        public string Type5 { get; set; }
        public string Type6 { get; set; }
        public string Type7 { get; set; }
        public string Type8 { get; set; }
        public string Type9 { get; set; }
    }
}
