﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    [TableName("HelperCategory")]
    public class HelperCategory
    {
        public HelperCategory()
        {
            this.Values = new HashSet<HelperValue>();
            this.Owners = new HashSet<HelperCategory>();
            this.IsVisible = true;
            this.IsMultiple = false;
            this.IsDeleteAllowed = true;
            this.ActiveStatus = ActiveStatus.Active;
            this.AlwaysShow = false;
            ReportConstraintSurveys = new HashSet<Survey>();
        }
        [PrimaryKey]
        public int CategoryId { get; set; }
        public string Code { get; set; }
        public string Text { get; set; }
        public int? ParentId { get; set; }
        public bool AlwaysShow { get; set; }
        public bool IsVisible { get; set; }
        public bool IsMultiple { get; set; }
        public bool IsDeleteAllowed { get; set; }
        public int? CompanyId { get; set; }
        public CategoryType Types { get; set; }
        public ActiveStatus ActiveStatus { get; set; }
        public virtual ICollection<HelperValue> Values { get; set; }
        public virtual HelperCategory Parent { get; set; }
        public virtual ICollection<HelperCategory> Owners { get; set; }
        public virtual ICollection<Survey> ReportConstraintSurveys { get; set; }
    }
}
