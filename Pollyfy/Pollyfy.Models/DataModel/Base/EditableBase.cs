﻿#region 

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#endregion

namespace Pollyfy.Models.DataModel.Base
{
    public class EditableBase : EntityBase
    {
        [Column(Order = 1200)]
        [DataType(DataType.DateTime)]
        public DateTime? CreatedOn { get; set; } = DateTime.Now;

        [Column(Order = 1300)]
        [DataType(DataType.DateTime)]
        public DateTime? ModifiedOn { get; set; }

        [Column(Order = 1400)]
        public int? CreatedBy { get; set; }

        [Column(Order = 1500)]
        public int? ModifiedBy { get; set; }

        [Column(Order = 1600)]
        public ActiveStatus ActiveStatus { get; set; }
    }
}