﻿#region 

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#endregion

namespace Pollyfy.Models.DataModel.Base
{
    public class EntityBase
    {
        [Column(Order = 0)]
        [PrimaryKey]
        public int Id { get; set; }
    }
}