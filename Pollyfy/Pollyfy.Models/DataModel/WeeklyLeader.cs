﻿#region

using Pollyfy.Models.DataModel.Base;

#endregion

namespace Pollyfy.Models.DataModel
{
    public class WeeklyLeader : EntityBase
    {
        public int ExPoint { get; set; }

        public int UserId { get; set; }

        public int SequenceNo { get; set; }


        public virtual User User { get; set; }
    }
}