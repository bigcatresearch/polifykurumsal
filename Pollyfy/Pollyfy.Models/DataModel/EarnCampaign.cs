﻿using System;

namespace Pollyfy.Models.DataModel
{
    public class EarnCampaign : Base.EditableBase
    {
        public EarnCampaign()
        {

            this.TrackingGuid = Guid.NewGuid();
            this.ActiveStatus = ActiveStatus.Active;
        }

        public Guid TrackingGuid { get; private set; }

        public int CampaignId { get; set; }

        public int UserId { get; set; }

        public string Note { get; set; }


        public virtual User User { get; set; }

        public virtual Campaign Campaign { get; set; }
    }
}