﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models
{
    [TableName("Enrollment")]
    public class Enrollment
    {
        public Enrollment()
        {
            this.CreatedDate = DateTime.Now;
            this.TrackingGuid = Guid.NewGuid();
            this.Status = EnrollmentStatus.Initial;
            this.Answers = new HashSet<Answer>();
            this.RepeatId = 1;
            this.ExPoint = 0;
            this.AllowedInfoShare = false;
        }

        [PrimaryKey]
        public int EnrollmentId { get; set; }
        public int UserId { get; set; }
        public int SurveyId { get; set; }
        public decimal? Score { get; set; }
        public int ExPoint { get; set; }
        public int? PromoId { get; set; }
        public decimal? Accuracy { get; set; }
        public int RepeatId { get; set; }
        public int? DeviceId { get; set; }
        public EnrollmentStatus Status { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public bool AllowedInfoShare { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid TrackingGuid { get; set; }
        public virtual User User { get; set; }
        public virtual Survey Survey { get; set; }
        public virtual Device Device { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual Promotion Promotion { get; set; }
    }
}
