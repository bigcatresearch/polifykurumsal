﻿using System;

namespace Pollyfy.Models
{
    [TableName("Answer")]
    public class Answer
    {
        public Answer()
        {
            this.CreatedDate = DateTime.Now;
        }

        [PrimaryKey]
        public int AnswerId { get; set; }

        public int UserId { get; set; }

        public int EnrollmentId { get; set; }

        public int SurveyId { get; set; }

        public int QuestionId { get; set; }

        public int? ChoiceId { get; set; }

        public string ResponseText { get; set; }

        public DateTime CreatedDate { get; set; }


        public virtual User User { get; set; }

        public virtual Enrollment Enrollment { get; set; }

        public virtual Survey Survey { get; set; }

        public virtual Question Question { get; set; }

        public virtual Choice Choice { get; set; }
    }
}