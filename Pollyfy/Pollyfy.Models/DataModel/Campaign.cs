﻿using Pollyfy.Models.Enum;
using System;
using System.Collections.Generic;

namespace Pollyfy.Models.DataModel
{
    public class Campaign : Base.EditableBase
    {
        public Campaign()
        {
            this.TrackingGuid = Guid.NewGuid();
            this.ActiveStatus = ActiveStatus.Active;
        }

        public string Name { get; set; }

        public Guid TrackingGuid { get; private set; }

        public string CompanyName { get; set; }

        public string WebSiteName { get; set; }

        public string Image { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Location { get; set; }

        public int Point { get; set; }

        public string Code { get; set; }

        public CampaignKind CampaignKind { get; set; }

        public string Message { get; set; }

        public int Count { get; set; }

        public int Discount { get; set; }

        public string Note { get; set; }


        public ICollection<EarnCampaign> EarnCampaigns { get; set; }
    }
}