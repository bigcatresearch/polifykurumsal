﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class OrganizationModel
    {
        public string Name { get; set; } // Firma Adı
        public string Website { get; set; } // Firma Websitesi
        public string Email { get; set; }
        public string Password { get; set; }
        public string Position { get; set; } // Pozisyon
        public string HasRDOffice { get; set; } // Arge Ofisi?
        public string HasTechOffice { get; set; }  // Teknokent?
        public string PersonalCount { get; set; } // Personel Sayısı
        //public string Sector { get; set; } // Sektör
        public string Location { get; set; } // Merkezin bulunduğu şehir
        public string FundStatus { get; set; } // Sermaye
        public string CountryOfInvestor { get; set; } // Yabancı sermaye ülkesi
        public string Imei { get; set; }
        public DeviceModel Device { get; set; }
        public OrganizationModel()
        {
            Name = string.Empty;
            Website = string.Empty;
            Email = string.Empty;
            Password = string.Empty;
            Position = string.Empty;
            HasRDOffice = string.Empty;
            HasTechOffice = string.Empty;
            PersonalCount = string.Empty;
            //Sector = string.Empty;
            Location = string.Empty;
            FundStatus = string.Empty;
            CountryOfInvestor = string.Empty;
        }
    }
}
