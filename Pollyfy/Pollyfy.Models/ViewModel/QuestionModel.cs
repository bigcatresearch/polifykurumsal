﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class QuestionModel
    {
        public QuestionModel()
        {
            this.Choices = new List<ChoiceModel>();
        }
        public string QuestionId { get; set; }
        public string SurveyId { get; set; }
        public string QuestionText { get; set; }
        public string Description { get; set; }
        public int QuestionType { get; set; }
        public bool IsRequired { get; set; }
        public string ImageUrl { get; set; }
        public bool IsImage { get; set; }
        public string DescribeIsImage { get; set; }
        public bool IsChoiceImage { get; set; }
        public IEnumerable<ChoiceModel> Choices { get; set; }
    }
}
