﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class UserPropertyModel
    {
        public string UserId { get; set; }
        public string CategoryId { get; set; }
        public string Caption { get; set; }
        public string ValueId { get; set; }
        public string Text { get; set; }
    }
}
