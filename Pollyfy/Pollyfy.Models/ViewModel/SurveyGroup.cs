﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class SurveyGroup
    {
        public SurveyGroup()
        {
            Surveys = new List<SurveyModel>();
        }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public string ColorHex { get; set; }
        public IEnumerable<SurveyModel> Surveys { get; set; }
    }
}
