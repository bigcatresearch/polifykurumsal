﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class ConstraintsModel
    {
        public string Consname { get; set; }
        public IEnumerable<string> Consvalue { get; set; }
    }
}
