﻿using Pollyfy.Models.Enum;

namespace Pollyfy.Models.ViewModel
{
    public class ChoiceModel
    {
        public string ChoiceId { get; set; }

        public string Text { get; set; }

        public int PassQuestion { get; set; }

        public string ChoiceDescribe { get; set; }
    }
}