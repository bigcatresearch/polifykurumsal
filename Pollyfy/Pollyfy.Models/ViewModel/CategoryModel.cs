﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class CategoryModel
    {
        public string Id { get; set; }
        public string ParentId { get; set; }
        public string Caption { get; set; }
        public int CategoryType { get; set; }
        public bool IsVisible { get; set; }
        public bool IsMultiple { get; set; }
        public bool AlwaysShow { get; set; }
    }
}
