﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class QueryModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string ValueId { get; set; }
        public string SurveyId { get; set; }
        public string CampaignId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string QuestionId { get; set; }
        public string ChoiceId { get; set; } // split it
        public string AnswerText { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string Message { get; set; }
        public bool? AddCompletedSurveys { get; set; }
        public string GroupBy { get; set; }
        public string FacebookToken { get; set; }
        public string GraphApiToken { get; set; }
        public string AllowedInfoShare { get; set; }
        public string MyReferanceNumber { get; set; }
        public string Note { get; set; }
    }
}
