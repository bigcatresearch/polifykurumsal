﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class ChartDataModel
    {
        public DateTime Date { get; set; }
        public int Count { get; set; }
        public long UnixDate { get; set; }
    }
}
