﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class SurveyReportModel
    {
        public SurveyReportModel()
        {
            ParticipationChartSeries = new List<ChartDataModel>();
            ParticipatCoords = new List<Coord>();
        }
        public int SurveyId { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public SurveyType SurveyType { get; set; }
        public int QuestionCount { get; set; }
        public DateTime? FirstPublishDate { get; set; }
        public DateTime? LastPublishDate { get; set; }
        public int CompletedCount { get; set; }
        public int ContinuedCount { get; set; }
        public List<ChartDataModel> ParticipationChartSeries { get; set; }
        public List<Coord> ParticipatCoords { get; set; }
        public DateTime? SerieStartDate { get; set; }
        public long SerieStartUnixDate { get; set; }
        public DateTime? SerieEndDate { get; set; }
        public long SerieEndUnixDate { get; set; }
    }
}
