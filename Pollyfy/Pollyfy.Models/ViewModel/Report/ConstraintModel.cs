﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class ConstraintModel
    {
        public int ValueId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string ValueName { get; set; }
        public int Count { get; set; }
        public int UserId { get; set; }
        
    }
}
