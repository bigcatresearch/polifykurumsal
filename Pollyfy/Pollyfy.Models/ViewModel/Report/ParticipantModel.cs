﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class ParticipantModel
    {
        public int RowNumber { get; set; }
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string IdentityNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string Gender { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Location { get; set; }
        public string Platform { get; set; }
        public string Imei { get; set; }
        public string Operator { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string DeviceName { get; set; }
        public int Duration { get; set; }
        public string FullingTime { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public bool AllowedInfoShare { get; set; }

    }
}
