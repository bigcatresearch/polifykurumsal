﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class SurveyModel
    {
        public string SurveyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int SurveyType { get; set; }
        public string SurveyTypeText { get; set; }
        public int UsageType { get; set; }
        public string UsageTypeText { get; set; }
        public bool InCompanyUsage { get; set; }
        public string CompanyName { get; set; }
       // public string Sector { get; set; }
        public string ImageUrl { get; set; }
        public string SurveyUrl { get; set; }
      //  public string Reward { get; set; }
        public string ExPoint { get; set; }
        public int QuestionCount { get; set; }
        public string Scopes { get; set; }
        public string FinalMessage { get; set; }
        public string PrivacyQuestion { get; set; }
        public int SpecialNumber { get; set; }
        public string FullingTime { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public PromotionModel Promotion { get; set; }
    }
}
