﻿namespace Pollyfy.Models.ViewModel
{
    public class GroupModel
    {
        /// <summary>
        /// QuestionGroup'un Id si GroupId
        /// </summary>
        public int GroupId { get; set; }

        public int SurveyId { get; set; }

        public int UserId { get; set; }

        public string GroupName { get; set; }

        public string GroupDescription { get; set; }

        public int UserCount { get; set; }

        public string Gender { get; set; }
    }
}