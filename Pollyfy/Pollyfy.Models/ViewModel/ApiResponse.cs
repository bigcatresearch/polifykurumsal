﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class ApiResponse
    {
        public bool Success { get; set; }
        public object Data { get; set; }
        public string Code { get; set; }
        public object Error { get; set; }
    }
}
