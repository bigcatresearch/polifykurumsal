﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class FileContainer
    {
        public FileContainer()
        {
            IsRequired = false;
        }
        public int CompanyId { get; set; }
        public string CompanyIdRef { get; set; }
        public int OwnerId { get; set; }
        public string OwnerIdRef { get; set; }
        public FileOwnerType OwnerType { get; set; }
        public string FileName { get; set; }
        public FileType FileType { get; set; }
        public string Link { get; set; }
        public bool IsRequired { get; set; }

    }
}
