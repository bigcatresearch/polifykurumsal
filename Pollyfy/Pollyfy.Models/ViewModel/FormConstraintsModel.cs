﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class FormConstraintsModel
    {
        public string Code { get; set; }
        public string Caption { get; set; }
        public List<object> Items { get; set; }
    }
}
