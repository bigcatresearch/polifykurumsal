﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class SimpleCategory
    {
        public SimpleCategory()
        {
            this.IsMultiple = false;
            this.Items = new List<SimpleItem>();
        }
        public string CategoryId { get; set; }
        public string Caption { get; set; }
        public bool IsMultiple { get; set; }
        public IEnumerable<SimpleItem> Items { get; set; }
    }
}
