﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class UserModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public decimal Balance { get; set; }
        public int ExPoint { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool IsPhoneValidated { get; set; }
        public bool IsEmailValidated { get; set; }
        public int CompletedCount { get; set; }
        public int ResponseCount { get; set; }
        public List<SurveyModel> CompletedSurveys { get; set; }
        public string Imei { get; set; }
        public DeviceModel Device { get; set; }
        public string FacebookId { get; set; }
        public string FacebookToken { get; set; }
        public string AccessToken { get; set; }
        public string ReferanceNumber { get; set; }
        public string MyReferanceNumber { get; set; }

        public string CustomProperty1 { get; set; }
        public string CustomProperty2 { get; set; }
        public string CustomProperty3 { get; set; }
        public string CustomProperty4 { get; set; }
        public string CustomProperty5 { get; set; }
        public string CustomProperty6 { get; set; }
        public string CustomProperty7 { get; set; }
        public string CustomProperty8 { get; set; }
        public string CustomProperty9 { get; set; }
        public string CustomProperty10 { get; set; }

        public UserModel()
        {
            this.CompletedSurveys = new List<SurveyModel>();
            this.CompletedCount = 0;
            this.ResponseCount = 0;
            this.IsEmailValidated = false;
            this.IsPhoneValidated = false;
            this.Balance = 0;
            this.ExPoint = 0;
        }
        public UserModel(User user)
        {
            this.UserId = user.UserId.ToString();
            this.BirthDate = user.BirthDate;
            this.Email = user.Email;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.UserName = user.UserName;
            this.Gender = user.Gender;
            this.IsEmailValidated = user.IsEmailValidated.HasValue ? user.IsEmailValidated.Value : false;
            this.IsPhoneValidated = user.IsPhoneValidated.HasValue ? user.IsPhoneValidated.Value : false;
            this.CompletedCount = 0;
            this.ResponseCount = 0;
            this.CompletedSurveys = new List<SurveyModel>();
            this.FacebookId = user.FacebookId;
            this.Balance = user.Balance.HasValue ? user.Balance.Value : 0;
            this.ExPoint = user.ExPoint;
            this.MyReferanceNumber = user.MyReferanceNumber;

            CustomProperty1 = user.CustomProperty1;
            CustomProperty2 = user.CustomProperty2;
            CustomProperty3 = user.CustomProperty3;
            CustomProperty4 = user.CustomProperty4;
            CustomProperty5 = user.CustomProperty5;
            CustomProperty6 = user.CustomProperty6;
            CustomProperty7 = user.CustomProperty7;
            CustomProperty8 = user.CustomProperty8;
            CustomProperty9 = user.CustomProperty9;
            CustomProperty10 = user.CustomProperty10;
        }
        public User ToUser()
        {
            User user = new User();
            user.CreatedDate = DateTime.Now;
            user.TrackingGuid = Guid.NewGuid();
            user.IsEmailValidated = this.IsEmailValidated;
            user.IsPhoneValidated = this.IsPhoneValidated;
            user.UserName = this.UserName;
            if (string.IsNullOrEmpty(user.UserName)) user.UserName = this.Email;
            user.FirstName = this.FirstName;
            user.LastName = this.LastName;
            user.Email = this.Email;
            user.Gender = this.Gender;
            user.BirthDate = this.BirthDate;
            user.FacebookId = this.FacebookId;
            user.ReferanceNumber = this.ReferanceNumber;

            user.CustomProperty1 = CustomProperty1;
            user.CustomProperty2 = CustomProperty2;
            user.CustomProperty3 = CustomProperty3;
            user.CustomProperty4 = CustomProperty4;
            user.CustomProperty5 = CustomProperty5;
            user.CustomProperty6 = CustomProperty6;
            user.CustomProperty7 = CustomProperty7;
            user.CustomProperty8 = CustomProperty8;
            user.CustomProperty9 = CustomProperty9;
            user.CustomProperty10 = CustomProperty10;
            return user;
        }

        public UserModel(OrganizationModel org)
        {
            //this.BirthDate = DateTime.MinValue;
            this.Email = org.Email;
            this.Password = string.IsNullOrEmpty(org.Password) ? org.Email : org.Password;
            this.FirstName = org.Name;
            this.LastName = "***";
            this.UserName = org.Email;
            //this.Gender = "";
            this.IsEmailValidated = false;
            this.IsPhoneValidated = false;
            this.CompletedCount = 0;
            this.ResponseCount = 0;
            this.CompletedSurveys = new List<SurveyModel>();
            //this.FacebookId = "";
            this.Balance = 0;
            this.ExPoint = 0;

            Imei = org.Imei;
            Device = org.Device;
            CustomProperty1 = org.Name;
            CustomProperty2 = org.Website;
            CustomProperty3 = org.Position;
            CustomProperty4 = org.HasRDOffice;
            CustomProperty5 = org.HasTechOffice;
            CustomProperty6 = org.PersonalCount;
          //  CustomProperty7 = org.Sector;
            CustomProperty8 = org.Location;
            CustomProperty9 = org.FundStatus;
            CustomProperty10 = org.CountryOfInvestor;
        }
    }
}
