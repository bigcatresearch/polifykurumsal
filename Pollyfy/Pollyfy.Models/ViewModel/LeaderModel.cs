﻿using System;

namespace Pollyfy.Models.ViewModel
{
    public class LeaderModel
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string UserId { get; set; }

        public string UserFullName { get; set; }

        public int Type { get; set; }

        public decimal Point { get; set; }

        public int SequenceNo { get; set; }

        public byte[] Image { get; set; }
    }
}