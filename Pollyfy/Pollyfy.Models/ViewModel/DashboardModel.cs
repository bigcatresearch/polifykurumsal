﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class DashboardModel
    {
        public DashboardModel()
        {
            this.ParticipationChartSeries = new List<ChartDataModel>();
        }
        public int UserCount { get; set; }
        public int OnlineCount { get; set; }
        public int SurveyCount { get; set; }
        public int PublishCount { get; set; }
        public int CompanyCount { get; set; }
        public List<ChartDataModel> ParticipationChartSeries { get; set; }
        public DateTime? SerieStartDate { get; set; }
        public long SerieStartUnixDate { get; set; }
        public DateTime? SerieEndDate { get; set; }
        public long SerieEndUnixDate { get; set; }
    }
}
