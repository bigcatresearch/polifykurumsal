﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class SimpleItem
    {
        public string ValueId { get; set; }
        public string Text { get; set; }
        public int Count { get; set; }
    }
}
