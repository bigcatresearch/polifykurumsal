﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class DeviceModel
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        public string Platform { get; set; }
        public string Name { get; set; }
        public string Imei { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Version { get; set; }
        public string Uuid { get; set; }
        public string NotifyKey { get; set; }
        public string Operator { get; set; }
        public Device ToDevice()
        {
            Device device = new Device();
            device.Imei = this.Imei;
            device.Manufacturer = this.Manufacturer;
            device.Model = this.Model;
            device.Name = this.Name;
            device.Platform = this.Platform;
            device.Uuid = this.Uuid;
            device.Version = this.Version;
            device.NotifyKey = this.NotifyKey;
            device.Operator = this.Operator;
            return device;
        }
    }
}
