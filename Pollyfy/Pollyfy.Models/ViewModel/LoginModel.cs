﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class LoginModel
    {
        public LoginModel()
        {
            IsExternalLogin = false;
        }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Imei { get; set; }
        public DeviceModel Device { get; set; }
        public bool IsExternalLogin { get; set; }
    }
}
