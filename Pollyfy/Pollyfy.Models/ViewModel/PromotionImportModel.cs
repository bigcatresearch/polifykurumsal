﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pollyfy.Models.ViewModel
{
    public class PromotionImportModel
    {
        public PromotionImportModel()
        {
        }
        public string CompanyName { get; set; }
        public string ContactUrl { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Discount { get; set; }
        public string Code { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public string CompanyIdRef { get; set; }
        public string OwnerIdRef { get; set; }
        public FileOwnerType OwnerType { get; set; }
        public string FileName { get; set; }
        public FileType FileType { get; set; }
    }
}
