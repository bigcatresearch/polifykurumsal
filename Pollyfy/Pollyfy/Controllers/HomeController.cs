﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pollyfy.Models;
using System.Security.Cryptography;
using Newtonsoft.Json;
using System.Net.Mail;
namespace Pollyfy.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CompanyIndex()
        {

            return View();
        }
        public ActionResult Activation(string Id)
        {

            return View();
        }
        public ActionResult Register()
        {

            return View();
        }
        
        public ActionResult ForgotPassword(string Username)
        {

            try
            {
                PollyfyEntities plfy = new PollyfyEntities();
                var result = from a in plfy.SurveyUser where a.Mail == Username select a.ID;
                Guid code = new Guid();
                bool isValidCode;
                var query = from a in plfy.VerifyCode where a.Code == code.ToString() select a;
                if (query != null)
                {
                    isValidCode = true;
                }
                if (isValidCode = true)
                {
                    code = new Guid();
                }

                if (result.FirstOrDefault() != null)
                {
                    MailMessage msg = new MailMessage();

                    msg.From = new MailAddress("safacimenli@gmail.com");
                    msg.To.Add(Username);
                    msg.Subject = "Pollyfy kullanıcı adı ve Şifreniz";
                    msg.Body = "Kullanıcı Adı:" + Username +"Doğrulama Kodu:" + code ;
                    SmtpClient client = new SmtpClient();
                    client.UseDefaultCredentials = true;
                    client.Host = "smtp.gmail.com";
                    client.Port = 587;
                    client.EnableSsl = true;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.Credentials = new System.Net.NetworkCredential("safacimenli@gmail.com", "1903Nesli!");
                    client.Timeout = 20000;
                    client.Send(msg);
                    return this.Json(new { msg = "Şifreniz mail adresinize gönderilmiştir" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return this.Json(new { msg = "Sistemlerimizde bu kullanıcı kayıtlı deildir." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                
                return this.Json(new { msg = "Bir hata oluştu tekrar deneyiniz" }, JsonRequestBehavior.AllowGet);
            }
        
        }
        public ActionResult SurveyListDetailWebService(string Username)
        {
            try
            {
                PollyfyEntities plfy=new PollyfyEntities();
                List<ResultedSurvey> rslt = new List<ResultedSurvey>();
                var result = from a in plfy.Answers
                             join b in plfy.Question on a.QuestionId equals b.ID
                             join c in plfy.Survey on b.Survey_Id equals c.ID
                             where a.username==Username
                             select new {toplamsoru=a.ID,anketadi=c.SurveyName,firmaismi=c.Firma_ismi,firmaresmi=c.CompanyImage };

                foreach (var item in result)
                {
                    ResultedSurvey rsl = new ResultedSurvey();
                    rsl.Amount = 100;
                    rsl.CompanyImage = item.firmaresmi;
                    rsl.CompanyName = item.firmaismi;
                    rsl.SurveyName = item.anketadi;
                    rslt.Add(rsl);
                }
                 return this.Json(rslt, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                
                return this.Json("Hata oluştu", JsonRequestBehavior.AllowGet);
            }
        
        }
        public ActionResult SurveyDetailWebService(int SurveyId)
        {
            try
            {
              PollyfyEntities plfy=new PollyfyEntities();
              List<SurveyModel> srvy = new List<SurveyModel>();
              var result = from a in plfy.Survey select a;
              foreach (var item in result)
              {
                  SurveyModel survey = new SurveyModel();
                  survey.Aciklama = item.Aciklama;
                  survey.AnketAdi = item.SurveyName;
                  survey.BitisSayisi = Convert.ToInt32(item.ExperiencePoint);
                  survey.BitisZamani = Convert.ToInt32(item.ExpirationTime);
                  survey.CinsiyetKapsam = Convert.ToInt32(item.Cinsiyet_Kapsam);
                  survey.FirmaFoto = item.CompanyImage;
                  survey.Kategori = item.Kategori;
                  survey.Lokasyon = item.Lokasyon;
                  survey.SoruSayisi = Convert.ToInt32(item.Soru_sayisi);
                  survey.Ucret = item.Ucret;
                  survey.YasKapsam = Convert.ToInt32(item.Yas_Kapsam);
                  srvy.Add(survey);
              }

               return this.Json(srvy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                
               return this.Json("Hata oluştu", JsonRequestBehavior.AllowGet);
            }
        
        }
        public ActionResult AnswerWebService(int QuestionId, int OptionId, string Username)
        {
            try
            {
                PollyfyEntities plfy = new PollyfyEntities();
                Answers ans = new Answers();
                ans.OptionId = OptionId;
                ans.QuestionId = QuestionId;
                ans.username = Username;
                plfy.Answers.Add(ans);
                plfy.SaveChanges();
                
                return this.Json("Anket başarıyla kaydedildi", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                
                return this.Json("Hata oluştu", JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult QuestionWebService(int SurveyId)
        {
            try
            {
            PollyfyEntities plfy = new PollyfyEntities();
            List<QuestionModel> QuestionList = new List<QuestionModel>();
            var result = from a in plfy.Question
                         join b in plfy.Options on a.ID equals b.Quartion_Id
                         where a.Survey_Id==SurveyId
                         select new { questionId=a.ID, QuestionContent=a.contents, OptionsType=a.OptionsType, optionvalue=b.contents, optionId=b.ID };
            foreach (var item in result)
            {
                QuestionModel qmodel = new QuestionModel();
                qmodel.QuestionId = item.questionId;
                qmodel.QuestionContent = item.QuestionContent;
                qmodel.OptionType = Convert.ToInt32(item.OptionsType);
                qmodel.OptionContent = item.optionvalue;
                qmodel.OptionId = item.optionId;
                QuestionList.Add(qmodel);
            }
            return this.Json(QuestionList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                return this.Json("Hata oluştu", JsonRequestBehavior.AllowGet);
            }
      
        
        }
        public ActionResult ProfileWebService(string Username)
        {
            try
            {
                List<ProfileModel> ProfileList = new List<ProfileModel>();
                PollyfyEntities plfy = new PollyfyEntities();
                var result = from a in plfy.SurveyUser where a.Mail == Username select a;
                foreach (var item in result)
                {
                    ProfileModel prf = new ProfileModel();
                    prf.ID = item.ID;
                    prf.Ad = item.Adi;
                    prf.Soyad = item.Soyadi;
                    prf.AnketSayisi = 0;
                    prf.Base64Image = "";
                    prf.KazancTutari = 0;
                    ProfileList.Add(prf);
                   
                }

                return this.Json(ProfileList, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {

                return this.Json(new { status = "0" }, JsonRequestBehavior.AllowGet);
            }
        
        }
        public ActionResult SurveyWebService(string Lokasyon)
        {
            try
            {
             List<Survey> SurveyList = new List<Survey>();
            PollyfyEntities plfy=new PollyfyEntities();
            var result = from a in plfy.Survey where a.Lokasyon==Lokasyon select a;
            foreach (var item in result)
            {
                Survey su = new Survey();
                su.ID = item.ID;
                su.Aciklama = item.Aciklama;
                su.Firma_ismi = item.Firma_ismi;
                su.Kategori = item.Kategori;
                su.Soru_sayisi = item.Soru_sayisi;
                su.CompanyImage = item.CompanyImage;
                su.ExperiencePoint = item.ExperiencePoint;
                su.ExpirationTime = item.ExpirationTime;
                su.Ucret = item.Ucret;
                su.SurveyName = item.SurveyName;
                SurveyList.Add(su);
            }
           
                return this.Json(SurveyList, JsonRequestBehavior.AllowGet);
           
            }
            catch (Exception)
            {

                return this.Json(new { status = "0" }, JsonRequestBehavior.AllowGet);
            }
           

        }

        public ActionResult LoginWebService(string username, string password)
        {
            try
            {
                PollyfyEntities plfy = new PollyfyEntities();
                bool isValidUser;
                string sifre = PollyfyMemberShipProvider.GetMD5Hash(password).ToString();
                var result = from a in plfy.SurveyUser where a.Mail == username && a.Sifre ==sifre  select a;
                if (result.FirstOrDefault().ID != null)
                {
                    isValidUser = true;
                }
                else
                {
                    isValidUser = false;
                }
                if (isValidUser)
                {
                    return this.Json(new { status = "1"}, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return this.Json(new { status = "2" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {

                return this.Json(new { status = "0" }, JsonRequestBehavior.AllowGet);
            }
        
        }
        public ActionResult RegisterWebService(string Ad, string Soyad, string email, string Password, int Yas, int Cinsiyet)
        {
            try
            {
                PollyfyEntities plfy = new PollyfyEntities();
                SurveyUser su = new SurveyUser();
                su.Adi = Ad;
                su.Soyadi = Soyad;
                su.Mail = email;
                su.Sifre = PollyfyMemberShipProvider.GetMD5Hash(Password);
                su.Yas = Yas;
                su.Cinsiyet = Cinsiyet;
                plfy.SurveyUser.Add(su);
                plfy.SaveChanges();
                return this.Json(new { status = "1" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                return this.Json(new { status = "0" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SurveyUserRegister(string Ad,string Soyad,string email,string Password,int Yas,int Cinsiyet)
        {
            try
            {
            PollyfyEntities plfy = new PollyfyEntities();
            SurveyUser su = new SurveyUser();
            su.Adi = Ad;
            su.Soyadi = Soyad;
            su.Mail = email;
            su.Sifre = PollyfyMemberShipProvider.GetMD5Hash(Password);
            su.Yas = Yas;
            su.Cinsiyet = Cinsiyet;
            plfy.SurveyUser.Add(su);
            plfy.SaveChanges();
            return this.Json(new  { status = "1" },JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

             return this.Json(new { status = "0" },JsonRequestBehavior.AllowGet);
            }
           
        }
        public ActionResult AjaxSaveUser(User us)
        {
            try
            {
            PollyfyEntities plfy = new PollyfyEntities();
            User ku = new User();
            ku.CompanyName = us.CompanyName;
            ku.Name = us.Name;
            ku.Surname = us.Surname;
            ku.Email = us.Email;
            ku.Phone = us.Phone;
            ku.Password =PollyfyMemberShipProvider.GetMD5Hash(us.Password);
            ku.Activation = false;
            plfy.User.Add(ku);
            plfy.SaveChanges();
            }
            catch (Exception e)
            {
                return this.Json(new { durum = "-1", bilgi = "Kullanıcı eklerken bir hata oluştu." });
                
            }
            Utility.SenEmail(us.Email, us.Name, us.Surname);
            return this.Json(new { durum = "1", bilgi = "Kullanıcı Ekleme Başarılı" });
        
        }
        public ActionResult LoginService(string email,string password)
        {

           


            return this.Json(new { status = "1" });
        }

    }
}
