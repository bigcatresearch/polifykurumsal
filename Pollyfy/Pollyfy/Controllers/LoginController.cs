﻿using Pollyfy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pollyfy.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Login()
        {
           
            return View();
        }
        public ActionResult AJAXCompanyLogin(string username,string password)
        {

            try
            {
                PollyfyEntities plfy = new PollyfyEntities();
                bool isValidUser;
                string sifre = PollyfyMemberShipProvider.GetMD5Hash(password).ToString();
                var result = from a in plfy.User where a.Email == username && a.Password == sifre select a;
                if (result.FirstOrDefault().ID != null)
                {
                    isValidUser = true;
                }
                else
                {
                    isValidUser = false;
                }
                if (isValidUser)
                {
                    return this.Json(new { status = "1" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return this.Json(new { status = "2" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {

                return this.Json(new { status = "0" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult AJAXCompanyRegister()
        {

            return this.Json(new { status = "2" });
        }
        
    }
}
