﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pollyfy.Models
{
    public class ResultedSurvey
    {

        public string SurveyName { get; set; }
        public string CompanyName { get; set; }
        public string CompanyImage { get; set; }
        public int Amount { get; set; }
    }
}