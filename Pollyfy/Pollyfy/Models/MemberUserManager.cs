﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pollyfy.Models;
namespace Pollyfy.Models
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", Justification = "none")]
    public class MemberUserManager
    {
        PollyfyEntities modelContainer = new  PollyfyEntities();

        public User MemberUser { get; set; }

        public MemberUserManager()
        {
            this.MemberUser = new User();
        }

        public User GetUserObjByUserName(string userName, string passWord)
        {
            User user = (from rec in this.modelContainer.User
                                      where rec.Email == userName && rec.Password == passWord.Trim()
                                      select rec).FirstOrDefault();
            return user;
        }

        public List<User> GetAllUsers()
        {
            return this.modelContainer.User.ToList();
        }

        public bool ChangeMemberDetails()
        {
            try
            {
                this.modelContainer.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public int RegisterUser()
        {
            return -1;
        }
        public int ChangePassword(string username, string oldpass, string newpass)
        {
            User user = (from rec in this.modelContainer.User
                                      where rec.Email == username && rec.Password == oldpass.Trim()
                                      select rec).FirstOrDefault();
            if (user != null)
            {
                user.Password = newpass.Trim();
                this.modelContainer.SaveChanges();
                return user.ID;
            }
            else
            {
                return 0;
            }
        }
        
    }
}