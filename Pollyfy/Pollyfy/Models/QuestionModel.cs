﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pollyfy.Models
{
    public class QuestionModel
    {
        public int QuestionId { get; set; }
        public int OptionId { get; set; }
        public string QuestionContent { get; set; }
        public string OptionContent { get; set; }
        public int OptionType {get;set;}
    }
}